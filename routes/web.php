<?php

//CMS Landing Page
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
//SAMPLE SAMPLE

//login
Route::get('/loginuser', 'CMSController@login')->name('loginmain');
Route::post('/logincheck', 'LoginUserController@login_user')->name('logincheck');
Route::get('/logout', 'LoginUserController@logout');

//changepassword
Route::get('/changepassword', 'CMSController@changepassword');
Route::post('/changepassworduser', 'CMSController@changepassword_user');

//register
Route::get('/register', 'CMSController@register');
Route::post('/registersubmit', 'RegisterUserController@register_user')->name('registersubmit');
Route::get('/getmother', 'RegisterUserController@get_mother');
Route::post('/activate/{id}', 'RegisterUserController@activate');
Route::post('/decline/{id}', 'RegisterUserController@decline');
Route::post('/deactivate/{id}', 'RegisterUserController@deactivate');
Route::post('/reactivate/{id}', 'RegisterUserController@reactivate');
// Route::get('/register', 'UsersController@signup')->name('register');

//consultancyfirms
Route::get('/consultancyfirms','ConsultancyFirmsController@view');
Route::post('/consultancyfirms','ConsultancyFirmsController@insertData');
Route::get('/consultancyfirms/add','ConsultancyFirmsController@addFirmView');
Route::get('/consultancyfirms/{Firm_ID}','ConsultancyFirmsController@updateFirmView');
Route::post('/consultancyfirms/update','ConsultancyFirmsController@updateData');
Route::post('/consultancyfirms/addExperts','ConsultancyFirmsController@addExperts')->name('addConsExp');
Route::post('/consultancyfirms/addProcs','ConsultancyFirmsController@addProcs')->name('addConsProcs');
Route::post('/consultancyfirms/updExp','ConsultancyFirmsController@updateExperts')->name('updConsExp');
Route::post('/consultancyfirms/updProcs','ConsultancyFirmsController@updateProcs')->name('updConsProcs');

//createcontract
// Route::get('/create-contract','CreateContractController@view');
Route::get('/create-contract','CreateContractController@getData');
Route::post('/create-contract','CreateContractController@insertData');
Route::get('create-contract/gettype/{id}','CreateContractController@getFirmType');

//managecontracts
Route::get('/manage-contracts','ManageContractsController@view');
Route::post('/manage-contracts/updateTermDate','ManageContractsController@updateTermDate');
Route::post('/manage-contracts/projUpdate','ManageContractsController@projectUpdate');
Route::get('/manage-contracts/paymentsSummary/{Contract_ID}','ManageContractsController@paymentSummary')->name('paymentSummary');

//viewcontract
Route::get('/manage-contracts/view-contract/{Contract_ID}','ViewContractController@view');
Route::get('/manage-contracts/edit-contract/{Contract_ID}','ViewContractController@editView');
Route::post('/manage-contracts/edit-contract/updateContrDtls/{Contract_ID}','ViewContractController@editUpdateContractDtls')->name('updateCntrctDtls');
Route::post('/manage-contracts/edit-contract/addPerfSecDtls/{Contract_ID}','ViewContractController@addPerfSecDtls')->name('addPerfSecDtls');
Route::post('/manage-contracts/edit-contract/updatePerfSecDtls/{Contract_ID}','ViewContractController@updatePerfSec')->name('updatePerfSecDtls');
Route::post('/manage-contracts/edit-contract/updateContrDeliverables/{Contract_ID}','ViewContractController@updateDeliverable')->name('updateContrDeliverables');
Route::post('/manage-contracts/edit-contract/addDeliverables/{Contract_ID}','ViewContractController@addDeliverables')->name('addContrDeliverables');
Route::post('/manage-contracts/edit-contract/addContractExp/{Contract_ID}','ViewContractController@addContractExp')->name('addContrExp');
Route::post('/manage-contracts/edit-contract/updateContractExp/{Contract_ID}','ViewContractController@updateContractExp')->name('updContrExp');

// deliverables
Route::get('/manage-contracts/deliverables/{Contract_ID}','DeliverablesController@view');
Route::post('/manage-contracts/deliverables/{Contract_ID}','DeliverablesController@amend');
Route::get('/manage-contracts/deliverables/paymentView/{Contract_ID}/{Deliverable_ID}','DeliverablesController@deliverablePaymentView');
Route::get('/manage-contracts/deliverables/paymentUpdateView/{Contract_ID}/{Deliverable_ID}','DeliverablesController@PaymentUpdateView');
Route::post('/manage-contracts/deliverables/paymentUpdate/{Deliverable_ID}','DeliverablesController@deliverablePaymentUpdate')->name('pmtUpdate');

//reports
Route::get('/reports-dashboard','ReportsController@dashboardview');
Route::get('/reports-dashboard/report-view/{Contract_ID}','ReportsController@view_report');
Route::get('/reports-dashboard/reports','ReportsController@downloadAll');
Route::post('/reports-dashboard/reports','ReportsController@downloadAll');
Route::get('/reports-dashboard/reportsexcel','ReportsController@downloadExcel');
Route::post('/reports-dashboard/reportsexcel','ReportsController@downloadExcel');

Route::get('/manage-contracts/reports/{Contract_ID}','ReportsController@view');
Route::get('/manage-contracts/reports/pdf/{Contract_ID}','ReportsController@download');
Route::post('/manage-contracts/reports/pdf/{Contract_ID}','ReportsController@download');
Route::post('/manage-contracts/reports/{Contract_ID}','ReportsController@view');

//history
Route::get('/manage-contracts/history/{Contract_ID}','HistoryController@view'); 

//project status view
Route::get('/manage-contracts/view-contract/status/{Contract_ID}','ViewContractController@viewStatus'); 

//sendemail 
Route::get('/sendemail', 'RegisterUserController@sendemail');

//dashboard
Route::get('/dashboard', 'DashboardController@dashboard')->name('dashboard');

//Users Management
Route::get('/users', 'UsersController@users');
Route::get('index','UsersController@index');
Route::get('index2','UsersController@index2');

//Email Notification
Route::get('/emailnotif', 'EmailNotifController@emailnotif');

Route::get('/midpointmessage/{id}', 'SendEmailController@midpointmessage');
Route::post('/midpointmessage/{id}', 'SendEmailController@sendemail');

//adduser
Route::post('/adduser', 'UsersController@adduser')->name('adduser');

//CRUDcontroller
Route::get('ajax-crud', 'UserCrudController@index');
Route::get('userrequest', 'UserCrudController@userrequest');
Route::get('disabledusers', 'UserCrudController@disabledusers');
Route::post('asyncfindagencysubid', 'UserCrudController@asyncfindagencysubid');
Route::post('getSubmissionID', 'UserCrudController@getSubmissionID');
Route::post('adduserman', 'UserCrudController@store');

Route::post('ajax-crud/update', 'UserCrudController@update')->name('ajax-crud.update');

Route::get('ajax-crud/destroy/{id}', 'UserCrudController@destroy');
Route::get('ajax-crud/approve/{id}', 'UserCrudController@approve');
Route::get('disable/{id}', 'UserCrudController@disable');
Route::get('/getAgency', 'UserCrudController@getAgency');
// Route::get('/users2', 'UserCrudController@users2')->middleware('auth');
Route::post('openauth', 'UserCrudController@openauth');
Route::post( 'disapprove', 'UserCrudController@disapprove');


Auth::routes();

