-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 15, 2020 at 11:26 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cmsinternal`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_type`
--

CREATE TABLE `access_type` (
  `access_ID` int(5) NOT NULL,
  `access_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_desc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `access_type`
--

INSERT INTO `access_type` (`access_ID`, `access_code`, `access_desc`) VALUES
(1, 'A', 'ADMIN_ICTS'),
(2, 'V', 'VIEWER'),
(3, 'E', 'EDITOR'),
(4, 'A1', 'ADMIN-PMD');

-- --------------------------------------------------------

--
-- Table structure for table `amendment_history`
--

CREATE TABLE `amendment_history` (
  `amendment_history_id` int(225) NOT NULL,
  `Deliverable_ID` int(20) NOT NULL,
  `Last_Amended_Date` datetime DEFAULT NULL,
  `Amended_By` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Date_Of_Amendment` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `amendment_history`
--

INSERT INTO `amendment_history` (`amendment_history_id`, `Deliverable_ID`, `Last_Amended_Date`, `Amended_By`, `Date_Of_Amendment`, `updated_at`, `created_at`) VALUES
(1, 19, '2020-07-23 00:00:00', 'hdfusi', '0000-00-00 00:00:00', '2020-07-22 19:44:15', '2020-07-22 19:44:15'),
(2, 19, '2020-07-23 00:00:00', 'hdfusi', '2020-07-22 20:24:31', '2020-07-22 20:24:31', '2020-07-22 20:24:31'),
(3, 19, '2020-07-24 00:00:00', 'hdfusi', '2020-07-22 20:28:38', '2020-07-22 20:28:38', '2020-07-22 20:28:38'),
(4, 1, '2020-09-16 00:00:00', 'hdfusi', '2020-09-04 12:58:03', '2020-09-04 12:58:03', '2020-09-04 12:58:03'),
(5, 1, '2020-09-16 00:00:00', 'hdfusi', '2020-09-04 14:36:02', '2020-09-04 14:36:02', '2020-09-04 14:36:02'),
(6, 1, '2020-09-24 00:00:00', 'hdfusi', '2020-09-04 14:38:14', '2020-09-04 14:38:14', '2020-09-04 14:38:14'),
(7, 76, '2020-09-01 00:00:00', 'hdfusi', '2020-09-23 13:00:53', '2020-09-23 13:00:53', '2020-09-23 13:00:53');

-- --------------------------------------------------------

--
-- Table structure for table `consultancy_firm`
--

CREATE TABLE `consultancy_firm` (
  `Firm_ID` int(25) NOT NULL,
  `Firm_Name` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Address` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_address` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telephone` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax_number` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact_person` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultancy_firm`
--

INSERT INTO `consultancy_firm` (`Firm_ID`, `Firm_Name`, `Address`, `email_address`, `telephone`, `fax_number`, `contact_person`, `designation`, `updated_at`, `created_at`) VALUES
(1, 'Test_Firm', 'Test_Address', 'testadress@test.com', '1342434363', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Test_Firm2', 'Test', 'testaddress@test.com', '43735835', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'test add firm', 'test add firm', 'test@test.com', '2364358323', NULL, NULL, NULL, '2020-05-20 12:14:41', '2020-05-20 12:14:41'),
(4, 'test toast', 'test toast', 'testtoast@gmail.com', '246328752', NULL, NULL, NULL, '2020-06-05 10:31:07', '2020-06-05 10:31:07'),
(5, 'test toast1', 'test toast1', 'testtoast@gmail.com', '134622872', NULL, NULL, NULL, '2020-06-05 10:50:41', '2020-06-05 10:50:41'),
(6, 'test add', '322 testing', 'hdfusi@neda.gov.ph', '5264432', '525232', 'test', NULL, '2020-10-07 16:32:33', '2020-09-04 10:29:35'),
(7, 'testdocs', 'testdocs', 'testdocs@test.com', '23628753', NULL, 'testdocs', 'testdocs', '2020-09-28 17:38:10', '2020-09-28 17:38:10'),
(8, 'testdocs1', 'testdocs', 'testdocs@test.com', '23628753', NULL, 'testdocs', 'testdocs', '2020-09-28 17:38:44', '2020-09-28 17:38:44'),
(9, 'testdocs2', 'testdocs', 'testdocs@test.com', '23628753', NULL, 'testdocs', 'testdocs', '2020-09-28 17:40:39', '2020-09-28 17:40:39'),
(10, 'testing cons', 'testing cons', 'testtoast@gmail.com', '42364', NULL, 'testing cons', 'testing cons', '2020-09-28 17:42:29', '2020-09-28 17:42:29'),
(11, 'testing cons1', 'testing cons1', 'testtoast@gmail.com', '354', NULL, 'testing cons1', 'testing cons1', '2020-09-28 17:44:19', '2020-09-28 17:44:19'),
(12, 'testwithdocs', 'testwithdocs', 'testwithdocs@test.com', '1231', NULL, 'testwithdocs', 'testwithdocs', '2020-09-28 18:02:57', '2020-09-28 18:02:57'),
(13, 'testwithdocs1', 'testwithdocs1', 'testwithdocs1@docs.com', '234626', NULL, 'testwithdocs1', 'testwithdocs1', '2020-09-28 18:05:14', '2020-09-28 18:05:14'),
(14, 'testing cons3', 'testing cons3', 'testing cons3@test.com', '1235246', NULL, 'testing cons3', 'testing cons3', '2020-09-28 18:08:46', '2020-09-28 18:08:46'),
(15, 'testing123', 'testing123', 'testing123@test', '1352', NULL, 'testing123', 'testing123', '2020-09-29 10:05:39', '2020-09-29 10:05:39'),
(16, 'testingprocs', 'testingprocs', 'testingprocs@test.com', '322432', NULL, 'testingprocs', 'testingprocs', '2020-09-29 10:08:13', '2020-09-29 10:08:13'),
(17, 'testwithID', 'testwithID', 'testwithID@gmail.com', '35262', NULL, 'testwithID', 'testwithID', '2020-09-29 10:47:24', '2020-09-29 10:47:24'),
(18, 'testfindocs1', 'testfindocs1', 'testfindocs1@test.com', 'testfindocs1', 'testfindocs1', 'testfindocs1', 'testfindocs1', '2020-09-29 18:04:06', '2020-09-29 18:04:06'),
(19, 'test experts', 'test experts', 'testexperts@test.com', '5264432', '2473462', 'test experts', 'test experts', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(20, 'test findocsnew', 'test findocs', 'testfindocs@gmail.com', '234626', '2473462', 'test findocs', 'test findocs', '2020-10-07 18:14:32', '2020-10-07 18:14:32'),
(21, 'test null proc', 'test null proc', 'testnulproc@test.com', '234626', '2473462', 'test null proc', 'test null proc', '2020-10-12 17:17:49', '2020-10-12 17:17:49'),
(22, 'proc null', 'proc null', 'procnull@test.com', '354', '2473462', 'testdocs', 'testwithdocs1', '2020-10-12 17:24:21', '2020-10-12 17:24:21'),
(23, 'proc null', 'proc null', 'procnull@test.com', '234626', '2473462', 'testdocs', 'testwithdocs1', '2020-10-12 17:25:29', '2020-10-12 17:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `consultant_documents`
--

CREATE TABLE `consultant_documents` (
  `consDoc_ID` int(20) NOT NULL,
  `Firm_ID` int(25) NOT NULL,
  `Document_Link` varchar(250) CHARACTER SET latin1 NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultant_documents`
--

INSERT INTO `consultant_documents` (`consDoc_ID`, `Firm_ID`, `Document_Link`, `updated_at`, `created_at`) VALUES
(1, 4, 'public/storage/PHILGEPS/cms forgetpass.png', '2020-09-28 18:05:14', '2020-09-28 18:05:14'),
(2, 4, 'public/storage/SEC_DTI/cms create contract.png', '2020-09-28 18:05:14', '2020-09-28 18:05:14'),
(3, 4, 'public/storage/BusinessPermit/cms forgetpass.png', '2020-09-28 18:05:14', '2020-09-28 18:05:14'),
(4, 4, 'public/storage/TaxClearance/dashboard.png', '2020-09-28 18:05:14', '2020-09-28 18:05:14'),
(5, 17, 'public/storage/PHILGEPS/YAMATW.docx', '2020-09-29 10:47:25', '2020-09-29 10:47:25'),
(6, 17, 'public/storage/SEC_DTI/YAMATW.docx', '2020-09-29 10:47:25', '2020-09-29 10:47:25'),
(7, 17, 'public/storage/BusinessPermit/YAMATW.docx', '2020-09-29 10:47:25', '2020-09-29 10:47:25'),
(8, 17, 'public/storage/TaxClearance/YAMATW.docx', '2020-09-29 10:47:25', '2020-09-29 10:47:25'),
(9, 18, 'public/storage/PHILGEPS/Miss Independent.docx', '2020-09-29 18:04:06', '2020-09-29 18:04:06'),
(10, 18, 'public/storage/SEC_DTI/YAMATW.docx', '2020-09-29 18:04:06', '2020-09-29 18:04:06'),
(11, 18, 'public/storage/BusinessPermit/Your Universe.docx', '2020-09-29 18:04:06', '2020-09-29 18:04:06'),
(12, 18, 'public/storage/TaxClearance/Your Universe.docx', '2020-09-29 18:04:06', '2020-09-29 18:04:06'),
(13, 6, 'public/storage/FinancialDocs/MINUTES OF THE MEETING_20201007.docx', '2020-10-07 11:19:09', '2020-10-07 11:19:09'),
(14, 19, 'public/storage/PHILGEPS/Miss Independent_20201007.docx', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(15, 19, 'public/storage/SEC_DTI/Miss Independent_20201007.docx', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(16, 19, 'public/storage/BusinessPermit/Miss Independent_20201007.docx', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(17, 19, 'public/storage/TaxClearance/Miss Independent_20201007.docx', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(18, 3, 'public/storage/FinancialDocs/My Fat Baby_20201007.docx', '2020-10-07 18:08:24', '2020-10-07 18:08:24'),
(19, 3, 'public/storage/FinancialDocs/VL_FUSI_20201007.pdf', '2020-10-07 18:08:24', '2020-10-07 18:08:24'),
(20, 19, 'public/storage/FinancialDocs/Miss Independent_20201007.docx', '2020-10-07 18:10:26', '2020-10-07 18:10:26'),
(21, 19, 'public/storage/PHILGEPS/You\'ll Be Safe Here_20201012.docx', '2020-10-12 10:04:23', '2020-10-12 10:04:23'),
(22, 23, 'public/storage/FinancialDocs/You\'ll Be Safe Here_20201013.docx', '2020-10-13 15:39:34', '2020-10-13 15:39:34'),
(23, 23, 'public/storage/FinancialDocs/Your Universe_20201013.docx', '2020-10-13 15:39:34', '2020-10-13 15:39:34');

-- --------------------------------------------------------

--
-- Table structure for table `consultant_experts`
--

CREATE TABLE `consultant_experts` (
  `expert_id` int(25) NOT NULL,
  `Firm_ID` int(25) NOT NULL,
  `expert_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expert_role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultant_experts`
--

INSERT INTO `consultant_experts` (`expert_id`, `Firm_ID`, `expert_name`, `expert_role`, `updated_at`, `created_at`) VALUES
(1, 19, 'test experts', 'test experts role', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(2, 19, 'test experts2', 'test experts role 2', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(3, 20, 'test findocs1', 'test findocs2', '2020-10-07 18:14:32', '2020-10-07 18:14:32'),
(4, 21, 'test null proc', 'test null proc', '2020-10-12 17:17:49', '2020-10-12 17:17:49'),
(5, 22, 'proc null', 'proc null', '2020-10-12 17:24:21', '2020-10-12 17:24:21'),
(6, 22, 'test experts', 'test experts role', '2020-10-12 17:25:29', '2020-10-12 17:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `consultant_procurements`
--

CREATE TABLE `consultant_procurements` (
  `id` int(255) NOT NULL,
  `Firm_ID` int(25) NOT NULL,
  `procurement` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `consultant_procurements`
--

INSERT INTO `consultant_procurements` (`id`, `Firm_ID`, `procurement`, `updated_at`, `created_at`) VALUES
(1, 16, 'ytets', '2020-09-29 10:08:13', '2020-09-29 10:08:13'),
(2, 16, 'tetsd', '2020-09-29 10:08:13', '2020-09-29 10:08:13'),
(3, 17, 'test', '2020-09-29 10:47:25', '2020-09-29 10:47:25'),
(4, 17, 'testdocs', '2020-09-29 10:47:25', '2020-09-29 10:47:25'),
(5, 18, 'test', '2020-09-29 18:04:06', '2020-09-29 18:04:06'),
(6, 3, 'test', '2020-10-01 11:27:10', '2020-10-01 11:27:10'),
(7, 6, 'test23', '2020-10-01 12:04:05', '2020-10-01 12:04:05'),
(8, 18, 'test4', '2020-10-07 08:31:41', '2020-10-07 08:31:41'),
(9, 6, 'test2112', '2020-10-07 11:19:09', '2020-10-07 11:19:09'),
(10, 6, 'testhrw', '2020-10-07 16:32:33', '2020-10-07 16:32:33'),
(11, 19, 'test experts', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(12, 19, 'test experts', '2020-10-07 18:06:37', '2020-10-07 18:06:37'),
(13, 3, 'test22', '2020-10-07 18:08:24', '2020-10-07 18:08:24'),
(14, 19, 'test21', '2020-10-07 18:10:26', '2020-10-07 18:10:26'),
(15, 20, 'test', '2020-10-07 18:14:32', '2020-10-07 18:14:32'),
(16, 19, 'test123', '2020-10-12 10:04:23', '2020-10-12 10:04:23'),
(17, 21, 'testasdfs', '2020-10-12 17:17:49', '2020-10-12 17:17:49'),
(18, 23, NULL, '2020-10-13 15:39:34', '2020-10-13 15:39:34');

-- --------------------------------------------------------

--
-- Table structure for table `contracts`
--

CREATE TABLE `contracts` (
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Contract_Name` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `Contract_Type` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `Contract_Cost` double DEFAULT NULL,
  `End_User` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `mode_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Implementing_Agency` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `Co_Implementing_Agency` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `Issuing_Company` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `NOA_Release_Date` datetime DEFAULT NULL,
  `NTP_Release_Date` datetime DEFAULT NULL,
  `Fund_Source` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Fund_Year` int(4) DEFAULT NULL,
  `ABC` double DEFAULT NULL,
  `Contract_Status` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `NOA_Conforme_Date` datetime NOT NULL,
  `NTP_Conforme_Date` date DEFAULT NULL,
  `Bond_Validity` date DEFAULT NULL,
  `Contract_Start_Date` datetime DEFAULT NULL,
  `Contract_End_Date` datetime DEFAULT NULL,
  `Contract_Completion_Date` datetime DEFAULT NULL,
  `Contract_Termination_Date` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contracts`
--

INSERT INTO `contracts` (`Contract_ID`, `Contract_Name`, `Contract_Type`, `Contract_Cost`, `End_User`, `mode_code`, `Implementing_Agency`, `Co_Implementing_Agency`, `Issuing_Company`, `NOA_Release_Date`, `NTP_Release_Date`, `Fund_Source`, `Fund_Year`, `ABC`, `Contract_Status`, `NOA_Conforme_Date`, `NTP_Conforme_Date`, `Bond_Validity`, `Contract_Start_Date`, `Contract_End_Date`, `Contract_Completion_Date`, `Contract_Termination_Date`, `updated_at`, `created_at`) VALUES
('2020AUG07CONS00022', 'test multiple perfSec', 'CONS', 24, 'test multiple perfSec', '', 'test multiple perfSec', 'test multiple perfSec', 'test multiple perfSec', '2020-07-27 00:00:00', '2020-08-05 00:00:00', 'test multiple perfSec', 2021, 434, 'Expiring', '2020-08-04 00:00:00', '2020-08-06', '2020-08-05', '2020-08-10 00:00:00', '2020-08-13 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-08-07 13:50:08'),
('2020AUG07CONS00023', 'test multiple perfSec', 'CONS', 3231, 'test validate', '', 'testpartners', 'testpartners', 'test multiple perfSec', '2020-08-03 00:00:00', '2020-08-05 00:00:00', 'test multiple perfSec', 2021, 3244, 'Expiring', '2020-08-04 00:00:00', '2020-08-06', '2020-08-20', '2020-08-10 00:00:00', '2020-08-31 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-08-07 13:54:03'),
('2020AUG07CONS00024', 'test multiple perfSec', 'CONS', 2342, 'test multiple perfSec', '', 'test multiple perfSec', 'test multiple perfSec', 'test multiple perfSec', '2020-08-03 00:00:00', '2020-08-05 00:00:00', 'test multiple perfSec', 2021, 4525, 'Expiring', '2020-08-04 00:00:00', '2020-08-07', '2020-08-28', '2020-08-10 00:00:00', '2020-08-27 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-08-07 14:38:11'),
('2020AUG11CONS00025', 'test multiple perfSec', 'CONS', 23, 'test validate', '', 'testpartners', 'testpartners', 'test dlvrble date', '2020-08-03 00:00:00', '2020-08-05 00:00:00', 'test validate', 2021, 324, 'Expiring', '2020-08-03 00:00:00', '2020-08-07', '2020-08-27', '2020-08-14 00:00:00', '2020-08-31 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-08-11 09:51:08'),
('2020AUG12CONS00026', 'testpartners', 'CONS', 2132, 'testpartners', '', 'testpartners', 'testpartners', 'test validate', '2020-08-03 00:00:00', '2020-08-05 00:00:00', 'test multiple perfSec', 2021, 43242, 'Expiring', '2020-08-05 00:00:00', '2020-08-07', '2020-08-04', '2020-08-10 00:00:00', '2020-08-28 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-08-12 11:28:47'),
('2020AUG13CONS00027', 'testpartners', 'CONS', 324, 'test validate', '', 'testpartners', 'testpartners', 'test validate', '2020-08-05 00:00:00', '2020-08-11 00:00:00', 'testpartners', 2021, 3252, 'New', '2020-08-07 00:00:00', '2020-08-15', '2020-08-28', '2020-08-17 00:00:00', '2020-10-31 00:00:00', NULL, NULL, '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
('2020JUL01CONS00012', 'test dlvrble date', 'CONS', 2387429, 'test dlvrble date', '', 'test dlvrble date', NULL, 'test dlvrble date', '2020-07-01 00:00:00', '2020-07-02 00:00:00', 'test dlvrble date', 2021, 3294289, 'Expiring', '0000-00-00 00:00:00', '2020-07-03', '2020-07-31', '2020-07-06 00:00:00', '2020-07-30 00:00:00', NULL, '2020-07-08 11:00:00', '2020-08-13 13:51:33', '2020-07-01 12:58:42'),
('2020JUL01CONS00013', 'test dlvrble date', 'CONS', 2042898, 'test dlvrble date', '', 'test dlvrble date', NULL, 'test dlvrble date', '2020-07-06 00:00:00', '2020-07-07 00:00:00', 'test dlvrble date', 2021, 2094382, 'Expiring', '0000-00-00 00:00:00', '2020-07-08', '2020-07-31', '2020-07-10 00:00:00', '2020-07-20 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-01 17:51:12'),
('2020JUL17CONS00014', 'testpartners', 'CONS', 2434243, 'testpartners', '', 'testpartners', 'testpartners', 'testpartners', '2020-07-01 00:00:00', '2020-07-07 00:00:00', 'testpartners', 2021, 3234221, 'Expiring', '2020-07-02 00:00:00', '2020-07-15', '2020-07-31', '2020-07-15 00:00:00', '2020-07-31 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-17 09:29:55'),
('2020JUL23CONS00015', 'testpartners', 'CONS', 323123, 'testpartners', '', 'testpartners', 'testpartners', 'testpartners', '2020-07-15 00:00:00', '2020-07-16 00:00:00', 'test validate', 2021, 4234241, 'Expiring', '2020-07-15 00:00:00', '2020-07-24', '2020-07-24', '2020-07-24 00:00:00', '2020-07-24 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 14:04:54'),
('2020JUL23CONS00016', 'testpartners', 'CONS', 323123, 'testpartners', '', 'testpartners', 'testpartners', 'testpartners', '2020-07-15 00:00:00', '2020-07-16 00:00:00', 'test validate', 2021, 4234241, 'Expiring', '2020-07-15 00:00:00', '2020-07-24', '2020-07-24', '2020-07-24 00:00:00', '2020-07-24 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 14:13:22'),
('2020JUL23CONS00017', 'testpartners', 'CONS', 1, 'test validate', '', 'test validate', 'test validate', 'test validate', '2020-07-23 00:00:00', '2020-08-07 00:00:00', 'test validate', 2021, 212, 'Expiring', '2020-07-30 00:00:00', '2020-08-14', '2020-07-23', '2020-08-28 00:00:00', '2020-09-05 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 14:19:41'),
('2020JUL23CONS00018', 'testpartners', 'CONS', 452523, 'test validate', '', 'testpartners', 'testpartners', 'test dlvrble date', '2020-07-23 00:00:00', '2020-07-16 00:00:00', 'test validate', 2021, 23524, 'Expiring', '2020-07-09 00:00:00', '2020-07-24', '2020-07-24', '2020-07-24 00:00:00', '2020-07-31 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 14:34:45'),
('2020JUL23CONS00019', 'testpartners', 'CONS', 2, 'test validate', '', 'testpartners', 'testpartners', 'test validate', '2020-07-02 00:00:00', '2020-07-06 00:00:00', 'test validate', 2021, 23, 'Expiring', '2020-07-03 00:00:00', '2020-07-07', '2020-07-16', '2020-07-10 00:00:00', '2020-07-13 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 14:59:55'),
('2020JUL23CONS00020', 'testpartners', 'CONS', 1324, 'test validate', '', 'testpartners', 'testpartners', 'test validate', '2020-07-01 00:00:00', '2020-07-07 00:00:00', 'testpartners', 2021, 2352, 'Expiring', '2020-07-03 00:00:00', '2020-07-09', '2020-07-30', '2020-07-14 00:00:00', '2020-07-30 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 15:11:49'),
('2020JUL23CONS00021', 'testpartners', 'CONS', 32, 'testpartners', '', 'testpartners', 'testpartners', 'testpartners', '2020-07-06 00:00:00', '2020-07-07 00:00:00', 'test validate', 2021, 523, 'Expiring', '2020-07-07 00:00:00', '2020-07-09', '2020-07-30', '2020-07-16 00:00:00', '2020-07-30 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-07-23 15:18:15'),
('2020JUN24CONS00002', 'Test Title Data', 'CONS', 21235135, 'test validate', '', 'testing', NULL, 'test validate', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'test validate', 2021, 32313453, 'Expiring', '0000-00-00 00:00:00', '2020-06-30', '2020-06-30', '2020-06-01 00:00:00', '2020-06-30 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 11:23:02'),
('2020JUN24CONS00003', 'test partner history', 'CONS', 2981823, 'test partner history', '', 'test partner history', NULL, 'test partner history', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'test partner history', 2021, 39183981, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-01', '2020-06-01 00:00:00', '2020-08-24 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 11:41:10'),
('2020JUN24CONS00004', 'testing partners', 'CONS', 2972878, 'testing partners', '', 'testing partners', NULL, 'testing partners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testing partners', 2021, 3098239, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-06-30', '2020-05-31 00:00:00', '2020-07-01 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 11:49:32'),
('2020JUN24CONS00005', 'testpartners', 'CONS', 2978923, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 3298229, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-01', '2020-06-01 00:00:00', '2020-07-01 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 13:34:55'),
('2020JUN24CONS00006', 'testpartners', 'CONS', 2873988, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 39283921, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-01', '2020-06-01 00:00:00', '2020-07-08 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 13:38:25'),
('2020JUN24CONS00007', 'testpartners', 'CONS', 2983794, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 3298391, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-01', '2020-06-01 00:00:00', '2020-07-01 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 16:52:36'),
('2020JUN24CONS00008', 'testpartners', 'CONS', 2983794, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 3298391, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-01', '2020-06-01 00:00:00', '2020-07-01 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-24 16:54:26'),
('2020JUN25CONS00009', 'testpartners', 'CONS', 2098042, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 3208202, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-06-30', '2020-06-01 00:00:00', '2020-07-09 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-25 10:27:20'),
('2020JUN25CONS00010', 'testpartners', 'CONS', 2354667, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 3291893, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-08', '2020-06-01 00:00:00', '2020-07-08 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-25 11:10:05'),
('2020JUN25CONS00011', 'testpartners', 'CONS', 2873982, 'testpartners', '', 'testpartners', NULL, 'testpartners', '2020-06-01 00:00:00', '2020-06-01 00:00:00', 'testpartners', 2021, 3208093, 'Expiring', '0000-00-00 00:00:00', '2020-06-01', '2020-07-01', '2020-06-01 00:00:00', '2020-07-01 00:00:00', NULL, NULL, '2020-08-13 13:51:33', '2020-06-25 12:17:48'),
('2020MAY29CONS00001', 'test all', 'CONS', 4521257, 'test all', '', 'test all', 'test all', 'test all', '2020-04-27 00:00:00', '2020-04-27 00:00:00', 'test all', 2121, 246262, 'Ongoing', '0000-00-00 00:00:00', '2020-04-27', '2020-05-31', '2020-04-27 00:00:00', '2020-06-01 00:00:00', NULL, NULL, '2020-09-18 10:41:48', '2020-05-29 14:30:03'),
('2020SEP10CONS00028', 'testperfSec', 'CONS', 21, 'testperfSec', 'SVP', 'testperfSec', 'testperfSec', 'testperfSec', '2020-09-02 00:00:00', '2020-09-04 00:00:00', 'testperfSec', 2021, 21, 'New', '2020-09-03 00:00:00', '2020-09-07', '2020-09-30', '2020-09-08 00:00:00', '2020-09-09 00:00:00', NULL, NULL, '2020-09-10 17:28:07', '2020-09-10 17:28:07'),
('2020SEP10CONS00029', 'testperfSec insert', 'CONS', 12, 'testperfSec insert', 'CB', 'testperfSec insert', 'testperfSec insert', 'testperfSec insert', '2020-09-01 00:00:00', '2020-09-03 00:00:00', 'testperfSec insert', 2021, 32, 'New', '2020-09-02 00:00:00', '2020-09-04', '2020-09-24', '2020-09-08 00:00:00', '2020-09-18 00:00:00', NULL, NULL, '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
('2020SEP10CONS00030', 'test insert', 'CONS', 12, 'test insert', 'NP', 'test insert', 'test insert', 'test insert', '2020-09-01 00:00:00', '2020-09-04 00:00:00', 'test insert', 2021, 324, 'New', '2020-09-02 00:00:00', '2020-09-07', '2020-09-30', '2020-09-15 00:00:00', '2020-09-30 00:00:00', NULL, NULL, '2020-09-10 17:40:57', '2020-09-10 17:40:57'),
('2020SEP10CONS00031', 'save test', 'CONS', 12, 'save test', 'CB', 'save test', 'save test', 'save test', '2020-09-01 00:00:00', '2020-09-04 00:00:00', 'save test', 2021, 213, 'New', '2020-09-03 00:00:00', '2020-09-07', '2020-09-30', '2020-09-16 00:00:00', '2020-09-30 00:00:00', NULL, NULL, '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
('2020SEP10CONS00032', 'test insert', 'CONS', 21, 'test insert', 'S', 'test insert', 'test insert', 'test insert', '2020-09-01 00:00:00', '2020-09-09 00:00:00', 'test insert', 2021, 32, 'New', '2020-09-01 00:00:00', '2020-09-11', '2020-09-02', '2020-09-14 00:00:00', '2020-09-30 00:00:00', NULL, NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `contract_experts`
--

CREATE TABLE `contract_experts` (
  `id` int(255) NOT NULL,
  `Contract_ID` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Firm_ID` int(25) NOT NULL,
  `expert_id` int(25) NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contract_remarks`
--

CREATE TABLE `contract_remarks` (
  `remarks_id` int(225) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `remarks_title` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reviewed_by` varchar(225) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_reviewed` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_remarks`
--

INSERT INTO `contract_remarks` (`remarks_id`, `Contract_ID`, `remarks_title`, `notes`, `reviewed_by`, `date_reviewed`, `updated_at`, `created_at`) VALUES
(1, '2020JUL01CONS00012', 'Project Status Update', NULL, NULL, NULL, '2020-07-23 10:58:55', '2020-07-23 10:58:55'),
(2, '2020JUL01CONS00013', 'Project Status Update', 'testing project update', 'hdfusi', '2020-07-23 00:00:00', '2020-07-23 11:21:23', '2020-07-23 11:21:23'),
(3, '2020JUL17CONS00014', 'Project Status Update', NULL, NULL, NULL, '2020-07-23 11:23:47', '2020-07-23 11:23:47'),
(4, '2020MAY29CONS00001', 'Project Status Update', 'Testing notes for project status', 'Hazel Fusi', '2020-09-18 00:00:00', '2020-09-18 10:41:49', '2020-09-18 10:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `contract_type`
--

CREATE TABLE `contract_type` (
  `Contract_Type_ID` int(10) NOT NULL,
  `Contract_Type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Contract_Desc` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contract_type`
--

INSERT INTO `contract_type` (`Contract_Type_ID`, `Contract_Type`, `Contract_Desc`) VALUES
(1, 'CONS', 'Consulting'),
(2, 'GIP', 'GIP (Goods and Infrastructure Projects)'),
(3, 'ALL', 'all types');

-- --------------------------------------------------------

--
-- Table structure for table `correspondence`
--

CREATE TABLE `correspondence` (
  `Correspondence_ID` int(20) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `Correspondence_Date` datetime DEFAULT NULL,
  `Originator` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Purpose` varchar(50) CHARACTER SET latin1 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `deliverables`
--

CREATE TABLE `deliverables` (
  `Deliverable_ID` int(20) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Deliverable` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Target_Completion_Date` datetime DEFAULT NULL,
  `Amended_Date` datetime DEFAULT NULL,
  `Actual_Submission_Date` datetime DEFAULT NULL,
  `Actual_Completion_Date` datetime DEFAULT NULL,
  `Payment_Status` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deliverables`
--

INSERT INTO `deliverables` (`Deliverable_ID`, `Contract_ID`, `Deliverable`, `Target_Completion_Date`, `Amended_Date`, `Actual_Submission_Date`, `Actual_Completion_Date`, `Payment_Status`, `updated_at`, `created_at`) VALUES
(1, '2020MAY29CONS00001', 'test all', '2020-06-02 00:00:00', '2020-09-24 00:00:00', NULL, NULL, 'Ongoing', '2020-09-04 14:38:14', '2020-05-29 14:30:05'),
(2, '2020MAY29CONS00001', 'test all', '2020-08-13 00:00:00', NULL, NULL, NULL, 'Paid', '2020-08-11 11:25:14', '2020-05-29 14:30:05'),
(3, '2020MAY29CONS00001', 'test all', '2020-06-01 00:00:00', NULL, NULL, NULL, NULL, '2020-06-23 18:20:00', '2020-05-29 14:30:05'),
(4, '2020JUN24CONS00002', 'testing123', '2020-08-12 00:00:00', '2020-06-19 00:00:00', NULL, NULL, 'Paid', '2020-08-11 12:24:29', '2020-06-24 11:23:02'),
(5, '2020JUN24CONS00002', 'testing345', '2020-08-13 00:00:00', NULL, NULL, NULL, 'Paid', '2020-08-11 13:21:07', '2020-06-24 11:23:02'),
(6, '2020JUN24CONS00002', 'testin123', '2020-08-04 00:00:00', NULL, NULL, NULL, 'Paid', '2020-08-11 13:21:00', '2020-06-24 11:23:02'),
(7, '2020JUN24CONS00003', 'test partner history1', NULL, NULL, NULL, NULL, 'Paid', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(8, '2020JUN24CONS00003', 'test partner history2', NULL, NULL, NULL, NULL, NULL, '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(9, '2020JUN24CONS00003', 'test partner history3', NULL, NULL, NULL, NULL, NULL, '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(10, '2020JUN24CONS00004', 'testing partners1', NULL, NULL, NULL, NULL, NULL, '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(11, '2020JUN24CONS00004', 'testing partners2', NULL, NULL, NULL, NULL, NULL, '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(12, '2020JUN24CONS00004', 'testing partners3', NULL, NULL, NULL, NULL, NULL, '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(13, '2020JUN24CONS00006', 'testpartners1', NULL, NULL, NULL, NULL, NULL, '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(14, '2020JUN24CONS00006', 'testpartners2', NULL, NULL, NULL, NULL, NULL, '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(15, '2020JUN24CONS00006', 'testpartners3', NULL, NULL, NULL, NULL, NULL, '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(16, '2020JUN25CONS00010', 'testpartners1', NULL, NULL, NULL, NULL, NULL, '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(17, '2020JUN25CONS00010', 'testpartners2', NULL, NULL, NULL, NULL, NULL, '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(18, '2020JUN25CONS00010', 'testpartners3', NULL, NULL, NULL, NULL, NULL, '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(19, '2020JUL01CONS00012', 'test dlvrble date1', '2020-07-01 00:00:00', '2020-08-07 00:00:00', NULL, NULL, NULL, '2020-07-22 20:28:38', '2020-07-01 12:58:42'),
(20, '2020JUL01CONS00012', 'test dlvrble date1', '2020-07-02 00:00:00', '2020-07-24 00:00:00', NULL, NULL, NULL, '2020-07-22 16:17:50', '2020-07-01 12:58:42'),
(21, '2020JUL01CONS00012', 'test dlvrble date1', '2020-07-06 00:00:00', '2020-07-07 00:00:00', NULL, NULL, NULL, '2020-07-22 18:50:12', '2020-07-01 12:58:42'),
(22, '2020JUL01CONS00012', 'test dlvrble date2', '2020-07-01 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(23, '2020JUL01CONS00012', 'test dlvrble date2', '2020-07-02 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(24, '2020JUL01CONS00012', 'test dlvrble date2', '2020-07-06 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(25, '2020JUL01CONS00012', 'test dlvrble date3', '2020-07-01 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(26, '2020JUL01CONS00012', 'test dlvrble date3', '2020-07-02 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(27, '2020JUL01CONS00012', 'test dlvrble date3', '2020-07-06 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(28, '2020JUL01CONS00013', 'test dlvrble date1', '2020-07-06 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 17:51:12', '2020-07-01 17:51:12'),
(29, '2020JUL01CONS00013', 'test dlvrble date2', '2020-07-08 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 17:51:13', '2020-07-01 17:51:13'),
(30, '2020JUL01CONS00013', 'test dlvrble date3', '2020-07-15 00:00:00', NULL, NULL, NULL, NULL, '2020-07-01 17:51:13', '2020-07-01 17:51:13'),
(31, '2020JUL17CONS00014', 'testpartners', '2020-07-15 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(32, '2020JUL17CONS00014', 'testpartners2', '2020-07-16 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(33, '2020JUL17CONS00014', 'testpartners3', '2020-07-23 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(34, '2020JUL23CONS00017', 'test validate2', '2020-07-23 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(35, '2020JUL23CONS00017', 'test validate2', '2020-07-23 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(36, '2020JUL23CONS00017', 'testin123', '2020-07-23 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(37, '2020JUL23CONS00018', 'testpartners2', '2020-07-09 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(38, '2020JUL23CONS00018', 'testpartners1', '2020-07-21 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(39, '2020JUL23CONS00018', 'test validate1', '2020-07-29 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(40, '2020JUL23CONS00019', 'test dlvrble date2', '2020-07-01 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(41, '2020JUL23CONS00019', 'testpartners1', '2020-07-09 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(42, '2020JUL23CONS00019', 'test validate2', '2020-07-10 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(43, '2020JUL23CONS00020', 'test dlvrble date2', '2020-07-06 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(44, '2020JUL23CONS00020', 'testpartners2', '2020-07-09 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(45, '2020JUL23CONS00020', 'testpartners1', '2020-07-28 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(46, '2020JUL23CONS00021', 'test validate2', '2020-07-07 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(47, '2020JUL23CONS00021', 'testpartners2', '2020-07-10 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(48, '2020JUL23CONS00021', 'testin123', '2020-07-30 00:00:00', NULL, NULL, NULL, 'Ongoing', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(49, '2020AUG07CONS00022', 'test multiple perfSec1', '2020-08-03 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 13:50:08', '2020-08-07 13:50:08'),
(50, '2020AUG07CONS00022', 'test multiple perfSec2', '2020-08-05 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 13:50:08', '2020-08-07 13:50:08'),
(51, '2020AUG07CONS00022', 'test multiple perfSec3', '2020-08-27 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 13:50:08', '2020-08-07 13:50:08'),
(52, '2020AUG07CONS00023', 'test multiple perfSec2', '2020-08-06 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 13:54:03', '2020-08-07 13:54:03'),
(53, '2020AUG07CONS00023', 'test multiple perfSec1', '2020-08-10 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 13:54:03', '2020-08-07 13:54:03'),
(54, '2020AUG07CONS00023', 'test multiple perfSec3', '2020-08-26 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 13:54:03', '2020-08-07 13:54:03'),
(55, '2020AUG07CONS00024', 'test multiple perfSec1', '2020-08-03 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 14:38:11', '2020-08-07 14:38:11'),
(56, '2020AUG07CONS00024', 'test multiple perfSec2', '2020-08-06 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 14:38:11', '2020-08-07 14:38:11'),
(57, '2020AUG07CONS00024', 'test multiple perfSec3', '2020-08-12 00:00:00', NULL, NULL, NULL, NULL, '2020-08-07 14:38:11', '2020-08-07 14:38:11'),
(58, '2020AUG11CONS00025', 'test validate2', '2020-08-03 00:00:00', NULL, NULL, NULL, NULL, '2020-08-11 09:51:08', '2020-08-11 09:51:08'),
(59, '2020AUG11CONS00025', 'testpartners1', '2020-08-13 00:00:00', NULL, NULL, NULL, NULL, '2020-08-11 09:51:08', '2020-08-11 09:51:08'),
(60, '2020AUG11CONS00025', 'test multiple perfSec2', '2020-08-27 00:00:00', NULL, NULL, NULL, NULL, '2020-08-11 09:51:08', '2020-08-11 09:51:08'),
(61, '2020AUG12CONS00026', 'testpartners2', '2020-08-10 00:00:00', NULL, NULL, NULL, NULL, '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(62, '2020AUG12CONS00026', 'test validate1', '2020-08-05 00:00:00', NULL, NULL, NULL, NULL, '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(63, '2020AUG12CONS00026', 'test multiple perfSec2', '2020-08-14 00:00:00', NULL, NULL, NULL, NULL, '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(64, '2020AUG13CONS00027', 'test dlvrble date2', '2020-08-10 00:00:00', NULL, NULL, NULL, NULL, '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(65, '2020AUG13CONS00027', 'test validate1', '2020-08-05 00:00:00', NULL, NULL, NULL, NULL, '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(66, '2020AUG13CONS00027', 'testpartners1', '2020-08-27 00:00:00', NULL, NULL, NULL, NULL, '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(67, '2020SEP10CONS00028', 'testperfSec1', '2020-09-16 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:28:07', '2020-09-10 17:28:07'),
(68, '2020SEP10CONS00028', 'testperfSec2', '2020-09-24 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:28:07', '2020-09-10 17:28:07'),
(69, '2020SEP10CONS00028', 'testperfSec3', '2020-09-30 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:28:07', '2020-09-10 17:28:07'),
(70, '2020SEP10CONS00029', 'testperfSec insert1', '2020-09-07 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(71, '2020SEP10CONS00029', 'testperfSec insert2', '2020-09-16 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(72, '2020SEP10CONS00029', 'testperfSec insert3', '2020-09-30 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(73, '2020SEP10CONS00030', 'test insert1', '2020-09-08 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:40:57', '2020-09-10 17:40:57'),
(74, '2020SEP10CONS00030', 'test insert2', '2020-09-08 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:40:57', '2020-09-10 17:40:57'),
(75, '2020SEP10CONS00030', 'test insert3', '2020-09-15 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:40:57', '2020-09-10 17:40:57'),
(76, '2020SEP10CONS00031', 'save test1', '2020-09-08 00:00:00', '2020-09-01 00:00:00', NULL, NULL, 'Ongoing', '2020-09-23 13:00:53', '2020-09-10 17:43:49'),
(77, '2020SEP10CONS00031', 'save test2', '2020-09-17 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
(78, '2020SEP10CONS00031', 'save test3', '2020-09-30 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
(79, '2020SEP10CONS00032', 'test insert1', '2020-09-01 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49'),
(80, '2020SEP10CONS00032', 'test insert2', '2020-09-10 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49'),
(81, '2020SEP10CONS00032', 'test insert3', '2020-09-23 00:00:00', NULL, NULL, NULL, NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `Document_ID` int(20) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Document_Link` varchar(250) CHARACTER SET latin1 NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`Document_ID`, `Contract_ID`, `Document_Link`, `updated_at`, `created_at`) VALUES
(1, '2020MAY29CONS00001', 'public/storage/supportingDocs/Fearless.jpg', '2020-05-29 14:30:04', '2020-05-29 14:30:04'),
(2, '2020JUN24CONS00002', 'public/storage/supportingDocs/captain-america-hot-toys-mms536-16-action-figure-avengers-endgame.jpg', '2020-06-24 11:23:02', '2020-06-24 11:23:02'),
(3, '2020JUN24CONS00003', 'public/storage/supportingDocs/81DD0b58-NL._AC_SL1500_.jpg', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(4, '2020JUN24CONS00003', 'public/storage/supportingDocs/20190503-delish-pineapple-baked-salmon-horizontal-ehg-450-1557771120.jpg', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(5, '2020JUN24CONS00003', 'public/storage/supportingDocs/213702364-352-k168881.jpg', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(6, '2020JUN24CONS00003', 'public/storage/supportingDocs/b2906978a75c4caf8bbf459ff0240243xl.jpg', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(7, '2020JUN24CONS00003', 'public/storage/supportingDocs/batman_dc-comics_silo (1).jpeg', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(8, '2020JUN24CONS00004', 'public/storage/supportingDocs/81DD0b58-NL._AC_SL1500_.jpg', '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(9, '2020JUN24CONS00004', 'public/storage/supportingDocs/batman_dc-comics_silo.webp', '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(10, '2020JUN24CONS00006', 'public/storage/supportingDocs/test.jpg', '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(11, '2020JUN25CONS00010', 'public/storage/supportingDocs/test.jpg', '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(12, '2020JUL01CONS00012', 'public/storage/supportingDocs/test.jpg', '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(13, '2020JUL01CONS00013', 'public/storage/supportingDocs/test.jpg', '2020-07-01 17:51:12', '2020-07-01 17:51:12'),
(14, '2020JUL17CONS00014', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(15, '2020JUL23CONS00017', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(16, '2020JUL23CONS00018', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(17, '2020JUL23CONS00019', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(18, '2020JUL23CONS00020', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(19, '2020JUL23CONS00021', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(20, '2020AUG07CONS00022', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-08-07 13:50:08', '2020-08-07 13:50:08'),
(21, '2020AUG07CONS00023', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-08-07 13:54:03', '2020-08-07 13:54:03'),
(22, '2020AUG07CONS00024', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-08-07 14:38:11', '2020-08-07 14:38:11'),
(23, '2020AUG11CONS00025', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-08-11 09:51:08', '2020-08-11 09:51:08'),
(24, '2020AUG12CONS00026', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(25, '2020AUG13CONS00027', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(26, '2020SEP10CONS00028', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-09-10 17:28:07', '2020-09-10 17:28:07'),
(27, '2020SEP10CONS00029', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(28, '2020SEP10CONS00030', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-09-10 17:40:57', '2020-09-10 17:40:57'),
(29, '2020SEP10CONS00031', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
(30, '2020SEP10CONS00032', 'public/storage/supportingDocs/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-09-10 18:09:49', '2020-09-10 18:09:49'),
(31, '2020MAY29CONS00001', 'public/storage/CFA/long-exposure-startrail-photography-lincoln-harrison-2.jpg', '2020-09-18 10:41:49', '2020-09-18 10:41:49');

-- --------------------------------------------------------

--
-- Table structure for table `firm_partners`
--

CREATE TABLE `firm_partners` (
  `id` int(25) NOT NULL,
  `Contract_ID` varchar(25) CHARACTER SET latin1 NOT NULL,
  `Firm_Lead_ID` int(25) DEFAULT NULL,
  `Firm_Partner_ID` int(25) DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `firm_partners`
--

INSERT INTO `firm_partners` (`id`, `Contract_ID`, `Firm_Lead_ID`, `Firm_Partner_ID`, `updated_at`, `created_at`) VALUES
(1, '2020JUN24CONS00002', NULL, NULL, '2020-06-24 11:23:02', '2020-06-24 11:23:02'),
(2, '2020JUN24CONS00004', 3, 3, '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(3, '2020JUN24CONS00006', NULL, 3, '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(4, '2020JUN25CONS00010', NULL, 3, '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(5, '2020JUL17CONS00014', NULL, 3, '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(6, '2020JUL23CONS00017', 1, 2, '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(7, '2020JUL23CONS00018', 2, NULL, '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(8, '2020JUL23CONS00018', 3, NULL, '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(9, '2020JUL23CONS00020', NULL, 2, '2020-07-23 15:11:49', '2020-07-23 15:11:49'),
(10, '2020JUL23CONS00020', NULL, 3, '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(11, '2020JUL23CONS00021', 1, NULL, '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(12, '2020JUL23CONS00021', NULL, 2, '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(13, '2020JUL23CONS00021', NULL, 3, '2020-07-23 15:18:15', '2020-07-23 15:18:15');

-- --------------------------------------------------------

--
-- Table structure for table `firm_projects`
--

CREATE TABLE `firm_projects` (
  `id` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Firm_ID` int(25) NOT NULL,
  `Firm_Type` varchar(25) CHARACTER SET latin1 NOT NULL,
  `firm_role` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `firm_projects`
--

INSERT INTO `firm_projects` (`id`, `Contract_ID`, `Firm_ID`, `Firm_Type`, `firm_role`, `updated_at`, `created_at`) VALUES
(1, '2020MAY29CONS00001', 1, 'S', '', '2020-05-29 14:30:04', '2020-05-29 14:30:04'),
(2, '2020JUN24CONS00002', 2, 'CORP', 'P', '2020-06-24 11:23:02', '2020-06-24 11:23:02'),
(3, '2020JUN24CONS00003', 1, 'CORP', 'L', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(4, '2020JUN24CONS00004', 1, 'CORP', 'L', '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(5, '2020JUN24CONS00006', 1, 'CORP', 'L', '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(6, '2020JUN25CONS00010', 1, 'CORP', 'L', '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(7, '2020JUN25CONS00011', 1, 'CORP', 'L', '2020-06-25 12:17:48', '2020-06-25 12:17:48'),
(8, '2020JUL01CONS00012', 1, 'S', NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(9, '2020JUL01CONS00013', 1, 'S', NULL, '2020-07-01 17:51:12', '2020-07-01 17:51:12'),
(10, '2020JUL17CONS00014', 3, 'CORP', 'P', '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(11, '2020JUL23CONS00015', 1, 'CORP', 'L', '2020-07-23 14:04:54', '2020-07-23 14:04:54'),
(13, '2020JUL23CONS00016', 1, 'CORP', 'L', '2020-07-23 14:13:22', '2020-07-23 14:13:22'),
(14, '2020JUL23CONS00017', 2, 'CORP', 'P', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(15, '2020JUL23CONS00017', 1, 'CORP', 'L', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(16, '2020JUL23CONS00017', 2, 'CORP', 'L', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(17, '2020JUL23CONS00018', 1, 'CORP', 'L', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(18, '2020JUL23CONS00018', 2, 'CORP', 'L', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(19, '2020JUL23CONS00018', 3, 'CORP', 'L', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(23, '2020JUL23CONS00019', 1, 'CORP', 'L', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(24, '2020JUL23CONS00019', 1, 'CORP', 'P', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(25, '2020JUL23CONS00019', 1, 'CORP', 'P', '2020-07-23 14:59:55', '2020-07-23 14:59:55'),
(28, '2020JUL23CONS00020', 1, 'CORP', 'L', '2020-07-23 15:11:49', '2020-07-23 15:11:49'),
(29, '2020JUL23CONS00020', 2, 'CORP', 'P', '2020-07-23 15:11:49', '2020-07-23 15:11:49'),
(30, '2020JUL23CONS00020', 3, 'CORP', 'P', '2020-07-23 15:11:49', '2020-07-23 15:11:49'),
(31, '2020JUL23CONS00021', 1, 'CORP', 'L', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(32, '2020JUL23CONS00021', 2, 'CORP', 'P', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(33, '2020JUL23CONS00021', 3, 'CORP', 'P', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(34, '2020AUG07CONS00022', 1, 'S', NULL, '2020-08-07 13:50:08', '2020-08-07 13:50:08'),
(35, '2020AUG07CONS00023', 1, 'S', NULL, '2020-08-07 13:54:03', '2020-08-07 13:54:03'),
(36, '2020AUG07CONS00024', 2, 'S', NULL, '2020-08-07 14:38:11', '2020-08-07 14:38:11'),
(37, '2020AUG11CONS00025', 1, 'S', NULL, '2020-08-11 09:51:08', '2020-08-11 09:51:08'),
(38, '2020AUG12CONS00026', 1, 'S', NULL, '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(39, '2020AUG13CONS00027', 1, 'S', NULL, '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(40, '2020SEP10CONS00028', 1, 'S', NULL, '2020-09-10 17:28:07', '2020-09-10 17:28:07'),
(41, '2020SEP10CONS00029', 1, 'S', NULL, '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(42, '2020SEP10CONS00030', 1, 'S', NULL, '2020-09-10 17:40:57', '2020-09-10 17:40:57'),
(43, '2020SEP10CONS00031', 1, 'S', NULL, '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
(44, '2020SEP10CONS00032', 1, 'S', NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `firm_role`
--

CREATE TABLE `firm_role` (
  `firm_role_id` int(3) NOT NULL,
  `firm_role` varchar(25) CHARACTER SET latin1 NOT NULL,
  `firm_role_desc` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `firm_role`
--

INSERT INTO `firm_role` (`firm_role_id`, `firm_role`, `firm_role_desc`) VALUES
(1, 'L', 'Lead Firm'),
(2, 'P', 'Partner');

-- --------------------------------------------------------

--
-- Table structure for table `firm_type`
--

CREATE TABLE `firm_type` (
  `Firm_Type_ID` int(5) NOT NULL,
  `Firm_Type` varchar(25) CHARACTER SET latin1 NOT NULL,
  `Firm_Type_Desc` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `firm_type`
--

INSERT INTO `firm_type` (`Firm_Type_ID`, `Firm_Type`, `Firm_Type_Desc`) VALUES
(1, 'S', 'Single'),
(2, 'CORP', 'Corporation'),
(3, 'P', 'Partnership'),
(4, 'JV', 'Joint Venture'),
(5, 'C', 'Consortium');

-- --------------------------------------------------------

--
-- Table structure for table `liquidated_damages`
--

CREATE TABLE `liquidated_damages` (
  `LD_ID` int(20) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Amount` double DEFAULT NULL,
  `Days` varchar(25) CHARACTER SET latin1 DEFAULT NULL,
  `Expiry_Date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `management_presentation`
--

CREATE TABLE `management_presentation` (
  `Presentation_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Level_Representation` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Purpose_Presentation` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Date_Presentation` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meeting`
--

CREATE TABLE `meeting` (
  `Meeting_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Reason_Purpose` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Date_Meeting` datetime DEFAULT NULL,
  `Date_Requested` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_05_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nda`
--

CREATE TABLE `nda` (
  `NDA_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `Date_NDA` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email_address`, `token`, `created_at`) VALUES
('hdfusi@neda.gov.ph', '$2y$10$QIxNZHDnYbZk9C0Bzis7teU1GAdiDuZ0mcuLbiLCj9gjb7lROtvz6', '2020-06-29 00:29:20');

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `Payment_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Deliverable_ID` int(20) NOT NULL,
  `Gross_Payment` float DEFAULT NULL,
  `Retention` float DEFAULT NULL,
  `VAT` float DEFAULT NULL,
  `Reimbursible_Amt` float DEFAULT NULL,
  `EWT` float DEFAULT NULL,
  `Liquidated_Damages` float DEFAULT NULL,
  `Net_Payment` float DEFAULT NULL,
  `Payment_Date` datetime DEFAULT NULL,
  `Disbursement_Voucher` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Payment_Status` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`Payment_ID`, `Contract_ID`, `Deliverable_ID`, `Gross_Payment`, `Retention`, `VAT`, `Reimbursible_Amt`, `EWT`, `Liquidated_Damages`, `Net_Payment`, `Payment_Date`, `Disbursement_Voucher`, `Payment_Status`, `updated_at`, `created_at`) VALUES
(1, '2020MAY29CONS00001', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-24 00:00:00', NULL, NULL, '2020-09-04 12:10:53', '2020-09-04 12:07:39'),
(2, '2020SEP10CONS00031', 76, 2453260000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-23 12:57:16', '2020-09-23 12:42:15');

-- --------------------------------------------------------

--
-- Table structure for table `performance_security`
--

CREATE TABLE `performance_security` (
  `Security_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Type` varchar(50) CHARACTER SET latin1 NOT NULL,
  `CashOR` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `CheckNum` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `CheckOR` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `CheckBank` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `BankNameBD` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `DraftBD` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `ValidityBD` datetime DEFAULT NULL,
  `BankNameBG` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `DraftBG` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `ValidityBG` datetime DEFAULT NULL,
  `BankNameILC` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `ILC` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `ValidityILC` datetime DEFAULT NULL,
  `CNameSurety` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `SuretyNum` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `ValiditySurety` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `performance_security`
--

INSERT INTO `performance_security` (`Security_ID`, `Contract_ID`, `Type`, `CashOR`, `CheckNum`, `CheckOR`, `CheckBank`, `BankNameBD`, `DraftBD`, `ValidityBD`, `BankNameBG`, `DraftBG`, `ValidityBG`, `BankNameILC`, `ILC`, `ValidityILC`, `CNameSurety`, `SuretyNum`, `ValiditySurety`, `updated_at`, `created_at`) VALUES
(1, '2020MAY29CONS00001', 'C', 'test all', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-05-29 14:30:05', '2020-05-29 14:30:05'),
(2, '2020JUN24CONS00002', 'BD', NULL, NULL, NULL, NULL, 'test', 'test123', '2020-07-10 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-24 11:23:02', '2020-06-24 11:23:02'),
(3, '2020JUN25CONS00010', 'CMC', NULL, 'gsofkj24', '452dgwe', 'testpartners', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(4, '2020JUL01CONS00012', 'C', 'fhe43d4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(5, '2020JUL01CONS00013', 'C', 'test dlvrble date', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-01 17:51:13', '2020-07-01 17:51:13'),
(6, '2020JUL17CONS00014', 'BD', NULL, NULL, NULL, NULL, 'test', 'test', '2020-07-23 00:00:00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(7, '2020JUL23CONS00017', 'C', '34r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(8, '2020JUL23CONS00018', 'C', '4523', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(9, '2020JUL23CONS00019', 'C', '34r', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 14:59:56', '2020-07-23 14:59:56'),
(10, '2020JUL23CONS00020', 'C', '2352', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(11, '2020JUL23CONS00021', 'C', '43252', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(12, '2020AUG12CONS00026', 'C', '2etfwt', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(13, '2020AUG13CONS00027', 'CMC', '2etfwt', 'djte53f', '452dgwe', 'dfhw', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(14, '2020SEP10CONS00028', 'CMC', 'testperfSec', 'testperfSec', 'testperfSec', 'testperfSec', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 17:28:08', '2020-09-10 17:28:08'),
(15, '2020SEP10CONS00029', 'CMC', 'testperfSec insert', 'testperfSec insert', 'testperfSec insert', 'testperfSec insert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(16, '2020SEP10CONS00030', 'CMC', 'test insert', 'test insert', 'test insert', 'test insert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 17:40:58', '2020-09-10 17:40:58'),
(17, '2020SEP10CONS00031', 'CMC', 'save test', 'save test', 'save test', 'save test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
(18, '2020SEP10CONS00032', 'C', 'test insert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49'),
(19, '2020SEP10CONS00032', 'CMC', NULL, 'test insert', 'test insert', 'test insert', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-09-10 18:09:49', '2020-09-10 18:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `procurement_mode`
--

CREATE TABLE `procurement_mode` (
  `mode_id` int(11) NOT NULL,
  `mode_code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mode_desc` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `procurement_mode`
--

INSERT INTO `procurement_mode` (`mode_id`, `mode_code`, `mode_desc`) VALUES
(1, 'CB', 'Competitive Bidding'),
(2, 'NP', 'Negotiated Procurement'),
(3, 'SVP', 'Small Value Procurement (Section 53.9)'),
(4, 'HTC', 'Highly Technical Consultant (Section 53.7)'),
(5, 'S', 'Scientific'),
(6, 'SAW', 'Scholarly or Artistic Work'),
(7, 'ETMS', 'Exclusive Technology and Media Services (Section 53.6)');

-- --------------------------------------------------------

--
-- Table structure for table `project_history`
--

CREATE TABLE `project_history` (
  `History_ID` int(50) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `History_Description` varchar(120) CHARACTER SET utf8mb4 NOT NULL,
  `History_Date` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `project_history`
--

INSERT INTO `project_history` (`History_ID`, `Contract_ID`, `History_Description`, `History_Date`, `updated_at`, `created_at`) VALUES
(1, '2020MAY29CONS00001', 'Deliverables Target Date Updated', '2020-06-23 15:39:30', '2020-06-23 15:39:30', '2020-06-23 15:39:30'),
(2, '2020MAY29CONS00001', 'Deliverables Target Date Updated', '2020-06-23 18:19:41', '2020-06-23 18:19:41', '2020-06-23 18:19:41'),
(3, '2020MAY29CONS00001', 'Deliverables Target Date Updated', '2020-06-23 18:20:00', '2020-06-23 18:20:00', '2020-06-23 18:20:00'),
(4, '2020JUN24CONS00003', 'Contract Created', '2020-06-24 11:41:10', '2020-06-24 11:41:10', '2020-06-24 11:41:10'),
(5, '2020JUN24CONS00004', 'Contract Created', '2020-06-24 11:49:32', '2020-06-24 11:49:32', '2020-06-24 11:49:32'),
(6, '2020JUN24CONS00006', 'Contract Created', '2020-06-24 13:38:25', '2020-06-24 13:38:25', '2020-06-24 13:38:25'),
(7, '2020JUN25CONS00010', 'Contract Created', '2020-06-25 11:10:05', '2020-06-25 11:10:05', '2020-06-25 11:10:05'),
(8, '2020JUN24CONS00002', 'Deliverables Amendment Date Updated', '2020-06-29 11:53:57', '2020-06-29 11:53:57', '2020-06-29 11:53:57'),
(9, '2020JUL01CONS00012', 'Contract Created', '2020-07-01 12:58:42', '2020-07-01 12:58:42', '2020-07-01 12:58:42'),
(10, '2020JUL01CONS00013', 'Contract Created', '2020-07-01 17:51:13', '2020-07-01 17:51:13', '2020-07-01 17:51:13'),
(11, '2020JUL01CONS00012', 'Deliverables Amendment Date (19) Updated', '2020-07-16 18:17:41', '2020-07-16 18:17:41', '2020-07-16 18:17:41'),
(12, '2020JUL17CONS00014', 'Contract Created', '2020-07-17 09:29:55', '2020-07-17 09:29:55', '2020-07-17 09:29:55'),
(13, '2020JUL01CONS00012', 'Contract 2020JUL01CONS00012 Termination Date Updated <br>', '2020-07-22 11:01:54', '2020-07-22 11:01:54', '2020-07-22 11:01:54'),
(14, '2020JUL01CONS00012', 'Deliverables Amendment Date (20) Updated', '2020-07-22 16:17:50', '2020-07-22 16:17:50', '2020-07-22 16:17:50'),
(15, '2020JUL01CONS00012', 'Deliverables Amendment Date (21) Updated', '2020-07-22 18:50:12', '2020-07-22 18:50:12', '2020-07-22 18:50:12'),
(16, '2020JUL01CONS00012', 'Deliverables Amendment Date (19) Updated', '2020-07-22 19:44:15', '2020-07-22 19:44:15', '2020-07-22 19:44:15'),
(17, '2020JUL01CONS00012', 'Deliverables Amendment Date (19) Updated', '2020-07-22 20:24:31', '2020-07-22 20:24:31', '2020-07-22 20:24:31'),
(18, '2020JUL01CONS00012', 'Deliverables Amendment Date (19) Updated', '2020-07-22 20:28:38', '2020-07-22 20:28:38', '2020-07-22 20:28:38'),
(19, '2020JUL23CONS00017', 'Contract Created', '2020-07-23 14:19:41', '2020-07-23 14:19:41', '2020-07-23 14:19:41'),
(20, '2020JUL23CONS00018', 'Contract Created', '2020-07-23 14:34:45', '2020-07-23 14:34:45', '2020-07-23 14:34:45'),
(21, '2020JUL23CONS00019', 'Contract Created', '2020-07-23 14:59:56', '2020-07-23 14:59:56', '2020-07-23 14:59:56'),
(22, '2020JUL23CONS00020', 'Contract Created', '2020-07-23 15:11:50', '2020-07-23 15:11:50', '2020-07-23 15:11:50'),
(23, '2020JUL23CONS00021', 'Contract Created', '2020-07-23 15:18:15', '2020-07-23 15:18:15', '2020-07-23 15:18:15'),
(24, '2020AUG07CONS00022', 'Contract Created', '2020-08-07 13:50:08', '2020-08-07 13:50:08', '2020-08-07 13:50:08'),
(25, '2020AUG07CONS00023', 'Contract Created', '2020-08-07 13:54:03', '2020-08-07 13:54:03', '2020-08-07 13:54:03'),
(26, '2020AUG07CONS00024', 'Contract Created', '2020-08-07 14:38:11', '2020-08-07 14:38:11', '2020-08-07 14:38:11'),
(27, '2020AUG11CONS00025', 'Contract Created', '2020-08-11 09:51:08', '2020-08-11 09:51:08', '2020-08-11 09:51:08'),
(28, '2020MAY29CONS00001', 'Deliverables Target Date (2) Updated', '2020-08-11 11:25:14', '2020-08-11 11:25:14', '2020-08-11 11:25:14'),
(29, '2020JUN24CONS00002', 'Deliverables Target Date (4) Updated', '2020-08-11 12:24:29', '2020-08-11 12:24:29', '2020-08-11 12:24:29'),
(30, '2020JUN24CONS00002', 'Deliverables Target Date (6) Updated', '2020-08-11 13:21:00', '2020-08-11 13:21:00', '2020-08-11 13:21:00'),
(31, '2020JUN24CONS00002', 'Deliverables Target Date (5) Updated', '2020-08-11 13:21:07', '2020-08-11 13:21:07', '2020-08-11 13:21:07'),
(32, '2020AUG12CONS00026', 'Contract Created', '2020-08-12 11:28:47', '2020-08-12 11:28:47', '2020-08-12 11:28:47'),
(33, '2020AUG13CONS00027', 'Contract Created', '2020-08-13 14:52:55', '2020-08-13 14:52:55', '2020-08-13 14:52:55'),
(34, '2020MAY29CONS00001', 'Contract 2020MAY29CONS00001 Payment Status for 1 Updated <br>', '2020-09-04 12:07:39', '2020-09-04 12:07:39', '2020-09-04 12:07:39'),
(35, '2020MAY29CONS00001', 'Contract 2020MAY29CONS00001 Payment Status for 1 Updated <br>', '2020-09-04 12:10:53', '2020-09-04 12:10:53', '2020-09-04 12:10:53'),
(36, '2020MAY29CONS00001', 'Deliverables Amendment Date (1) Updated', '2020-09-04 12:58:03', '2020-09-04 12:58:03', '2020-09-04 12:58:03'),
(37, '2020MAY29CONS00001', 'Deliverables Amendment Date (1) Updated', '2020-09-04 14:36:02', '2020-09-04 14:36:02', '2020-09-04 14:36:02'),
(38, '2020MAY29CONS00001', 'Deliverables Amendment Date (1) Updated', '2020-09-04 14:38:14', '2020-09-04 14:38:14', '2020-09-04 14:38:14'),
(39, '2020SEP10CONS00028', 'Contract Created', '2020-09-10 17:28:07', '2020-09-10 17:28:08', '2020-09-10 17:28:08'),
(40, '2020SEP10CONS00029', 'Contract Created', '2020-09-10 17:34:30', '2020-09-10 17:34:30', '2020-09-10 17:34:30'),
(41, '2020SEP10CONS00030', 'Contract Created', '2020-09-10 17:40:58', '2020-09-10 17:40:58', '2020-09-10 17:40:58'),
(42, '2020SEP10CONS00031', 'Contract Created', '2020-09-10 17:43:49', '2020-09-10 17:43:49', '2020-09-10 17:43:49'),
(43, '2020SEP10CONS00032', 'Contract Created', '2020-09-10 18:09:49', '2020-09-10 18:09:49', '2020-09-10 18:09:49'),
(44, '2020MAY29CONS00001', 'Contract 2020MAY29CONS00001 Project Status Updated <br>Notes:Testing notes for project status', '2020-09-18 10:41:49', '2020-09-18 10:41:49', '2020-09-18 10:41:49'),
(45, '2020SEP10CONS00031', 'Contract 2020SEP10CONS00031 Payment Status for 76 Updated <br>', '2020-09-23 12:42:15', '2020-09-23 12:42:15', '2020-09-23 12:42:15'),
(46, '2020SEP10CONS00031', 'Contract 2020SEP10CONS00031 Payment Status for 76 Updated <br>', '2020-09-23 12:50:19', '2020-09-23 12:50:19', '2020-09-23 12:50:19'),
(47, '2020SEP10CONS00031', 'Contract 2020SEP10CONS00031 Payment Status for 76 Updated <br>', '2020-09-23 12:51:42', '2020-09-23 12:51:42', '2020-09-23 12:51:42'),
(48, '2020SEP10CONS00031', 'Contract 2020SEP10CONS00031 Payment Status for 76 Updated <br>', '2020-09-23 12:55:09', '2020-09-23 12:55:09', '2020-09-23 12:55:09'),
(49, '2020SEP10CONS00031', 'Contract 2020SEP10CONS00031 Payment Status for 76 Updated <br>', '2020-09-23 12:57:16', '2020-09-23 12:57:16', '2020-09-23 12:57:16');

-- --------------------------------------------------------

--
-- Table structure for table `security_type`
--

CREATE TABLE `security_type` (
  `Security_Type_ID` int(5) NOT NULL,
  `Type` varchar(5) CHARACTER SET latin1 NOT NULL,
  `Type_Description` varchar(30) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `security_type`
--

INSERT INTO `security_type` (`Security_Type_ID`, `Type`, `Type_Description`) VALUES
(1, 'C', 'Cash'),
(2, 'CMC', 'Cashier or Manager\'s Check'),
(3, 'BD', 'Bank Draft'),
(4, 'BG', 'Bank Guarantee'),
(5, 'ILC', 'Irrevocable Letter of Credit'),
(6, 'SB', 'Surety Bond');

-- --------------------------------------------------------

--
-- Table structure for table `systemlogs`
--

CREATE TABLE `systemlogs` (
  `username` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `activity` text CHARACTER SET latin1 DEFAULT NULL,
  `ipaddress` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `updated_at` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `systemlogs`
--

INSERT INTO `systemlogs` (`username`, `activity`, `ipaddress`, `created_at`, `updated_at`) VALUES
('hdfusi', 'Login Successful', '::1', '2020-05-06 09:19:36', '2020-05-06 09:19:36'),
(NULL, 'Login Failed', '::1', '2020-05-07 10:00:39', '2020-05-07 10:00:39'),
('hdfusi', 'Login Successful', '::1', '2020-05-07 10:00:54', '2020-05-07 10:00:54'),
('hdfusi', 'Login Successful', '::1', '2020-05-08 10:07:56', '2020-05-08 10:07:56'),
('hdfusi', 'Login Successful', '::1', '2020-05-11 07:30:44', '2020-05-11 07:30:44'),
('hdfusi', 'Login Successful', '::1', '2020-05-11 10:37:48', '2020-05-11 10:37:48'),
('hdfusi', 'Login Successful', '::1', '2020-05-12 07:32:12', '2020-05-12 07:32:12'),
('hdfusi', 'Login Successful', '::1', '2020-05-12 14:21:54', '2020-05-12 14:21:54'),
('hdfusi', 'Login Successful', '::1', '2020-05-13 07:48:24', '2020-05-13 07:48:24'),
('hdfusi', 'Login Failed', '::1', '2020-05-14 13:06:50', '2020-05-14 13:06:50'),
('hdfusi', 'Login Successful', '::1', '2020-05-14 13:07:08', '2020-05-14 13:07:08'),
('hdfusi', 'Login Successful', '::1', '2020-05-14 14:44:20', '2020-05-14 14:44:20'),
('hdfusi', 'Login Successful', '::1', '2020-05-15 07:28:26', '2020-05-15 07:28:26'),
('hdfusi', 'Login Successful', '::1', '2020-05-18 09:15:20', '2020-05-18 09:15:20'),
('hdfusi', 'Login Successful', '::1', '2020-05-19 08:36:15', '2020-05-19 08:36:15'),
('hdfusi', 'Login Successful', '::1', '2020-05-20 07:49:12', '2020-05-20 07:49:12'),
('hdfusi', 'Login Successful', '::1', '2020-05-21 08:19:47', '2020-05-21 08:19:47'),
('hdfusi', 'Login Successful', '::1', '2020-05-22 08:50:00', '2020-05-22 08:50:00'),
('hdfusi', 'Login Successful', '::1', '2020-05-26 08:44:59', '2020-05-26 08:44:59'),
('hdfusi', 'Login Successful', '::1', '2020-05-27 08:30:18', '2020-05-27 08:30:18'),
('hdfusi', 'Login Successful', '::1', '2020-05-28 08:47:05', '2020-05-28 08:47:05'),
('hdfusi', 'Login Successful', '::1', '2020-05-29 07:53:40', '2020-05-29 07:53:40'),
('hdfusi', 'Login Failed', '::1', '2020-06-01 06:41:04', '2020-06-01 06:41:04'),
('hdfusi', 'Login Successful', '::1', '2020-06-01 06:41:19', '2020-06-01 06:41:19'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-02 12:43:35', '2020-06-02 12:43:35'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-03 08:06:49', '2020-06-03 08:06:49'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-03 19:15:51', '2020-06-03 19:15:51'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 08:40:29', '2020-06-04 08:40:29'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 11:50:50', '2020-06-04 11:50:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 11:58:54', '2020-06-04 11:58:54'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 14:12:09', '2020-06-04 14:12:09'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 14:22:50', '2020-06-04 14:22:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 15:27:43', '2020-06-04 15:27:43'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 15:27:58', '2020-06-04 15:27:58'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 15:52:10', '2020-06-04 15:52:10'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 15:52:28', '2020-06-04 15:52:28'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 16:09:49', '2020-06-04 16:09:49'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 17:04:19', '2020-06-04 17:04:19'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 18:42:32', '2020-06-04 18:42:32'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 18:43:25', '2020-06-04 18:43:25'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-04 18:44:55', '2020-06-04 18:44:55'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-05 07:25:00', '2020-06-05 07:25:00'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-05 07:42:34', '2020-06-05 07:42:34'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-05 08:26:52', '2020-06-05 08:26:52'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-08 07:12:45', '2020-06-08 07:12:45'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-09 07:18:10', '2020-06-09 07:18:10'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-09 12:47:57', '2020-06-09 12:47:57'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-10 07:29:58', '2020-06-10 07:29:58'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-10 10:53:45', '2020-06-10 10:53:45'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-10 10:54:04', '2020-06-10 10:54:04'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-10 12:22:08', '2020-06-10 12:22:08'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-10 16:35:43', '2020-06-10 16:35:43'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-11 15:01:43', '2020-06-11 15:01:43'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-17 11:24:03', '2020-06-17 11:24:03'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-17 11:24:24', '2020-06-17 11:24:24'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-06-17 18:05:44', '2020-06-17 18:05:44'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-17 18:05:50', '2020-06-17 18:05:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-18 07:52:13', '2020-06-18 07:52:13'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-18 12:23:46', '2020-06-18 12:23:46'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-18 12:43:49', '2020-06-18 12:43:49'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-18 13:01:36', '2020-06-18 13:01:36'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-18 16:05:23', '2020-06-18 16:05:23'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-19 07:25:07', '2020-06-19 07:25:07'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-19 13:43:13', '2020-06-19 13:43:13'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-22 07:43:09', '2020-06-22 07:43:09'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-22 10:26:38', '2020-06-22 10:26:38'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-22 13:13:50', '2020-06-22 13:13:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-22 18:22:03', '2020-06-22 18:22:03'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-23 08:11:47', '2020-06-23 08:11:47'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-23 11:52:18', '2020-06-23 11:52:18'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-23 17:49:27', '2020-06-23 17:49:27'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-24 07:42:56', '2020-06-24 07:42:56'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-24 11:23:29', '2020-06-24 11:23:29'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-06-24 16:50:55', '2020-06-24 16:50:55'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-24 16:51:03', '2020-06-24 16:51:03'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-25 08:09:28', '2020-06-25 08:09:28'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-25 10:20:34', '2020-06-25 10:20:34'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-26 08:13:24', '2020-06-26 08:13:24'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-26 11:54:52', '2020-06-26 11:54:52'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-26 17:24:43', '2020-06-26 17:24:43'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-29 08:30:20', '2020-06-29 08:30:20'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-06-30 08:02:24', '2020-06-30 08:02:24'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-30 08:02:32', '2020-06-30 08:02:32'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-06-30 12:39:07', '2020-06-30 12:39:07'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-01 08:26:20', '2020-07-01 08:26:20'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-01 16:09:13', '2020-07-01 16:09:13'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-07-02 09:16:43', '2020-07-02 09:16:43'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-02 09:16:50', '2020-07-02 09:16:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-03 13:55:16', '2020-07-03 13:55:16'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-06 07:35:54', '2020-07-06 07:35:54'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-06 09:35:37', '2020-07-06 09:35:37'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-06 20:02:55', '2020-07-06 20:02:55'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-07 07:52:11', '2020-07-07 07:52:11'),
('testuser', 'Login Successful', '127.0.0.1', '2020-07-07 13:35:39', '2020-07-07 13:35:39'),
('testuser', 'Login Successful', '127.0.0.1', '2020-07-07 13:38:42', '2020-07-07 13:38:42'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 13:40:09', '2020-07-07 13:40:09'),
('testuser', 'Login Successful', '127.0.0.1', '2020-07-07 13:40:18', '2020-07-07 13:40:18'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 13:43:24', '2020-07-07 13:43:24'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 13:44:19', '2020-07-07 13:44:19'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 13:44:30', '2020-07-07 13:44:30'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-07 13:47:55', '2020-07-07 13:47:55'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 13:48:13', '2020-07-07 13:48:13'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-07 14:08:11', '2020-07-07 14:08:11'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:17:58', '2020-07-07 14:17:58'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:18:27', '2020-07-07 14:18:27'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:19:55', '2020-07-07 14:19:55'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:20:38', '2020-07-07 14:20:38'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:25:54', '2020-07-07 14:25:54'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-07-07 14:28:45', '2020-07-07 14:28:45'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-07-07 14:28:55', '2020-07-07 14:28:55'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-07-07 14:29:28', '2020-07-07 14:29:28'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:44:00', '2020-07-07 14:44:00'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:49:22', '2020-07-07 14:49:22'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:51:32', '2020-07-07 14:51:32'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:52:27', '2020-07-07 14:52:27'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:53:54', '2020-07-07 14:53:54'),
('testuser', 'Login Failed', '127.0.0.1', '2020-07-07 14:54:53', '2020-07-07 14:54:53'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-07 15:13:38', '2020-07-07 15:13:38'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-08 07:46:12', '2020-07-08 07:46:12'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-08 10:30:33', '2020-07-08 10:30:33'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 10:46:24', '2020-07-09 10:46:24'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:01:02', '2020-07-09 12:01:02'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:01:37', '2020-07-09 12:01:37'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:03:30', '2020-07-09 12:03:30'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:04:47', '2020-07-09 12:04:47'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:06:21', '2020-07-09 12:06:21'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 12:07:29', '2020-07-09 12:07:29'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 12:07:50', '2020-07-09 12:07:50'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:08:59', '2020-07-09 12:08:59'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 12:12:06', '2020-07-09 12:12:06'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 12:14:23', '2020-07-09 12:14:23'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 12:15:27', '2020-07-09 12:15:27'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 12:15:48', '2020-07-09 12:15:48'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-09 14:11:46', '2020-07-09 14:11:46'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-09 14:12:53', '2020-07-09 14:12:53'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-10 07:24:09', '2020-07-10 07:24:09'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-10 14:21:34', '2020-07-10 14:21:34'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-13 10:10:32', '2020-07-13 10:10:32'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-07-13 12:08:40', '2020-07-13 12:08:40'),
('hdfusitesting123', 'Login Failed', '127.0.0.1', '2020-07-13 12:08:48', '2020-07-13 12:08:48'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-13 12:08:55', '2020-07-13 12:08:55'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-13 18:27:05', '2020-07-13 18:27:05'),
('user.test', 'Login Successful', '127.0.0.1', '2020-07-13 18:52:46', '2020-07-13 18:52:46'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-14 15:37:17', '2020-07-14 15:37:17'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-15 18:01:50', '2020-07-15 18:01:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-16 08:32:17', '2020-07-16 08:32:17'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-16 17:12:11', '2020-07-16 17:12:11'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-17 09:25:52', '2020-07-17 09:25:52'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-17 13:18:24', '2020-07-17 13:18:24'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-17 18:47:44', '2020-07-17 18:47:44'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-20 11:17:54', '2020-07-20 11:17:54'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-21 07:27:41', '2020-07-21 07:27:41'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-22 06:47:21', '2020-07-22 06:47:21'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-22 11:00:03', '2020-07-22 11:00:03'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-22 13:12:42', '2020-07-22 13:12:42'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-22 18:37:56', '2020-07-22 18:37:56'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-23 08:47:38', '2020-07-23 08:47:38'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-23 10:57:46', '2020-07-23 10:57:46'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-24 07:58:10', '2020-07-24 07:58:10'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-24 16:20:24', '2020-07-24 16:20:24'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-27 10:13:11', '2020-07-27 10:13:11'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-27 16:11:51', '2020-07-27 16:11:51'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-27 18:01:34', '2020-07-27 18:01:34'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-28 08:52:10', '2020-07-28 08:52:10'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-28 11:34:15', '2020-07-28 11:34:15'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-28 14:14:06', '2020-07-28 14:14:06'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-29 13:57:18', '2020-07-29 13:57:18'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-30 08:07:27', '2020-07-30 08:07:27'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-07-30 15:20:05', '2020-07-30 15:20:05'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-04 13:41:07', '2020-08-04 13:41:07'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-05 07:58:57', '2020-08-05 07:58:57'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-05 18:46:54', '2020-08-05 18:46:54'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-06 09:22:35', '2020-08-06 09:22:35'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-07 09:16:27', '2020-08-07 09:16:27'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-07 13:01:05', '2020-08-07 13:01:05'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-07 13:04:42', '2020-08-07 13:04:42'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-10 11:49:57', '2020-08-10 11:49:57'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-11 09:48:27', '2020-08-11 09:48:27'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-11 15:27:12', '2020-08-11 15:27:12'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-11 18:06:57', '2020-08-11 18:06:57'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-12 09:10:33', '2020-08-12 09:10:33'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-12 10:29:30', '2020-08-12 10:29:30'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-12 17:50:24', '2020-08-12 17:50:24'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-13 11:17:28', '2020-08-13 11:17:28'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-17 09:16:25', '2020-08-17 09:16:25'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-08-17 09:47:54', '2020-08-17 09:47:54'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-01 11:42:37', '2020-09-01 11:42:37'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-02 08:03:30', '2020-09-02 08:03:30'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-02 13:14:50', '2020-09-02 13:14:50'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-03 08:40:26', '2020-09-03 08:40:26'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-04 07:48:40', '2020-09-04 07:48:40'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-07 11:05:01', '2020-09-07 11:05:01'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-09-07 16:36:24', '2020-09-07 16:36:24'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-08 08:33:55', '2020-09-08 08:33:55'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-09 13:53:25', '2020-09-09 13:53:25'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-10 09:34:04', '2020-09-10 09:34:04'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-10 12:17:07', '2020-09-10 12:17:07'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-14 10:12:02', '2020-09-14 10:12:02'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-09-14 13:03:14', '2020-09-14 13:03:14'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-14 13:03:20', '2020-09-14 13:03:20'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-15 09:25:12', '2020-09-15 09:25:12'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-15 13:05:26', '2020-09-15 13:05:26'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-16 09:33:21', '2020-09-16 09:33:21'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-16 14:49:40', '2020-09-16 14:49:40'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-16 18:17:42', '2020-09-16 18:17:42'),
('hdfusi', 'Login Failed', '127.0.0.1', '2020-09-17 08:46:56', '2020-09-17 08:46:56'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-17 08:47:04', '2020-09-17 08:47:04'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-17 13:46:53', '2020-09-17 13:46:53'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-18 08:11:42', '2020-09-18 08:11:42'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-21 08:15:23', '2020-09-21 08:15:23'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-21 12:13:41', '2020-09-21 12:13:41'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-22 08:29:13', '2020-09-22 08:29:13'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-23 07:39:56', '2020-09-23 07:39:56'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-23 11:10:34', '2020-09-23 11:10:34'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-24 13:03:51', '2020-09-24 13:03:51'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-25 08:48:04', '2020-09-25 08:48:04'),
('1\' OR 1=1 -- -', 'Login Failed', '127.0.0.1', '2020-09-25 09:09:58', '2020-09-25 09:09:58'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-25 10:15:16', '2020-09-25 10:15:16'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-28 08:03:05', '2020-09-28 08:03:05'),
('user.test', 'Login Successful', '127.0.0.1', '2020-09-28 08:06:39', '2020-09-28 08:06:39'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-28 08:12:19', '2020-09-28 08:12:19'),
('user.test', 'Login Failed', '127.0.0.1', '2020-09-28 08:14:03', '2020-09-28 08:14:03'),
('user.test', 'Login Failed', '127.0.0.1', '2020-09-28 08:14:13', '2020-09-28 08:14:13'),
('user.test', 'Login Failed', '127.0.0.1', '2020-09-28 08:15:15', '2020-09-28 08:15:15'),
('user.test', 'Login Successful', '127.0.0.1', '2020-09-28 08:15:39', '2020-09-28 08:15:39'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-28 10:20:38', '2020-09-28 10:20:38'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-28 15:58:11', '2020-09-28 15:58:11'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-09-28 16:00:41', '2020-09-28 16:00:41'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-09-29 09:07:58', '2020-09-29 09:07:58'),
('user.test', 'Login Failed', '127.0.0.1', '2020-09-29 09:43:19', '2020-09-29 09:43:19'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-09-29 09:43:28', '2020-09-29 09:43:28'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-10-01 10:29:49', '2020-10-01 10:29:49'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-01 10:42:17', '2020-10-01 10:42:17'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-10-02 13:23:47', '2020-10-02 13:23:47'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-02 13:49:19', '2020-10-02 13:49:19'),
('hdfusi', 'Login Successful', '127.0.0.1', '2020-10-05 12:02:29', '2020-10-05 12:02:29'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-05 16:09:26', '2020-10-05 16:09:26'),
('user.test1', 'Login Failed', '127.0.0.1', '2020-10-06 17:47:22', '2020-10-06 17:47:22'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-06 17:47:41', '2020-10-06 17:47:41'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-07 08:27:55', '2020-10-07 08:27:55'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-08 09:52:55', '2020-10-08 09:52:55'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-08 17:33:20', '2020-10-08 17:33:20'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-09 12:02:56', '2020-10-09 12:02:56'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-12 09:29:52', '2020-10-12 09:29:52'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-12 15:27:11', '2020-10-12 15:27:11'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-13 12:29:00', '2020-10-13 12:29:00'),
('user.test1', 'Login Successful', '127.0.0.1', '2020-10-14 08:20:17', '2020-10-14 08:20:17');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `Training_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Training_Purpose` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `Training_Activities` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `Training_Date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(30) NOT NULL,
  `lname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `fname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `mname` varchar(50) CHARACTER SET latin1 NOT NULL,
  `username` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(150) CHARACTER SET latin1 DEFAULT NULL,
  `position` varchar(50) CHARACTER SET latin1 NOT NULL,
  `office_staff` varchar(50) CHARACTER SET latin1 NOT NULL,
  `division_unit` varchar(50) CHARACTER SET latin1 NOT NULL,
  `email_address` varchar(50) CHARACTER SET latin1 NOT NULL,
  `gender` varchar(20) CHARACTER SET latin1 NOT NULL,
  `telephone_number` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Contract_Type` varchar(20) CHARACTER SET latin1 NOT NULL,
  `access_type` varchar(5) CHARACTER SET latin1 DEFAULT NULL,
  `account_creation_date` datetime NOT NULL,
  `account_status` int(2) NOT NULL,
  `account_lock` int(2) NOT NULL,
  `account_approval_date` datetime DEFAULT NULL,
  `updated_at` datetime NOT NULL,
  `created_at` datetime NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_first` int(11) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `lname`, `fname`, `mname`, `username`, `password`, `position`, `office_staff`, `division_unit`, `email_address`, `gender`, `telephone_number`, `Contract_Type`, `access_type`, `account_creation_date`, `account_status`, `account_lock`, `account_approval_date`, `updated_at`, `created_at`, `remember_token`, `is_first`) VALUES
(1, 'fusi', 'hazel', 'de las alas', 'hdfusi', '$2y$10$wQLMM5wtZGeKCbCDHCY4IuKkw4DGEDKWRTPdAURCgvHhYsECQ5IRG', 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '08348392', 'ALL', 'A', '2020-03-24 19:30:19', 1, 0, '2020-03-24 19:30:19', '2020-06-17 11:16:13', '0000-00-00 00:00:00', NULL, 0),
(2, 'user', 'test', 'tst', 'user.test', '$2y$10$97eDxj01IvK/7JBScTBh2uPdUfV5yedjT06H8Nme9HF7jcyrmkXtC', 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '3198012', 'CONS', 'V', '2020-06-11 12:56:43', 1, 0, '2020-07-08 07:46:38', '2020-07-08 07:46:38', '2020-06-11 12:56:43', NULL, 0),
(3, 'user', 'testreg', 'a', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '234231', 'CONS', NULL, '2020-07-07 15:09:55', 2, 0, NULL, '2020-07-14 17:38:31', '2020-07-07 15:09:55', NULL, 1),
(4, 'autotest', 'test', 'tst', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '2134232', 'GIP', NULL, '2020-07-10 12:27:57', 2, 0, NULL, '2020-07-14 17:40:46', '2020-07-10 12:27:57', NULL, 1),
(5, 'user', 'test', 'tst', 'user.test1', '$2y$10$zErF4dvQeG2ipOUXQVfmEuRZ5/kgU57IydJhvOHXoxe4q.2Wa1lmO', 'test', 'AS', 'PMD', 'hdfusi@neda.gov.ph', 'F', '2323132', 'CONS', 'A1', '2020-07-10 12:31:03', 1, 0, '2020-09-28 08:13:28', '2020-09-28 08:17:09', '2020-07-10 12:31:03', NULL, 0),
(6, 'user', 'test', 'tst', NULL, NULL, 'test', 'AS', 'PMD', 'hdfusi@neda.gov.ph', 'F', '2323132', 'CONS', NULL, '2020-07-10 12:32:42', 0, 0, NULL, '2020-07-10 12:32:42', '2020-07-10 12:32:42', NULL, 1),
(7, 'user', 'test', 'tst', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '2134232', 'CONS', NULL, '2020-07-10 12:58:14', 0, 0, NULL, '2020-07-10 12:58:14', '2020-07-10 12:58:14', NULL, 1),
(8, 'userautoemail', 'test', 'tst', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '2134232', 'CONS', NULL, '2020-07-10 13:06:54', 0, 0, NULL, '2020-07-10 13:06:55', '2020-07-10 13:06:55', NULL, 1),
(9, 'testautoemail', 'email', 'test', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '2134232', 'CONS', NULL, '2020-07-13 09:43:09', 0, 0, NULL, '2020-07-13 09:43:09', '2020-07-13 09:43:09', NULL, 1),
(10, 'testmail', 'test', 'mail', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '2134232', 'ALL', NULL, '2020-07-13 19:34:21', 0, 0, NULL, '2020-07-13 19:34:21', '2020-07-13 19:34:21', NULL, 1),
(11, 'testmail2', 'test', 'tst', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'F', '2134232', 'GIP', NULL, '2020-07-13 19:36:42', 0, 0, NULL, '2020-07-13 19:36:42', '2020-07-13 19:36:42', NULL, 1),
(12, 'testing3', 'test', 'tst', NULL, NULL, 'CP II', 'ICTS', 'ISDD', 'hdfusi@neda.gov.ph', 'M', '2134232', 'CONS', NULL, '2020-07-13 19:40:01', 0, 0, NULL, '2020-07-13 19:40:01', '2020-07-13 19:40:01', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `warranty`
--

CREATE TABLE `warranty` (
  `Warranty_ID` int(25) NOT NULL,
  `Contract_ID` varchar(20) CHARACTER SET latin1 NOT NULL,
  `Form` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Amount` double DEFAULT NULL,
  `Expiry_Date` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_type`
--
ALTER TABLE `access_type`
  ADD PRIMARY KEY (`access_ID`);

--
-- Indexes for table `amendment_history`
--
ALTER TABLE `amendment_history`
  ADD PRIMARY KEY (`amendment_history_id`),
  ADD KEY `Deliverable_ID` (`Deliverable_ID`);

--
-- Indexes for table `consultancy_firm`
--
ALTER TABLE `consultancy_firm`
  ADD PRIMARY KEY (`Firm_ID`);

--
-- Indexes for table `consultant_documents`
--
ALTER TABLE `consultant_documents`
  ADD PRIMARY KEY (`consDoc_ID`);

--
-- Indexes for table `consultant_experts`
--
ALTER TABLE `consultant_experts`
  ADD PRIMARY KEY (`expert_id`);

--
-- Indexes for table `consultant_procurements`
--
ALTER TABLE `consultant_procurements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`Contract_ID`);

--
-- Indexes for table `contract_experts`
--
ALTER TABLE `contract_experts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contract_remarks`
--
ALTER TABLE `contract_remarks`
  ADD PRIMARY KEY (`remarks_id`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `contract_type`
--
ALTER TABLE `contract_type`
  ADD PRIMARY KEY (`Contract_Type_ID`);

--
-- Indexes for table `correspondence`
--
ALTER TABLE `correspondence`
  ADD PRIMARY KEY (`Correspondence_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `deliverables`
--
ALTER TABLE `deliverables`
  ADD PRIMARY KEY (`Deliverable_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`Document_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `firm_partners`
--
ALTER TABLE `firm_partners`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `firm_projects`
--
ALTER TABLE `firm_projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Contract_ID` (`Contract_ID`),
  ADD KEY `Firm_ID` (`Firm_ID`);

--
-- Indexes for table `firm_role`
--
ALTER TABLE `firm_role`
  ADD PRIMARY KEY (`firm_role_id`);

--
-- Indexes for table `firm_type`
--
ALTER TABLE `firm_type`
  ADD PRIMARY KEY (`Firm_Type_ID`);

--
-- Indexes for table `liquidated_damages`
--
ALTER TABLE `liquidated_damages`
  ADD PRIMARY KEY (`LD_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `management_presentation`
--
ALTER TABLE `management_presentation`
  ADD PRIMARY KEY (`Presentation_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `meeting`
--
ALTER TABLE `meeting`
  ADD PRIMARY KEY (`Meeting_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nda`
--
ALTER TABLE `nda`
  ADD PRIMARY KEY (`NDA_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email_address`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`Payment_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`),
  ADD KEY `Deliverable_ID` (`Deliverable_ID`);

--
-- Indexes for table `performance_security`
--
ALTER TABLE `performance_security`
  ADD PRIMARY KEY (`Security_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `procurement_mode`
--
ALTER TABLE `procurement_mode`
  ADD PRIMARY KEY (`mode_id`);

--
-- Indexes for table `project_history`
--
ALTER TABLE `project_history`
  ADD PRIMARY KEY (`History_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `security_type`
--
ALTER TABLE `security_type`
  ADD PRIMARY KEY (`Security_Type_ID`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`Training_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `warranty`
--
ALTER TABLE `warranty`
  ADD PRIMARY KEY (`Warranty_ID`),
  ADD KEY `Contract_ID` (`Contract_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_type`
--
ALTER TABLE `access_type`
  MODIFY `access_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `amendment_history`
--
ALTER TABLE `amendment_history`
  MODIFY `amendment_history_id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `consultancy_firm`
--
ALTER TABLE `consultancy_firm`
  MODIFY `Firm_ID` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `consultant_documents`
--
ALTER TABLE `consultant_documents`
  MODIFY `consDoc_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `consultant_experts`
--
ALTER TABLE `consultant_experts`
  MODIFY `expert_id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `consultant_procurements`
--
ALTER TABLE `consultant_procurements`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `contract_experts`
--
ALTER TABLE `contract_experts`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contract_remarks`
--
ALTER TABLE `contract_remarks`
  MODIFY `remarks_id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `contract_type`
--
ALTER TABLE `contract_type`
  MODIFY `Contract_Type_ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `correspondence`
--
ALTER TABLE `correspondence`
  MODIFY `Correspondence_ID` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `deliverables`
--
ALTER TABLE `deliverables`
  MODIFY `Deliverable_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `Document_ID` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `firm_partners`
--
ALTER TABLE `firm_partners`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `firm_projects`
--
ALTER TABLE `firm_projects`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `firm_role`
--
ALTER TABLE `firm_role`
  MODIFY `firm_role_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `firm_type`
--
ALTER TABLE `firm_type`
  MODIFY `Firm_Type_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `liquidated_damages`
--
ALTER TABLE `liquidated_damages`
  MODIFY `LD_ID` int(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `management_presentation`
--
ALTER TABLE `management_presentation`
  MODIFY `Presentation_ID` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `meeting`
--
ALTER TABLE `meeting`
  MODIFY `Meeting_ID` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `nda`
--
ALTER TABLE `nda`
  MODIFY `NDA_ID` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `Payment_ID` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `performance_security`
--
ALTER TABLE `performance_security`
  MODIFY `Security_ID` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `procurement_mode`
--
ALTER TABLE `procurement_mode`
  MODIFY `mode_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `project_history`
--
ALTER TABLE `project_history`
  MODIFY `History_ID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `security_type`
--
ALTER TABLE `security_type`
  MODIFY `Security_Type_ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `Training_ID` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(30) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `warranty`
--
ALTER TABLE `warranty`
  MODIFY `Warranty_ID` int(25) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `amendment_history`
--
ALTER TABLE `amendment_history`
  ADD CONSTRAINT `amendment_history_ibfk_1` FOREIGN KEY (`Deliverable_ID`) REFERENCES `deliverables` (`Deliverable_ID`);

--
-- Constraints for table `contract_remarks`
--
ALTER TABLE `contract_remarks`
  ADD CONSTRAINT `contract_remarks_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `correspondence`
--
ALTER TABLE `correspondence`
  ADD CONSTRAINT `correspondence_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `deliverables`
--
ALTER TABLE `deliverables`
  ADD CONSTRAINT `deliverables_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `documents`
--
ALTER TABLE `documents`
  ADD CONSTRAINT `documents_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `firm_partners`
--
ALTER TABLE `firm_partners`
  ADD CONSTRAINT `firm_partners_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `firm_projects`
--
ALTER TABLE `firm_projects`
  ADD CONSTRAINT `firm_projects_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`),
  ADD CONSTRAINT `firm_projects_ibfk_2` FOREIGN KEY (`Firm_ID`) REFERENCES `consultancy_firm` (`Firm_ID`);

--
-- Constraints for table `liquidated_damages`
--
ALTER TABLE `liquidated_damages`
  ADD CONSTRAINT `liquidated_damages_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `management_presentation`
--
ALTER TABLE `management_presentation`
  ADD CONSTRAINT `management_presentation_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `meeting`
--
ALTER TABLE `meeting`
  ADD CONSTRAINT `meeting_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `nda`
--
ALTER TABLE `nda`
  ADD CONSTRAINT `nda_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`),
  ADD CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`Deliverable_ID`) REFERENCES `deliverables` (`Deliverable_ID`);

--
-- Constraints for table `performance_security`
--
ALTER TABLE `performance_security`
  ADD CONSTRAINT `performance_security_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `project_history`
--
ALTER TABLE `project_history`
  ADD CONSTRAINT `project_history_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);

--
-- Constraints for table `warranty`
--
ALTER TABLE `warranty`
  ADD CONSTRAINT `warranty_ibfk_1` FOREIGN KEY (`Contract_ID`) REFERENCES `contracts` (`Contract_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
