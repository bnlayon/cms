<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFundSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fund_sources', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fundSource',255);
            $table->timestamps();
        });

        DB::table('fund_sources')->insert(
            ['fundSource'=> 'HUMAN RESOURCE DEVELOPMENT FUND'],
            ['fundSource'=> 'INFRASTRUCTURE DEVELOPMENT PROJECTS FUND'],
            ['fundSource'=> 'INNOVATION FUND'],
            ['fundSource'=> 'MONITORING AND EVALUATION FUND'],
            ['fundSource'=> 'PROJECT DEVELOPMENT AND OTHER RELATED STUDIES'],
            ['fundSource'=> 'RESEARCH AND DEVELOPMENT FUND'],
            ['fundSource'=> 'SUB-COMMITTEE ON SUSTAINABLE DEVELOPMENT GOALS FUND'],
            ['fundSource'=> 'VALUE ENGINEERING/VALUE ANALYSIS FUND'],
            ['fundSource'=> 'COMMON POOL'],
            ['fundSource'=> 'STAFF ALLOCATION'],
            ['fundSource'=> 'MEDIUM-TERM INFORMATION AND COMMUNICATIONS TECHNOLOGY HARMONIZATION INITIATIVE (MITHI) FUND'],
            ['fundSource'=> 'NEDA INFORMATION NETWORK PROJECT (NINP) FUND']
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fund_sources');
    }
}
