<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Deliverables extends Model
{
    protected $primaryKey = 'Deliverable_ID';
}
