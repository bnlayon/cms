<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class performance_security extends Model
{
    public $table = "performance_security";
    protected $primaryKey = 'Security_ID';
}
