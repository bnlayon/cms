<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Mail;
use Auth;
use App\Jobs\EmailOnExpiringStatus;

class DailyUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:updateExpiring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Daily Check of Contract/Deliverables End Date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        dispatch(new EmailOnExpiringStatus);
    }
}
