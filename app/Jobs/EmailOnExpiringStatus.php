<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;
use Auth;

class EmailOnExpiringStatus implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $myDate = date("Y-m-d H:i:s", strtotime( date( "Y-m-d", strtotime( date("Y-m-d") ) ) . "+1 month" ) );
        $expiring = \App\Contracts::where('Contract_End_Date','<=',$myDate)
        ->where(function($query){
            $query->where('Contract_Status','!=','Closed')
            ->orWhere('Contract_Status','!=','Completed')
            ->orWhere('Contract_Status','!=','Cancelled');
        })->get();
        $deliverables = \App\Deliverables::get();
        $amendcount=0;
        $targetcount=0;
        foreach($deliverables as $deli){
            if(!is_null($deli->Amended_Date)){
                if($deli->Amended_Date<=$myDate){
                    $amendcount=$amendcount+1;
                }
            }else{
                if($deli->Target_Completion_Date <=$myDate){
                    $targetcount=$targetcount+1;
                }
            }
        }
        if($amendcount!=0){
            $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587
            $mail->Username = $_ENV['MAIL_USERNAME'];
            $mail->Password = $_ENV['MAIL_PASSWORD'];;
            $mail->IsHTML(true);

            $admin = \App\Users::where('access_type','=','A1')
            ->orWhere('access_type','=','A');

            ob_start(); //STARTS THE OUTPUT BUFFER
            include('ReminderEmail.blade.php'); //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
            $some_page_contents = ob_get_contents() ; //PUT THE CONTENTS INTO A VARIABLE
            ob_clean(); //CLEAN OUT THE OUTPUT BUFFER
            $mail->Body = $some_page_contents;
            $mail->SetFrom('contracts.neda@gmail.com');
            $mail->Subject = 'CMS NOTICE: Deliverables Near Deadline (Amended Date)';
            foreach($admin as $email){
            $mail->AddAddress($email);
            }
            $mail->Send();
        }
        if($targetcount!=0){
            $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587
            $mail->Username = $_ENV['MAIL_USERNAME'];
            $mail->Password = $_ENV['MAIL_PASSWORD'];;
            $mail->IsHTML(true);

            $admin = \App\Users::where('access_type','=','A1')
            ->orWhere('access_type','=','A');

            ob_start(); //STARTS THE OUTPUT BUFFER
            include('ReminderEmail1.blade.php'); //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
            $some_page_contents = ob_get_contents() ; //PUT THE CONTENTS INTO A VARIABLE
            ob_clean(); //CLEAN OUT THE OUTPUT BUFFER
            $mail->Body = $some_page_contents;
            $mail->SetFrom('contracts.neda@gmail.com');
            $mail->Subject = 'CMS NOTICE: Deliverables Near Deadline';
            foreach($admin as $email){
            $mail->AddAddress($email);
            }
            $mail->Send();
        }
            
        // dd($expiring);
        if($expiring){
            foreach($expiring as $data){
                $data->Contract_Status = 'Expiring';
                $data->save();
            }
            $expiring = \App\Contracts::where('Contract_End_Date','<=',$myDate)
            ->where(function($query){
                $query->where('Contract_Status','!=','Closed')
                ->orWhere('Contract_Status','!=','Completed')
                ->orWhere('Contract_Status','!=','Cancelled');
            })->get();
            $count=count($expiring);
            $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
            $mail->IsSMTP(); // enable SMTP
            $mail->SMTPAuth = true; // authentication enabled
            $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
            $mail->Host = "smtp.gmail.com";
            $mail->Port = 465; // or 587
            $mail->Username = $_ENV['MAIL_USERNAME'];
            $mail->Password = $_ENV['MAIL_PASSWORD'];;
            $mail->IsHTML(true);

            $admin = \App\Users::where('access_type','=','A1')
            ->orWhere('access_type','=','A');

            ob_start(); //STARTS THE OUTPUT BUFFER
            include('ReminderEmailExpiring.blade.php'); //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
            $some_page_contents = ob_get_contents() ; //PUT THE CONTENTS INTO A VARIABLE
            ob_clean(); //CLEAN OUT THE OUTPUT BUFFER
            $mail->Body = $some_page_contents;
            $mail->SetFrom('contracts.neda@gmail.com');
            $mail->Subject = 'CMS NOTICE: Expiring Contracts';
            foreach($admin as $email){
                $mail->AddAddress($email);
            }
            $mail->Send();
        }
    }
}
