<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class Users extends Model implements CanResetPassword,
\Illuminate\Contracts\Auth\Authenticatable
{
    use Notifiable;

    protected $primaryKey = 'user_id';
    
    protected $fillable = [
        'username',
        'password',
        'fname',
        'lname',
        'mname',
        'office_staff',
        'division_unit',
        'position',
        'email_address',
        'telephone_number',
        'gender',
        'access_type',
        'Contract_Type',
        'account_creation_date',
        'account_lock',
        'account_approval_date'
    ];

    public function getEmailForPasswordReset(){
        return $this->email_address;
    }
    
    public function sendPasswordResetNotification($token){
        $this->notify(new ResetPasswordNotification($token));
    }

    public function setRememberToken($value)
    {
        if (! empty($this->getRememberTokenName())) {
            $this->{$this->getRememberTokenName()} = $value;
        }
    }

    public function getRememberTokenName()
    {
        return $this->rememberTokenName;
    }

    public function getAuthIdentifierName()
    {
        return $this->getKeyName();
    }

    public function getAuthIdentifier()
    {
        return $this->{$this->getAuthIdentifierName()};
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

    public function getRememberToken()
    {
        if (! empty($this->getRememberTokenName())) {
            return (string) $this->{$this->getRememberTokenName()};
        }
    }
    public function getEmailAttribute() {

        return $this->attributes['email_address'];

    }

    public function setEmailAttribute($value) {

        $this->attributes['email_address'] = $value;

    }

}
