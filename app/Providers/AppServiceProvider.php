<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use App\Observers\UserObserver;
use App\Observers\ContractsObserver;
use Illuminate\Support\ServiceProvider;
use App\Users;
use App\Contracts;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Users::observe(UserObserver::class);
        Contracts::observe(ContractsObserver::class);

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
