<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PDF;
use App\Contracts;
use App\consultancy_firm;
use App\performance_security;
use App\Documents;
use App\Deliverables;
use DB;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Carbon\Carbon;
use Excel;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(Request $request,$Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $view = request('reportDataView');
        $data = Contracts::where('Contract_ID',$Contract_ID)->get();
        $yearmonth = strval(date('m/d/Y'));
        $perfSec = DB::table('performance_security')
        ->join('security_type','security_type.Type','=','performance_security.Type')
        ->where('Contract_ID',$Contract_ID)->get();
        $documents = Documents::where('Contract_ID',$Contract_ID)->get();
        $deliverables = Deliverables::where('Contract_ID',$Contract_ID)->get();
        $pmtStat = Deliverables::where('Contract_ID',$Contract_ID)->pluck('Payment_Status')->all();
        $consultant = DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.Contract_ID')
        ->join('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->join('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
        ->where('firm_projects.Contract_ID','=',$Contract_ID)->get(); 
        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','Paid')->count();
        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','Ongoing')->count();
        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$Contract_ID)->whereNull('Payment_Status')->count();

        return view('reports',compact('Contract_ID','data','yearmonth','view','consultant','perfSec','documents','deliverables','pmtStat','deliverablescountPAID','deliverablescountONGOING','deliverablescountNULL'));
    }

    public function dashboardview(){
        $contract = DB::table('contracts')->select('contracts.Contract_ID','contracts.Contract_Name','contracts.Contract_Cost','contracts.Contract_Status')
                    ->get();

        $firmname = DB::table('firm_projects')->select('firm_projects.Contract_ID','consultancy_firm.Firm_Name')
                    ->leftJoin('consultancy_firm','consultancy_firm.Firm_ID','=','firm_projects.Firm_ID')
                    ->get();

        $status = DB::table('contracts')
                    ->leftJoin('deliverables','deliverables.Contract_ID','=','contracts.Contract_ID')
                    ->select('contracts.Contract_ID','deliverables.Payment_Status')
                    ->distinct('contracts.Contract_ID')
                    ->orderBy('Contract_ID')
                    ->get();
        return view('reports-dashboard',compact('contract','firmname','status'));
    }

    public function view_report($Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $data = Contracts::where('Contract_ID',$Contract_ID)->get();
        $yearmonth = strval(date('m/d/Y'));

        $deliverables = \App\Deliverables::where('Contract_ID',$Contract_ID)->get();
        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','Paid')->count();
        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','Ongoing')->count();
        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$Contract_ID)->whereNull('Payment_Status')->count();
        
        return view('reports2',compact('data','yearmonth','Contract_ID','deliverables','deliverablescountPAID','deliverablescountONGOING','deliverablescountNULL'));
    }

    public function download(Request $request, $Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $view = request('reportDataView');
        $data = Contracts::where('Contract_ID',$Contract_ID)->get();
        $date = strval(date('m/d/Y'));
        $perfSec = DB::table('performance_security')
        ->join('security_type','security_type.Type','=','performance_security.Type')
        ->where('Contract_ID',$Contract_ID)->get();
        $documents = Documents::where('Contract_ID',$Contract_ID)->get();
        $deliverables = Deliverables::where('Contract_ID',$Contract_ID)->get();
        $pmtStat = Deliverables::where('Contract_ID',$Contract_ID)->pluck('Payment_Status')->all();
        $consultant = DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.Contract_ID')
        ->join('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->join('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
        ->where('firm_projects.Contract_ID','=',$Contract_ID)->get();
        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','Paid')->count();
        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','Ongoing')->count();
        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$Contract_ID)->whereNull('Payment_Status')->count();

        $pdf = PDF::loadView('reportspdf', compact('data','date','view','perfSec','documents','deliverables','pmtStat','consultant','deliverablescountPAID','deliverablescountONGOING','deliverablescountNULL'));
        return $pdf->stream();
    }

    public function downloadAll(Request $request){
        $view = request('reportDataView');
        // dd($view);
        $contract = DB::table('contracts')->get();
        $date = strval(date('m/d/Y'));
        $deliverable = DB::table('deliverables')->select('deliverables.Contract_ID','deliverables.Deliverable_ID','deliverables.Payment_Status')
                    ->join('contracts','contracts.Contract_ID','=','deliverables.Contract_ID')
                    ->get();

        $firmname=DB::table('firm_projects')->select('firm_projects.Contract_ID','consultancy_firm.Firm_Name')
                    ->leftJoin('consultancy_firm','consultancy_firm.Firm_ID','=','firm_projects.Firm_ID')
                    ->get();
        $consultant = DB::table('consultancy_firm')
                    ->select('consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.Contract_ID')
                    ->join('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
                    ->join('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
                    ->get();
        $perfSec = DB::table('performance_security')
                    ->join('security_type','security_type.Type','=','performance_security.Type')
                    ->get();
        $documents = Documents::get();
        $deliverables = Deliverables::get();

        // dd($contract);   
        $pdf = PDF::loadView('allreportspdf', compact('contract','deliverable','firmname','date','view','consultant','documents','deliverables','perfSec'))->setPaper('legal', 'landscape');
        return $pdf->stream();
    }

    public function downloadExcel(Request $request){
        $view = request('reportDataView1');
        $contract = DB::table('contracts')
        ->join('firm_projects','firm_projects.Contract_ID','=','contracts.Contract_ID')
        ->join('consultancy_firm','consultancy_firm.Firm_ID','=','firm_projects.Firm_ID')->get();
        $date = strval(date('mdY'));
        $file = 'Excel Report '.$date.'.xlsx';
        $firm=DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.Contract_ID')
        ->join('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->join('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')->get();

            $spreadsheet = new Spreadsheet();
            
            $spreadsheet->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('R')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('S')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('T')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('U')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('V')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getColumnDimension('W')->setAutoSize(true);
            $spreadsheet->getActiveSheet()->getStyle('A1:W1')->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setARGB('808080');

            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'CONTRACT ID');
            $sheet->setCellValue('B1', 'PROJECT TITLE');
            $sheet->setCellValue('C1', 'CONSULTANT/FIRM NAME');
            $sheet->setCellValue('D1', 'FIRM TYPE');
            $sheet->setCellValue('E1', 'END USER');
            $sheet->setCellValue('F1', 'IMPLEMENTING AGENCY');
            $sheet->setCellValue('G1', 'CO-IMPLEMENTING AGENCY');
            $sheet->setCellValue('H1', 'NOA DATE');
            $sheet->setCellValue('I1', 'NTP DATE');
            $sheet->setCellValue('J1', 'NTP CONFORME DATE');
            $sheet->setCellValue('K1', 'CONTRACT START DATE');
            $sheet->setCellValue('L1', 'CONTRACT END DATE');
            $sheet->setCellValue('M1', 'CONTRACT AMOUNT');
            $sheet->setCellValue('N1', 'ABC');
            $sheet->setCellValue('O1', 'FUND SOURCE');
            $sheet->setCellValue('P1', 'FUND YEAR');
            $sheet->setCellValue('Q1', 'PERFORMANCE SECURITY');
            $sheet->setCellValue('R1', 'INSURER OF PERFORMANCE SECURITY');
            $sheet->setCellValue('S1', 'SUPPORTING DOCUMENTS SUBMITTED');
            $sheet->setCellValue('T1', 'DELIVERABLES WITH STATUS');
            $sheet->setCellValue('U1', 'DELIVERABLES OVERALL PAYMENT STATUS');
            $sheet->setCellValue('V1', 'PROJECT STATUS');

            $styleArrayFirstRow = [
                'font' => [
                    'bold' => true,
                ],
            ];
    
            //Retrieve Highest Column (e.g AE)
            $highestColumn = $sheet->getHighestColumn();
            
            //set first row bold
            $sheet->getStyle('A1:' . $highestColumn . '1' )->applyFromArray($styleArrayFirstRow);

            $i=2;
            foreach ($contract as $row) { 
                $deliverables = \App\Deliverables::where('Contract_ID',$row -> Contract_ID)->get();
                $deli = \App\Deliverables::select('Deliverable','Payment_Status')->where('Contract_ID',$row -> Contract_ID)->get();
                $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$row -> Contract_ID)->where('Payment_Status','Paid')->count();
                $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$row -> Contract_ID)->where('Payment_Status','Ongoing')->count();
                $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$row -> Contract_ID)->whereNull('Payment_Status')->count();
                $documents = DB::table('documents')->where('Contract_ID',$row -> Contract_ID)->where('Document_Link','like','%supportingDocs%')->pluck('Document_Link');
                $performance = \App\performance_security::where('Contract_ID',$row -> Contract_ID)->get();
                $issuingCompany = \App\performance_security::where('Contract_ID',$row -> Contract_ID)->pluck('Issuing_Company');
                
                $sheet->setCellValue('A'.$i , $row->Contract_ID); 

                if(in_array('PrjName', $view)){
                    $sheet->setCellValue('B'.$i , $row->Contract_Name); 
                }
                if(in_array('ConsultancyFirm', $view)){
                    foreach($firm as $f){
                        if($f->Contract_ID == $row->Contract_ID ){
                            $sheet->setCellValue('C'.$i , $f->Firm_Name); 
                        }
                    }
                }
                if(in_array('FirmType', $view)){
                    foreach($firm as $f){
                        if($f->Contract_ID == $row->Contract_ID ){
                            $sheet->setCellValue('D'.$i , $f->Firm_Type_Desc);
                        }
                    } 
                }
                if(in_array('EU', $view)){
                    $sheet->setCellValue('E'.$i , $row->End_User); 
                }
                if(in_array('IAgency', $view)){
                    $sheet->setCellValue('F'.$i , $row->Implementing_Agency); 
                }
                if(in_array('CoIAgency', $view)){
                    $sheet->setCellValue('G'.$i , $row->Co_Implementing_Agency); 
                }
                if(in_array('NOADt', $view)){
                    $sheet->setCellValue('H'.$i , $row->NOA_Release_Date); 
                }
                if(in_array('NTPDt', $view)){
                    $sheet->setCellValue('I'.$i , $row->NTP_Release_Date); 
                }
                if(in_array('NTPConfDt', $view)){
                    $sheet->setCellValue('J'.$i , $row->NTP_Conforme_Date); 
                }
                if(in_array('CStartDt', $view)){
                    $sheet->setCellValue('K'.$i , $row->Contract_Start_Date); 
                }
                if(in_array('CEndDt', $view)){
                    $sheet->setCellValue('L'.$i , $row->Contract_End_Date); 
                }
                if(in_array('CCost', $view)){
                    $sheet->setCellValue('M'.$i , number_format($row->Contract_Cost,2)); 
                }
                if(in_array('CABC', $view)){
                    $sheet->setCellValue('N'.$i , number_format($row->ABC,2)); 
                }
                if(in_array('FundSource', $view)){
                    $sheet->setCellValue('O'.$i , $row->Fund_Source); 
                }
                if(in_array('FundYr', $view)){
                    $sheet->setCellValue('P'.$i , $row->Fund_Year); 
                }
                if(in_array('Performance', $view)){
                    foreach($performance as $perf){
                        $cell = $sheet->getCell('Q'.$i);
                        $val = $cell->getValue();
                        if($perf->Type =='C'){
                            $data = $val."\n Type:".$perf->Type." \n Official Receipt:".$perf->CashOR." \n Bond Validity:".$perf->CashBV." \n Notes:".$perf->CashBVNotes;
                        }else if($perf->Type =='CMC'){
                            $data = $val."\n Type:".$perf->Type." \n Check Number:".$perf->CheckNum." \n Official Receipt:".$perf->CheckOR." \n Bank:".$perf->CheckBank." \n Bond Validity:".$perf->CheckBV." \n Notes:".$perf->CheckBVNotes;
                        }else if($perf->Type =='BD'){
                            $data = $val."\n Type:".$perf->Type." \n Bank Name:".$perf->BankNameBD." \n Bank Draft:".$perf->DraftBD." \n Validity:".$perf->ValidityBD." \n Notes:".$perf->ValidityBDNotes;
                        }else if($perf->Type =='BG'){
                            $data = $val."\n Type:".$perf->Type." \n Bank Name:".$perf->BankNameBG." \n Bank Draft:".$perf->DraftBG." \n Validity:".$perf->ValidityBG." \n Notes:".$perf->ValidityBGNotes;
                        }else if($perf->Type =='ILC'){
                            $data = $val."\n Type:".$perf->Type." \n Bank Name:".$perf->BankNameILC." \n ILC:".$perf->ILC." \n Validity:".$perf->ValidityILC." \n Notes:".$perf->ValidityILCNotes;
                        }else if($perf->Type =='SB'){
                            $data = $val."\n Type:".$perf->Type." \n Company Name:".$perf->CNameSurety." \n Surety Num:".$perf->SuretyNum." \n Validity:".$perf->ValiditySurety." \n Notes:".$perf->ValiditySuretyNotes;
                        }else{

                        }
                        $sheet->setCellValue('Q'.$i , $data);  
                    }
                    $spreadsheet->getActiveSheet()->getStyle('Q'.$i)->getAlignment()->setWrapText(true);
                }
                if(in_array('IssuingCompany', $view)){
                    foreach($issuingCompany as $issuing){
                        $cell = $sheet->getCell('R'.$i);
                        $val = $cell->getValue();
                        $data = $val." \n ".$issuing;
                        $sheet->setCellValue('R'.$i , $data); 
                    }
                    $spreadsheet->getActiveSheet()->getStyle('R'.$i)->getAlignment()->setWrapText(true);
                }
                if(in_array('SuppDocs', $view)){
                    foreach($documents as $docs){
                        $cell = $sheet->getCell('S'.$i);
                        $val = $cell->getValue();
                        $data = $val." \n ".basename($docs);
                        $sheet->setCellValue('S'.$i , $data); 
                    }
                    $spreadsheet->getActiveSheet()->getStyle('S'.$i)->getAlignment()->setWrapText(true);
                }
                if(in_array('Deliverables', $view)){
                    foreach($deli as $del){
                        $cell = $sheet->getCell('T'.$i);
                        $val = $cell->getValue();
                        $data = $val." \n ".$del->Deliverable." - ".$del->Payment_Status;
                        $sheet->setCellValue('T'.$i , $data); 
                    }
                    $spreadsheet->getActiveSheet()->getStyle('T'.$i)->getAlignment()->setWrapText(true);
                }
                if(in_array('PmtStat', $view)){
                    $stopforongoing = 0;
                
                    foreach($deliverables as $deliverable){
                        if($deliverable->Payment_Status == 'Ongoing'){
                            $sheet->setCellValue('U'.$i ,$deliverable->Payment_Status );
                            
                            $stopforongoing = 1;                                      
                        }
                        if($stopforongoing == 1){
                            break;
                        }
                    }
                    if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID)){
                        $sheet->setCellValue('U'.$i ,'Paid');                    
                    }
                    if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID)){
                        $sheet->setCellValue('U'.$i ,'-');                       
                    }
                    if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID)){
                        $sheet->setCellValue('U'.$i ,'Ongoing'); 
                    }
                }
                if(in_array('PrjStat', $view)){
                    $sheet->setCellValue('V'.$i , $row->Contract_Status);
                }

                
                $i++;
            }

            $writer = new Xlsx($spreadsheet);
            $writer= \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment; filename="'.$file.'"');
            $writer->save("php://output");
    }

}
