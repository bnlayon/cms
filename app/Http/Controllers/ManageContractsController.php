<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Alert;
use Carbon\Carbon;
use App\Deliverables;
use Hashids\Hashids;

class ManageContractsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(){
        
        $contracts = DB::table('contracts')
                    ->select('Contract_ID','Contract_Name','Contract_Status','Contract_Termination_Date')
                    ->orderBy('Contract_ID')
                    ->get();
        
        $status = DB::table('contracts')
                    ->leftJoin('deliverables','deliverables.Contract_ID','=','contracts.Contract_ID')
                    ->select('contracts.Contract_ID','deliverables.Payment_Status')
                    ->distinct('contracts.Contract_ID')
                    ->orderBy('Contract_ID')
                    ->get();



        // $status1 = DB::table('contracts')
        //             ->leftJoin('deliverables','deliverables.Contract_ID','=','contracts.Contract_ID')
        //             ->select('contracts.Contract_ID','deliverables.Payment_Status')
        //             ->distinct('contracts.Contract_ID')
        //             ->orderBy('Contract_ID')
        //             ->get()->toArray();
        // $arr = [];
        // if(in_array($status1))
        // dd($status);  
        return view('manage-contracts',compact('contracts','status'));
    }

    public function updateTermDate(Request $request){
        $contractid = decrypt(request('contractid'));
        $notes = request('notes');
        $contractUpdate = \App\Contracts::find($contractid);
        $contractUpdate->Contract_Termination_Date = request('termDate');
        $contractUpdate->save();

        $history = new \App\project_history();
        $history->Contract_ID = $contractid;
        $history->History_Description = "Contract ".$contractid." Termination Date Updated <br>Notes:".$notes;
        $history->History_Date = Carbon::now();
        $history->save();

        Alert::success('Termination Date Updated', '');

        return Redirect::back();

    }

    public function projectUpdate(Request $request){
        $contractid = decrypt(request('contractid'));;
        $contractupdate = \App\Contracts::find($contractid);
        $contractupdate->Contract_Status = request('prjstat');
        if(request('prjstat')=='Completed'){
            $contractupdate->Contract_Completion_Date = Carbon::now();
        }
        $contractupdate->save(); 
        
        $remarks = new \App\contract_remarks();
        $remarks->Contract_ID = $contractid;
        $remarks->remarks_title = "Project Status Update";
        $remarks->notes = request('notes');
        $remarks->date_reviewed = request('reviewDate');
        $remarks->save();

        if($request->hasFile('CFA')){
            $filesSubmitted = $request->file('CFA');
            $fileWithExt = $filesSubmitted->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $request->file('CFA')->getClientOriginalExtension();
            $filename = $file.".".$ext;
            $path = $request->file('CFA')->StoreAs('public/storage/CFA',$filename);
            $documents = new \App\Documents();
            $documents->Contract_ID = $contractid;
            $documents->Document_Link = $path;
            $documents->save();
        }

        $history = new \App\project_history();
        $history->Contract_ID = $contractid;
        $history->History_Description = "Contract ".$contractid." Project Status Updated <br>Notes:".request('notes');
        $history->History_Date = Carbon::now();
        $history->save();

        Alert::success('Project Status Updated', '');

        return Redirect::back();
    }

    public function paymentSummary($Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $contractName = \App\Contracts::where('Contract_ID',$Contract_ID)->pluck('Contract_Name')->first();
        $deliverables = \App\Deliverables::where('Contract_ID',$Contract_ID)->get();
        $payments = \App\Payment::where('Contract_ID',$Contract_ID)->get();
        return view('alldelipmtView',compact('Contract_ID','contractName','payments','deliverables'));
    }

}
