<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		p{
			font-family: Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<body>

<p><b>Department:</b> <?php echo $user->office_staff.'-'.$user->division_unit; ?></p>

<p><b>User Account Request by:</b> <?php echo $user->lname.", ".$user->fname." ".$user->mname; ?></p>

<p>Dear Sir/Ma'am: </p>

<p>This is to inform you that you were unable to successfully accomplish the Contract Management System Online Sign-up Sheet due to the following reason/s:</p>

<ul>
	<li><p><?php echo $reason; ?></p></li>
</ul>                                                                     

<p>Kindly address the above-mentioned deficiency as soon as possible register again in the CMS Online Registration Page</p>

<p>For any inquiries, please do not hesitate to coordinate with the CMS Administrator through e-mail address: contracts.neda@gmail.com. </p>

<p>Thank you. </p>

<p><b>CMS Administrator</p>

</body>
</html>