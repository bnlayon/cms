<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Alert;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;

class ViewContractController extends Controller
{
    // checking login of user
    public function __construct()
    {
        $this->middleware('auth');
    }
    // initial view of contract data
    public function view($Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $contract=\App\Contracts::where('Contract_ID',$Contract_ID)->get();
        $deliverables=\App\Deliverables::where('Contract_ID',$Contract_ID)->get();
        $payments = \App\Payment::where('Contract_ID',$Contract_ID)->get();
        $pmtStatus=\App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','=','Paid')->pluck('Payment_Status')->toArray();
        $document=\App\Documents::where('Contract_ID',$Contract_ID)->pluck('Document_Link');
        $firm=DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_ID','consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.firm_role','firm_role.firm_role_desc')
        ->leftJoin('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->leftJoin('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
        ->leftJoin('firm_role','firm_role.firm_role','firm_projects.firm_role')
        ->where('firm_projects.Contract_ID','=',$Contract_ID)->distinct()->get();
        $type = DB::table('firm_projects')
        ->join('firm_type','firm_type.Firm_Type','=','firm_projects.Firm_Type')
        ->where('firm_projects.Contract_ID',$Contract_ID)
        ->pluck('firm_type.Firm_Type_Desc')->first();
        $perf=DB::table('performance_security')
        ->join('security_type','security_type.Type','performance_security.Type')
        ->where('Contract_ID',$Contract_ID)->get();
        $perfIssuing = DB::table('performance_security')
        ->join('security_type','security_type.Type','=','performance_security.Type')
        ->where('performance_security.Contract_ID',$Contract_ID)->pluck("Issuing_Company")->first();
        $experts=\App\contract_experts::where('Contract_ID',$Contract_ID)->get();
        $firmtype = DB::table('firm_type')->select('Firm_Type','Firm_Type_Desc')->get();
        $firmname = DB::table('consultancy_firm')->select('Firm_ID','Firm_Name')->orderBy('Firm_Name')->get();
        $firmRole = DB::table('firm_role')->select('firm_role','firm_role_desc')->get();
        $proc = DB::table('procurement_mode')
        ->join('contracts','contracts.mode_code','=','procurement_mode.mode_code')
        ->where('contracts.Contract_ID',$Contract_ID)
        ->pluck('procurement_mode.mode_desc')->first();
        // dd($firm);  
        return view('view-contract',compact('perfIssuing','payments','Contract_ID','proc','type','contract','deliverables','document','firm','perf','pmtStatus','experts','firmname','firmtype','firmRole'));
    }
    // edit view of contract
    public function editView($Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $name = \App\Contracts::where('Contract_ID',$Contract_ID)->pluck('Contract_Name')->first();
        $contract=\App\Contracts::where('Contract_ID',$Contract_ID)->get();
        $contractType = DB::table('contracts')->join('contract_type','contract_type.Contract_Type','contracts.Contract_Type')
        ->where('contracts.Contract_ID','=',$Contract_ID)->pluck('contract_type.Contract_Desc')->first();
        $deliverables=\App\Deliverables::where('Contract_ID',$Contract_ID)->get();
        $pmtStatus=\App\Deliverables::where('Contract_ID',$Contract_ID)->where('Payment_Status','=','Paid')->pluck('Payment_Status')->toArray();
        $document=\App\Documents::where('Contract_ID',$Contract_ID)->pluck('Document_Link');
        $firm=DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_ID','consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.firm_role','firm_role.firm_role_desc')
        ->leftJoin('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->leftJoin('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
        ->leftJoin('firm_role','firm_role.firm_role','firm_projects.firm_role')
        ->where('firm_projects.Contract_ID','=',$Contract_ID)->distinct()->get();
        // dd($firm);
        $firm2=DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_ID','consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.firm_role','firm_role.firm_role_desc')
        ->leftJoin('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->leftJoin('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
        ->leftJoin('firm_role','firm_role.firm_role','firm_projects.firm_role')
        ->where('firm_projects.Contract_ID','=',$Contract_ID)->distinct()->get();
        // dd($firm2);
        $type = DB::table('firm_type')
        ->select('firm_type.Firm_Type_Desc')
        ->join('firm_projects','firm_projects.Firm_Type','=','firm_type.Firm_Type')
        ->where('firm_projects.Contract_ID',$Contract_ID)->pluck('firm_type.Firm_Type_Desc')->first();
        $typecode = DB::table('firm_type')
        ->select('firm_type.Firm_Type')
        ->join('firm_projects','firm_projects.Firm_Type','=','firm_type.Firm_Type')
        ->where('firm_projects.Contract_ID',$Contract_ID)->pluck('firm_type.Firm_Type')->first();
        $perf=DB::table('performance_security')
        ->join('security_type','security_type.Type','=','performance_security.Type')
        ->where('performance_security.Contract_ID',$Contract_ID)->get();
        $perfIssuing = DB::table('performance_security')
        ->join('security_type','security_type.Type','=','performance_security.Type')
        ->where('performance_security.Contract_ID',$Contract_ID)->pluck("Issuing_Company")->first();
        $experts=\App\contract_experts::where('Contract_ID',$Contract_ID)->get();
        $firmtype = DB::table('firm_type')->select('Firm_Type','Firm_Type_Desc')->get();
        $firmname = DB::table('consultancy_firm')->select('Firm_ID','Firm_Name')->orderBy('Firm_Name')->get();
        $firmRole = DB::table('firm_role')->select('firm_role','firm_role_desc')->get();
        $secType = DB::table('security_type')->select('Type','Type_Description')->get();
        $procmode = DB::table('procurement_mode')->select('mode_code','mode_desc')->get();
        $proc = DB::table('procurement_mode')->select('contracts.Contract_ID','procurement_mode.mode_code','procurement_mode.mode_desc')
        ->join('contracts','contracts.mode_code','=','procurement_mode.mode_code')
        ->where('contracts.Contract_ID',$Contract_ID)->get();
        // dd($proc);  
        return view('edit-contract',compact('perfIssuing','Contract_ID','typecode','name','firm2','contractType','type','proc','procmode','secType','contract','deliverables','document','firm','perf','pmtStatus','experts','firmname','firmtype','firmRole'));
    }
    // updating of contract details
    public function editUpdateContractDtls(Request $request){
        
        $id = decrypt(request('cid'));
        $existType = request('typecode');
        $existID = request('existID');
        $contractdata = \App\Contracts::find($id);
        $firm=DB::table('consultancy_firm')
        ->select('consultancy_firm.Firm_ID','consultancy_firm.Firm_Name','firm_type.Firm_Type_Desc','firm_projects.firm_role','firm_role.firm_role_desc')
        ->join('firm_projects','firm_projects.Firm_ID','=','consultancy_firm.Firm_ID')
        ->join('firm_type','firm_type.Firm_Type','firm_projects.Firm_Type')
        ->join('firm_role','firm_role.firm_role','firm_projects.firm_role')
        ->where('firm_projects.Contract_ID','=',$id)->get();
        $firmnamedata = request('firmName');
        // dd($firmnamedata);
        $firmdataNew = request('firmNameNew'); 
        // dd($firmdataNew);
        $firmrole = request('firmrole');
        $ftype = request('editfirmType');
        
        $editContractCost = $request['editcontractCost'];
        $editContractCost = str_replace(',', '', $editContractCost);
        $editABC = $request['editABC'];
        $editABC = str_replace(',', '', $editABC);
        // dd($firm);
        if(!is_null($contractdata)){
            $contractdata->Contract_Name = request('editcontractName');
            $contractdata->Contract_Cost = $editContractCost;
            $contractdata->End_User = request('endUser');
            if(!is_null(request('udProcMode'))){
                $contractdata->mode_code = request('udProcMode');
            }
            $contractdata->Implementing_Agency = request('implementingAgency');
            $contractdata->Co_Implementing_Agency = request('coimplementingAgency');
            if(!is_null(request('NOA'))){
                $contractdata->NOA_Release_Date = request('NOA');
            }
            if(!is_null(request('NTP'))){
                $contractdata->NTP_Release_Date = request('NOA');
            }
            $contractdata->Fund_Source = request('fundSource'); 
            $contractdata->Fund_Year = request('fundYear'); 
            $contractdata->ABC = $editABC; 
            if(!is_null(request('NOAConforme'))){
                $contractdata->NOA_Conforme_Date = request('NOAConforme');
            }
            if(!is_null(request('ntpconformeDate'))){
                $contractdata->NTP_Conforme_Date = request('ntpconformeDate');
            }
            if(!is_null(request('contractStartDate'))){
                $contractdata->Contract_Start_Date = request('contractStartDate');
            }
            if(!is_null(request('contractEndDate'))){
                $contractdata->Contract_End_Date = request('contractEndDate');
            }
            if(!is_null(request('completionDate'))){
                $contractdata->Contract_Completion_Date = request('completionDate');
            }
            if(!is_null(request('terminationDate'))){
                $contractdata->Contract_Termination_Date = request('terminationDate');
            }
            $contractdata->save();

                if($existType == "S" && !is_null(request('firmNameNew'))){
                    $projectsNew = new \App\firm_projects();
                    $projectsNew->Firm_ID = request('firmNameNew');
                    $projectsNew->Contract_ID = $id;
                    $projectsNew->Firm_Type = $existType;
                    $projectsNew->save();
                    // dd($projectsNew);
                }else{
                    $firmname=request('PartnerFirmName');
                    $firmrole=request('PartnerFirmRole');
                    $partnername = request('PartnerFirmName');
                    $partnerrole = request('PartnerFirmRole');
                        // dd($partnerrole);
                    if(!empty(request('PartnerFirmName'))){
                        $count_ids = count(request('PartnerFirmName'));
                
                        for($i = 0; $i < $count_ids; $i++){
                            $project_firm = new \App\firm_projects();
                            $project_firm->Firm_ID             = $partnername[$i];
                            $project_firm->Contract_ID         = $id;
                            $project_firm->Firm_Type           = $existType;
                            $project_firm->firm_role           = $partnerrole[$i];
                            $project_firm->save();

                            if($project_firm->firm_role == 'L'){
                                $project_firm2                      = new \App\firm_partners();
                                $project_firm2->Contract_ID         = $id;
                                $project_firm2->Firm_Lead_ID        = $partnername[$i];
                                $project_firm2->save();
                            }else{
                                $project_firm2                      = new \App\firm_partners();
                                $project_firm2->Contract_ID         = $id;
                                $project_firm2->Firm_Partner_ID     = $partnername[$i];
                                $project_firm2->save();
                            }
                        } 
                    }
                }
            //     }
            // }

            if($request->hasFile('docs')){
                $filesSubmitted = $request->file('docs');
                foreach($filesSubmitted as $files){
                    $docs = new \App\Documents();
                    $fileWithExt = $files->getClientOriginalName();
                    $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
                    $ext = $files->getClientOriginalExtension();
                    $filename = $file.".".$ext;
                    $path = $files->StoreAs('public/storage/supportingDocs',$filename);
                    $docs->Contract_ID = $id;
                    $docs->Document_Link = $path;
                    $docs->save();
                }
            }
            
            $history = new \App\project_history();
            $history->Contract_ID = $id;
            $history->History_Description = "Contract".$id." Details Updated";
            $history->History_Date = Carbon::now();
            $history->save();
            Alert::success('Contract Details Updated', '');
            return Redirect::back()->with('conSuccess','Contract Details Updated');

        }else{
            Alert::error('Contract not Updated', 'Contract data not found');
        }
 
    }

    public function addPerfSecDtls(Request $request){
        $id = decrypt(request('cid'));
        $perfSecadd = request('editperfSecVal');
        
        foreach($perfSecadd as $perf){
            $PerfSecInsert = new \App\performance_security();
            $PerfSecInsert->Contract_ID = $id;
            $PerfSecInsert->Issuing_Company = request('issuingCompany');
            if($perf == 'C'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->CashOR = request('cornumber');
                $PerfSecInsert->CashBV = request('cvalidityperiod');
                $PerfSecInsert->CashBVNotes = request('cvalinotes');
                $PerfSecInsert->save();
            }elseif($perf == 'CMC'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->CheckNum = request('checknumber');
                $PerfSecInsert->CheckOR = request('cmcornumber');
                $PerfSecInsert->CheckBank = request('cmcbankname');
                $PerfSecInsert->CheckBV = request('cmcvalperiod');
                $PerfSecInsert->CheckBVNotes = request('cmcvalinotes');
                $PerfSecInsert->save();
            }elseif($perf == 'BD'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->BankNameBD = request('bdbankname');
                $PerfSecInsert->DraftBD = request('bdbankdraft');
                $PerfSecInsert->ValidityBD = request('bdvalperiod');
                $PerfSecInsert->ValidityBDNotes = request('bdvalnotes');
                $PerfSecInsert->save();
            }elseif($perf == 'BG'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->BankNameBG = request('bgbankname');
                $PerfSecInsert->DraftBG = request('bgbankguarantee');
                $PerfSecInsert->ValidityBG = request('bgvalperiod');
                $PerfSecInsert->ValidityBGNotes = request('bgvalnotes');
                $PerfSecInsert->save();
            }elseif($perf == 'ILC'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->BankNameILC = request('ilcbankname');
                $PerfSecInsert->ILC = request('loc');
                $PerfSecInsert->ValidityILC = request('ilcvalperiod');
                $PerfSecInsert->ValidityILCNotes = request('ilcvalnotes');
                $PerfSecInsert->save();
            }elseif($perf == 'SB'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->CNameSurety = request('sbcompanyname');
                $PerfSecInsert->SuretyNum = request('sbnumber');
                $PerfSecInsert->ValiditySurety = request('sbvalperiod');
                $PerfSecInsert->ValiditySuretyNotes = request('sbvalnotes');
                $PerfSecInsert->save();
            }elseif($perf == 'NA'){
                $PerfSecInsert->Type = $perf;
                $PerfSecInsert->save();
            }
        }
        $history = new \App\project_history();
        $history->Contract_ID = $id;
        $history->History_Description = "Contract".$id." Performance Security Details Added";
        $history->History_Date = Carbon::now();
        $history->save();
        Alert::success('Performance Security Created', '');
        return Redirect::back()->with('perfsecSuccess','Performance Security Created');
    }

    public function updatePerfSec(Request $request){
        $id = decrypt(request('cid'));
        $pid = decrypt(request('pid'));
        $type1 = request('pftype1');
        $type2 = request('pftype2');
        $type3 = request('pftype3');
        $type4 = request('pftype4');
        $type5 = request('pftype5');
        $type6 = request('pftype6');
        $type7 = request('pftype7');

        $perfsec = \App\performance_security::where(['Security_ID'=> $pid],['Contract_ID'=>$id])->get();
        // dd($perfsec);
        if(!is_null($perfsec)){
            foreach($perfsec as $perf){
                // dd($id);
                // dd($perf->Contract_ID);
                if($perf->Contract_ID == $id){
                    $perf->Issuing_Company = request('issuingCompany');
                    if($type1 == $perf->Type){
                        $perf->Type = $type1;
                        $perf->CashOR = request('cornumber');
                        $perf->CashBV = request('cvalidityperiod');
                        $perf->CashBVNotes = request('cvalinotes');
                        $perf->save();
                    }elseif($type2 == $perf->Type){
                        $perf->Type = $type2;
                        $perf->CheckNum = request('checknumber');
                        $perf->CheckOR = request('cmcornumber');
                        $perf->CheckBank = request('cmcbankname');
                        $perf->CheckBV = request('cmcvalperiod');
                        $perf->CheckBVNotes = request('cmcvalinotes');
                        $perf->save();
                    }elseif($type3 == $perf->Type){
                        $perf->Type = $type3;
                        $perf->BankNameBD = request('bdbankname');
                        $perf->DraftBD = request('bdbankdraft');
                        $perf->ValidityBD = request('bdvalperiod');
                        $perf->ValidityBDNotes = request('bdvalnotes');
                        $perf->save();
                    }elseif($type4 == $perf->Type){
                        $perf->Type = $type4;
                        $perf->BankNameBG = request('bgbankname');
                        $perf->DraftBG = request('bgbankguarantee');
                        $perf->ValidityBG = request('bgvalperiod');
                        $perf->ValidityBGNotes = request('bgvalnotes');
                        $perf->save();
                    }elseif($type5 == $perf->Type){
                        $perf->Type = $type5;
                        $perf->BankNameILC = request('ilcbankname');
                        $perf->ILC = request('loc');
                        $perf->ValidityILC = request('ilcvalperiod');
                        $perf->ValidityILCNotes = request('ilcvalnotes');
                        $perf->save();
                    }elseif($type6 == $perf->Type){
                        $perf->Type = $type6;
                        $perf->CNameSurety = request('sbcompanyname');
                        $perf->SuretyNum = request('sbnumber');
                        $perf->ValiditySurety = request('sbvalperiod');
                        $perf->ValiditySuretyNotes = request('sbvalnotes');
                        $perf->save();
                    }elseif($type7 == $perf->Type){
                        $perf->Type = $type7;
                        $perf->save();
                    }
                }else{
                    Alert::error('Contract ID mismatch', '');
                }
                $history = new \App\project_history();
                $history->Contract_ID = $id;
                $history->History_Description = "Contract".$id." Performance Security Details Updated";
                $history->History_Date = Carbon::now();
                $history->save();
                Alert::success('Performance Security Updated', '');
                return Redirect::back()->with('perfsecSuccess','Performance Security Updated');
            }
        }else {
            Alert::error('Contract not Updated', 'Performance Security data not found');
            return Redirect::back()->with('perfsecerror','Performance Security Updated');
        }
    }

    public function addDeliverables(Request $request){
        $id = decrypt(request('cid'));
        $del = request('editdeliverables');
        $date = request('editdeliverablesTD');
        $notes = request('editdeliverablesTDNotes');
        if(!is_null($del)){

            foreach($del as $index => $value){
                $deliverables = new \App\Deliverables();
                $deliverables->Contract_ID = $id;
                $deliverables->Deliverable = $del[$index];
                $deliverables->Target_Completion_Date = $date[$index];
                $deliverables->TCD_Notes = $notes[$index];
                $deliverables->save();
            }

            $history = new \App\project_history();
            $history->Contract_ID = $id;
            $history->History_Description = "New Deliverable added on ".$id.". Contract Details Updated";
            $history->History_Date = Carbon::now();
            $history->save();
            Alert::success('Deliverables Updated', '');
            return Redirect::back()->with('delSuccess','Deliverables Updated');
        }else{
            Alert::error('Contract not Updated', 'Deliverables data not found');
            return Redirect::back()->with('delerror','Deliverables data not found');
        }
    }

    public function updateDeliverable(Request $request){
        $did = request('dlvrbleid');
        $id = decrypt(request('cid'));
        $del = request('editdeliverables');
        $date = request('editdeliverablesTD');
        $notes = request('editdeliverablesTDNotes');
        $deliverables = \App\Deliverables::where('Contract_ID',$id)->get();
        // dd($deliverables);

        if(!is_null($deliverables)){

            foreach($deliverables as $existing => $exist){
                if($exist->Deliverable_ID == $did[$existing]){
                    $exist->Contract_ID = $id;
                    $exist->Deliverable = $del[$existing];
                    $exist->Target_Completion_Date = $date[$existing];
                    $exist->TCD_Notes = $notes[$existing];
                    $exist->save();
                }
            }

            $history = new \App\project_history();
            $history->Contract_ID = $id;
            $history->History_Description = "Updated Deliverables of Contract ".$id.". Contract Details Updated";
            $history->History_Date = Carbon::now();
            $history->save();
            Alert::success('Deliverables Updated', '');
            return Redirect::back()->with('delSuccess','Deliverables Updated');
        }else{
            Alert::error('Contract not Updated', 'Deliverables data not found');
            return Redirect::back()->with('delerror','Deliverables data not found');
        }
    }

    public function addContractExp(Request $request){
        $id = decrypt(request('cid'));
        $dataRetrieved = request('expname');
        $exprole = request('exprole');
        $firm_id = decrypt(request('firmnameedit'));
        if(is_null($dataRetrieved) && is_null($exprole)){
            Alert::error('Contract Experts not updated', '');
            return Redirect::back()->with('expError','Contract Experts not updated');
        }else{
            foreach($dataRetrieved as $index => $value){
                $experts = new \App\contract_experts();
                $experts->Contract_ID = $id;
                if(!is_null($firm_id)){
                    $experts->Firm_ID = $firm_id;
                }else{
                    $experts->Firm_ID = '0';
                }
                $experts->expert_name = $dataRetrieved[$index];
                $experts->expert_role = $exprole[$index];
                $experts->save();
            }
            $history = new \App\project_history();
            $history->Contract_ID = $id;
            $history->History_Description = "Experts of Contract ".$id." Added. Contract Details Updated";
            $history->History_Date = Carbon::now();
            $history->save();
            Alert::success('Contract Experts Added', '');
            return Redirect::back()->with('expSuccess','Contract Experts Added');
            
        }
    }

    public function updateContractExp(Request $request){

        $this->validate($request,[
            'exprole' => 'required'
        ],[
            'exprole.required' => ':attribute is required'
        ]);
        
        $id = decrypt(request('cid'));
        $expid = request('expid');
        $firm_id = decrypt(request('firmnameedit'));
        $dataRetrieved = request('expname');
        $exprole = request('exprole');
        $experts = \App\contract_experts::where('Contract_ID',$id)->get();

        if(!is_null($experts)){
            foreach($experts as $index => $value){
                if($value->id == $expid[$index]){
                    $value->Contract_ID = $id;
                    $value->Firm_ID = $firm_id;
                    $value->expert_name = $dataRetrieved[$index];
                    $value->expert_role = $exprole[$index];
                    $value->save();
                }
            }
            $history = new \App\project_history();
            $history->Contract_ID = $id;
            $history->History_Description = "Experts of Contract ".$id." Updated. Contract Details Updated";
            $history->History_Date = Carbon::now();
            $history->save();
            Alert::success('Contract Experts Updated', '');
            return Redirect::back()->with('expSuccess','Contract Experts Updated');
        }else{
            Alert::error('Contract Experts data not found', '');
            return Redirect::back()->with('experror','Contract Experts data not found');
        }
    }

    public function viewStatus($Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $name = \App\Contracts::where('Contract_ID',$Contract_ID)->pluck('Contract_Name')->first();
        $status = \App\Contracts::where('Contract_ID','=',$Contract_ID)->pluck('Contract_Status')->first();
        // dd($status);
        $deliverableStat = \App\Deliverables::where('Contract_ID',$Contract_ID)
                            ->distinct('Payment_Status')
                            ->pluck('Payment_Status')->toArray();
        // dd($deliverableStat);
        if(is_null($deliverableStat)){
            $deliStat = "No Status";
        }else {
            if(empty($deliverableStat)){
                $deliStat = "No Status";
            }else{
                if(in_array('Ongoing',$deliverableStat)){
                    $deliStat = "Ongoing";
                }else{
                    $deliStat = "Paid";
                }
            }
        }

        $getcfa = \App\Documents::where('Document_Link','like','%CFA%')
                        ->where('Contract_ID',$Contract_ID)
                        ->pluck('Document_Link');
        $remarks = \App\contract_remarks::where('Contract_ID',$Contract_ID)->get();
        // dd($remarks);
        return view('projectStatView',compact('name','Contract_ID','status','deliStat','getcfa','remarks'));
    }
}