<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		p{
			font-family: Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<body>

<p><b>Department:</b> <?php echo $user->department; ?></p>


<p>Dear Sir/Ma'am: </p>

<p>As the CMS Administrator, this is to provide your user account information to access the Contract Management System. </p>

<p><b>Username:</b> <?php echo $generatedusername; ?></p>

<p><b>Password:</b> <?php echo $generatedpassword; ?></p>

<p>Kindly note as well the following reminders: </p>
<p>A) Each user account cannot be used/accessed simultaneously by more than one user; </p>
<p>B) The Contract Management System generates <b>system logs</b>, which contain electronic record of transactions/interactions of each user account in the system for monitoring and accountability purposes; and</p>
<p>C) User names and passwords are <b>case sensitive</b>.</p>

<p>Thank you. </p>

<p><b>CMS Administrator</p>

</body>
</html>