<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Session;
use Carbon\Carbon;
use Alert;
use Illuminate\Validation\Rule;

class ConsultancyFirmsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function view (){
        $firms = \App\consultancy_firm::get();
        $docs1 = \App\consultant_documents::where('Document_Link','like','%PHILGEPS%')
                        ->orderBy('created_at', 'DESC')
                        ->limit(1)
                        ->get();
                        // dd($docs1);
        $docs2 = \App\consultant_documents::where('Document_Link','like','%SEC_DTI%')
                        ->orderBy('created_at', 'DESC')
                        ->limit(1)
                        ->get();
        $docs3 = \App\consultant_documents::where('Document_Link','like','%BusinessPermit%')
                        ->orderBy('created_at', 'DESC')
                        ->limit(1)
                        ->get();
        $docs4 = \App\consultant_documents::where('Document_Link','like','%TaxClearance%')
                        ->orderBy('created_at', 'DESC')
                        ->limit(1)
                        ->get();
        $findocs = \App\consultant_documents::where('Document_Link','like','%FinancialDocs%')
                        ->orderBy('created_at', 'DESC')
                        ->limit(2)
                        ->get();
        $procsjoined = \App\consultant_procurements::get();
        $experts = \App\consultant_experts::get();
            
        return view('consultancyfirms',compact('firms','docs1','docs2','docs3','docs4','findocs','procsjoined','experts'));
    }

    public function addFirmView(){
        return view('addFirm');
    }

    public function updateFirmView($id){
        $id = decrypt($id);
        $firmname = \App\consultancy_firm::where('Firm_ID','=',$id)->pluck('Firm_Name')->first();
        $firm = \App\consultancy_firm::where('Firm_ID','=',$id)->get();
        $legal1 = \App\consultant_documents::where('Firm_ID','=',$id)
                    ->where('Document_Link','like','%PHILGEPS%')
                    ->orderBy('created_at','DESC')
                    ->limit(1)
                    ->get();
        $legal2 = \App\consultant_documents::where('Firm_ID','=',$id)
                    ->where('Document_Link','like','%SEC_DTI%')
                    ->orderBy('created_at', 'DESC')
                    ->limit(1)
                    ->get();
        $legal3 = \App\consultant_documents::where('Firm_ID','=',$id)
                    ->where('Document_Link','like','%BusinessPermit%')
                    ->orderBy('created_at', 'DESC')
                    ->limit(1)
                    ->get();
        $legal4 = \App\consultant_documents::where('Firm_ID','=',$id)
                    ->where('Document_Link','like','%TaxClearance%')
                    ->orderBy('created_at', 'DESC')
                    ->limit(1)
                    ->get();
        $findocs = \App\consultant_documents::where('Firm_ID','=',$id)
                    ->where('Document_Link','like','%FinancialDocs%')
                    ->orderBy('created_at', 'DESC')
                    ->limit(2)
                    ->get();
        $experts = \App\consultant_experts::where('Firm_ID','=',$id)->get();
        $expertscount = \App\consultant_experts::where('Firm_ID','=',$id)->count();

        $procurements = \App\consultant_procurements::where('Firm_ID','=',$id)->get();
        $procurementscount = \App\consultant_procurements::where('Firm_ID','=',$id)->count();

        return view('updateFirm',compact('id','firm','firmname','legal1','legal2','legal3','legal4','findocs','experts','expertscount','procurements','procurementscount'));
    }

    public function insertData(Request $request){
        $this->getValidate(null);
        $this->validate($request, [
            'newFirmName' => 'required',
            'newFirmAddress' => 'required',
            'newFirmEmail'  => 'required',
            'newFirmTelephone' => 'required',
            'newContactPerson' => 'required',
            'newDesignation' => 'required'
        ]);

        $firm                   = new \App\consultancy_firm();
        $firm->Firm_Name        = request('newFirmName');
        $firm->Address          = request('newFirmAddress');
        $firm->email_address    = request('newFirmEmail');
        $firm->alt_email1       = request('newFirmAltEmail1');
        $firm->alt_email2       = request('newFirmAltEmail2');
        $firm->alt_email3       = request('newFirmAltEmail3');
        $firm->contact_person   = request('newContactPerson');
        $firm->designation      = request('newDesignation');
        $firm->telephone        = request('newFirmTelephone');
        $firm->fax_number       = request('newFirmFax');
        $firm->save();

        $firmId = \App\consultancy_firm::where('Firm_Name','=',request('newFirmName'))->pluck('Firm_ID')->first();
        $date=date('Ymd');
        $experts = request('newexperts');
        $exprole = request('newexpertsrole');
        if(!is_null($experts)){
            foreach($experts as $index => $value){
                $save = new \App\consultant_experts();
                $save->Firm_ID = $firmId;
                $save->expert_name = $experts[$index];
                $save->expert_role = $exprole[$index];
                $save->save();
            }
        }

        if($request->hasFile('newphilgeps')){
            $filesSubmitted1 = $request->file('newphilgeps');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted1->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted1->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted1->StoreAs('public/storage/PHILGEPS',$filename);
            $docs->Firm_ID = $firmId;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('newsecdti')){
            $filesSubmitted2 = $request->file('newsecdti');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted2->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted2->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted2->StoreAs('public/storage/SEC_DTI',$filename);
            $docs->Firm_ID = $firmId;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('newbuspermit')){
            $filesSubmitted3 = $request->file('newbuspermit');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted3->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted3->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted3->StoreAs('public/storage/BusinessPermit',$filename);
            $docs->Firm_ID = $firmId;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('newtaxclrnc')){
            $filesSubmitted4 = $request->file('newtaxclrnc');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted4->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted4->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted4->StoreAs('public/storage/TaxClearance',$filename);
            $docs->Firm_ID = $firmId;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('newfindocs')){
            $filesSubmitted5 = $request->file('newfindocs');
            foreach($filesSubmitted5 as $files){
                $docs = new \App\consultant_documents();
                $fileWithExt = $files->getClientOriginalName();
                $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
                $ext = $files->getClientOriginalExtension();
                $filename = $file."_".$date.".".$ext;
                $path = $files->StoreAs('public/storage/FinancialDocs',$filename);
                $docs->Firm_ID = $id;
                $docs->Document_Link = $path;
                $docs->save();
            }
        }
        if(!empty(request('procjoined'))){
            $proc = request('procjoined');
            foreach($proc as $data){
                if(!is_null($data)){
                    $procs = new \App\consultant_procurements();
                    $procs->Firm_ID = $firmId;
                    $procs->procurement = $data;
                    $procs->save();
                }
            }
        }

        Alert::success('Consultancy Firm Created', '');
        return Redirect::back()->with('consSuccess','Consultancy Firm Created');
    }

    public function updateData(Request $request){
        $id = request('firmid');
        $firmdata = \App\consultancy_firm::where('Firm_ID','=',$id)->first();
        $date=date('Ymd');
        if(request('updFirmName')!=$firmdata->Firm_Name){
            $firmdata->Firm_Name = request('updFirmName');
        }
        if(request('updFirmAddress')!=$firmdata->Address){
            $firmdata->Address = request('updFirmAddress');
        }
        if(request('updFirmEmail')!=$firmdata->email_address){
            $firmdata->email_address = request('updFirmEmail');
        }
        if(request('updFirmAltEmail1')!=$firmdata->alt_email1){
            $firmdata->alt_email1 = request('updFirmAltEmail1');
        }
        if(request('updFirmAltEmail2')!=$firmdata->alt_email2){
            $firmdata->alt_email2 = request('updFirmAltEmail2');
        }
        if(request('updFirmAltEmail3')!=$firmdata->alt_email3){
            $firmdata->alt_email3 = request('updFirmAltEmail3');
        }
        if(request('updContactPerson')!=$firmdata->contact_person){
            $firmdata->contact_person = request('updContactPerson');
        }
        if(request('updDesignation')!=$firmdata->designation){
            $firmdata->designation = request('updDesignation');
        }
        if(request('updFirmTelephone')!=$firmdata->telephone){
            $firmdata->telephone = request('updFirmTelephone');
        }
        if(request('updFirmFax')!=$firmdata->fax_number){
            $firmdata->fax_number = request('updFirmFax');
        }
        $firmdata->save();

        if($request->hasFile('updphilgeps')){
            $filesSubmitted1 = $request->file('updphilgeps');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted1->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted1->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted1->StoreAs('public/storage/PHILGEPS',$filename);
            $docs->Firm_ID = $id;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('updsecdti')){
            $filesSubmitted2 = $request->file('updsecdti');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted2->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted2->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted2->StoreAs('public/storage/SEC_DTI',$filename);
            $docs->Firm_ID = $id;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('updbuspermit')){
            $filesSubmitted3 = $request->file('updbuspermit');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted3->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted3->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted3->StoreAs('public/storage/BusinessPermit',$filename);
            $docs->Firm_ID = $id;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('updtaxclrnc')){
            $filesSubmitted4 = $request->file('updtaxclrnc');
            $docs = new \App\consultant_documents();
            $fileWithExt = $filesSubmitted4->getClientOriginalName();
            $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
            $ext = $filesSubmitted4->getClientOriginalExtension();
            $filename = $file."_".$date.".".$ext;
            $path = $filesSubmitted4->StoreAs('public/storage/TaxClearance',$filename);
            $docs->Firm_ID = $id;
            $docs->Document_Link = $path;
            $docs->save();
        }
        if($request->hasFile('updfindocs')){
            $filesSubmitted5 = $request->file('updfindocs');
            foreach($filesSubmitted5 as $files){
                $docs = new \App\consultant_documents();
                $fileWithExt = $files->getClientOriginalName();
                $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
                $ext = $files->getClientOriginalExtension();
                $filename = $file."_".$date.".".$ext;
                $path = $files->StoreAs('public/storage/FinancialDocs',$filename);
                $docs->Firm_ID = $id;
                $docs->Document_Link = $path;
                $docs->save();
            }
        }
        
        Alert::success('Consultancy Firm Updated', '');
        return Redirect::back()->with('consSuccess','Consultancy Firm Updated');
    }


    public function addExperts(Request $request){
        $id = request('firmid');
        $id = decrypt($id);
        $expname = request('updaddexperts');
        $exprole = request('updaddexpertsrole');
        // dd($expname);
        if(!is_null($expname)){
            foreach($expname as $index => $value){
                $table = new \App\consultant_experts();
                $table->Firm_ID = $id;
                $table->expert_name = $expname[$index];
                $table->expert_role = $exprole[$index];
                $table->save();
            }
        }
        Alert::success('Consultancy Firm Experts Updated', '');
        return Redirect::back()->with('consSuccess','Consultancy Firm Experts Added');
    }

    public function addProcs(Request $request){
        $id = request('firmid');
        // dd($id);
        $id = decrypt($id);
        if(!is_null(request('updaddprocjoined'))){
            $dataRetrieved = request('updaddprocjoined');
            foreach($dataRetrieved as $data){
                $procs = new \App\consultant_procurements();
                $procs->Firm_ID = $id;
                $procs->procurement = $data;
                $procs->save();
            }
        }
        Alert::success('Consultancy Firm Procurements Added', '');
        return Redirect::back()->with('consSuccess','Consultancy Firm Procurements Added');
    }

    public function updateExperts(Request $request){
        $expertID = decrypt(request('expid'));
        $firmid = request('firmid');
        $name = request('updexperts');
        $role = request('updexpertsrole');
        $exp = \App\consultant_experts::where('expert_id',$expertID)->get();


        if(!is_null($exp)){
            foreach($exp as $index => $value){
                if($value->Firm_ID == $firmid[$index]){
                    $value->expert_name = $name[$index];
                    $value->expert_role = $role[$index];
                    $value->save();
                    Alert::success('Consultancy Firm Experts Updated', '');
                    return Redirect::back()->with('consSuccess','Consultancy Firm Experts Updated');
                }
               
            }
        }else{
            Alert::error('Consultancy Firm Experts Not Updated', '');
            return Redirect::back()->with('consSuccess','Consultancy Firm Experts Not Updated');
        }

    }

    public function updateProcs(Request $request){
        $firmid = request('firmid');
        $procjoined = request('updprocjoined');
        $procid = decrypt(request('procid'));
        $procs = \App\consultant_procurements::where('id',$procid)->get();

        if(!is_null($procs)){
            foreach($procs as $pr => $value){
                if($value->Firm_ID == $firmid[$pr]){
                    $value->procurement = $procjoined[$pr];
                    $value->save();
                    Alert::success('Consultancy Firm Procurements Updated', '');
                    return Redirect::back()->with('consSuccess','Consultancy Firm Procurements Updated');
                }
            }
        }else{
            Alert::error('Consultancy Firm Procurements Not Updated', '');
            return Redirect::back()->with('consSuccess','Consultancy Firm Procurements Not Updated');
        }

    }
    public function getValidate($request_id): void
    {
        request()->validate([
            'newFirmName' => ['required', Rule::unique('consultancy_firm', 'Firm_Name')->ignore($request_id)]
        ]);
    }
}

