<?php

namespace App\Http\Controllers;

use Alert;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;

class SendEmailController extends Controller
{
    public function midpointmessage($id){  
        $submission = \App\Submissions::find($id);
        $fundingsources = \App\Fundingsources::all();
        $tier1statuses = \App\Tier1statuses::all();
        $tier2statuses = \App\Tier2statuses::all();

        return view('finalmessage', compact('submission','fundingsources','tier1statuses','tier2statuses'));
    }

    public function sendemail(Request $request, $id){
        $submission = \App\Submissions::find($id);
        $agencyid = $submission->agency_id;

        $agency = \App\Agencies::find($submission->agency_id);
        $agencyname = $agency->Abbreviation;

        $fundingsources = \App\Fundingsources::all();
        $tier1statuses = \App\Tier1statuses::all();
        $tier2statuses = \App\Tier2statuses::all();

        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587

        $mail->Username = $_ENV['MAIL_USERNAME'];
        $mail->Password = $_ENV['MAIL_PASSWORD'];
        $mail->IsHTML(true);

        $mail->SetFrom('contracts.neda@gmail.com');
        
        $mail->Subject = $agencyname." - SUMMARY OF SUBMISSION IN THE CMS";
        // VALIDATED PAPS IN THE CONTRACT MANAGEMENT SYSTEM
        ob_start(); //STARTS THE OUTPUT BUFFER
        include('finalmessage.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        $some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        ob_clean();  //CLEAN OUT THE OUTPUT BUFFER
        $mail->Body = $some_page_contents;
        $mail->AddReplyTo('contracts.neda@gmail.com');
        $mail->AddAddress($submission->head_email);
        $mail->AddAddress($submission->mother_email);

        foreach ($submission->getUserDetails($submission->id) as $useremail){
            $mail->AddCC($useremail->email);
        }
        
        //$mail->AddCC("BNLayon@neda.gov.ph");
        $mail->AddCC("contracts.neda@gmail.com");
        //$mail->AddCC("MLDelRosario@neda.gov.ph");

       
        $submission->update(array('final' => '1', 'final_time' => NOW()));
       
        // if($submission->midpoint != 1 )
        // {
        //     $submission->update(array('midpoint' => '1', 'midpoint_time' => NOW()));
        // }
        // elseif($submission->midpoint = 1 && $submission->final != 1 && $submission->validated != 1 )
        // {
        //     $submission->update(array('final' => '1', 'final_time' => NOW()));
        // }
        



        $mail->Send();

        if(!$mail->send()) 
        {
             Alert::error('Email Failed', $mail->ErrorInfo);
                return back();
        } 
        else 
        {
         Alert::success('Email Sent', '');
                return back();
        }
    }
   
}
