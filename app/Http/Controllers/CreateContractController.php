<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Alert;

class CreateContractController extends Controller
{
    // checker if user is logged in
    public function __construct()
    {
        $this->middleware('auth');
    }
// initial data display on contract creation
    public function contype(){
        $contractTypeDB = DB::table('contract_type')->select('Contract_Type','Contract_Desc')->where('Contract_Type','!=','ALL')->get();
        return view('createoption',compact('contractTypeDB'));
    }
    public function getData(){
        $firmtype = DB::table('firm_type')->select('Firm_Type','Firm_Type_Desc')->get();
        $firmname = DB::table('consultancy_firm')->select('Firm_ID','Firm_Name')->orderBy('Firm_Name')->get();
        $contractTypeDB = DB::table('contract_type')->select('Contract_Type','Contract_Desc')->where('Contract_Type','!=','ALL')->get();
        $contractCount =  DB::table('contracts')->select('Contract_ID')->count();
        $lastcontract = DB::table('contracts')->select('Contract_ID')->latest()->value('Contract_ID');
        $lastcontractCount = (int)substr($lastcontract,-5);
        $secType = DB::table('security_type')->select('Type','Type_Description')->get();
        $firmRole = DB::table('firm_role')->select('firm_role','firm_role_desc')->get();
        $yearmonth = strval(date('YMd'));
        $procmode = DB::table('procurement_mode')->select('mode_code','mode_desc')->get();
        $experts = \App\consultant_experts::get();
        $fundsources = \App\Fund_Sources::get();
        $endusers = \App\end_users::get();
        
        if($contractCount==0){
           $counter = $contractCount+1; 
        }else{
            $counter = $lastcontractCount+1;
        }
        $counter= str_pad($counter, 6, "0", STR_PAD_LEFT);
        return view('create-contract',compact('endusers','fundsources','firmtype','firmname','yearmonth','counter','contractTypeDB','secType','firmRole','procmode','experts'));
    }
// getting firm type (Single,Joint Venture, etc.)
    public function getFirmType($id){
        $firmname = DB::table('consultancy_firm')
        ->select('firm_type.Firm_Type','firm_type.Firm_Type_Desc')
        ->join('firm_type','firm_type.Firm_Type','=','consultancy_firm.Firm_Type')
        ->where('consultancy_firm.Firm_ID',$id)->get();
        return json_encode($firmname);
    }

    public function insertData(Request $request){
// validation of data to insert
        $this->validate($request, [
            'contractID' => 'required',
            'ProjectTitle' => 'required',
            'ConsultancyFirm' => 'required',
            'firmType' => 'required',
            'hiddenCType' => 'required',
            'endUser' => 'required',
            'implementingAgency' => 'required',
            'NTP' => 'required',
            'conformeDate' => 'required',
            'NOAConforme' => 'required',
            'contractStartDate' => 'required',
            'contractEndDate' => 'required',
            'contractCost' => 'required',
            'ABC' => 'required',
            'issuingCompany' => 'required',
            'fundSource' => 'required',
            'fundYear' => 'required',
            'NOA' => 'required',
            'deliverables' => 'required',
            'dates' => 'required',
            'docs' => 'required',
            'perfSecVal' => 'required',
            'ProcMode' => 'required'
        ],[
            'ProjectTitle.required'=> ':attribute is required',
            'ConsultancyFirm.required'=> ':attribute is required',
            'firmType.required'=> ':attribute is required',
            'hiddenCType.required'=> ':attribute is required',
            'endUser.required'=> ':attribute is required',
            'implementingAgency.required'=> ':attribute is required',
            'NTP.required'=> ':attribute is required',
            'conformeDate.required'=> ':attribute is required',
            'NOAConforme.required'=> ':attribute is required',
            'contractStartDate.required'=> ':attribute is required',
            'contractEndDate.required'=> ':attribute is required',
            'contractCost.required'=> ':attribute is required',
            'ABC.required'=> ':attribute is required',
            'issuingCompany.required'=> ':attribute is required',
            'fundSource.required'=> ':attribute is required',
            'fundYear.required'=> ':attribute is required',
            'NOA.required'=> ':attribute is required',
            'deliverables.required'=> ':attribute is required',
            'dates.required'=> ':attribute is required',
            'docs.required'=> ':attribute is required',
            'perfSecVal.required'=> ':attribute is required',
            'ProcMode.required'=> ':attribute is required',
            
        ]);
        
        if(!is_null(request('perfSecVal'))){
            $fndData = \App\Fund_Sources::where('id',request('fundSource'))->pluck('fundSource')->first();
            $enduserData = \App\end_users::where('id',request('endUser'))->pluck('endUsers')->first();
            $contract = new \App\Contracts();
            $contract->Contract_ID = request('contractID');
            $contract->Contract_Name = request('ProjectTitle');
            $contract->Contract_Type = request('hiddenCType');
            $contract->Contract_Cost = request('contractCost');
            $contract->End_User = $enduserData;
            $contract->mode_code = request('ProcMode');
            $contract->Implementing_Agency = request('implementingAgency');
            $contract->Co_Implementing_Agency = request('coimplementingAgency');
            $contract->NOA_Release_Date = request('NOA');
            $contract->NTP_Release_Date = request('NTP');
            $contract->Fund_Source = $fndData;
            $contract->Fund_Year = request('fundYear');
            $contract->ABC = request('ABC');
            $contract->Contract_Status = "New";
            $contract->NOA_Conforme_Date = request('NOAConforme');
            $contract->NTP_Conforme_Date = request('conformeDate');
            $contract->Contract_Start_Date = request('contractStartDate');
            $contract->Contract_End_Date = request('contractEndDate');
            $contract->save();

            if(request('firmType') != 'S'){
                $partners = new \App\firm_partners();
                $projects = new \App\firm_projects();
                $count = request('PartnerCount');
                
                $projects->Contract_ID = request('contractID');
                $projects->Firm_ID = request('ConsultancyFirm');
                $projects->Firm_Type = request('firmType');
                $projects->firm_role = request('firmrole');
                $projects->save();
                
                if($projects->firm_role == 'L'){
                    $project_firm2                      = new \App\firm_partners();
                    $project_firm2->Contract_ID         = $projects->Contract_ID;
                    $project_firm2->Firm_Lead_ID        = $projects->Firm_ID;
                    $project_firm2->save();
                }else{
                    $project_firm2                      = new \App\firm_partners();
                    $project_firm2->Contract_ID         = $projects->Contract_ID;
                    $project_firm2->Firm_Partner_ID     = $projects->Firm_ID;
                    $project_firm2->save();
                }

                $firmname=request('PartnerFirmName');
                $firmrole=request('PartnerFirmRole');
                $partnername = request('PartnerFirmName');
                $partnerrole = request('PartnerFirmRole');
                    // dd($partnerrole);
                if(!empty(request('PartnerFirmName'))){
                    $count_ids = count(request('PartnerFirmName'));
              
                    for($i = 0; $i < $count_ids; $i++){
                        $project_firm = new \App\firm_projects();
                        $project_firm->Firm_ID             = $partnername[$i];
                        $project_firm->Contract_ID         = $projects->Contract_ID;
                        $project_firm->Firm_Type           = $projects->Firm_Type;
                        $project_firm->firm_role           = $partnerrole[$i];
                        $project_firm->save();
                        
                        $proc = new \App\consultant_procurements();
                        $proc->Firm_ID =  $partnername[$i];
                        $proc->procurement = request('ProjectTitle');
                        $proc->save();

                        if($project_firm->firm_role == 'L'){
                            $project_firm2                      = new \App\firm_partners();
                            $project_firm2->Contract_ID         = $projects->Contract_ID;
                            $project_firm2->Firm_Lead_ID        = $partnername[$i];
                            $project_firm2->save();
                        }else{
                            $project_firm2                      = new \App\firm_partners();
                            $project_firm2->Contract_ID         = $projects->Contract_ID;
                            $project_firm2->Firm_Partner_ID     = $partnername[$i];
                            $project_firm2->save();
                        }
                    } 
                }
            }else{
                $projects = new \App\firm_projects();
                $projects->Contract_ID = request('contractID');
                $projects->Firm_ID = request('ConsultancyFirm');
                $projects->Firm_Type = request('firmType');
                $projects->save();
            }

            if($request->hasFile('docs')){
                $filesSubmitted = $request->file('docs');
                foreach($filesSubmitted as $files){
                    $docs = new \App\Documents();
                    $fileWithExt = $files->getClientOriginalName();
                    $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
                    $ext = $files->getClientOriginalExtension();
                    $filename = $file.".".$ext;
                    $path = $files->StoreAs('public/storage/supportingDocs',$filename);
                    $docs->Contract_ID = request('contractID');
                    $docs->Document_Link = $path;
                    $docs->save();
                }
            }

            if(!is_null(request('experts'))){
                $dataRetrieved = request('experts');
                $exprole = request('expertsrole');
                foreach($dataRetrieved as $index => $value){
                    $experts = new \App\contract_experts();
                    $experts->Contract_ID = request('contractID');
                    $experts->Firm_ID = request('ConsultancyFirm');
                    $experts->expert_name = $dataRetrieved[$index];
                    $experts->expert_role = $exprole[$index];
                    $experts->save();
                }
            }
//SAMPLE
            if(!is_null(request('deliverables'))){
                $dataRetrieved = request('deliverables');
                $dates = request('dates');
                $notes = request('dnotes');
                foreach($dataRetrieved as $index => $value){
                    $deliverables = new \App\Deliverables();
                    $deliverables->Deliverable = $dataRetrieved[$index];
                    $deliverables->Target_Completion_Date = $dates[$index];
                    $deliverables->TCD_Notes = $notes[$index];
                    $deliverables->Contract_ID = request('contractID');
                    $deliverables->save();
                }
            }

            $history = new \App\project_history();
            $history->Contract_ID = request('contractID');
            $history->History_Description = "Contract Created";
            $history->History_Date = Carbon::now();
            $history->save();

            $perfSec = request('perfSecVal');
            // dd($perfSec);
            foreach($perfSec as $perf){
                $PerfSecInsert = new \App\performance_security();
                $PerfSecInsert->Contract_ID = request('contractID');
                $PerfSecInsert->Issuing_Company = request('issuingCompany');
                if($perf == 'C'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->CashOR = request('CashOR');
                    $PerfSecInsert->CashBV = request('ValidityCash');
                    $PerfSecInsert->CashBVNotes = request('ValiditycashNotes');
                    $PerfSecInsert->save();
                }elseif($perf == 'CMC'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->CheckNum = request('CheckNum');
                    $PerfSecInsert->CheckOR = request('CheckOR');
                    $PerfSecInsert->CheckBank = request('CheckBank');
                    $PerfSecInsert->CheckBV = request('ValidityCheck');
                    $PerfSecInsert->CheckBVNotes = request('ValidityCheckNotes');
                    $PerfSecInsert->save();
                }elseif($perf == 'BD'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->BankNameBD = request('BankNameBD');
                    $PerfSecInsert->DraftBD = request('DraftBD');
                    $PerfSecInsert->ValidityBD = request('ValidityBD');
                    $PerfSecInsert->ValidityBDNotes = request('ValidityBDNotes');
                    $PerfSecInsert->save();
                }elseif($perfSec == 'BG'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->BankNameBG = request('BankNameBG');
                    $PerfSecInsert->DraftBG = request('DraftBG');
                    $PerfSecInsert->ValidityBG = request('ValidityBG');
                    $PerfSecInsert->ValidityBGNotes = request('ValidityBGNotes');
                    $PerfSecInsert->save();
                }elseif($perfSec == 'ILC'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->BankNameILC = request('BankNameILC');
                    $PerfSecInsert->ILC = request('ILC');
                    $PerfSecInsert->ValidityILC = request('ValidityILC');
                    $PerfSecInsert->ValidityILCNotes = request('ValidityILCNotes');
                    $PerfSecInsert->save();
                }elseif($perfSec == 'SB'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->CNameSurety = request('CNameSurety');
                    $PerfSecInsert->SuretyNum = request('SuretyNum');
                    $PerfSecInsert->ValiditySurety = request('ValiditySurety');
                    $PerfSecInsert->ValiditySuretyNotes = request('ValiditySuretyNotes');
                    $PerfSecInsert->save();
                }elseif($perfSec == 'NA'){
                    $PerfSecInsert->Type = $perf;
                    $PerfSecInsert->save();
                }
            }

            $procs = new \App\consultant_procurements();
            $procs->Firm_ID =  request('ConsultancyFirm');
            $procs->procurement = request('ProjectTitle');
            $procs->save();

            Alert::success('Contract Created', '');
            // return back()->withStatus(__('Contract Creation Success'));
            return Redirect::back()->with('contractSuccess','Contract Creation Success');
        }else{
            Alert::error('Contract Creation Failed', 'Performance Security REQUIRED');
            // return back()->withStatus(__('Contract Creation Failed: Performance Security REQUIRED'));
            return Redirect::back()->with('contractFailed','Contract Creation Failed: Performance Security REQUIRED');
        }
    }

}
