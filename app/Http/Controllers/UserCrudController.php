<?php

namespace App\Http\Controllers;

use App\Users;
use Validator;
use App\Agencies;
use Carbon\Carbon;
use App\Submission;
use App\Submissions;
use Illuminate\Http\Request;
use App\Jobs\SendEmailtoUser;
use App\Jobs\SendEmailDisapprove;
use Illuminate\Support\Facades\DB;

class UserCrudController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rows = $_GET['rows'];
        $allusers = DB::table('users AS A')
            ->join('submissions AS B', 'A.submission_id', '=', 'B.id')
            ->where('A.account_approval_date', '!=', null)
            ->where('A.username', '!=', null)
            ->select(
                'A.lname',
                'A.fname',
                'A.mname',
                'A.user_id',
                'A.username',
                'A.office_staff',
                'A.division_unit',
                'A.position',
                'A.email_address',
                'A.telephone_number',
                'A.gender',
                'A.contract_type',
                'A.access_type',
                'A.submission_id'
            )
            ->paginate($rows);
        return response()->json($allusers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form_data = array(
            'username'         => $request->user['username'],
            'password'         => bcrypt('password'),
            'fname'            => $request->user['fname'],
            'lname'            => $request->user['lname'],
            'mname'            => $request->user['mname'],
            'office_staff'     => $request->user['office_staff'],
            'division_unit'    => $request->user['division_unit'],
            'position'         => $request->user['position'],
            'email_address'    => $request->user['email_address'],
            'telephone_number' => $request->user['telephone_number'],
            'gender'           => $request->user['gender'],
            'access_type'    =>  $request->user['access_type']
        );

        $user = Users::create($form_data);

        return response()->json($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = Users::findOrFail($request->data['user_id']);
        $data->lname = $request->data['lname'];
        $data->fname = $request->data['fname'];
        $data->mname = $request->data['mname'];
        $data->designation = $request->data['designation'];
        $data->telnumber = $request->data['telnumber'];
        $data->faxnumber = $request->data['faxnumber'];
        $data->email = $request->data['email_address'];
        $data->gender = $request->data['gender'];
        $data->access_type = $request->data['access_type'];
        if ($request->data['password'] != null) {
            $data->password = bcrypt($request->data['password']);
        }
        $data->save();
        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Users::findOrFail($id);
        $data->delete();
    }
    public function approve($id)
    {
        $user = Users::findOrFail($id);
        $user->status = 2;
        $user->save();
        $agency = DB::table('submissions as A')
            ->join('agencies as B', 'A.agency_id', '=', 'B.id')
            ->where('A.id', '=', $user->submission_id)
            ->select( 'UACS_AGY_DSC')
            ->first();
        $user->agency = $agency;
        $job = (new SendEmailtoUser($user))
            ->delay(Carbon::now()->addSeconds(5));
        dispatch($job);
        return response()->json($id);
    }
    public function disable($id)
    {
        $data = Users::findOrFail($id);
        $data->status = 1;
        $data->save();
        return response()->json($id);
    }
    public function users2()
    {
        return view('users2');
    }
    public function userrequest()
    {
        $rows = $_GET['rows'];
        $allusers2 = DB::table('users AS A')
            ->join('submissions AS B', 'A.submission_id', '=', 'B.id')
            ->where('A.status', '=', 1)
            ->where('A.is_first', '!=', 0)
            ->select(
                'A.username',
                'A.password',
                'A.fname',
                'A.lname',
                'A.mname',
                'A.office_staff',
                'A.division_unit',
                'A.position',
                'A.email_address',
                'A.telephone_number',
                'A.gender',
                'A.access_type',
                'A.contract_type',
                'A.account_creation_date',
                'A.account_lock',
                'A.account_approval_date'
            )
            ->paginate($rows);
        return response()->json($allusers2);
    }
    public function disabledusers()
    {
        $rows = $_GET['rows'];
        $allusers3 = DB::table('users AS A')
            ->join('submissions AS B', 'A.submission_id', '=', 'B.id')
            ->join('agencies AS C', 'C.id', '=', 'B.agency_id')
            ->where('A.status', "=", 1)
            ->where('A.is_first', "=", 0)
            ->select(
                'A.username',
                'A.password',
                'A.fname',
                'A.lname',
                'A.mname',
                'A.office_staff',
                'A.division_unit',
                'A.position',
                'A.email_address',
                'A.telephone_number',
                'A.gender',
                'A.access_type',
                'A.contract_type',
                'A.account_creation_date',
                'A.account_lock',
                'A.account_approval_date'
            )
            ->paginate($rows);
        return response()->json($allusers3);
    }
    public function asyncfindagencysubid(Request $request)
    {
        $agency = Agencies::where('UACS_AGY_DSC', 'like', '%' . Request(' query') . '%')->get();
        return response()->json($agency);
    }
    public function getSubmissionID(Request $request)
    {
        $subid = Submissions::where('agency_id', $request->id)
            ->select('id')->first();
        if ($subid) {
            return response()->json($subid);
        } else {
            return response()->json(['id' => 'No Record of Head of the Agency']);
        }
    }
    public function openauth(Request $request)
    {
        $submission = Submissions::findOrFail( $request->user['submission_id']);
        return response()->json( $submission->authorization_form);
    }
    public function disapprove( Request $request)
    {
        $params = $request;
        $job = (new SendEmailDisapprove($params))
            ->delay(Carbon::now()->addSeconds(5));
        dispatch($job);
        $data = Users::findOrFail($params['item']['id']);
        $submission = Submissions::findOrFail($data['submission_id']);
        // File Deletion
        $path = public_path() . "/images/" . $submission->authorization_form;
        unlink($path);
        // Record Auto Delete
        $data->delete();
        $submission->delete();
        return response()->json( 'Record Deleted Successfully');
    }
}
