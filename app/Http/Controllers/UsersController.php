<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Alert;
use App\Users;
use Carbon\Carbon;
use App\Submissions;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Jobs\SendEmailtoAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use DataTables;

class UsersController extends Controller
{
    private $submissionid;
    public function users(){
        $requests = Users::where('account_status','=','0')->get();
        $active = Users::where('account_status','=','1')->get();
        return view('users',compact('requests','active'));

    }
    public function index()
    {
        return DataTables::of(Users::query())->make(true);
    }
    public function index2()
    {
        return DataTables::of(users_forapprovals::query())->addColumn('action')->rawColumns(['action'])->make(true);
    }
    public function signup()
    {
        return view('register');
    }
    public function addUser(Request $request)
    {
        // dd($request->users);
        foreach ($request->users as $key => $user) {
            //head of agency
            if ($key == 0) {
                if ($request->agency['motheragency_id'] == 0) {
                    $user['mlname'] = '';
                    $user['mfname'] = '';
                    $user['mmname'] = '';
                    $user['mdesignation'] = '';
                    $user['mtelnumber'] = '';
                    $user['mfaxnumber'] = '';
                    $user['memail'] = '';
                }
                $submission = Submissions::create([
                    'agency_id' => $request->agency['id'],
                    'motheragency_id' => $request->agency['motheragency_id'],
                    'head_lname' => $user['lname'],
                    'head_fname' => $user['fname'],
                    'head_mname' => $user['mname'],
                    'head_designation' => $user['designation'],
                    'head_telnumber' => $user['telnumber'],
                    'head_faxnumber' => $user['faxnumber'],
                    'head_email' => $user['email_address'],
                    'mother_lname' => $user['mlname'],
                    'mother_fname' => $user['mfname'],
                    'mother_mname' => $user['mmname'],
                    'mother_designation' => $user['mdesignation'],
                    'mother_telnumber' => $user['mtelnumber'],
                    'mother_faxnumber' => $user['mfaxnumber'],
                    'mother_email' => $user['memail'],
                    'authorization_form' => $this->submissionid
                ]);
                //focals
            } else if ($key >= 1) {
                Users::create([
                    'lname' => $user['lname'],
                    'fname' => $user['fname'],
                    'mname' => $user['mname'],
                    'designation' => $user['designation'],
                    'telnumber' => $user['telnumber'],
                    'faxnumber' => $user['faxnumber'],
                    'email_address' => $user['email_address'],
                    'password' => bcrypt($user['password']),
                    'access_type' => 'A',
                    'status' => 1,
                    'username' => $request->agency['Abbreviation'] . '_' . $user['lname'],
                    'submission_id' => $submission->id,
                    'is_first' => true,
                    'gender' => $user['gender']
                ]);
                $job = (new SendEmailtoAdmin($user))
                    ->delay(Carbon::now()->addSeconds(5));
                dispatch($job);
            }
        }
        return $this->submissionid;
    }


}
