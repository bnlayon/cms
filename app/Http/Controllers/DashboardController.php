<?php

namespace App\Http\Controllers;

use Alert;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Controllers\Controller;
use App\Charts\donutChart;
use App\Charts\barChart;
use App\Contracts;
use App\Deliverables;
use Charts;
use DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard(){
        $new = \App\Contracts::where('Contract_Status','New')->count();
        $ongoing = \App\Contracts::where('Contract_Status','Ongoing')->count();
        $completed = \App\Contracts::where('Contract_Status','Completed')->count();
        $closed =\App\Contracts::where('Contract_Status','Closed')->count();
        $cancelled = \App\Contracts::where('Contract_Status','Cancelled')->count();
        $expiring = \App\Contracts::where('Contract_Status','Expiring')->count();

        return view('dashboard',compact('ongoing','completed','closed','cancelled','expiring','new'));
    }

    
}
