<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Alert;
use Session;
use DB;

class DeliverablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view($id){
        $id = decrypt($id);
        $projectTitle = \App\Contracts::where('Contract_ID','=',$id)->get();
        $selected = \App\Deliverables::where('Contract_ID','=',$id)->get();
        $history = \App\project_history::where([['Contract_ID','=',$id],['History_Description','like','Amendment']])->orderBy('History_Date', 'DESC')->get();
        $amendment = DB::table('amendment_history')
        ->join('deliverables','deliverables.Deliverable_ID','=','amendment_history.Deliverable_ID')
        ->join('contracts','contracts.Contract_ID','=','deliverables.Contract_ID')
        ->orderBy('amendment_history_id','DESC')->get();
        return view('deliverables',compact('selected','id','history','amendment','projectTitle'));
    }

    public function amend(Request $request){
        
        $id=decrypt(request('deliverableid'));
        $deliverables = \App\Deliverables::find($id);
        $lastamendeddate = $deliverables->Amended_Date;
        $history = new \App\project_history();

        if(!is_null(request('targetdate')) || !is_null(request('adnotes'))){
            $deliverables->Target_Completion_Date = request('targetdate');
            $deliverables->TCD_Notes = request('tcdnotes');
            $deliverables->save();

            $history->Contract_ID = decrypt(request('contractid'));
            $history->History_Description = "Deliverables Target Date (".$id.") Updated";
            $history->History_Date = Carbon::now();
            $history->save();
        }
        if(!is_null(request('amendDate')) || !is_null(request('adnotes'))){
            $deliverables->Amended_Date = request('amendDate');
            $deliverables->AD_Notes = request('adnotes');
            $deliverables->save();

            $amendment_history = new \App\amendment_history();
            $amendment_history->Deliverable_ID = $deliverables->Deliverable_ID;
            $amendment_history->Last_Amended_Date = request('amendDate');
            $amendment_history->Amended_By = Session::get('username');
            $amendment_history->Date_Of_Amendment = Carbon::now();
            $amendment_history->save();

            $history->Contract_ID = decrypt(request('contractid'));
            $history->History_Description = "Deliverables Amendment Date (".$id.") Updated";
            $history->History_Date = Carbon::now();
            $history->save();
        }

        if(!is_null(request('submissiondate')) || !is_null(request('asdnotes')) || !is_null(request('reviewedBy'))){
            if(!is_null(request('submissiondate'))){
                $deliverables->Actual_Submission_Date = request('submissiondate');
            }
            if(!is_null(request('asdnotes'))){
                $deliverables->ASD_Notes = request('asdnotes');
            }if(!is_null(request('reviewedBy'))){
                $deliverables->ASD_ReviewedBy = request('reviewedBy');
            }
            $deliverables->save();

            $history->Contract_ID = decrypt(request('contractid'));
            $history->History_Description = "Deliverables Actual Completion Date (".$id.") Updated";
            $history->History_Date = Carbon::now();
            $history->save();
        }

        if(!is_null(request('actualdate')) || !is_null(request('acdnotes'))){
            $deliverables->Actual_Completion_Date = request('actualdate');
            $deliverables->ACD_Notes = request('acdnotes');
            $deliverables->save();

            $history->Contract_ID = decrypt(request('contractid'));
            $history->History_Description = "Deliverables Actual Completion Date (".$id.") Updated";
            $history->History_Date = Carbon::now();
            $history->save();
        }
        return Redirect::back();
    }

    public function deliverablePaymentView($Contract_ID,$Deliverable_ID){
        $Contract_ID = decrypt($Contract_ID);
        $Deliverable_ID=decrypt($Deliverable_ID);
        $contractName = \App\Contracts::where('Contract_ID',$Contract_ID)->pluck('Contract_Name')->first();
        $deliverableName = \App\Deliverables::where('Deliverable_ID',$Deliverable_ID)->pluck('Deliverable')->first();
        $payment = \App\Payment::where('Deliverable_ID',$Deliverable_ID)->get();
        return view('paymentView',compact('payment','Contract_ID','contractName','deliverableName'));
    }

    public function PaymentUpdateView($Contract_ID,$Deliverable_ID){
        $dContract_ID = decrypt($Contract_ID);
        $dDeliverable_ID = decrypt($Deliverable_ID);
        $contractName = \App\Contracts::where('Contract_ID',$dContract_ID)->pluck('Contract_Name')->first();
        $deliverableName = \App\Deliverables::where('Deliverable_ID',$dDeliverable_ID)->pluck('Deliverable')->first();
        $payment = \App\Payment::where('Deliverable_ID',$dDeliverable_ID)->get();
        return view('paymentUpdateView',compact('dContract_ID','dDeliverable_ID','contractName','deliverableName','Contract_ID','Deliverable_ID','payment'));
    }

    public function deliverablePaymentUpdate(Request $request){
        
        $deliverableid = decrypt(request('deliverableid'));
        $contractid= decrypt(request('contractid'));
        // dd($deliverableid);
        $data = \App\Payment::where('Deliverable_ID',$deliverableid)->first();
        
        if(is_null($data)){
            
            if($request->has('paymentStatus')){
                $deliverableTbl = \App\Deliverables::find($deliverableid);
                $deliverableTbl->Payment_Status = request('paymentStatus');
                $deliverableTbl->save();
            }
            
            $payment = new \App\Payment();
            $payment->Contract_ID = $contractid;
            $payment->Deliverable_ID = $deliverableid;
            $payment->Gross_Payment = request('grossPayment');
            $payment->Retention = request('retention');
            $payment->VAT = request('vat');
            $payment->Reimbursible_Amt = request('reimbursible');
            $payment->EWT = request('ewt');
            $payment->Net_Payment = request('netpmt');
            $payment->Liquidated_Damages = request('lqdtdDmgs');
            $payment->Payment_Date = request('pmtDate');
            $payment->Payment_Status = request('paymentStatus');
            $payment->save();

            if($request->hasFile('dbmtvch')){
                $filesSubmitted = $request->file('dbmtvch');
                $fileWithExt = $filesSubmitted->getClientOriginalName();
                $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
                $ext = $request->file('dbmtvch')->getClientOriginalExtension();
                $filename = $file.".".$ext;
                $path = $request->file('dbmtvch')->StoreAs('public/storage/DisbursementVouchers',$filename);
                $payment->Disbursement_Voucher = $path;
                $payment->save();
            }
            

            $history = new \App\project_history();
            $history->Contract_ID = $contractid;
            $history->History_Description = "Contract ".$contractid." Payment Status for ".$deliverableid." Updated <br>";
            $history->History_Date = Carbon::now();
            $history->save();

            Alert::success('Payment Updated', '');
            return redirect('/manage-contracts/deliverables/'.$contractid);
        }else{
            
            if($request->has('paymentStatus')){
                $deliverableTbl = \App\Deliverables::find($deliverableid);
                $deliverableTbl->Payment_Status = request('paymentStatus');
                $deliverableTbl->save();
            }
            
            $data->Gross_Payment = request('grossPayment');
            $data->Retention = request('retention');
            $data->VAT = request('vat');
            $data->Reimbursible_Amt = request('reimbursible');
            $data->EWT = request('ewt');
            $data->Net_Payment = request('netpmt');
            $data->Liquidated_Damages = request('lqdtdDmgs');
            $data->Payment_Date = request('pmtDate');

            if($request->hasFile('dbmtvch')){
                $filesSubmitted = $request->file('dbmtvch');
                $fileWithExt = $filesSubmitted->getClientOriginalName();
                $file = pathInfo($fileWithExt,PATHINFO_FILENAME);
                $ext = $files->getClientOriginalExtension();
                $filename = $file.".".$ext;
                $path = $files->StoreAs('public/storage/DisbursementVouchers',$filename);
                $data->Disbursement_Voucher = $path;
            }
            $data->Payment_Status = request('paymentStatus');
            $data->save();
            
            $history = new \App\project_history();
            $history->Contract_ID = $contractid;
            $history->History_Description = "Contract ".$contractid." Payment Status for ".$deliverableid." Updated <br>";
            $history->History_Date = Carbon::now();
            $history->save();

            Alert::success('Payment Status Updated', '');
            return redirect('/manage-contracts/deliverables/'.encrypt($contractid));
        }
    }
    
}
