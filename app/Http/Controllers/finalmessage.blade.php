<?php ini_set('max_execution_time', 6000); ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
      <div class="container">
        <br>
        <div class="card">
          <div class="card-body">
            <img src="http://contracts.neda.gov.ph/img/headeremail.png" height="232px" style="text-align:center"><br/><br/>
            <h5><b><?php echo date("F d, Y"); ?></b></h5>
            <br>
            <span style="text-transform:uppercase;"><?php echo $submission->head_fname." ".$submission->head_mname." ".$submission->head_lname; ?></span><br>
            <?php 
            foreach ($submission->getAgencyDetails($submission->agency_id) as $agencydetail){
                echo $agencydetail->UACS_AGY_DSC;
            }
            ?>
            <?php if (!empty($submission->motheragency_id)){ ?>
            <br> <br>  
            <span style="text-transform:uppercase;"><?php echo $submission->mother_fname." ".$submission->mother_mname." ".$submission->mother_lname; ?></span><br>
            <?php 
            foreach ($submission->getAgencyDetails($submission->motheragency_id) as $agencydetail){
                echo $agencydetail->UACS_AGY_DSC;
            }
            ?>
            <?php } ?>
            <br> <br>  
            <p>Dear Ma'am(s)/Sir(s):</p>
            <p>This is to provide the <b>final list of priority programs and projects (PAPs) endorsed by your Agency/Office</b> for inclusion in the <b>Updated 2017-2022 Public Investment Program (PIP)</b> and <b>Three (3)-Year Rolling Infrastructure Program (TRIP) Fiscal Year (FY) 2021-2023</b> as input to the FY 2021 Budget Preparation. Said PAPs were submitted by your Authorized Agency PIP/TRIP Focals through the Contract Management System following the deadline of submission on <b>October 25, 2019 and</b> generated as of <b>October 28, 2019</b>:</p>

            <center><strong>Completed PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <!-- <th>PIP Code</th> -->
                    <th>PAP Title</th>
                    <th>Sources of Financing</th>
                    <th>Spatial Coverage</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectCompleted_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <!-- <td><?php echo $agencyTier1->code; ?></td> -->
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td>
                        <?php foreach ($submission->getProjectFS_v2($agencyTier1->id) as $agencyFS){ ?>
                        <?php 
                        if($agencyFS->fsource_id == 1){
                            echo "NG-Local / LGU";
                        }elseif($agencyFS->fsource_id == 2){
                            echo "NG-ODA Loan";
                        }elseif($agencyFS->fsource_id == 3){
                            echo "NG-ODA Grant";
                        }elseif($agencyFS->fsource_id == 4){
                            echo "GOCC/GFI"; 
                        }elseif($agencyFS->fsource_id == 5){
                            echo "Private Sector";
                        }elseif($agencyFS->fsource_id == 6){
                            echo "Others";
                        } ?>
                        <?php } ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                </tr>
                <?php } ?>
            </table><br>

            <center><strong>Ongoing PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <!-- <th>PIP Code</th> -->
                    <th>PAP Title</th>
                    <th>Sources of Financing</th>
                    <th>Spatial Coverage</th>
                    <th>Total Project Cost</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectOngoing_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <!-- <td><?php echo $agencyTier1->code; ?></td> -->
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td>
                        <?php foreach ($submission->getProjectFS_v2($agencyTier1->id) as $agencyFS){ ?>
                        <?php 
                        if($agencyFS->fsource_id == 1){
                            echo "NG-Local / LGU";
                        }elseif($agencyFS->fsource_id == 2){
                            echo "NG-ODA Loan";
                        }elseif($agencyFS->fsource_id == 3){
                            echo "NG-ODA Grant";
                        }elseif($agencyFS->fsource_id == 4){
                            echo "GOCC/GFI"; 
                        }elseif($agencyFS->fsource_id == 5){
                            echo "Private Sector";
                        }elseif($agencyFS->fsource_id == 6){
                            echo "Others";
                        } ?>
                        <?php } ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                    <td>
                        <?php
                         $local_inv = $agencyTier1->projects_investments->sum('local');
                         $loan_inv = $agencyTier1->projects_investments->sum('loan');
                         $grant_inv = $agencyTier1->projects_investments->sum('grant');
                         $gocc_inv = $agencyTier1->projects_investments->sum('gocc');
                         $lgu_inv = $agencyTier1->projects_investments->sum('lgu');
                         $private_inv = $agencyTier1->projects_investments->sum('private');
                         $others_inv = $agencyTier1->projects_investments->sum('others');

                         $total_investment = $local_inv+$loan_inv+$grant_inv+$gocc_inv+$lgu_inv+$private_inv+$others_inv;

                         $local_infra = $agencyTier1->projects_infrastructures->sum('local');
                         $loan_infra = $agencyTier1->projects_infrastructures->sum('loan');
                         $grant_infra = $agencyTier1->projects_infrastructures->sum('grant');
                         $gocc_infra = $agencyTier1->projects_infrastructures->sum('gocc');
                         $lgu_infra = $agencyTier1->projects_infrastructures->sum('lgu');
                         $private_infra = $agencyTier1->projects_infrastructures->sum('private');
                         $others_infra = $agencyTier1->projects_infrastructures->sum('others');

                         $total_infra = $local_infra + $loan_infra+$grant_infra+$gocc_infra+$lgu_infra+$private_infra+$others_infra;

                             
                         if($total_investment == 0){
                            echo '₱'.number_format($total_infra,2);
                         }
                         else {
                            echo '₱'.number_format($total_investment,2);
                         }
                         
                        ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
            <br>
            <center><strong>Proposed PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <!-- <th>PIP Code</th> -->
                    <th>PAP Title</th>
                    <th>Sources of Financing</th>
                    <th>Spatial Coverage</th>
                    <th>Level of Readiness</th>
                    <th>Total Project Cost</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectProposed_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <!-- <td><?php echo $agencyTier1->code; ?></td> -->
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td>
                        <?php foreach ($submission->getProjectFS_v2($agencyTier1->id) as $agencyFS){ ?>
                        <?php 
                        if($agencyFS->fsource_id == 1){
                            echo "NG-Local / LGU";
                        }elseif($agencyFS->fsource_id == 2){
                            echo "NG-ODA Loan";
                        }elseif($agencyFS->fsource_id == 3){
                            echo "NG-ODA Grant";
                        }elseif($agencyFS->fsource_id == 4){
                            echo "GOCC/GFI"; 
                        }elseif($agencyFS->fsource_id == 5){
                            echo "Private Sector";
                        }elseif($agencyFS->fsource_id == 6){
                            echo "Others";
                        } ?>
                        <?php } ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                    <td>
                        <?php 
                        if($agencyTier1->tier2_status == 1){
                            echo "Level 1 - CIP";
                        }elseif($agencyTier1->tier2_status == 2){
                            echo "Level 2 - CIP";
                        }elseif($agencyTier1->tier2_status == 3){
                            echo "Level 3 - CIP";
                        }elseif($agencyTier1->tier2_status == 4){
                            echo "Level 4 - CIP"; 
                        }elseif($agencyTier1->tier2_status == 5){
                            echo "Level 1 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 6){
                            echo "Level 2 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 7){
                            echo "Level 3 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 8){
                            echo "Level 4 - Non-CIP";
                        } ?>
                    </td>

                    <td>
                        <?php
                         $local_inv = $agencyTier1->projects_investments->sum('local');
                         $loan_inv = $agencyTier1->projects_investments->sum('loan');
                         $grant_inv = $agencyTier1->projects_investments->sum('grant');
                         $gocc_inv = $agencyTier1->projects_investments->sum('gocc');
                         $lgu_inv = $agencyTier1->projects_investments->sum('lgu');
                         $private_inv = $agencyTier1->projects_investments->sum('private');
                         $others_inv = $agencyTier1->projects_investments->sum('others');

                         $total_investment = $local_inv+$loan_inv+$grant_inv+$gocc_inv+$lgu_inv+$private_inv+$others_inv;

                         $local_infra = $agencyTier1->projects_infrastructures->sum('local');
                         $loan_infra = $agencyTier1->projects_infrastructures->sum('loan');
                         $grant_infra = $agencyTier1->projects_infrastructures->sum('grant');
                         $gocc_infra = $agencyTier1->projects_infrastructures->sum('gocc');
                         $lgu_infra = $agencyTier1->projects_infrastructures->sum('lgu');
                         $private_infra = $agencyTier1->projects_infrastructures->sum('private');
                         $others_infra = $agencyTier1->projects_infrastructures->sum('others');

                         $total_infra = $local_infra + $loan_infra+$grant_infra+$gocc_infra+$lgu_infra+$private_infra+$others_infra;

                             
                         if($total_investment == 0){
                            echo '₱'.number_format($total_infra,2);
                         }
                         else {
                            echo '₱'.number_format($total_investment,2);
                         }
                         
                        ?>
                    </td>

                </tr>
                <?php } ?>
            </table>
            <br>            
            <br>
            <p>Further, below is/are the PAPs previously submitted as part of the 2017-2022 PIP but were not endorsed by your Agency/Office this time for inclusion in the <b>Updated 2017-2022 PIP and TRIP FY 2021-2023 as input to the FY 2021 budget preparation</b> either because the Agency PIP/TRIP Focal (a) dropped the PAP(s) from the list; and/or (b) did not update/complete the required information:  </p>

            <center><strong>Dropped PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PAP Title</th>
                    <th>Reason</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectDropped($submission->agency_id) as $agencyDropped){ ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyDropped->title ?></td>
                    <td>Dropped</td>
                </tr>
                <?php } ?>                
            </table>
            <br>

            <center><strong>Incomplete PAP Details</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PAP Title</th>
                    <th>Reason</th>
                </tr>
                <?php $a = 0; ?>                
                <?php foreach ($submission->getProjectDrafts($submission->agency_id) as $agencyDropped){ ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyDropped->title ?></td>
                    <td>Not updated/Incomplete PAP details</td>
                </tr>
                <?php } ?>
            </table>
            <br>

            <p>The NEDA Secretariat will validate the list of endorsed completed, ongoing and proposed PAPs, and may coordinate with your Office for any questions/clarifications on your submission, and submit the list to the concerned appropriate inter-agency body/committee/cluster, for confirmation. Subsequently, the list of confirmed PAPs will be provided to the Department of Budget and Management, as input to the FY 2021 budget preparation. </p>
            
            <p>Thank you and warm regards.</p>

            <br>

            <p>Very truly yours,<br>
            <b>PIP and TRIP Secretariat</b></p>

          </div>
        </div>
      </div>

      
</body>
</html>