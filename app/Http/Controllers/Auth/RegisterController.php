<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Alert;
use App\Document;
use App\Http\Requests;
use App\Product;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Mail;
use Carbon\Carbon;

class RegisterController extends Controller{

    public function showRegistrationForm(){
        return view('register');
    }

    public function register_user(Request $request){

        $token = $request->input('g-recaptcha-response');

        if($token){
            
            $user                   = new \App\Users();
            $user->lname            = request('1_loginLastname');
            $user->fname            = request('1_loginFirstname');
            $user->mname            = request('1_loginMiddlename');
            $user->office_staff     = request('1_loginOfficeStaff');
            $user->division_unit    = request('1_loginDivUnit');
            $user->position         = request('1_loginPosition');
            $user->email_address    = request('1_email');
            $user->telephone_number = request('1_loginTel');
            $user->gender           = request('1_loginGender');
            $user->account_status   = '0';
            $user->account_lock     = '0';
            $user->account_creation_date = Carbon::now();
            
            $type = request('1_loginContType');
                if(in_array('CONS',$type,true) && in_array('GIP',$type,true)){
                    $user->contract_type    = "ALL";
                }else if(in_array('GIP',$type,true)){
                    $user->contract_type    = "GIP";
                }else if(in_array('CONS',$type,true)){
                    $user->contract_type    = "CONS";
                }else{
                    $user->contract_type    = " ";
                }
            $user->save();

            return redirect('login')->with('regSuccess','Registration Successful. Please wait for an email of approval regarding your account');
        } else{
            
            return back()->withErrors([
                    'message' => 'Please fill up the captcha'
                ]);
        }
    }

    public function activate(Request $request, $id){
        
        \App\Users::where("user_id", $id)
        ->update(['account_status'=> '1']);

        $user = \App\Users::find($id);
        $emailofuser = $user->email_address;

        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->Username = $_ENV['MAIL_USERNAME'];
        $mail->Password = $_ENV['MAIL_PASSWORD'];
        $mail->IsHTML(true);
        
        $user = \App\Users::find($id);
        $generatedusername = $user->lname.".".$user->fname;
        $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $pin = mt_rand(100, 999) . mt_rand(100, 999) . $characters[rand(0, strlen($characters) - 1)] . $characters[rand(0, strlen($characters) - 1)];
        $generatedpassword = str_shuffle($pin);

        \App\Users::where("user_id", $id)
        ->update([
            'username' => $generatedusername,
            'password'=> bcrypt($generatedpassword),
            'access_type' => request('access_type'),
            'account_approval_date' => Carbon::now()]);

        ob_start(); //STARTS THE OUTPUT BUFFER
        include('approve.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        $some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        ob_clean();  //CLEAN OUT THE OUTPUT BUFFER
        $mail->Body = $some_page_contents;
        $mail->SetFrom('contracts.neda@gmail.com');
        $mail->Subject = 'CMS Account Activation';
        $mail->AddAddress($emailofuser);
        // $mail->AddCC("cms@neda.gov.ph");
        $mail->Send();
        
        Alert::success('Account Approved', '');

        return back()->with('approveSuccess','Account Approved');
    }

    public function decline(Request $request, $id){
        
        \App\Users::where("user_id", $id)
            ->update(['account_status'=> '2']);
    
        $user = \App\Users::find($id);
        $emailofuser = $user->email_address;

        $reason = request('reason');

        if($reason == "Others"){
            $reason = request('reason_others');
        }

        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->Username = $_ENV['MAIL_USERNAME'];
        $mail->Password = $_ENV['MAIL_PASSWORD'];
        $mail->IsHTML(true);
        $user = \App\Users::find($id);

        ob_start(); //STARTS THE OUTPUT BUFFER
        include('decline.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        $some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        ob_clean();  //CLEAN OUT THE OUTPUT BUFFER
        $mail->Body = $some_page_contents;
        $mail->SetFrom('contracts.neda@gmail.com');
        $mail->Subject = 'CMS Account Activation';
        $mail->AddAddress($emailofuser);
        $mail->Send();

        Alert::success('Account Declined', '');
        return back();
    }

    public function deactivate(Request $request, $id){
        \App\Users::where("user_id", $id)
            ->update(['account_status'=> '3']);

        Alert::success('Account Deactivated', '');
        return back();
    }

    public function reactivate(Request $request, $id){
        \App\Users::where("user_id", $id)
            ->update(['account_status'=> '4']);

        Alert::success('Account Reactivated', '');
        return back();
    }

}
