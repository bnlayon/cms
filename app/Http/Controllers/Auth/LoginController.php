<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Providers\RouteServiceProvider;
use Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('web');
        $this->middleware('guest')->except('logout');
    }

    public function login_user(Request $request) {
       
        $username = request('loginUsername');
        $password = request('loginPassword');
        $token = $request->input('g-recaptcha-response');
        
        if($token){
            if(!auth()->attempt(['username' => $username, 'password' => $password])) {
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
            return back()->withErrors([
                'message' => 'Please check your credentials and try again.'
            ]);        
                
            }

            $user = \App\Users::where('username', $username)->first();
            Session::put('username',$user->username);
            Session::put('access_type',$user->access_type);
            
            
            if($user->account_lock == 1) {
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
                return Redirect::back()->withErrors('Account is locked. Please contact CMS Administrator.');
                
            }

            if($user->account_status == 0) {
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
                return Redirect::back()->withErrors('Account is unapproved yet. Please contact CMS Administrator.');
                
            }

            if($user->is_first == 1){
               return redirect('/changepassword');
            }else{
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();     
                $logs->activity  = 'Login Successful';
                $logs->username  = $username;
                $logs->save();
                return redirect('dashboard');
            }
        }else{
            return back()->withErrors([
                    'message' => 'Please fill up the captcha'
            ]);
                $logs            = new \App\Systemlogs();
                $logs->ipaddress = request()->ip();
                $logs->activity  = 'Login Failed';
                $logs->username  = $username;
                $logs->save();
        }
        
    } 

    public function logout() {
        auth()->logout();
        Session::flush();
        return redirect('/');
    }

}
