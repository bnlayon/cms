<?php

namespace App\Http\Controllers;

use Alert;
use App\City;
use App\Province;
use Auth;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Mail;
use App\Http\Controllers\Controller;

class CMSController extends Controller
{
    public function home(){
        return view('/');
    }
    public function login(){
        
        return view('login');
    }
    
    public function register(){
        return view('register');
    }

    public function links(){
        $links = \App\Links::where('archive', '=', 0)->get();
        $links_archives = \App\Links::where('archive', '=', 1)->get();
        $agencytype = $this->getAgencyType();

        return view('links', compact('links', 'links_archives', 'agencytype'));
    }

    public function changepassword(){
        return view('changepassword');
    }

    public function changepassword_user(Request $request) {

        $user = auth()->user();

        if (!(Hash::check(request('oldPassword'), $user->password))) {
            return back()->withErrors([
                'message' => 'Your current password does not match with the password you provided.'
            ]); 
        }

        if(strcmp(request('newPassword'), request('confirmPassword')) != 0){
            return back()->withErrors([
                'message' => 'New Password and Confirm Password does not match.'
            ]); 
        }

        $user->password = bcrypt($request->get('newPassword'));
        if($user->is_first == 1)
            $user->is_first = 0;
        $user->save();
 
        return redirect('/dashboard');
    }

}
