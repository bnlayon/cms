<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HistoryController extends Controller
{
    public function view($Contract_ID){
        $Contract_ID = decrypt($Contract_ID);
        $history = \App\project_history::where('Contract_ID',$Contract_ID)->orderBy('History_Date','DESC')->get();

        return view('history', compact('history'));
    }
}
