<?php ini_set('max_execution_time', 6000); ?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
      <div class="container">
        <br>
        <div class="card">
          <div class="card-body">
            <center><img src="http://contracts.neda.gov.ph/img/headeremail.png" height="232px" align="center"></center><br/><br/>
            <h5><b><?php echo date("F d, Y"); ?></b></h5>
            <br>
            <span style="text-transform:uppercase;"><?php echo $submission->head_fname." ".$submission->head_mname." ".$submission->head_lname; ?></span><br>
            <?php 
            foreach ($submission->getAgencyDetails($submission->agency_id) as $agencydetail){
                echo $agencydetail->UACS_AGY_DSC;
            }
            ?>
            <?php if (!empty($submission->motheragency_id)){ ?>
            <br> <br>  
            <span style="text-transform:uppercase;"><?php echo $submission->mother_fname." ".$submission->mother_mname." ".$submission->mother_lname; ?></span><br>
            <?php 
            foreach ($submission->getAgencyDetails($submission->motheragency_id) as $agencydetail){
                echo $agencydetail->UACS_AGY_DSC;
            }
            ?>
            <?php } ?>
            <br> <br>  
            <p>Dear Ma'am(s)/Sir(s):</p>
            <p>This is to inform you that the following priority programs and projects (PAPs) were endorsed by your Agency/Office for inclusion in the Updated 2017-2022 Public Investment Program (PIP) and Three (3)-Year Rolling Infrastructure Program (TRIP) FY 2021-2023 as input to the Fiscal Year (FY) 2021 Budget Preparation. Said PAPs were submitted by your Authorized Agency PIP/TRIP Focal(s) through the PIP Online (PIPOL) System as of <b><?php echo date("F d, Y"); ?></b>:</p>
            <center><strong>Completed PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PIP Code</th>
                    <th>PAP Title</th>
                    <th>Mode of Implementation</th>
                    <th>Spatial Coverage</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectCompleted_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyTier1->code; ?></td>
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td>
                        <?php foreach ($submission->getProjectFS_v2($agencyTier1->id) as $agencyFS){ ?>
                        <?php 
                        if($agencyFS->fsource_id == 1){
                            echo "NG-Local / LGU";
                        }elseif($agencyFS->fsource_id == 2){
                            echo "NG-ODA Loan";
                        }elseif($agencyFS->fsource_id == 3){
                            echo "NG-ODA Grant";
                        }elseif($agencyFS->fsource_id == 4){
                            echo "GOCC/GFI"; 
                        }elseif($agencyFS->fsource_id == 5){
                            echo "Private Sector";
                        }elseif($agencyFS->fsource_id == 6){
                            echo "Others";
                        } ?>
                        <?php } ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                </tr>
                <?php } ?>
            </table><br>

            <center><strong>Ongoing PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PIP Code</th>
                    <th>PAP Title</th>
                    <th>Mode of Implementation</th>
                    <th>Spatial Coverage</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectOngoing_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyTier1->code; ?></td>
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td>
                        <?php foreach ($submission->getProjectFS_v2($agencyTier1->id) as $agencyFS){ ?>
                        <?php 
                        if($agencyFS->fsource_id == 1){
                            echo "NG-Local / LGU";
                        }elseif($agencyFS->fsource_id == 2){
                            echo "NG-ODA Loan";
                        }elseif($agencyFS->fsource_id == 3){
                            echo "NG-ODA Grant";
                        }elseif($agencyFS->fsource_id == 4){
                            echo "GOCC/GFI"; 
                        }elseif($agencyFS->fsource_id == 5){
                            echo "Private Sector";
                        }elseif($agencyFS->fsource_id == 6){
                            echo "Others";
                        } ?>
                        <?php } ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                </tr>
                <?php } ?>
            </table>
            <br>
            <center><strong>Proposed PAPs</strong></center>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PIP Code</th>
                    <th>PAP Title</th>
                    <th>Mode of Implementation</th>
                    <th>Spatial Coverage</th>
                    <th>Level of Readiness</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectProposed_v2($submission->agency_id) as $agencyTier1) { ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyTier1->code; ?></td>
                    <td><?php echo $agencyTier1->title; ?></td>
                    <td>
                        <?php foreach ($submission->getProjectFS_v2($agencyTier1->id) as $agencyFS){ ?>
                        <?php 
                        if($agencyFS->fsource_id == 1){
                            echo "NG-Local / LGU";
                        }elseif($agencyFS->fsource_id == 2){
                            echo "NG-ODA Loan";
                        }elseif($agencyFS->fsource_id == 3){
                            echo "NG-ODA Grant";
                        }elseif($agencyFS->fsource_id == 4){
                            echo "GOCC/GFI"; 
                        }elseif($agencyFS->fsource_id == 5){
                            echo "Private Sector";
                        }elseif($agencyFS->fsource_id == 6){
                            echo "Others";
                        } ?>
                        <?php } ?>
                    </td>
                    <td><?php echo $agencyTier1->spatial; ?></td>
                    <td>
                        <?php 
                        if($agencyTier1->tier2_status == 1){
                            echo "Level 1 - CIP";
                        }elseif($agencyTier1->tier2_status == 2){
                            echo "Level 2 - CIP";
                        }elseif($agencyTier1->tier2_status == 3){
                            echo "Level 3 - CIP";
                        }elseif($agencyTier1->tier2_status == 4){
                            echo "Level 4 - CIP"; 
                        }elseif($agencyTier1->tier2_status == 5){
                            echo "Level 1 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 6){
                            echo "Level 2 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 7){
                            echo "Level 3 - Non-CIP";
                        }elseif($agencyTier1->tier2_status == 8){
                            echo "Level 4 - Non-CIP";
                        } ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
            <br>            
            <br>
            <p>Further, below is/are the PAPs previously submitted as part of the 2017-2022 PIP as input to the FY 2020 budget preparation but were not endorsed by your Agency/Office this time for inclusion in the Updated 2017-2022 PIP and TRIP FY 2021-2023 as input to the FY 2021 budget preparation because the Agency PIP/TRIP Focal(s) (a) dropped the PAP(s) from the list; and/or (b) did not update/complete the required information in the PIPOL System:  </p>
            <table border="1" align="center">
                <tr>
                    <th>No.</th>
                    <th>PAP Title</th>
                    <th>Reason</th>
                </tr>
                <?php $a = 0; ?>
                <?php foreach ($submission->getProjectDropped($submission->agency_id) as $agencyDropped){ ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyDropped->title ?></td>
                    <td>Dropped</td>
                </tr>
                <?php } ?>
                <?php foreach ($submission->getProjectDrafts($submission->agency_id) as $agencyDropped){ ?>
                <tr>
                    <td><?php echo $a = $a + 1; ?></td>
                    <td><?php echo $agencyDropped->title ?></td>
                    <td>Not updated/Incomplete PAP details</td>
                </tr>
                <?php } ?>
            </table>
            <br>

            <p>Should you wish to revise/update the abovementioned PAPs submitted for inclusion in the Updated PIP and TRIP FY 2021-2023 as input to the FY 2021 budget preparation, the Authorized PIP/TRIP Focals will be given additional days <b><u>from October 17 to October 25, 2019 to access the PIPOL System and make the necessary changes in the Agency submission(s)</u></b>. After the said period, the list of priority PAPs saved as endorsed in the PIPOL System will be considered as the final PAPs submitted by the Agency for inclusion in the <b>Updated 2017-2022 PIP and TRIP FY 2021-2023 as input to the FY 2021 budget preparation</b>. We will also provide the summary of the final list of endorsed PAPs in the PIPOL System by <b>October 28, 2019</b>, for reference. </p>

            <p>For questions/concerns, please coordinate with the PIP and TRIP Secretariats through the following contact details:</p>

            <p>a)   <b>PIP Secretariat</b>: Email address: &pip@neda.gov.ph or telephone numbers: (02) 8631-0945 to 60 local no. 404; and </p>

            <p>b)   <b>TRIP Secretariat</b>: Email address: INFRACOM@neda.gov.ph or telephone numbers: (02) 8631-3724/ 8631-0945 to 60 local no. 312.</p>

            <p>Thank you and warm regards.</p>

            <br>

            <p>Very truly yours,<br>
            <b>PIP and TRIP Secretariat</b></p>

          </div>
        </div>
      </div>
</body>
</html>