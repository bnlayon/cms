<?php

namespace App\Observers;

use App\Contracts;

class ContractsObserver
{
    /**
     * Handle the contracts "created" event.
     *
     * @param  \App\Contracts  $contracts
     * @return void
     */
    public function created(Contracts $contracts)
    {
        //
    }

    /**
     * Handle the contracts "updated" event.
     *
     * @param  \App\Contracts  $contracts
     * @return void
     */
    public function updated(Contracts $contracts)
    {
        //
    }

    /**
     * Handle the contracts "deleted" event.
     *
     * @param  \App\Contracts  $contracts
     * @return void
     */
    public function deleted(Contracts $contracts)
    {
        //
    }

    /**
     * Handle the contracts "restored" event.
     *
     * @param  \App\Contracts  $contracts
     * @return void
     */
    public function restored(Contracts $contracts)
    {
        //
    }

    /**
     * Handle the contracts "force deleted" event.
     *
     * @param  \App\Contracts  $contracts
     * @return void
     */
    public function forceDeleted(Contracts $contracts)
    {
        //
    }
}
