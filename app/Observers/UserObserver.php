<?php

namespace App\Observers;

use App\Users;
use Mail;
use Auth;   

class UserObserver
{
    /**
     * Handle the users "created" event.
     *
     * @param  \App\Users  $users
     * @return void
     */
    public function created(Users $users)
    {
        $mail = new \PHPMailer\PHPMailer\PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = "smtp.gmail.com";
        $mail->Port = 465; // or 587
        $mail->Username = $_ENV['MAIL_USERNAME'];
        $mail->Password = $_ENV['MAIL_PASSWORD'];;
        $mail->IsHTML(true);

        $email = $users->email_address;
        $name = $users->lname.", ".$users->fname." ".$users->mname;
        $division = $users->division_unit;
        $office = $users->office_staff;
        $number = $users->telephone_number;
        $admin = \App\Users::where('access_type','=','A');

        ob_start(); //STARTS THE OUTPUT BUFFER
        include('newuser.blade.php');  //INCLUDES YOUR PHP PAGE AND EXECUTES THE PHP IN THE FILE
        $some_page_contents = ob_get_contents() ;  //PUT THE CONTENTS INTO A VARIABLE
        ob_clean();  //CLEAN OUT THE OUTPUT BUFFER
        $mail->Body = $some_page_contents;
        $mail->SetFrom('contracts.neda@gmail.com');
        $mail->Subject = 'CMS New Account for Approval';
        foreach($admin as $email){
            $mail->AddAddress($email);
        }
        $mail->Send();
    }

    /**
     * Handle the users "updated" event.
     *
     * @param  \App\Users  $users
     * @return void
     */
    public function updated(Users $users)
    {
        //
    }

    /**
     * Handle the users "deleted" event.
     *
     * @param  \App\Users  $users
     * @return void
     */
    public function deleted(Users $users)
    {
        //
    }

    /**
     * Handle the users "restored" event.
     *
     * @param  \App\Users  $users
     * @return void
     */
    public function restored(Users $users)
    {
        //
    }

    /**
     * Handle the users "force deleted" event.
     *
     * @param  \App\Users  $users
     * @return void
     */
    public function forceDeleted(Users $users)
    {
        //
    }
}
