<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		p{
			font-family: Arial, Helvetica, sans-serif;
		}
	</style>
</head>
<body>

<p>NOTICE TO ADMINISTRATOR - New Account for Approval </p>

<p><b>Email:</b> <?php echo $email; ?></p>

<p><b>Name:</b> <?php echo $name; ?></p>

<p><b>Division:</b><?php echo $division; ?></p>

<p><b>Staff:</b><?php echo $office; ?></p>

<p><b>Telephone Number:</b><?php echo $number; ?></p>

<p> <i>Go to <a href="http://contracts.neda.gov.ph">CMS Online</a> to your account to approve the above user.</i> </p>

<p><strong>Contract Management System</strong></p>


</body>
</html>