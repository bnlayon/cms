<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project_history extends Model
{
    public $table = "project_history";

    protected $primaryKey = 'History_ID';
}
