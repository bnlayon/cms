<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    public $table = "payment";
    protected $primaryKey = 'Payment_ID';
}
