<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class consultancy_firm extends Model
{
    public $table = "consultancy_firm";

    protected $primaryKey = 'Firm_ID';
}
