$(document).ready(function(){

    $("#newfirm").click(function(){
        $('#newFirmModal').modal('show');
    });
    $("#closenewfirm").click(function(){
        $('#newFirmModal').modal('hide');
    });

});

$(document).ready(function () {
    var maxField = 30; //Input fields increment limitation
    var addButton = $('#newexpertsButton'); //Add button selector
    var wrapper = $('#new_experts'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="newexperts[]" id="newexperts[]" size=50 required><input type="text" placeholder="Expert Role" name="newexpertsrole[]" id="newexpertsrole[]" size=30 required><a href="javascript:void(0);" class="remove_exp"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_exp', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

$(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#procjoinedButton'); //Add button selector
    var wrapper = $('#new_procjoined'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="procjoined[]" id="procjoined[]" size=30 required><a href="javascript:void(0);" class="remove_proc"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_proc', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

$(document).ready(function () {
    var maxField = 30; //Input fields increment limitation
    var addButton = $('#updexpertsButton'); //Add button selector
    var wrapper = $('#upd_experts'); //Input field wrapper
    var fieldHTML = '<div class="row"><div class="col"><label for="editcontractName">Expert</label><input class="form-control form-control-sm" type="text" name="updexperts[]" id="updexperts[]" size=50></div><div class="col"><label for="editcontractName">Expert Role</label><input class="form-control form-control-sm" type="text" placeholder="Expert Role" name="updexpertsrole[]" id="updexpertsrole[]" size=30></div><a href="javascript:void(0);" class="remove_procj"><i class="icon-close"></i></a></div><br>'; //New input field html 
    var x = parseInt($("#expcount").val()) +1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_procj', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

$(document).ready(function () {
    var maxField = 30; //Input fields increment limitation
    var addButton = $('#updexpertsButton1'); //Add button selector
    var wrapper = $('#upd_experts1'); //Input field wrapper
    var fieldHTML = '<div class="row"><div class="col"><label for="editcontractName">Expert</label><input class="form-control form-control-sm" type="text" name="updaddexperts[]" id="updaddexperts[]" size=50></div><div class="col"><label for="editcontractName">Expert Role</label><input class="form-control form-control-sm" type="text" placeholder="Expert Role" name="updaddexpertsrole[]" id="updaddexpertsrole[]" size=30></div><a href="javascript:void(0);" class="remove_procj"><i class="icon-close"></i></a></div><br>'; //New input field html 
    var x = parseInt($("#expcount").val()) +1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_procj', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

$(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#updprocjoinedbtn'); //Add button selector
    var wrapper = $('#upd_procjoined'); //Input field wrapper
    var fieldHTML = '<div class="row"><input class="form-control form-control-sm" type="text" name="updprocjoined[]" id="updprocjoined[]" size=70><a href="javascript:void(0);" class="remove_procj"><i class="icon-close"></i></a></div><br>'; //New input field html 
    var x = parseInt($("#proccount").val()) +1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_procj', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

$(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#updprocjoinedbtn1'); //Add button selector
    var wrapper = $('#upd_procjoined1'); //Input field wrapper
    var fieldHTML = '<div class="row"><input class="form-control form-control-sm" type="text" name="updprocjoined[]" id="updprocjoined[]" size=70><a href="javascript:void(0);" class="remove_procj"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = parseInt($("#proccount").val()) +1; //Initial field counter is 1
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_procj', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

function editFirm(id){
    $(id).modal('show');
}
function closeEditFirm(id){
    $(id).modal('hide');
}

function viewFirmDTL(id){
    $(id).modal('show');
}
function closeEditFirm(id){
    $(id).modal('hide');
}

function viewProc(id){
    $(id).modal('show');
}
function closeProc(id){
    $(id).modal('hide');
}

function viewLegal(id){
    $(id).modal('show');
}
function closeLegal(id){
    $(id).modal('hide');
}

function viewFin(id){
    $(id).modal('show');
}
function closeFinDocs(id){
    $(id).modal('hide');
}
