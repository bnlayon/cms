// $(document).on('click', '.viewModal', function(event) {
// 	event.preventDefault();
// 	//var table = $('.ftable').DataTable();
// 	$('#_id').hide();
// 	$('#receive').text('Receive');
// 	$('#close').hide();
// 	var stuff = $(this).data('info').split('\\');
// 	//console.log('modal clicked');
// 	fillmodalData(stuff);
// 	$.ajax({
// 		url: 'viewFile',
// 		type: 'POST',
// 		dataType: 'json',
// 		data: {
// 			'_token': $('input[name=_token]').val(),
// 			'doccode': $('#_doccode').val()
// 		},
// 		success: function(data) {
// 			//console.log(data);
// 			$.each(data,function(i,item) {
// 				 var markup = "<tr><td><a href=uploads\\" + item.filename + " target=_blank >" + item.filename +"</a></td>"
// 				 +"<td>"+ item.size +"</td>"+"</tr>";
// 				$('.ftable').append(markup);
// 			});
// 		}
// 	});
	
// 	$('#viewModal').modal('show');
// });

// function fillmodalData(details){
// 	$('#modTitle').text(details[0]);
// 	$('#doccodesp').text(details[1]);
// 	$('#projcodesp').text(details[2]);
// 	$('#datedocsp').text(details[3]);
// 	$('#doctypesp').text(details[4]);
// 	$('#fromsp').text(details[5]);
// 	$('#tosp').text(details[6]);
// 	$('#signatorysp').text(details[7]);
// 	$('#forsp').text(details[8]);
// 	$('#ccsp').text(details[9]);
// 	$('#subjsp').text(details[10]);
// 	$('#descsp').text(details[11]);
// 	$('#linkdocsp').text(details[12]);
// 	$('#doctracksp').text(details[13]);
// 	$('#dateassignsp').text(details[14]);
// 	$('#timesp').text(details[15]);
// 	$('#duesp').text(details[16]);
// 	$('#toofficesp').text(details[17]);
// 	$('#actionsp').text(details[18]);
// 	$('#_doccode').val(details[19]);
// 	$('#_id').val(details[20]);
// }

// $('.modal-footer').on('click', '#receive', function(event) {
// 	event.preventDefault();
// 	$.ajax({
// 			url: 'saveRemarks',
// 			type: 'POST',
// 			dataType: 'json',
// 			data: {
// 				'_token': $('input[name=_token]').val(),
// 				'id': $('#_id').val(),
// 				'remarks': $('#remarkstx').val()
// 			},
// 			success: function(data) {
// 				swal("Good job!", "Document Received!", "success");
// 			}
// 		});
	
// });

// $(document).ready(function() {
//     $('#incoming').DataTable({
//         responsive: true,
//         'order': [[ 0, 'DESC' ]]
//     });
// });
// $(document).ready(function() {
//     $('#outgoing').DataTable({
//         responsive: true,
//         'order': [[ 0, 'DESC' ]]
//     });
// });

$(document).ready(function($) {

	$('#incoming').DataTable({
        responsive: true,
        'order': [[ 0, 'DESC' ]]
    });

    $('#outgoing').DataTable({
        responsive: true,
        'order': [[ 0, 'DESC' ]]
    });

	var isDisabled = $('#textarea2').prop('disabled');
	if (isDisabled) {
		$('#fileAction').hide();
		$('.delete').hide();
	}else {
		$('#fileAction').show();
		$('.delete').show();
	}

	// var docuCode = $(this).find(".docuCode").text();
	$('.docuCode').each(function(index) {
		//console.log(index + $(this).text());
	});
	var d;
	$.ajax({
		url: './notReceivedDocs',
		type: 'GET',
		dataType: 'json',
		data: {
			'_token': $('input[name=_token]').val()
		},
		success: function(data) {
			$('.docuCode').each(function(index) {
				var doc = $(this).text();
				for (i = 0; i < data.length; i++) {
					//console.log(data[i]['doccode']);
					if (doc == data[i]['doccode']) {
					 	//console.log(doc);
					 	// $('#' + doc).prepend('<span class="h3 text-danger">*</span>');
					 	$('#' + doc).addClass('danger');
					}
				}
			});
		}
	});

});

$(document).on('click', '.showFileTable', function(event) {
	event.preventDefault();
	var stuff = $(this).data('info');
	//console.log(stuff);
	$('.did').val(stuff);
	$.ajax({
		url: './file',
		type: 'POST',
		dataType: 'json',
		data: {
			'_token': $('input[name=_token]').val(),
			'id'    : stuff
		},
		success: function(data) {
			//console.log(data[0]);
			var fileLocation = './storage/uploads/';
			$("#fileTableBody tr").remove();
			$.each(data, function(i, item) {
				var date = new Date(item.created_at);
				var dateFormatted = date.formatDate("MM/dd/yyyy");
				$('#fileTableBody').append(
					'<tr>' +
					'<td>'+ item.filename + '&nbsp;<a target="_blank" href="' + 
					fileLocation + item.doc_id + '/' + item.filename + 
					'"><i class="fa fa-eye"></i></a>' +'</td>' +
					'<td>'+ item.size +'</td>' +
					'<td>'+ dateFormatted +'</td>' +
				'</tr>');
			});
		}
	});	
});

$(document).on('click', '.showLinkedTable', function(event) {
	event.preventDefault();
	var docId = $(this).data('info');

	$('.did').val(docId);
	$.ajax({
		url: './linked',
		type: 'GET',
		dataType: 'json',
		data: {
			'_token': $('input[name=_token]').val(),
			'id'    : docId
		},
		success: function(data) {

			$("#documentTableBody tr").remove();

			$.each(data, function(i, item) {
				$('#documentTableBody').append(
					'<tr>' +
						'<td>'+ item.doccode + '&nbsp;<a href="'
						  + '"><i class="icon-eye-open"></i></a>' +
						'</td>' +
						'<td>'+ item.projcode +'</td>' +
						'<td>'+ item.datedoc +'</td>' +
						'<td>'+ item.subj +'</td>' +
						'<td>'+ item.doctype +'</td>' +
				'</tr>');
			});
		}
	});	
});

$(document).on('click', '#addfile', function(event) {
	event.preventDefault();
	var stuff = $(this).data('info');
	if (!$('#textarea2').prop('disabled')) {
		$('.delete').hide();
		$('#size').val(stuff);
		$('#fileModal').modal('show');
	}
});

$(document).on('click', '.delete', function(event) {
	event.preventDefault();
	var stuff = $(this).data('info');
	$.ajax({
		url: '../destroyer',
		type: 'POST',
		data: {
			'_token': $('input[name=_token]').val(),
			'id'    : stuff
		},
		success: function(data) {
			//console.log(data);
			window.location.reload();
		}
	});	
});


