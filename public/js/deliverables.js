$(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#deliverableButton'); //Add button selector
    var wrapper = $('#new_deliverable'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="deliverables[] id="deliverables[]" size=30 required> <input placeholder="Select Target Date" type="date" name="dates[]" id="dates[]"><input placeholder="Notes" type="text" name="dnotes[]" id="dnotes[]" size=40><a href="javascript:void(0);" class="remove_button"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = 3; //Initial field counter is 3
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 3 || x < maxField){
                addButton.show();
            } 
        });
});
