function ShowModalProject(id)
{
    $(id).modal('show');
}

function closeprjmodal(id){
  $(id).modal('hide');
}

function editTermination(id){
    $(id).modal('show');
}
function closeEditTermination(id){
    $(id).modal('hide');
}

function showAmendmentWindow(id){
    $(id).modal('show');
}

function shownotesWindow(id){
    $(id).modal('show');
}

function hidenotesWindow(id){
    $(id).modal('show');
}

function hideAmendmentWindow(id){
    $(id).modal('hide');
}

function viewSelection(id){
    $(id).modal('show');
}

function hideSelection(id){
    $(id).modal('hide');
}

function viewPS(id){
    $(id).modal('show');
}

function closePS(id){
    $(id).modal('hide');
}
$(document).ready(function () {
    $("#selectall").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
    })
})

function view(id){
    $(id).modal('show');
}

function close(id){
    $(id).modal('hide');
}

function minConformeEdit(startDate) {
    //disable previous dates for end date
    document.getElementById("ConformeDateEdit").min = startDate;
}

function minStartEdit(startDate) {
    //disable previous dates for end date
    document.getElementById("startDateEdit").min = startDate;
}

function minEndEdit(startDate) {
    //disable previous dates for end date
    document.getElementById("endDateEdit").min = startDate;
}

$(document).ready(function(){
    $(window).on('load', function () {
        $('#CashData').hide();
        $('#CheckPSData').hide();
        $('#DraftPSData').hide();
        $('#GuaranteePSData').hide();
        $('#ILCPSData').hide();
        $('#SuretyPSData').hide();
        $('.partnerfirms').hide();
    });

    $("select[name='editperfSecVal[]']").change(function () {
        var str = "";
        var str2 = "";
        $("select[name='editperfSecVal[]'] option:selected").each(function () {
            str = $(this).val();
            // alert(str);
            if (str == "C") {
                CashData.style.display = "block";
            } else if (str == "CMC") {
                CheckPSData.style.display = "block";
            } else  if (str == "BD") {
                DraftPSData.style.display = "block";
            } else if (str == "BG") {
                GuaranteePSData.style.display = "block";
            } else if (str == "ILC") {
                ILCPSData.style.display = "block";
            } else if (str == "SB") {
                SuretyPSData.style.display = "block";
            }
        });
        $("select[name='editperfSecVal[]'] option:not(:selected)").each(function () {
            str2 = $(this).val();

            if (str2 == "C") {
                // alert(str);
                CashData.style.display = "none";
            } else if (str2 == "CMC") {
                CheckPSData.style.display = "none";
            } else if (str2 == "BD") {
                DraftPSData.style.display = "none";
            } else if (str2 == "BG") {
                GuaranteePSData.style.display = "none";
            } else if (str2 == "ILC") {
                ILCPSData.style.display = "none";
            } else if (str2 == "SB") {
                SuretyPSData.style.display = "none";
            }
        });
    });
});

// $(document).ready(function () {
//     $("input:checkbox").click(function(){
//         $('#all').not(this).prop('checked', this.checked);
//     })
// });

$(document).ready(function () {
    $('#allexcel').click(function(){
        $('.checkexcel').not(this).prop('checked', this.checked);
    })
});

$(document).ready(function(){
   
    var val = $("#initialType").val();
    if(val == "Single"){
       $('.role').hide();
        $('.others').hide();
    }
    
 });

 $(document).ready(function () {
    var maxField = 10; //Input fields increment limitation
    var addButton = $('#deliverableButtonedit'); //Add button selector
    var wrapper = $('#new_deliverable'); //Input field wrapper
    var fieldHTML = '<div class="row"><div class="col"><div class="form-group"><label for="editdeliverables">Deliverable:</label><input class="form-control form-control-sm" type="text" aria-label="Default" name="editdeliverables[]" id="editdeliverables[]"> </div></div><div class="col"><div class="form-group"><label for="editdeliverablesTD">Target Date:</label><input class="form-control form-control-sm" type="date" aria-label="Default" name="editdeliverablesTD[]" id="editdeliverablesTD[]"></input></div></div><div class="col"><div class="form-group"><label for="editdeliverablesTDNotes">Notes:</label><input class="form-control form-control-sm" type="text" aria-label="Default" name="editdeliverablesTDNotes[]" id="editdeliverablesTDNotes[]"></div></div><a href="javascript:void(0);" class="remove_button"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = $("#delintcnt").val(); //Initial field counter is 3
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.remove_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 3 || x < maxField){
                addButton.show();
            } 
        });
});


 $(document).ready(function () {
    var maxField = 30; //Input fields increment limitation
    var addButton = $('#expertsButtonedit'); //Add button selector
    var wrapper = $('#new_expertsEdit'); //Input field wrapper
    var fieldHTML = '<div class="row"><div class="col"><div class="form-group"><label for="contractType">Expert Name</label><input class="form-control form-control-sm"  type="text" size=50 name="expname[]" id="expname[]" required></div></div><div class="col"><div class="form-group"><label for="contractType">Expert Role</label><input class="form-control form-control-sm"  type="text" name="exprole[]" id="exprole[]" size=50 required><small id="emailHelp" class="form-text text-muted">Input <strong>N/A</strong> when no roles indicated. Do not leave blank</small></div></div> <a href="javascript:void(0);" class="removeexp_button"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = $("#expintcnt").val(); //Initial field counter is 3
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.removeexp_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

function editaddpartners(count) {
    $('.editpartnerfirms').hide().slice(0, count).show();
}

$(function(){
    $("input[name='reportDataView[]']").change(function(){
      var len = $("input[name='reportDataView[]']:checked").length;
      if(len == 0){
        $("input[name='selectViewBtn']").prop("disabled", true);
      }else{
        $("input[name='selectViewBtn']").removeAttr("disabled");
      }
    });
    $("input[name='reportDataView[]']").trigger('change');
  });

  $(function(){
    $("input[name='reportDataView1[]']").change(function(){
      var len = $("input[name='reportDataView1[]']:checked").length;
      if(len == 0){
        $("input[name='selectViewBtn1']").prop("disabled", true);
      }else{
        $("input[name='selectViewBtn1']").removeAttr("disabled");
      }
    });
    $("input[name='reportDataView1[]']").trigger('change');
  });
