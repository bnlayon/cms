$(document).ready(function(){
    $('select[multiple]').multiselect();
});

$(document).ready(function () {
    $('#modalctype').attr('hidden',false);
    window.addEventListener("load",function(){
        $('#modalctype').modal("show");
    });
    
    $(window).on('load', function () {
        $('#ContractConsulting').hide();
        $('newFirmModal').hide();
        $("#performanceSecModal").modal("hide"); 
        $("#ChoicePSModal").modal("hide");
        $('#notification').modal("show");
    });

    $('#conType').click(function () {
        var radioValue = $("input[name='CType']:checked").val();

        if (radioValue == 'CONS') {

            $('#modalctype').modal('hide');
            $('#ContractConsulting').show();
            $('#others').hide();
            $('#OtherFirm2').hide();
            $('#OtherFirm3').hide();
            $('#OtherFirm4').hide();
            $('#OtherFirm5').hide();
            $('.partnerfirms').hide();
            $('.role').hide();
            $('.others').hide();
            contractType();

            document.getElementById("hiddenCType").value = radioValue;

        } else {
            window.alert("Contract Type not yet available");
        }
    });

    // disable e
    var costinput = document.getElementById('contractCost');
    costinput.addEventListener("keydown", function(e) {
        // prevent: "e", "=", ",", "-", "."
        if ([69, 187, 189].includes(e.keyCode)) {
          e.preventDefault();
        }
    });
    var abc = document.getElementById('ABC');
    abc.addEventListener("keydown", function(e) {
        // prevent: "e", "=", ",", "-", "."
        if ([69, 187, 189].includes(e.keyCode)) {
          e.preventDefault();
        }
    });
    
    //performance security options
    $("#perfSec").click(function () {
        $("#performanceSecModal").modal("show");
        $("#CashData").hide();
        $("#CheckPSData").hide();
        $("#DraftPSData").hide();
        $("#GuaranteePSData").hide();
        $("#ILCPSData").hide();
        $("#SuretyPSData").hide();
    });

    $('#perfSecSelect').click(function(){
        var selectedperf = $('#perfSecVal').val();
        $("#performanceSecModal").modal("hide");
        $('#selectedperfsec').text(selectedperf);
    });
    
    $("#closemodal1").click(function () {
        $("#performanceSecModal").modal('hide');
    });
    $("#closemodal2").click(function () {
        $("#ChoicePSModal").modal("hide");
    });

    $("#ChoicePSModalBtn").click(function(){
        $("#performanceSecModal").modal("hide");
        $("#ChoicePSModal").modal("hide");
    });

    $("#contractData").validate({
        rules:{
            perfSecVal: "required",
            CashOR:{
                required:function(){
                     if($("#perfSecVal").val() == "C"){
                        return true;
                     }
                }
            },
            CheckNum:{
                required:function(){
                    if($("#perfSecVal").val() == "CMC"){
                        return true;
                    }
                }
            },
            CheckOR:{
                required:function(element){
                    if($("#perfSecVal").val() == "CMC"){
                        return true;
                    }
                }
            },
            CheckBank:{
                required:function(element){
                    if($("#perfSecVal").val() == "CMC"){
                        return true;
                    }
                }
            },
            BankNameBD:{
                required:function(element){
                    if($("#perfSecVal").val() == "BD"){
                        return true;
                    }
                }
            },
            DraftBD:{
                required:function(element){
                    if($("#perfSecVal").val() == "BD"){
                        return true;
                    }
                }
            },
            ValidityBD:{
                required:function(element){
                    if($("#perfSecVal").val() == "BD"){
                        return true;
                    }
                }
            },
            BankNameBG:{
                required:function(element){
                    if($("#perfSecVal").val() == "BG"){
                        return true;
                    }
                }
            },
            DraftBG:{
                required:function(element){
                    if($("#perfSecVal").val() == "BG"){
                        return true;
                    }
                }
            },
            ValidityBG:{
                required:function(element){
                    if($("#perfSecVal").val() == "BG"){
                        return true;
                    }
                }
            },
            BankNameILC:{
                required:function(element){
                    if($("#perfSecVal").val() == "ILC"){
                        return true;
                    }
                }
            },
            ILC:{
                required:function(element){
                    if($("#perfSecVal").val() == "ILC"){
                        return true;
                    }
                }
            },
            ValidityILC:{
                required:function(element){
                    if($("#perfSecVal").val() == "ILC"){
                        return true;
                    }
                }
            },
            CNameSurety:{
                required:function(element){
                    if($("#perfSecVal").val() == "SB"){
                        return true;
                    }
                }
            },
            SuretyNum:{
                required:function(element){
                    if($("#perfSecVal").val() == "SB"){
                        return true;
                    }
                }
            },
            ValiditySurety:{
                required:function(element){
                    if($("#perfSecVal").val() == "SB"){
                        return true;
                    }
                }
            }
        },
        messages:{
            perfSecVal:{
                required:"Performance Security Details REQUIRED!",
            },
            CashOR:{
                required:"Cash Details REQUIRED!",
            },
            CheckNum:{
                required:"Check Details REQUIRED!",
            },
            CheckOR:{
                required:"Check Details REQUIRED!",
            },
            CheckBank:{
                required:"Check Details REQUIRED!",
            },
            BankNameBD:{
                required:"Bank Draft Details REQUIRED!",
            },
            DraftBD:{
                required:"Bank Draft Details REQUIRED!",
            },
            ValidityBD:{
                required:"Bank Draft Details REQUIRED!",
            },
            BankNameBG:{
                required:"Bank Guarantee Details REQUIRED!",
            },
            DraftBG:{
                required:"Bank Guarantee Details REQUIRED!",
            },
            ValidityBG:{
                required:"Bank Guarantee Details REQUIRED!",
            },
            BankNameILC:{
                required:"ILC Details REQUIRED!",
            },
            ILC:{
                required:"ILC Details REQUIRED!",
            },
            ValidityILC:{
                required:"ILC Details REQUIRED!",
            },
            CNameSurety:{
                required:"Surety Bond Details REQUIRED!",
            },
            SuretyNum:{
                required:"Surety Bond Details REQUIRED!",
            },
            ValiditySurety:{
                required:"Surety Bond Details REQUIRED!",
            }
        },
        errorPlacement:function(error,element){
            error.insertAfter(element);
            alert(error.html());
        }
    });

});

$(document).ready(function () {
    $("select[name='perfSecVal[]']").change(function () {
            var str = "";
            var str2 = "";
            $("select[name='perfSecVal[]'] option:selected").each(function () {
                str = $(this).val();
                // alert(str);
                if (str == "C") {
                    CashData.style.display = "block";
                } else if (str == "CMC") {
                    CheckPSData.style.display = "block";
                } else  if (str == "BD") {
                    DraftPSData.style.display = "block";
                } else if (str == "BG") {
                    GuaranteePSData.style.display = "block";
                } else if (str == "ILC") {
                    ILCPSData.style.display = "block";
                } else if (str == "SB") {
                    SuretyPSData.style.display = "block";
                }
            });
            $("select[name='perfSecVal[]'] option:not(:selected)").each(function () {
                str2 = $(this).val();

                if (str2 == "C") {
                    // alert(str);
                    CashData.style.display = "none";
                } else if (str2 == "CMC") {
                    CheckPSData.style.display = "none";
                } else if (str2 == "BD") {
                    DraftPSData.style.display = "none";
                } else if (str2 == "BG") {
                    GuaranteePSData.style.display = "none";
                } else if (str2 == "ILC") {
                    ILCPSData.style.display = "none";
                } else if (str2 == "SB") {
                    SuretyPSData.style.display = "none";
                }
            });
        });
});

$(document).ready(function () {
    var maxField = 30; //Input fields increment limitation
    var addButton = $('#expertsButton'); //Add button selector
    var wrapper = $('#new_experts'); //Input field wrapper
    var fieldHTML = '<div><input type="text" name="experts[]" id="experts[]" size="50" required><input type="text" placeholder="Expert Role" name="expertsrole[]" id="expertsrole[]" size=30 required> <a href="javascript:void(0);" class="removeexp_button"><i class="icon-close"></i></a></div>'; //New input field html 
    var x = 1; //Initial field counter is 3
        //Once add button is clicked
        $(addButton).click(function(){
            //Check maximum number of input fields
            if(x < maxField){ 
                x++; //Increment field counter
                $(wrapper).append(fieldHTML); //Add field html
                if(x == maxField){
                    addButton.hide();
                }
            }
        });
        $(wrapper).on('click', '.removeexp_button', function(e){
            e.preventDefault();
            $(this).parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            if(x == 1 || x < maxField){
                addButton.show();
            } 
        });
});

function minConforme(startDate) {
    //disable previous dates for end date
    document.getElementById("conformeDate").min = startDate;
}

function minStart(startDate) {
    //disable previous dates for end date
    document.getElementById("contractStartDate").min = startDate;
}

function minEnd(startDate) {
    //disable previous dates for end date
    document.getElementById("contractEndDate").min = startDate;
}


function setFirmType(selectedFirm) {
    if (selectedFirm == 'S') {
        $('.role').hide();
        $('.others').hide();
    } else {
        $('.others').show();
        $('.role').show();
    }
}
// add partner firms
function addpartners(count) {
    $('.partnerfirms').hide().slice(0, count).show();
}

//set contract id
function contractType() {
    var ele = document.getElementsByName('CType');
    var counter = document.getElementById("contractIDCounter").value;

    for (i = 0; i < ele.length; i++) {
        if (ele[i].checked) {
            document.getElementById("selectedCType").innerHTML = ele[i].value;
            document.getElementById("contractID").value += ele[i].value += counter;
        }
    }
}

function setExperts(id){
    document.getElementById("expid").value = id;
}
