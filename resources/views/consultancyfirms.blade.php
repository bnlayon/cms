@extends ('layouts')

@section ('content')
    
    @if(Session::has('consSuccess'))
        <div class="alert alert-success">
            {{ session()->get('consSuccess') }}
        </div>
    @endif
    
    <section class="firms-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <div class="col-lg-12 col-md-12">
                    <h2 style="display:inline;float:left;" class="display h4">CONSULTANCY FIRMS</h2><br>
                    @if(Auth::user()->access_type=='A1')
                    <span style="float:right;"><a href="{{ asset('/consultancyfirms/add')}}" target="_blank" id="newfirm" >ADD NEW FIRM</a><br>
                    @endif
                 </div>
            </div>
            <table id="firmslist" class="table table-bordered gridview">
                <thead class="table thead">
                    <tr>
                        <th>CONSULTANCY FIRM </th>
                        <th>PROCUREMENTS</th>
                        @if(Auth::user()->access_type=='A1')
                        <th>DOCUMENTS</th>
                        <th>ACTION</th>
                        @endif
                    </tr>
                </thead>
                <tbody class="table tbody">
                    @if(count($firms) == 0)
                        <tr>
                            <td colspan=4 style="text-align:center"> <?php echo "No data available." ?></td>
                        </tr>
                    @else
                        @foreach($firms as $data)
                            <tr>
                                <td> 
                                    <a href="javascript:void(0);" id="viewfirmdtl" title="Firm Details"data-toggle="modal" data-target="#viewfirmdtlmodal{{$data -> Firm_ID}}" onclick="java_script_:viewFirmDTL('#viewfirmdtlmodal{{$data -> Firm_ID}}')"><?php echo $data -> Firm_Name ?></a> 
                                    <div id="viewfirmdtlmodal{{$data -> Firm_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Firm Details:</strong>
                                                    <button type="button" data-dismiss="modal" id="closeviewprocmodal" aria-label="Close" class="close"><span
                                                                aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        <label for="ProcMode">Firm Name</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> Firm_Name}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Firm Address</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> Address}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Email Address</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> email_address}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Alternate Email Address1</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> alt_email1}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Alternate Email Address 2</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> alt_email2}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Alternate Email Address 3</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> alt_email3}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Contact Person</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> contact_person}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Designation</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> designation}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Telephone</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> telephone}}" disabled>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="ProcMode">Fax Number</label>
                                                        <br>
                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                            value="{{$data -> fax_number}}" disabled>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="ProcMode">Expert/s and Role/s</label>
                                                        @foreach($experts as $expert)
                                                            @if($data->Firm_ID == $expert->Firm_ID)
                                                                <div class="row">
                                                                    <div class="col">
                                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                                            value="{{$expert->expert_name}}" disabled>
                                                                    </div>
                                                                    <div class="col">
                                                                        <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                                            value="{{$expert->expert_role}}" disabled>
                                                                    </div>
                                                                </div>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                               <td> 
                                    <a href="javascript:void(0);" id="viewproc" data-toggle="modal" data-target="#viewprocmodal{{$data -> Firm_ID}}" onclick="java_script_:viewProc('#viewprocmodal{{$data -> Firm_ID}}')">VIEW</a> 
                                    <div id="viewprocmodal{{$data -> Firm_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <form action="post">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <strong>Procurements:</strong>
                                                        <button type="button" data-dismiss="modal" id="closeviewprocmodal" aria-label="Close" class="close" onclick="java_script_:closeProc('#viewprocmodal{{$data -> Firm_ID}}')"><span
                                                                aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-borderless">
                                                        <?php $count = 1;  ?>
                                                            @foreach($procsjoined as $procs)
                                                                @if(!is_null($procs->procurement))
                                                                    @if($data->Firm_ID == $procs->Firm_ID)
                                                                        <div class="row">
                                                                            <div class="col">
                                                                                    <?php echo $count; $count+=1; ?>
                                                                            </div>
                                                                            <div class="col">
                                                                                <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                                                                    value="{{$procs->procurement}}" disabled>
                                                                            </div>
                                                                        </div>
                                                                    @endif
                                                                @endif
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                @if(Auth::user()->access_type=='A1')
                                <td>
                                    <a href="javascript:void(0);" title="Legal Documents" data-toggle="modal" data-target="#viewlegalmodal{{$data -> Firm_ID}}" onclick="java_script_:viewLegal('#viewlegalmodal{{$data -> Firm_ID}}')">Legal</a><br>
                                    <div id="viewlegalmodal{{$data -> Firm_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                                <div class="modal-content text-center">
                                                <div class="modal-header">
                                                    <strong>LEGAL DOCUMENTS:</strong>
                                                    <button type="button" data-dismiss="modal" id="closelegalmod" aria-label="Close" class="close"  onclick="java_script_:closeLegal('#viewlegalmodal{{$data -> Firm_ID}}')"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col">
                                                            PHILGEPS:
                                                        </div>
                                                        <div class="col">
                                                                @foreach($docs1 as $doc1)
                                                                    @if($data->Firm_ID == $doc1->Firm_ID)
                                                                        <a href="{{url('/storage/storage/PHILGEPS')}}/{{basename($doc1->Document_Link)}}" target="_blank">{{basename($doc1->Document_Link)}}</a><br>
                                                                    @endif
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            SEC/DTI:
                                                        </div>
                                                        <div class="col">
                                                                @foreach($docs2 as $doc2)
                                                                    @if($data->Firm_ID == $doc2->Firm_ID)
                                                                        <a href="{{url('/storage/storage/SEC_DTI')}}/{{basename($doc2->Document_Link)}}" target="_blank">{{basename($doc2->Document_Link)}}</a><br>
                                                                    @endif
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            Business Permit:
                                                        </div>
                                                        <div class="col">
                                                                @foreach($docs3 as $doc3)
                                                                    @if($data->Firm_ID == $doc3->Firm_ID)
                                                                        <a href="{{url('/storage/storage/BusinessPermit')}}/{{basename($doc3->Document_Link)}}" target="_blank">{{basename($doc3->Document_Link)}}</a><br>
                                                                    @endif
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            Tax Clearance:
                                                        </div>
                                                        <div class="col">
                                                                @foreach($docs4 as $doc4)
                                                                    @if($data->Firm_ID == $doc4->Firm_ID)
                                                                        <a href="{{url('/storage/storage/TaxClearance')}}/{{basename($doc4->Document_Link)}}" target="_blank">{{basename($doc4->Document_Link)}}</a><br>
                                                                    @endif
                                                                @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="javascript:void(0);" title="Financial Documents"  data-toggle="modal" data-target="#viewFinDocs{{$data -> Firm_ID}}" onclick="java_script_:viewFin('#viewFinDocs{{$data -> Firm_ID}}')">Financial</a>
                                    <div id="viewFinDocs{{$data -> Firm_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <input type="hidden" name="firmid" id="firmid" value="{{$data -> Firm_ID}}">
                                                <div class="modal-content text-center">
                                                    <div class="modal-header">
                                                        <strong>FINANCIAL DOCUMENTS:</strong>
                                                        <button type="button" data-dismiss="modal" id="closeupdfirm" aria-label="Close" class="close" onclick="java_script_:closeFinDocs('#viewFinDocs{{$data -> Firm_ID}}')"><span
                                                                aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            @foreach($findocs as $fdocs)
                                                                @if($data->Firm_ID == $fdocs->Firm_ID)
                                                                    <a href="{{url('/storage/storage/TaxClearance')}}/{{basename($fdocs->Document_Link)}}" target="_blank">{{basename($fdocs->Document_Link)}}</a><br>
                                                                @endif
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                <td> 
                                    <?php 
                                        $id = $data -> Firm_ID;
                                        $id = encrypt($id);
                                    ?>
                                    <a href="{{ asset('/consultancyfirms') }}/{{$id}}" id="updatefirm" >UPDATE</a>
                                </td>
                                @endif
                            </tr>
                        @endforeach
                    @endif
                </tbody> 
            </table> 
        </div>
    </section>


@endsection