@extends ('layouts')

@section ('content')
    @if(is_null(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
    @endif
    <section class="firms-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <div class="col-lg-8 col-md-8">
                    <h2 class="display h4">REPORTS DASHBOARD</h2>
                </div>
            </div>
            <div style="float:right">DOWNLOAD REPORT</div><br>
                <div style="float:right">
                    <a href="javascript:void(0);"onclick="java_script_:view('#ReportSelect')" data-id="reportselect" id="ReportSelectlink" name="ReportSelectlink"  data-toggle="modal" data-target="#ReportSelect">
                        <img src="{{ asset('img/pdf.png') }}" height=35px width=30px title="Download PDF Report">
                    </a>
                    <div id="ReportSelect" name="ReportSelect" role="dialog" class="modal modal-open">
                        <div class="modal-dialog">
                            <form method="post" id="selectViewForm" target="_blank"
                                action="{{ asset('/reports-dashboard/reports')}}" novalidate=""
                                enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <strong>Select Data to view:</strong>
                                        <button type="button" data-dismiss="modal"
                                            onclick="java_script_:hide('#ReportSelect')"
                                            id="closemodalreportoption" aria-label="Close" class="close"><span
                                                aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-borderless">
                                            <tbody style="text-align:left; padding:0;margin:0">
                                                <tr >
                                                    <td colspan = 4> <small id="emailHelp" class="form-text text-muted"><strong>NOTE:</strong> Maximum of 7 fields ONLY</small></td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="PrjName"></td>
                                                    <td>Project Name</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="FundSource"></td>
                                                    <td>Fund Source</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="ConsultancyFirm"></td>
                                                    <td>Consultancy Firm</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="FundYr"></td>
                                                    <td>Fund Year</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="FirmType"></td>
                                                    <td>Firm Type</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="Performance"></td>
                                                    <td>Performance Security</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="EU"></td>
                                                    <td>End User</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="BondValidity"></td>
                                                    <td>Bond Validity</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="IAgency"></td>
                                                    <td>Implementing Agency</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="IssuingCompany"></td>
                                                    <td>Issuing Company</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="CoIAgency"></td>
                                                    <td>co-Implementing Agency</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="SuppDocs"></td>
                                                    <td>Supporting Documents</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="NOADt"></td>
                                                    <td>Notice of Acceptance Date</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="Deliverables"></td>
                                                    <td>Deliverables</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="NTPDt"></td>
                                                    <td>Notice to Proceed Date</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="PmtStat"></td>
                                                    <td>Payment Status</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="NTPConfDt"></td>
                                                    <td>NTP Conforme Date</td>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="PrjStat"></td>
                                                    <td>Project Status</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="CStartDt"></td>
                                                    <td>Contract Start Date</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="CEndDt"></td>
                                                    <td>Contract End Date</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="CCost"></td>
                                                    <td>Contract Cost</td>
                                                </tr>
                                                <tr>
                                                    <td><input type="checkbox" name="reportDataView[]"
                                                            id="reportDataView[]" value="CABC"></td>
                                                    <td>Approved Budget for Contract(ABC)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" id="selectViewBtn" name="selectViewBtn"
                                            class="btn btn-primary" disabled  value="View Report">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>&nbsp&nbsp&nbsp&nbsp
                    <a href="javascript:void(0);" onclick="java_script_:view('#ReportSelect1')" data-id="reportselect1" id="ReportSelectlink1" name="ReportSelectlink1"  data-toggle="modal" data-target="#ReportSelect1">
                        <img src="{{ asset('img/excel.png') }}" title="Download Excel Report" height="35px" width="30px">
                    </a>
                    <div id="ReportSelect1" name="ReportSelect1" role="dialog" class="modal modal-open">
                        <div class="modal-dialog">
                            <form method="post" id="selectViewForm1" target="_blank"
                                action="{{asset('/reports-dashboard/reportsexcel')}}" novalidate=""
                                enctype="multipart/form-data">
                                {{csrf_field()}}

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <strong>Select Data to view:</strong>
                                        <button type="button" data-dismiss="modal"
                                            onclick="java_script_:hide('#ReportSelect')"
                                            id="closemodalreportoption" aria-label="Close" class="close"><span
                                                aria-hidden="true">×</span></button>
                                    </div>
                                    <div class="modal-body">
                                        <table class="table table-borderless">
                                            <tbody style="text-align:left; padding:0;margin:0">
                                                <tr>
                                                    <td><input type="checkbox" name="allexcel" id="allexcel"  onchange="document.getElementById('selectViewBtn1').disabled = !this.checked;"></td>
                                                    <td>Select All</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="PrjName"></td>
                                                    <td>Project Name</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="FundSource"></td>
                                                    <td>Fund Source</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="ConsultancyFirm"></td>
                                                    <td>Consultancy Firm</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="FundYr"></td>
                                                    <td>Fund Year</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="FirmType"></td>
                                                    <td>Firm Type</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="Performance"></td>
                                                    <td>Performance Security</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="EU"></td>
                                                    <td>End User</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="BondValidity"></td>
                                                    <td>Bond Validity</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="IAgency"></td>
                                                    <td>Implementing Agency</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="IssuingCompany"></td>
                                                    <td>Issuing Company</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="CoIAgency"></td>
                                                    <td>co-Implementing Agency</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="SuppDocs"></td>
                                                    <td>Supporting Documents</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="NOADt"></td>
                                                    <td>Notice of Acceptance Date</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="Deliverables"></td>
                                                    <td>Deliverables</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="NTPDt"></td>
                                                    <td>Notice to Proceed Date</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="PmtStat"></td>
                                                    <td>Payment Status</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="NTPConfDt"></td>
                                                    <td>NTP Conforme Date</td>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="PrjStat"></td>
                                                    <td>Project Status</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="CStartDt"></td>
                                                    <td>Contract Start Date</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="CEndDt"></td>
                                                    <td>Contract End Date</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="CCost"></td>
                                                    <td>Contract Cost</td>
                                                </tr>
                                                <tr>
                                                    <td><input class="checkexcel" type="checkbox" name="reportDataView1[]"
                                                            id="reportDataView1[]" value="CABC"></td>
                                                    <td>Approved Budget for Contract(ABC)</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="modal-footer">
                                        <input type="submit" id="selectViewBtn1" name="selectViewBtn1"
                                            class="btn btn-primary" disabled  value="View Report">
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div> <br><br>
                <table id="reportdashboard" name="reportdashboard" class="table table-bordered gridview">
                    <thead class="table thead">
                        <tr>
                            <th>PROJECT/CONTRACT ID</th>
                            <th>PROJECT TITLE</th>
                            <th>CONTRACT AMOUNT</th>
                            <th>CONSULTANT/S</th>
                            <th>PROJECT STATUS</th>
                            <th>PAYMENT STATUS</th>
                            <th>DETAILS</th>
                        </tr>
                    </thead>
                    <tbody class="table tbody">
                    @if(count($contract)==0)
                    <tr>
                        <td colspan=6  style="text-align:center">No data available</td> 
                    </tr>
                    @else
                        @foreach($contract as $index => $value)
                        <tr>
                            <td> {{$value->Contract_ID}}  </td>
                            <td> {{$value->Contract_Name}} </td>
                            <td> <?php echo number_format($value->Contract_Cost,2)?> </td>
                            <td> 
                                @foreach($firmname as $firm)
                                    @if($contract[$index]->Contract_ID == $firm->Contract_ID)
                                        {{$firm->Firm_Name}}<br>
                                    @endif
                                @endforeach
                            </td>
                            <td> {{$value->Contract_Status}} </td>
                            <td> 
                                
                                <?php 
                                        $deliverables = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->get();
                                        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->where('Payment_Status','Paid')->count();
                                        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->where('Payment_Status','Ongoing')->count();
                                        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->whereNull('Payment_Status')->count();
                                    ?>

                                    <?php $stopforongoing = 0; ?>
                                    @foreach($deliverables as $deliverable)
                                        @if($deliverable->Payment_Status == 'Ongoing')
                                            {{ $deliverable->Payment_Status }}
                                            <?php $stopforongoing = 1; ?>                                        
                                        @endif
                                        <?php if($stopforongoing == 1) { ?>
                                            @break
                                        <?php } ?>
                                    @endforeach
                                        
                                    @if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Paid
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID))
                                        -
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Ongoing
                                    @endif
                            </td>
                            <?php $id = encrypt($value->Contract_ID); ?>
                            <td> <a href="{{asset('/reports-dashboard/report-view')}}/{{$id}}">DETAILS</a> </td>
                        </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </section>

@endsection