@extends ('layouts')

@section ('content')

<section class="dashboard-header section-padding">
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">DASHBOARD</h2>
            </div>
        </div>
        <iframe width="100%" height="700" src="https://app.powerbi.com/view?r=eyJrIjoiZTg2YzZiYzktZTcyMi00MGI2LWE5ZjMtMTRiNjc1MzJlNzZiIiwidCI6IjUyYzA4NGNjLWNkMTUtNDY3MS04YTU3LWMxOTU2NWJjZGZjMiIsImMiOjEwfQ%3D%3D&pageName=ReportSection" frameborder="0" allowFullScreen="true"></iframe>
    </div>
    <div id="notification" role="dialog" class="modal modal-open">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <strong>CONTRACT STATUS</strong>
                    <button type="button" data-dismiss="modal" id="closemodal1" aria-label="Close" class="close"><span
                            aria-hidden="true">×</span></button>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td>
                               NEW: <strong>{{$new}}</strong> Contract/s<br>
                               ONGOING: <strong>{{$ongoing}}</strong> Contract/s<br>
                               COMPLETED: <strong>{{$completed}}</strong> Contract/s <br>
                               CLOSED: <strong>{{$closed}}</strong> Contract/s <br>
                               CANCELLED: <strong>{{$cancelled}}</strong> Contract/s <br>
                               EXPIRING: <span style="color:red"><strong >{{$expiring}}</strong></span> Contract/s <br>

                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" id="perfSecSelect" data-toggle="modal" data-target="#ChoicePSModal"
                        data-dismiss="modal" class="btn btn-primary" value="Close">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection