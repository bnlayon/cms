    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{ asset('img/neda.jpg') }}" alt="person" class="img-fluid rounded-circle">
          @if(!isset(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
          @else
            <h2 class="h5">{{Auth::user()->username}}</h2>  
          @endif
          </div>
          <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong>CMS</strong><strong class="text-primary">v1</strong></a></div>
        </div>
        <!-- Sidebar Navigation Menus-->
        <div class="main-menu">
          <h5 class="sidenav-heading">Main</h5>
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li><a href="{{ asset('/dashboard') }}"> <i class="icon-line-chart"></i>Dashboard</a></li>
            
            @switch(Auth::user()->access_type)
            @case('A')
            <li><a href="{{ asset('/create-contract') }}"> <i class="icon-form"></i>Create Contracts</a></li>
            <li><a href="{{ asset('/manage-contracts') }}"> <i class="icon-pencil-case"></i>Manage Contracts</a></li>
            <li><a href="{{ asset('/reports-dashboard') }}"> <i class="icon-padnote"></i>Reports</a></li>
            <li><a href="{{ asset('/consultancyfirms') }}"> <i class="icon-list"></i>Consultancy Firms</a></li>
            
            <h5 class="sidenav-heading">Admin</h5>
            <li><a href="{{ asset('/users') }}"> <i class="icon-user"></i>User Management</a></li>
            @break
            @case('V')
            <li><a href="{{ asset('/reports-dashboard') }}"> <i class="icon-padnote"></i>Reports</a></li>
            <li><a href="{{ asset('/consultancyfirms') }}"> <i class="icon-list"></i>Consultancy Firms</a></li>
            @break

            @case('E')
            <li><a href="{{ asset('/manage-contracts') }}"> <i class="icon-pencil-case"></i>Manage Contracts</a></li>
            <li><a href="{{ asset('/reports-dashboard') }}"> <i class="icon-padnote"></i>Reports</a></li>
            <li><a href="{{ asset('/consultancyfirms') }}"> <i class="icon-list"></i>Consultancy Firms</a></li>
            @break

            @case('ADBD')
            <li><a href="{{ asset('/manage-contracts') }}"> <i class="icon-pencil-case"></i>Manage Contracts</a></li>
            <li><a href="{{ asset('/reports-dashboard') }}"> <i class="icon-padnote"></i>Reports</a></li>
            @break

            @case('A1')
            <li><a href="{{ asset('/create-contract') }}"> <i class="icon-form"></i>Create Contracts</a></li>
            <li><a href="{{ asset('/manage-contracts') }}"> <i class="icon-pencil-case"></i>Manage Contracts</a></li>
            <li><a href="{{ asset('/reports-dashboard') }}"> <i class="icon-padnote"></i>Reports</a></li>
            <li><a href="{{ asset('/consultancyfirms') }}"> <i class="icon-list"></i>Consultancy Firms</a></li>
            
            @break

            @default
            @endswitch
      </div>
    </nav>