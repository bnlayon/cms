@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Manuals </li>
      </ul>
    </div>
  </div>

  <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Manuals</h1>
      </header>
      <embed width="100%" height="750" src="files/manual.pdf" type="application/pdf"></embed>
    </div>
  </section>


@endsection