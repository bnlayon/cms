@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">MANAGE CONTRACTS</h2>
            </div>
        </div>
        <div class="breadcrumb-holder">
            <div class="container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a></li>
                    <li class="breadcrumb-item active"> Deliverable Payment Summary for {{$contractName}}</li>
                </ul>
            </div>
        </div>
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead class="table thead-light">
                            <tr>
                                <th>DELIVERABLE</th>
                                <th>PAYMENT DATE</th>
                                <th>GROSS PAYMENT</th>
                                <th>RETENTION</th>
                                <th>VAT</th>
                                <th>REIMBURSIBLE AMOUNT</th>
                                <th>EWT</th>
                                <th>LIQUIDATED DAMAGES</th>
                                <th>NET PAYMENT</th>
                                <th>DISBURSEMENT VOUCHER</th>
                                <th>PAYMENT STATUS</th>
                            </tr>
                        </thead>
                        <tbody class="table tbody">
                            @foreach($deliverables as $del)
                                <tr>
                                    <td>{{$del->Deliverable}}</td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                {{$pay->Payment_Date}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->Gross_Payment))
                                                <?php echo number_format($pay->Gross_Payment,2)?> 
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->Retention))
                                                <?php echo number_format($pay->Retention,2)?> 
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->VAT))
                                                <?php echo number_format($pay->VAT,2)?> 
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->Reimbursible_Amt))
                                                <?php echo number_format($pay->Reimbursible_Amt,2)?>
                                                @endif 
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->EWT))
                                                <?php echo number_format($pay->EWT,2)?> 
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->Liquidated_Damages))
                                                <?php echo number_format($pay->Liquidated_Damages,2)?>
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                @if(!is_null($pay->Net_Payment))
                                                <?php echo number_format($pay->Net_Payment,2)?>
                                                @endif
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @foreach($payments as $pay)
                                            @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                {{$pay->Disbursement_Voucher}}
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>{{$del->Payment_Status}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection