@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    @if(is_null(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
    @endif
    @if(Auth::user()->access_type=='V')
            <script>
                window.location = "/dashboard";
            </script>
    @endif
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">MANAGE CONTRACTS</h2>
            </div>
        </div>
        <div class="breadcrumb-holder">
            <div class="container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a></li>
                    <li class="breadcrumb-item active"> Project History </li>
                </ul>
            </div>
        </div>
        <table id="firms" class="table table-bordered gridview">
            <thead class="table thead">
                <tr>
                    <th>HISTORY DESCRIPTION</th>
                    <th>HISTORY DATE</th>
                </tr>
            </thead>
            <tbody class="table tbody">
                @if(count($history)==0)
                    <tr>
                        <td  colspan=2 style="text-align:center">No data available.</td>
                    </tr>
                @else
                    <?php $count=1;?>
                    @foreach($history as $data)
                       <tr>
                            <td><?php echo $data -> History_Description?></td>
                            <td><?php echo $data -> History_Date?></td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</section>


@endsection