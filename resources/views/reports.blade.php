@extends ('layouts')

@section ('content')
  
    <section class="firms-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <div class="col-lg-3 col-md-3">
                    <h2 class="display h4">REPORTS</h2><br>
                </div>
            </div>
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ asset('/reports-dashboard') }}"> Reports Dashboard</a></li>
                        <li class="breadcrumb-item active">Contract Report</li>
                    </ul>
                </div>
            </div>
            <br>
            <table id="pdf" class="table">
                @foreach($data as $report) 
                    <tbody class="table table-borderless tbody text-center">
                        <tr>
                            <td colspan=2><a href="{{asset('/manage-contracts/reports/pdf')}}/{{$Contract_ID}}" target="_blank" style="float:right">Save as PDF</a></td>
                        </tr>
                        <tr>
                            <td colspan=2><strong>Contract Monitoring <br> Status Report for {{$yearmonth}} <br> for <br> {{$report->Contract_Name}} </strong> </td>
                        </tr>
                        @if(in_array('PrjName', $view))
                            <tr>
                                <td>Project Name</td>
                                <td style="text-align:left"><strong>{{$report->Contract_Name}}</strong></td>
                            </tr>
                        @endif
                        @if(in_array('ConsultancyFirm', $view))
                            <tr>
                                <td>Consultant</td>
                                <td style="text-align:left">
                                    <strong>
                                        @foreach($consultant as $firm)
                                            {{$firm->Firm_Name}}
                                        @endforeach
                                    </strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('FirmType', $view))
                            <tr>
                                <td>Firm Type</td>
                                <td style="text-align:left">
                                    <strong>
                                        @foreach($consultant as $firm)
                                            {{$firm->Firm_Type_Desc}}
                                        @endforeach
                                    </strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('EU', $view))
                            <tr>
                                <td>End-User</td>
                                <td style="text-align:left">
                                    <strong>{{$report->End_User}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('IAgency', $view))
                            <tr>
                                <td>Implementing Agency</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Implementing_Agency}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('CoIAgency', $view))
                            <tr>
                                <td>Implementing Agency</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Co_Implementing_Agency}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('NOADt', $view))
                            <tr>
                                <td>Notice of Acceptance Date</td>
                                <td style="text-align:left">
                                    <strong>{{$report->NOA_Release_Date}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('NTPDt', $view))
                            <tr>
                                <td>Notice To Proceed Date</td>
                                <td style="text-align:left">
                                    <strong>{{$report->NTP_Release_Date}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('NTPConfDt', $view))
                            <tr>
                                <td>NTP Conforme Date</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Conforme_Date}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('CStartDt', $view))
                            <tr>
                                <td>Contract Start Date</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Contract_Start_Date}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('CEndDt', $view))
                            <tr>
                                <td>Contract End Date</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Contract_End_Date}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('CCost', $view))
                            <tr>
                                <td>Contract Cost</td>
                                <td style="text-align:left">
                                    <strong><?php echo number_format($report->Contract_Cost,2)?></strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('CABC', $view))
                            <tr>
                                <td>Approved Budget for Costing</td>
                                <td style="text-align:left">
                                    <strong><?php echo number_format($report->ABC,2)?></strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('FundSource', $view))
                            <tr>
                                <td>Fund_Source</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Fund_Source}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('FundYr', $view))
                            <tr>
                                <td>Fund Year</td>
                                <td style="text-align:left">
                                    <strong>{{$report->FundYr}}</strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('Performance', $view))
                            <tr>
                                <td>Performance Security</td>
                                <td style="text-align:left"> 
                                    @if(!is_null($perfSec))
                                        @foreach($perfSec as $sec)
                                            Type:<strong>{{$sec->Type_Description}}</strong><br>
                                            @if($sec->Type == 'C')
                                                Official Receipt: <strong>{{$sec->CashOR}}</strong>
                                            @elseif($sec->Type == 'CMC')
                                                Check Number: <strong>{{$sec->CheckNum}}</strong><br>
                                                Official Receipt: <strong>{{$sec->CheckOR}}</strong><br>
                                                Bank Name: <strong>{{$sec->CheckBank}}</strong>
                                            @elseif($sec->Type == 'BD')
                                                Bank Name: <strong>{{$sec->BankNameBD}}</strong><br>
                                                Bank Draft: <strong>{{$sec->DraftBD}}</strong><br>
                                                Validity: <strong>{{$sec->ValidityBD}}</strong>
                                            @elseif($sec->Type == 'BG')
                                                Bank Name: <strong>{{$sec->BankNameBG}}</strong><br>
                                                Bank Guarantee: <strong>{{$sec->DraftBG}}</strong><br>
                                                Validity: <strong>{{$sec->ValidityBG}}</strong>
                                            @elseif($sec->Type == 'ILC')
                                                Bank Name: <strong>{{$sec->BankNameILC}}</strong><br>
                                                Bank Guarantee: <strong>{{$sec->ILC}}</strong><br>
                                                Validity: <strong>{{$sec->ValidityILC}}</strong>
                                            @elseif($sec->Type == 'SB')
                                                Bank Name: <strong>{{$sec->CNameSurety}}</strong><br>
                                                Bank Guarantee: <strong>{{$sec->SuretyNum}}</strong><br>
                                                Validity: <strong>{{$sec->ValiditySurety}}</strong>
                                            @endif
                                        @endforeach
                                    @else
                                        No Performance Security Available
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if(in_array('BondValidity', $view))
                            <tr>
                                <td>Bond Validity</td>
                                <td style="text-align:left">
                                    @if(!is_null($perfSec))
                                        @foreach($perfSec as $sec)
                                            <strong>{{$sec->Bond_Validity}}</strong><br>
                                        @endforeach
                                    @else
                                        No Bond Validity Available
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if(in_array('IssuingCompany', $view))
                            <tr>
                                <td>Issuing Company</td>
                                <td style="text-align:left">
                                    @if(!is_null($perfSec))
                                        @foreach($perfSec as $sec)
                                            <strong>{{$sec->Issuing_Company}}</strong><br>
                                        @endforeach
                                    @else
                                        No Issuing Company Available
                                    @endif
                                </td>
                            </tr>
                        @endif
                        @if(in_array('SuppDocs', $view))
                            <tr>
                                <td>Supporting Documents</td>
                                <td style="text-align:left">
                                    <strong>
                                        @foreach($documents as $docs)
                                            {{basename($docs->Document_Link)}} <br>
                                        @endforeach
                                    </strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('Deliverables', $view))
                            <tr>
                                <td>Deliverables</td>
                                <td style="text-align:left">
                                    @foreach($deliverables as $deli)
                                        Deliverable: <strong>{{$deli->Deliverable}}</strong> <br>
                                        Target Date: <strong>{{$deli->Target_Completion_Date}}</strong> <br>
                                        @if(!is_null($deli->Amended_Date))
                                            Amended Date: <strong>{{$deli->Amended_Date}}</strong> <br>
                                        @endif
                                        @if(!is_null($deli->Actual_Completion_Date))
                                            Amended Date: <strong>{{$deli->Actual_Completion_Date}}</strong> <br>
                                        @endif
                                        Payment Status: <strong>{{$deli->Payment_Status}} </strong> <br>
                                        @endforeach
                                    </strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('PmtStat', $view))
                            <tr>
                                <td>Payment Status</td>
                                <td style="text-align:left">
                                    <strong>
                                    <?php $stopforongoing = 0; ?>
                                    @foreach($deliverables as $deliverable)
                                        @if($deliverable->Payment_Status == 'Ongoing')
                                            {{ $deliverable->Payment_Status }}
                                            <?php $stopforongoing = 1; ?>                                        
                                        @endif
                                        <?php if($stopforongoing == 1) { ?>
                                            @break
                                        <?php } ?>
                                    @endforeach
                                        
                                    @if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Paid
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID))
                                        -
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Ongoing
                                    @endif
                                    </strong>
                                </td>
                            </tr>
                        @endif
                        @if(in_array('PrjStat', $view))
                            <tr>
                                <td>Project Status</td>
                                <td style="text-align:left">
                                    <strong>{{$report->Contract_Status}}</strong>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                @endforeach
            </table>
        </div>
    </section>

@endsection