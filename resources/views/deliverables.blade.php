@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    @if(is_null(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
    @endif
    @if(Auth::user()->access_type=='V')
            <script>
                window.location = "/dashboard";
            </script>
    @endif
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">MANAGE CONTRACTS</h2>
            </div>
        </div>
        <div class="breadcrumb-holder">
            <div class="container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a></li>
                    <li class="breadcrumb-item active"> 
                        Deliverables for <?php foreach($projectTitle as $pt){
                            if($pt->Contract_ID == $id){
                                echo $pt->Contract_Name;
                            }
                        } ?>
                    </li>
                </ul>
            </div>
        </div>
        @include('error')
        <table id="firms" class="table table-bordered gridview" style="table-layout:fixed">
            <thead class="table thead">
                <tr>
                    <th>DELIVERABLES</th>
                    <th>TARGET COMPLETION DATE</th>
                    <th>AMENDED DATE</th>
                    <th>ACTUAL SUBMISSION DATE</th>
                    <th>ACTUAL COMPLETION DATE</th>
                    <th>ACTION</th>
                    <th>PAYMENT</th>
                </tr>
            </thead>
            <tbody class="table tbody">
                @if(count($selected)==0)
                    <tr>
                        <td  colspan=4 style="text-align:center">No data available.</td>
                    </tr>
                @else
                    <?php $count=1; ?>
                    @foreach($selected as $data)
                      <tr>
                      <form action="{{asset('/manage-contracts/deliverables')}}/{{$data->Contract_ID}}" method="post">
                        {{csrf_field()}} 
                        
                        <?php 
                            $deliverableid = encrypt($data->Deliverable_ID);
                            $contractid = encrypt($data->Contract_ID); 
                        ?>
                        <input type="hidden" id="deliverableid" name="deliverableid" value="{{$deliverableid}}">
                        <input type="hidden" id="contractid" name="contractid" value="{{$contractid}}">
                            <td><?php echo $data -> Deliverable?></td>
                            @if(!is_null($data -> Target_Completion_Date) || !is_null($data -> TCD_Notes))
                                <td><?php echo date('d-M-Y', strtotime($data -> Target_Completion_Date));?>
                                <input type="hidden" name="targetdate" id="targetdate" value="{{$data -> Target_Completion_Date}}"><input value="..." type="button" type="button" onclick="java_script_:shownotesWindow('#tcdnotesmodal{{$data -> Deliverable_ID}}')" name="tcdnotes" id="tcdnotes" data-toggle="modal" data-target="#tcdnotesmodal{{$data -> Deliverable_ID}}">
                                <div id="tcdnotesmodal{{$data -> Deliverable_ID}}" name="tcdnotesmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data -> Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                        <textarea name="tcdnotes" id="tcdnotes" cols="60" rows="5"><?php echo $data -> TCD_Notes?></textarea>
                                                        </tr>  
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                </td>
                            @elseif(is_null($data -> Target_Completion_Date) || is_null($data -> TCD_Notes))
                            <td>
                                <input style="font-size: .70rem" type="date" name="targetdate" id="targetdate">
                                <input type="hidden" name="targetdate" id="targetdate" value="{{$data -> Target_Completion_Date}}"><input value="..." type="button" type="button" onclick="java_script_:showTCDWindow('#tcdnotesmodal{{$data -> Deliverable_ID}}')" name="tcdnotes" placeholder="Notes"  id="tcdnotes" data-toggle="modal" data-target="#tcdnotesmodal{{$data -> Deliverable_ID}}">
                                <div id="tcdnotesmodal{{$data -> Deliverable_ID}}" name="tcdnotesmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hideTCDWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                        <textarea name="tcdnotes" id="tcdnotes" cols="60" rows="5"><?php echo $data -> TCD_Notes?></textarea>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @else
                            <td>
                            <input style="font-size: .70rem" type="date" name="targetdate" id="targetdate"><input value="..." type="button" type="button" onclick="java_script_:shownotesWindow('#tcdnotesmodal{{$data -> Deliverable_ID}}')" name="tcdnotes" placeholder="Notes"  id="tcdnotes" data-toggle="modal" data-target="#tcdnotesmodal{{$data -> Deliverable_ID}}">
                            <div id="tcdnotesmodal{{$data -> Deliverable_ID}}" name="tcdnotesmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="tcdnotes" id="tcdnotes" cols="60" rows="5"><?php echo $data -> TCD_Notes?></textarea>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @endif
                            @if(!is_null($data -> Amended_Date) || !is_null($data -> AD_Notes))
                            <td>
                                <input style="font-size: .70rem" type="date" name="amendDate" id="amendDate"><input value="..." type="button" type="button" data-id="{{$data->Deliverable_ID}}" onclick="java_script_:shownotesWindow('#adnotesmodal{{$data -> Deliverable_ID}}')" name="adnotesbtn" id="adnotesbtn" data-toggle="modal" data-target="#adnotesmodal{{$data -> Deliverable_ID}}"><br>
                                
                                    @if(!is_null($data -> Amended_Date))
                                        <small><strong>Previous "Amended Date":<br> </strong>
                                        <?php echo date('d-M-Y',strtotime($data -> Amended_Date))?>
                                    @endif
                                        <br>
                                <a href="javascript:void(0)" onclick="java_script_:showAmendmentWindow('#AmendmentModal{{$data -> Contract_ID}}')" id="Amendmentlink{{$data -> Contract_ID}}" data-id="{{$data -> Deliverable_ID}}" name="Amendmentlink{{$data -> Contract_ID}}" data-toggle="modal" data-target="#AmendmentModal{{$data -> Contract_ID}}">Amendment History</a></small>
                                <div id="AmendmentModal{{$data -> Contract_ID}}" name="AmendmentModal{{$data -> Contract_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Amendment History: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:closepmtmodal('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <th>Last Amended Date</th>
                                                            <th>Date of Amendment</th>
                                                            <th>Amended By</th>
                                                        </tr>              
                                                        @if(!is_null($amendment))
                                                            @foreach($amendment as $amend)
                                                                @if($amend->Deliverable_ID == $data->Deliverable_ID)
                                                                    <tr>
                                                                        <td>{{$amend->Last_Amended_Date}}</td>
                                                                        <td>{{$amend->Date_Of_Amendment}}</td>
                                                                        <td>{{$amend->Amended_By}}</td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @else
                                                            <tr>
                                                                <td colspan=3 style="text-align:center">No data available</td>
                                                            </tr>
                                                        @endif                                          
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div id="adnotesmodal{{$data -> Deliverable_ID}}" name="adnotesmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data -> Deliverable_ID}} </strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="adnotes" id="adnotes" cols="60" rows="5"><?php echo $data -> AD_Notes?></textarea>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @else
                            <td><input style="font-size: .70rem" type="date" name="amendDate" id="amendDate"><input value="..." type="button" type="button" onclick="java_script_:shownotesWindow('#adnotesmodal{{$data -> Deliverable_ID}}')" name="adnotes" placeholder="Notes"  id="adnotes" data-toggle="modal" data-target="#adnotesmodal{{$data -> Deliverable_ID}}">
                            <div id="adnotesmodal{{$data -> Deliverable_ID}}" name="adnotesmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="adnotes" id="adnotes" cols="60" rows="5"><?php echo $data -> AD_Notes?></textarea>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @endif
                            @if(!is_null($data -> Actual_Submission_Date))
                            <td><?php !empty($data -> Actual_Submission_Date) ? $date=date('d-M-Y', strtotime($data -> Actual_Submission_Date)) : $date=null; echo $date;?>
                            <input style="font-size: .68rem" placeholder="Reviewed By"type="text" name="reviewedBy" id="reviewedBy" value="{{$data->ASD_ReviewedBy}}"><input value="..." type="button" name="asdnotes" id="asdnotes" data-target="#asdmodal{{$data -> Deliverable_ID}}" onclick="java_script_:shownotesWindow('#asdmodal{{$data -> Deliverable_ID}}')" data-toggle="modal">
                            <div id="asdmodal{{$data -> Deliverable_ID}}" name="asdmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="asdnotes" id="asdnotes" cols="60" rows="5"><?php echo $data -> ASD_Notes?></textarea>
                                                        </tr>    
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div></td>
                            @elseif(is_null($data -> Actual_Submission_Date)||!is_null($data -> ASD_Notes) || !is_null($data -> ASD_ReviewedBy))
                            <td><input style="font-size: .70rem" type="date" name="submissiondate" id="submissiondate">
                            <input style="font-size: .68rem" placeholder="Reviewed By"type="text" name="reviewedBy" id="reviewedBy" value="{{$data->ASD_ReviewedBy}}">
                            <input value="..." type="button" name="asdnotes" id="asdnotes" data-target="#asdmodal{{$data -> Deliverable_ID}}" onclick="java_script_:shownotesWindow('#asdmodal{{$data -> Deliverable_ID}}')" data-toggle="modal">
                            <div id="asdmodal{{$data -> Deliverable_ID}}" name="asdmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="asdnotes" id="asdnotes" cols="60" rows="5"><?php echo $data -> ASD_Notes?></textarea>
                                                        </tr>    
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @else
                            <td><input style="font-size: .70rem" type="date" name="submissiondate" id="submissiondate">
                            <input style="font-size: .68rem" placeholder="Reviewed By"type="text" name="reviewedBy" id="reviewedBy" value="{{$data->ASD_ReviewedBy}}"><input value="..." type="button" name="asdnotes" id="asdnotes" data-target="#asdmodal{{$data -> Deliverable_ID}}" onclick="java_script_:shownotesWindow('#asdmodal{{$data -> Deliverable_ID}}')" data-toggle="modal">
                            <div id="asdmodal{{$data -> Deliverable_ID}}" name="asdmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="asdnotes" id="asdnotes" cols="60" rows="5"><?php echo $data -> ASD_Notes?></textarea>
                                                        </tr>  
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @endif
                            @if(!is_null($data -> Actual_Completion_Date))
                            <td><?php echo $data -> Actual_Completion_Date?><input value="..." type="button" name="acdnotes" id="acdnotes" data-target="#acdmodal{{$data -> Deliverable_ID}}" onclick="java_script_:shownotesWindow('#acdmodal{{$data -> Deliverable_ID}}')" data-toggle="modal">
                                <div id="acdmodal{{$data -> Deliverable_ID}}" name="acdmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="acdnotes" id="acdnotes" cols="60" rows="5"><?php echo $data -> ACD_Notes?></textarea>
                                                        </tr>        
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @else
                            <td><?php echo $data -> Actual_Completion_Date?><input style="font-size: .70rem" type="date" name="actualdate" id="actualdate"><input value="..." type="button" name="asdnotes" id="asdnotes" data-target="#asdmodal{{$data -> Deliverable_ID}}" onclick="java_script_:shownotesWindow('#acdmodal{{$data -> Deliverable_ID}}')" data-toggle="modal">
                               <div id="acdmodal{{$data -> Deliverable_ID}}" name="acdmodal{{$data -> Deliverable_ID}}" role="dialog" class="modal modal-open">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                                <div class="modal-header">
                                                    <strong>Deliverable Notes: {{$data -> Contract_ID}}-{{$data->Deliverable_ID}}</strong>
                                                    <button type="button" data-dismiss="modal" 
                                                        onclick="java_script_:hidenotesWindow('#PaymentUpdateModal{{$data -> Contract_ID}}')"
                                                        id="closemodalupdpmt{{$data -> Contract_ID}}" aria-label="Close" class="close"><span
                                                            aria-hidden="true">×</span></button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-borderless">
                                                        <tr>
                                                            <textarea name="acdnotes" id="acdnotes" cols="60" rows="5"><?php echo $data -> ACD_Notes?></textarea>
                                                        </tr>      
                                                    </table>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            </td>
                            @endif
                            <td><input style="font-size: 0.80rem" type="submit" class="btn btn-primary" value="Update Details"></td>
                            </form>
                            <td>   
                                <a href="{{asset('/manage-contracts/deliverables/paymentView')}}/{{$contractid}}/{{$deliverableid}}" title="View Payment Details" target="_blank"> <?php $count = count($selected); $percent = (1/$count)*100; echo number_format((float)$percent, 2, '.', '').'%';?></a>
                                <a href="{{asset('/manage-contracts/deliverables/paymentUpdateView')}}/{{$contractid}}/{{$deliverableid}}"  title="Update Payment" target="_blank" id="paymentUpdateLink{{$data -> Deliverable_ID}}" data-id="{{$data -> Contract_ID}}" name="paymentUpdateLink{{$data -> Contract_ID}}">Update</a>

                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
    </div>
</section>



@endsection