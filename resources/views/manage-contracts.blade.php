@extends ('layouts')

@section ('content')
    
    <section class="firms-header section-padding">
    @if(is_null(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
    @endif
        <div class="container-fluid">
            <h2 style="display:inline;float:left;" class="display h4">MANAGE CONTRACTS</h2><span style="float:right;"><a href="{{url('/storage/template.docx')}}" target="_blank">Download Contract Amendment Template</a></span><br><br>
            <table id="firms" class="table table-bordered gridview">
                <thead class="table thead">
                    <tr>
                        <th>PROJECT TITLE</th>
                        <th>DELIVERABLES</th>
                        <th>PAYMENT STATUS</th>
                        @if(Auth::user()->access_type == 'A' || Auth::user()->access_type =='A1' || Auth::user()->access_type =='E')
                        <th>PROJECT STATUS</th>
                        <th>PROJECT TERMINATION DATE</th>
                        <th>REPORTS</th>
                        @endif
                        <th>PROJECT HISTORY</th>
                    </tr>
                </thead>
                <tbody class="table tbody" style="text-align:center">
                    @if(count($contracts) == 0)
                        <tr>
                            <td colspan=7 style="text-align:center"> <?php echo "No data available." ?></td>
                        </tr>
                    @else


                        @foreach($contracts as $contract => $value)
                            <tr>
                                <td>
                                <?php 
                                    $id = $value -> Contract_ID;
                                    $id = encrypt($id);
                                ?>
                                <a href="{{ asset('/manage-contracts/view-contract')}}/{{$id}}" title="View Contract Details"><?php echo $value -> Contract_Name ?> </a></td>
                                <td><a href="{{ asset('/manage-contracts/deliverables')}}/{{$id }}" title="View Deliverable Details">Deliverables</a></td>
                                <td>
                                    <?php 
                                        $deliverables = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->get();
                                        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->where('Payment_Status','Paid')->count();
                                        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->where('Payment_Status','Ongoing')->count();
                                        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->whereNull('Payment_Status')->count();
                                    ?>
                                    <a href="{{ route('paymentSummary',['Contract_ID' => $id]) }}">
                                        <?php $stopforongoing = 0; ?>
                                        @foreach($deliverables as $deliverable)
                                            @if($deliverable->Payment_Status == 'Ongoing')
                                                {{ $deliverable->Payment_Status }}
                                                <?php $stopforongoing = 1; ?>                                        
                                            @endif
                                            <?php if($stopforongoing == 1) { ?>
                                                @break
                                            <?php } ?>
                                        @endforeach
                                            
                                        @if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                            Paid
                                        @endif

                                        @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID))
                                            None
                                        @endif

                                        @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                            Ongoing
                                        @endif
                                    </a>
                                </td>
                                @if(Auth::user()->access_type == 'A' || Auth::user()->access_type =='A1' || Auth::user()->access_type =='E')
                                <td> 
                                    <?php echo $value -> Contract_Status ?> <a href="javascript:void(0)" title="Update Project Status" onclick="java_script_:ShowModalProject('#ProjectUpdateModal{{$value -> Contract_ID}}')" data-id="{{$value -> Contract_ID}}" id="projUpdateLink{{$value -> Contract_ID}}" name="projUpdateLink{{$id}}"  data-toggle="modal" data-target="#ProjectUpdateModal{{$value -> Contract_ID}}">Update</a>
                                    <div id="ProjectUpdateModal{{$value -> Contract_ID}}" name="ProjectUpdateModal{{$value -> Contract_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <form method="post" id="prjForm{{$id}}" action="{{ asset('/manage-contracts/projUpdate')}}" novalidate=""  enctype="multipart/form-data">
                                                {{csrf_field()}}

                                                <input type="hidden" name="contractid" id="contractid" value="{{$id}}">
            
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <strong>Project Status Update: {{$value -> Contract_ID  }}</strong>
                                                        <button type="button"  data-dismiss="modal" onclick="java_script_:closeprjmodal('#ProjectUpdateModal{{$id}}')" id="closemodalupdprj{{$id}}" aria-label="Close" class="close"><span
                                                                aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-borderless">
                                                            <tr>
                                                                <td> Project Status:</td>
                                                                <td>
                                                                    <select name="prjstat" id="prjstat">
                                                                    <option disabled="" selected="" value="">Select Status</option>
                                                                    <option value="Ongoing">Ongoing</option>
                                                                    <option value="Completed">Completed</option>
                                                                    <option value="Closed">Closed</option>
                                                                    <option value="Cancelled">Cancelled</option>
                                                                    </select>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Deliverables Status:</td>
                                                                <td>
       
                                                                    <a href="{{asset('/manage-contracts/deliverables')}}/{{$id}}" target="_blank">see Details</a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Certificate of Final Acceptance:</td>
                                                                <td><input type="file" name="CFA" id="CFA"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Input Date:</td>
                                                                <td><input type=date name="reviewDate" id="reviewDate" size=40></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Project Update:</td>
                                                                <td><textarea name="notes" id="notes" cols="40" rows="9"></textarea></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" id="projUpdBtn" name="projUpdBtn" class="btn btn-primary" value="Update">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                <td> 
                                    <?php if(!is_null($value->Contract_Termination_Date)){
                                        echo date('d-M-Y', strtotime($value->Contract_Termination_Date));
                                    }?> 
                                    <a href="javascript:void(0)" title="Update Termination Date" onclick="java_script_:editTermination('#EditTermDate{{$value -> Contract_ID}}')" data-id="{{$value->Contract_ID}}" id="EditTermDatelink{{$value -> Contract_ID}}" name="EditTermDatelink{{$value -> Contract_ID}}"  data-toggle="modal" data-target="#EditTermDate{{$value -> Contract_ID}}">Update</a> 
                                    <div id="EditTermDate{{$value -> Contract_ID}}" name="EditTermDate{{$value -> Contract_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <form method="post" id="termDateForm{{$id}}" action="{{ asset('/manage-contracts/updateTermDate')}}" novalidate=""  enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <input type="hidden" name="contractid" id="contractid" value="{{$id}}">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <strong>Termination Date Update: {{$value -> Contract_ID}}</strong>
                                                        <button type="button" data-dismiss="modal" onclick="java_script_:closeEditTermination('#EditTermDate{{$id}}')" id="closemodaltermdate{{$id}}" aria-label="Close" class="close"><span
                                                                aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-borderless">
                                                            <tr>
                                                                <td> Termination Date:</td>
                                                                <td>
                                                                    <input type="datetime-local" name="termDate" id="termDate">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Notes:</td>
                                                                <td><textarea name="notes" id="notes" cols="50" rows="10"></textarea></td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" id="termDateBtn{{$id}}" name="termDateBtn{{$id}}" class="btn btn-primary" value="Update">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                <td> 
                                    <a href="javascript:void(0)" title="View Report" onclick="java_script_:viewSelection('#ReportSelect{{$value -> Contract_ID}}')" data-id="{{$value -> Contract_ID}}" id="ReportSelectlink{{$value -> Contract_ID}}" name="ReportSelectlink{{$value -> Contract_ID}}"  data-toggle="modal" data-target="#ReportSelect{{$value -> Contract_ID}}">View</a>
                                    <div id="ReportSelect{{$value -> Contract_ID}}" name="ReportSelect{{$value -> Contract_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <form method="post" id="selectViewForm{{$value -> Contract_ID}}"  target="_blank" action="{{ asset('/manage-contracts/reports/pdf')}}/{{$id}}" novalidate=""  enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <strong>Select Data to view: {{$value -> Contract_ID}}</strong>
                                                        <button type="button" data-dismiss="modal" onclick="java_script_:hideSelection('#ReportSelect{{$id}}')" id="closemodaltermdate{{$id}}" aria-label="Close" class="close"><span
                                                                aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-borderless">
                                                            <tbody style="text-align:left; padding:0;margin:0">
                                                                <tr>
                                                                    <td><input type="checkbox" name="selectall" id="selectall"></td><td>Select All</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="PrjName"></td><td>Project Name</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="FundSource"></td><td>Fund Source</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="ConsultancyFirm"></td><td>Consultancy Firm</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="FundYr"></td><td>Fund Year</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="FirmType"></td><td>Firm Type</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="Performance"></td><td>Performance Security</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="EU"></td><td>End User</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="BondValidity"></td><td>Bond Validity</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="IAgency"></td><td>Implementing Agency</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="IssuingCompany"></td><td>Issuing Company</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CoIAgency"></td><td>co-Implementing Agency</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="SuppDocs"></td><td>Supporting Documents</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="NOADt"></td><td>Notice of Acceptance Date</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="Deliverables"></td><td>Deliverables</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="NTPDt"></td><td>Notice to Proceed Date</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="PmtStat"></td><td>Payment Status</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="NTPConfDt"></td><td>NTP Conforme Date</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="PrjStat"></td><td>Project Status</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CStartDt"></td><td>Contract Start Date</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CEndDt"></td><td>Contract End Date</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CCost"></td><td>Contract Cost</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CABC"></td><td>Approved Budget for Contract(ABC)</td>
                                                                </tr>
                                                            </tbody>    
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" id="selectViewBtn{{$id}}" name="selectViewBtn{{$id}}" class="btn btn-primary" value="View Report">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                                @endif
                                <td> <a href="{{ asset('/manage-contracts/history')}}/{{$id }}" title="View Project History">View</a> </td>
                            </tr>
                        @endforeach
                    @endif   
                </tbody> 
            </table> 
        </div>
    </section>

    
    

@endsection