      <footer class="main-footer">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6">
              <p>National Economic and Development Authority &copy; 2020</p>
            </div>
            <div class="col-sm-6 text-right">
              <p>Design by <a href="http://www.neda.gov.ph/bid-bulletin-no-1-system-development-of-the-neda-central-support-office-information-system-ncso-is/icts/" class="external">NEDA-ICTS</a></p>
            </div>
          </div>
        </div>
      </footer>