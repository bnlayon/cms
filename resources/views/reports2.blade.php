@extends ('layouts')

@section ('content')
  
    <section class="firms-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <div class="col-lg-3 col-md-3">
                    <h2 class="display h4">REPORTS</h2><br>
                </div>
            </div>
            <div class="breadcrumb-holder">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ asset('/reports-dashboard') }}"> Reports Dashboard</a></li>
                            <li class="breadcrumb-item active">Contract Report</li>
                        </ul>
                    </div>
                </div>
                <br>
                <table id="pdf" class="table">
                        @foreach($data as $report)
                        <tbody class="table table-borderless tbody text-center">
                            <tr>
                            <?php
                                $id = encrypt($Contract_ID);
                            ?>
                                <td colspan=2>
                                <a href="javascript:void(0)" style="float:right" title="View Report" onclick="java_script_:viewSelection('#ReportSelect{{$Contract_ID}}')" data-id="{{$Contract_ID}}" id="ReportSelectlink{{$Contract_ID}}" name="ReportSelectlink{{$Contract_ID}}"  data-toggle="modal" data-target="#ReportSelect{{$Contract_ID}}">Save as PDF</a>
                                    <div id="ReportSelect{{$Contract_ID}}" name="ReportSelect{{$Contract_ID}}" role="dialog" class="modal modal-open">
                                        <div class="modal-dialog">
                                            <form method="post" id="selectViewForm{{$Contract_ID}}"  target="_blank" action="{{asset('/manage-contracts/reports/pdf')}}/{{$id}}" novalidate=""  enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <strong>Select Data to view: <?php echo $Contract_ID ?></strong>
                                                        <button type="button" data-dismiss="modal" onclick="java_script_:hideSelection('#ReportSelect{{$id}}')" id="closemodaltermdate{{$id}}" aria-label="Close" class="close"><span
                                                                aria-hidden="true">×</span></button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-borderless">
                                                            <tbody style="text-align:left; padding:0;margin:0">
                                                                <tr>
                                                                    <td><input type="checkbox" name="selectall" id="selectall"></td><td>Select All</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="PrjName"></td><td>Project Name</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="FundSource"></td><td>Fund Source</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="ConsultancyFirm"></td><td>Consultancy Firm</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="FundYr"></td><td>Fund Year</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="FirmType"></td><td>Firm Type</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="Performance"></td><td>Performance Security</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="EU"></td><td>End User</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="BondValidity"></td><td>Bond Validity</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="IAgency"></td><td>Implementing Agency</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="IssuingCompany"></td><td>Issuing Company</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CoIAgency"></td><td>co-Implementing Agency</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="SuppDocs"></td><td>Supporting Documents</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="NOADt"></td><td>Notice of Acceptance Date</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="Deliverables"></td><td>Deliverables</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="NTPDt"></td><td>Notice to Proceed Date</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="PmtStat"></td><td>Payment Status</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="NTPConfDt"></td><td>NTP Conforme Date</td>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="PrjStat"></td><td>Project Status</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CStartDt"></td><td>Contract Start Date</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CEndDt"></td><td>Contract End Date</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CCost"></td><td>Contract Cost</td>
                                                                </tr>
                                                                <tr>
                                                                    <td><input type="checkbox" name="reportDataView[]" id="reportDataView[]" value="CABC"></td><td>Approved Budget for Contract(ABC)</td>
                                                                </tr>
                                                            </tbody>    
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <input type="submit" id="selectViewBtn{{$id}}" name="selectViewBtn{{$id}}" class="btn btn-primary" value="View Report">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan=2><strong>Contract Monitoring <br> Status Report for {{$yearmonth}} <br> for <br> {{$report->Contract_Name}} </strong> </td>
                            </tr>
                            <tr>
                                <td>End-User</td>
                                <td><strong>{{$report->End_User}}</strong></td>
                            </tr>
                            <tr>
                                <td>Project Title</td>
                                <td><strong>{{$report->Contract_Name}}</strong></td>
                            </tr>
                            <tr>
                                <td>Contract Amount</td>
                                <td><strong><?php echo number_format($report->Contract_Cost,2)?></strong></td>
                            </tr>
                            <tr>
                                <td>Consultant</td>
                                <td><strong></strong></td>
                            </tr>
                            <tr>
                                <td>Project Status</td>
                                <td><strong>{{$report->Contract_Status}}</strong></td>
                            </tr>
                            <tr>
                                <td>Payment Status</td>
                                <td><strong> 
                                <?php $stopforongoing = 0; ?>
                                    @foreach($deliverables as $deliverable)
                                        @if($deliverable->Payment_Status == 'Ongoing')
                                            {{ $deliverable->Payment_Status }}
                                            <?php $stopforongoing = 1; ?>                                        
                                        @endif
                                        <?php if($stopforongoing == 1) { ?>
                                            @break
                                        <?php } ?>
                                    @endforeach
                                        
                                    @if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Paid
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID))
                                        -
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Ongoing
                                    @endif
                                </strong></td>
                            </tr>
                            <tr>
                                <td>Contract Duration</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>&nbsp&nbsp&nbsp&nbsp&nbspStart Date</td>
                                <td><strong>{{$report->Contract_Start_Date}}</strong></td>
                            </tr>
                            <tr>
                                <td>&nbsp&nbsp&nbsp&nbsp&nbspEnd Date</td>
                                <td><strong>{{$report->Contract_End_Date}}</strong></td>
                            </tr>
                            <tr>
                                <td>NTP Release Date</td>
                                <td><strong>{{$report->NTP_Release_Date}}</strong></td>
                            </tr>
                        </tbody>
                         @endforeach
                </table>
        </div>
    </section>

@endsection