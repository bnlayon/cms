
<p><strong>Authorized User: {{$user['lname']}}, {{$user['fname']}} {{$user['mname']}} </strong></p>
<br>
<p>Dear Sir/Ma'am:</p>
<p>As an authorized user of the Contract Management System, this is to provide your user account information to access the Contract Management System.
Please be informed that the CMS can now be accessed using the account credentials provided since September 9
until October 11, 2019.</p>
<p>Username: {{$user['username']}}</p>
<p>Password: As submitted in the sign-up page</p>
<p>Kindly note as well the following reminders:</p>
<p>A) Each user account cannot be used/accessed simultaneously by more than one user;</p>
<p>B) The CMS generates system logs, which contain electronic record of transactions/interactions of each user
account in the system for monitoring and accountability purposes; and</p>
<p>C) User names and passwords are <strong>case sensitive.</strong></p>
<p>Should you have any inquiries, please do not hesitate to coordinate with the CMS Administrator at contact numbers: DL 631-2165 or TL 631-0945 local no.: 404 or through e-mail address:
contracts.neda@gmail.com.</p>
<p>Thank you.</p>
<p><strong>CMS Administrator</strong></p>
