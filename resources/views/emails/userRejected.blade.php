<p><strong>Agency: {{$params['item']['UACS_AGY_DSC']}}</strong></p>
<p><strong>
    Agency PIP/TRIP Focal: {{$params['item']['lname']}}, 
    {{$params['item']['fname']}} {{$params['item']['mname']}} 
</strong></p>
<p>Dear Sir/Ma'am:</p>
<p>This is to inform you that you were unable to successfully accomplish the PIPOL System Sign-up Page due to the following
reason/s:</p>
@if ($params['option'] === 3)
    <p><strong>{{$params['others']}}</strong></p>
@elseif($params['option'] === 2)
    <p><strong>
        The file uploaded in the PIP System Sign-up Page does not contain the 
        prescribed authorization form. Please download the Authorization Form 
        through this <a href="{{url('/storage/authform.docx')}}">
            link</a>.
    </strong></p>
@elseif($params['option'] === 1)
    <p><strong>
        The authorization form uploaded in the 
        PIPOL System Online Sign-up Page is not duly signed by the Head of your Agency
        and/or Mother Agency.
    </strong></p>
@endif
<p>Kindly address the abovementioned deficiency as soon as possible and accomplish again the PIPOL System Sign-up Page so
the Authorized Agency PIP/TRIP Focal can be issued with username and password. Please note that the System cannot be
accessed at the moment. Kindly wait for the issuance of the PIP Updating and TRIP Formulation Guidelines along with
advisory on the date of the opening of the PIPOL System for the submission of priority programs and projects for
inclusion in the Updated 2017-2022 PIP and TRIP for FY 2020-2022 as input to the FY 2021 budget preparation.</p>
<p>For any inquiries, please do not hesitate to coordinate with the PIP Secretariat of the NEDA-Public Investment Staff at
contact numbers: DL 631-2165 or TL 631-0945 local no.: 404 or through e-mail address: pip@neda.gov.ph.</p>
<p>Thank you.</p>
<p><strong>PIP Secretariat</strong></p>