@extends ('layouts')

@section ('content')
  <div class="breadcrumb-holder">
    <div class="container-fluid">
      <ul class="breadcrumb">
        <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
        <li class="breadcrumb-item active">Email Notification </li>
      </ul>
    </div>
  </div>

  <section class="forms">
    <div class="container-fluid">
      <header> 
        <h1 class="h3 display">Send Email Notification</h1>
      </header>
    </div>

    <div class="panel-body">
      <ul class="nav nav-pills nav-justified" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#mid" role="tab">Send Email Notification</a>
            </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content tablewrapper">
            <div class="tab-pane active" id="mid" role="tabpanel" style="background-color: white">
              <table id="example" class="table table-striped table-bordered" style="width:100%">
              <thead>
                  <tr>
                      <th>Agency</th>
                      <th>CMS Users Email Addresses</th>
                      <th>Head Email Address</th>
                      <th>Mother Email Address</th>
                      <th>Actions</th>
                      <!-- <th>Midpoint</th> -->
                      <th>Final</th>
                      <!-- <th>Validation</th> -->
                  </tr>
              </thead>
              <tbody>   
                @foreach ($uniqueAgencys as $uniqueAgency)
                  <tr>
                      <td>
                        @foreach ($uniqueAgency->getAgencyDetails($uniqueAgency->agency_id) as $agencydetail)
                          {{ $agencydetail->Abbreviation }}
                        @endforeach
                      </td>
                      <td>
                        @foreach ($uniqueAgency->getUserDetails($uniqueAgency->id) as $useremail)
                          @if($useremail->username == '')

                          @else
                            {{ $useremail->email }}
                          @endif
                          
                        @endforeach
                      </td> 
                      <td>
                        @if($useremail->username == '')

                          @else
                            {{$uniqueAgency->head_email}}
                          @endif
                        
                      </td>
                      <td>
                          @if($useremail->username == '')

                          @else
                            {{$uniqueAgency->mother_email}}
                          @endif
                        
                      </td>
                      <td>

                        <a href="{{ asset('/midpointmessage') }}/{{$uniqueAgency->id}}" onclick="w = window.open(this.href);return false;w.close();"><button type="button" data-toggle="modal" class="btn btn-primary"><i class="far fa-envelope"></i></button></a>
                      </td>  
                      <!-- <td>{{ $uniqueAgency->midpoint_time }}</td> -->
                      <td>{{ $uniqueAgency->final_time }}</td>
                      <!-- <td>{{ $uniqueAgency->validated_time }}</td> -->
                  </tr> 
                @endforeach           
              </tbody>
              <tfoot>
                  <tr>  
                      <th>Agency</th>
                      <th>CMS Users Email Addresses</th>
                      <th>Head Email Address</th>
                      <th>Mother Email Address</th>
                      <th>Actions</th>
                      <th>Email Status</th>
                  </tr>
              </tfoot>
              </table>
            </div>      
      </div>
    </div>
  </section>

<script type="text/javascript">
    $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#midpointemail thead tr').clone(true).appendTo( '#midpointemail thead' );
    $('#midpointemail thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
 
        $( 'input', this ).on( 'keyup change', function () {
            if ( table.column(i).search() !== this.value ) {
                table
                    .column(i)
                    .search( this.value )
                    .draw();
            }
        } );
    } );
 
    var table = $('#midpointemail').DataTable( {
        orderCellsTop: true,
        fixedHeader: true
    } );
    } );
</script>

@endsection