@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-5 col-md-5">
                <h2 class="display h4">MANAGE CONTRACTS</h2>
            </div>
        </div>
        <div class="breadcrumb-holder">
            <div class="container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a></li>
                    <li class="breadcrumb-item active"> Deliverable Payment Update for {{$contractName}} - {{$deliverableName}} </li>
                </ul>
            </div>
        </div>
        @include('error')
        <form method="POST" action="{{ asset('manage-contracts/deliverables/paymentUpdate')}}/{{$Deliverable_ID}}" novalidate="" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="hidden" name="contractid" id="contractid" value="{{$Contract_ID}}">
            <input type="hidden" name="deliverableid" id="deliverableid" value={{$Deliverable_ID}}>
            <table class="table">
                <tbody class="table table-borderless tbody">
                @foreach($payment as $data)
                    <tr>
                        <td> Gross Payment:</td>
                        @if(!is_null($data->Gross_Payment))
                        <td><input type="number" name="grossPayment" id="grossPayment" value="<?php echo $data->Gross_Payment ?>" size=40></td>
                        @else
                        <td><input type="number" name="grossPayment" id="grossPayment" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>10% Retention:</td>
                        @if(!is_null($data->Gross_Payment))
                        <td><input type="number" name="retention" value="<?php echo $data->Retention ?>" id="retention" size=40></td>
                        @else
                        <td><input type="number" name="retention" id="retention" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Value Added Tax (VAT):</td>
                        @if(!is_null($data->VAT))
                        <td><input type="number" name="vat" value="<?php echo $data->VAT ?>" id="vat" size=40></td>
                        @else
                        <td><input type="number" name="vat" id="vat" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Reimbursible Amount:</td>
                        @if(!is_null($data->Reimbursible_Amt))
                        <td><input type="number" name="reimbursible" value="<?php echo $data->Reimbursible_Amt ?>" id="reimbursible" size=40></td>
                        @else
                        <td><input type="number" name="reimbursible" id="reimbursible" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>EWT:</td>
                        @if(!is_null($data->EWT))
                        <td><input type="number" name="ewt" value="<?php echo $data->EWT ?>" id="ewt" size=40></td>
                        @else
                        <td><input type="number" name="ewt" id="ewt" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Net Payment:</td>
                        @if(!is_null($data->Net_Payment))
                        <td><input type="number" name="netpmt" value="<?php echo $data->Net_Payment ?>" id="netpmt" size=40></td>
                        @else
                        <td><input type="number" name="netpmt" id="netpmt" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Liquidated Damages:</td>
                        @if(!is_null($data->Liquidated_Damages))
                        <td><input type="number" name="lqdtdDmgs" value="<?php echo $data->Liquidated_Damages ?>" id="lqdtdDmgs" size=40></td>
                        @else
                        <td><input type="number" name="lqdtdDmgs" id="lqdtdDmgs" size=40>
                        @endif
                        </td>
                    </tr>
                    <tr>
                        <td>Payment Date:</td>
                        @if(!is_null($data->Payment_Date))
                        <td><input type="number" name="pmtDate" value="<?php echo $data->Payment_Date ?>" id="pmtDate" size=40></td>
                        @else
                        <td><input type="date" name="pmtDate" id="pmtDate" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Disbursement Voucher:</td>
                        @if(!is_null($data->Disbursement_Voucher))
                        <td><input type="file" name="dbmtvch" value="<?php echo $data->Disbursement_Voucher ?>" id="dbmtvch" size=40></td>
                        @else
                        <td><input type="file" name="dbmtvch" id="dbmtvch" size=40></td>
                        @endif
                    </tr>
                    <tr>
                        <td>Status:</td>
                        @if(!is_null($data->Payment_Status))
                        <td>
                            <select name="paymentStatus" id="paymentStatus">
                                <option disabled="" value="">Select Status
                                </option>
                                <option value="Ongoing" <?php if($data->Payment_Status=="Ongoing") echo 'selected="selected"'; ?>>Ongoing</option>
                                <option value="Paid" <?php if($data->Payment_Status=="Paid") echo 'selected="selected"'; ?>>Paid</option>
                            </select>
                         </td>
                        @else
                        <td>
                            <select name="paymentStatus" id="paymentStatus">
                                <option disabled="" selected="" value="">Select Status
                                </option>
                                <option value="Ongoing">Ongoing</option>
                                <option value="Paid">Paid</option>
                            </select>
                        </td>
                        @endif
                    </tr>
                @endforeach    
                    <tr style="text-align:center">
                        <td colspan=2>
                            <input type="submit" id="pmtUpdBtn" name="pmtUpdBtn" class="btn btn-primary" value="Update">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
    </div>
</section>

@endsection