<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contract Report</title>
    <script>
        var logout = 'Session Expired. Login again to continue';
        var timeout;
        var url =  "{{ asset('/login') }}"; // route path;
        document.onmousemove = function(){
            clearTimeout(timeout);
                timeout = setTimeout(function () {
                    var confirmstatus = confirm( logout );
                    
                    if(confirmstatus === true) {
                       window.location.href = url;
                    }
                }, 60000*120);
            };
    </script>
</head>

<body>
    <table id="pdf" style="width:device-width;border: 1px solid black;">
        <tr>
            <td colspan=8 style="text-align:center;"><img src="{{ public_path('img/header.png') }}" alt="NEDA HEADER"></td>
        </tr>
        <tr>
            <td colspan=8 style="text-align:center;font-family:Arial Narrow; font-size:15"><strong>CONTRACT MONITORING <br> Status Report for </strong>{{$date}} <br><br><br> </td>
        </tr>
        <tr>
            <td colspan=8 style="background-color:rgb(220,220,220)"><br></td>
        </tr>
        <tr>
            <td style="padding:10"></td>
        </tr>
        <tr>
            <td colspan=8 style="background-color:rgb(0,0,0);font-size:2px"><br></td>
        </tr>
        <tr>
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">NO.</th> 
            @if(in_array('PrjName', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">PROJECT NAME</th>
            @endif
            @if(in_array('ConsultancyFirm', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">CONSULTANT/ FIRM NAME</th>
            @endif
            @if(in_array('FirmType', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">FIRM TYPE</th>
            @endif
            @if(in_array('EU', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">END USER</th> 
            @endif
            @if(in_array('IAgency', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">IMPLEMENTING AGENCY</th> 
            @endif
            @if(in_array('CoIAgency', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">CO-IMPLEMENTING AGENCY</th> 
            @endif
            @if(in_array('NOADt', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">NOA RELEASE DATE</th> 
            @endif
            @if(in_array('NTPDt', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">NTP RELEASE DATE</th> 
            @endif
            @if(in_array('NTPConfDt', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">NTP CONFORME DATE</th> 
            @endif
            @if(in_array('CStartDt', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">CONTRACT START</th>
            @endif
            @if(in_array('CEndDt', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">CONTRACT END</th>
            @endif
            @if(in_array('CCost', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">CONTRACT COST</th> 
            @endif
            @if(in_array('CABC', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">ABC</th> 
            @endif
            @if(in_array('FundSource', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">FUND SOURCE</th> 
            @endif
            @if(in_array('FundYr', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">FUND YEAR</th> 
            @endif
            @if(in_array('Performance', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">PERFORMANCE SECURITY</th> 
            @endif
            @if(in_array('BondValidity', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">BOND VALIDITY</th> 
            @endif
            @if(in_array('IssuingCompany', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">ISSUING COMPANY</th> 
            @endif
            @if(in_array('SuppDocs', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">DOCUMENTS</th> 
            @endif
            @if(in_array('Deliverables', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">DELIVERABLES</th> 
            @endif
            @if(in_array('PmtStat', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">PAYMENT STATUS</th>
            @endif
            @if(in_array('PrjStat', $view))
            <th style="border-left: 3px solid black;border-bottom: 3px solid black;border-right: 3px solid black;">PROJECT STATUS</th>
            @endif
        </tr>
        <?php $count = 1;  ?>
        @foreach($contract as $index => $value)
            <tr>    
                <td style="border-left: 2px solid black;border-bottom: 2px solid black;"><?php echo $count; $count+=1; ?></td>
                @if(in_array('PrjName', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value -> Contract_Name}}<br><br></td>
                @endif
                @if(in_array('ConsultancyFirm', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                        @foreach($firmname as $firm)
                            @if($contract[$index]->Contract_ID == $firm->Contract_ID)
                                {{$firm->Firm_Name}}<br>
                            @endif
                        @endforeach
                        <br><br>
                    </td>
                @endif
                @if(in_array('FirmType', $view))
                <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                    @foreach($consultant as $cons)
                        @if($contract[$index]->Contract_ID == $cons->Contract_ID)
                            {{$cons->Firm_Type_Desc}}<br>
                        @endif
                    @endforeach
                    <br><br>
                </td>
                @endif
                @if(in_array('EU', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->End_User}}<br><br></td>
                @endif
                @if(in_array('IAgency', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->Implementing_Agency}}<br><br></td>
                @endif
                @if(in_array('CoIAgency', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->Co_Implementing_Agency}}<br><br></td>
                @endif
                @if(in_array('NOADt', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->NOA_Release_Date}}<br><br></td>
                @endif
                @if(in_array('NTPDt', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->NTP_Release_Date}}<br><br></td>
                @endif
                @if(in_array('NTPConfDt', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->NTP_Conforme_Date}}<br><br></td>
                @endif
                @if(in_array('CStartDt', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->Contract_Start_Date}}<br><br></td>
                @endif
                @if(in_array('CEndDt', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->Contract_End_Date}}<br><br></td>
                @endif
                @if(in_array('CCost', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;"><?php echo number_format($value->Contract_Cost,2)?><br><br></td>
                @endif
                @if(in_array('CABC', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;"><?php echo number_format($value->ABC,2)?><br><br></td>
                @endif
                @if(in_array('FundSource', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->Fund_Source}}<br><br></td>
                @endif
                @if(in_array('FundYr', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">{{$value->Fund_Year}}<br><br></td>
                @endif
                @if(in_array('Performance', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                        @if(!is_null($perfSec))
                            @foreach($perfSec as $sec)
                                @if($contract[$index]->Contract_ID == $sec->Contract_ID)
                                    Type:<strong>{{$sec->Type_Description}}</strong><br>
                                    @if($sec->Type == 'C')
                                        Official Receipt: <strong>{{$sec->CashOR}}</strong>
                                        Bond Validity: <strong>{{$sec->CashBV}}</strong>
                                        Bond Validity Notes: <strong>{{$sec->CashBVNotes}}</strong>
                                    @elseif($sec->Type == 'CMC')
                                        Check Number: <strong>{{$sec->CheckNum}}</strong><br>
                                        Official Receipt: <strong>{{$sec->CheckOR}}</strong><br>
                                        Bank Name: <strong>{{$sec->CheckBank}}</strong>
                                        Bond Validity: <strong>{{$sec->CheckBV}}</strong>
                                        Bond Validity Notes: <strong>{{$sec->CheckBVNotes}}</strong>
                                    @elseif($sec->Type == 'BD')
                                        Bank Name: <strong>{{$sec->BankNameBD}}</strong><br>
                                        Bank Draft: <strong>{{$sec->DraftBD}}</strong><br>
                                        Validity: <strong>{{$sec->ValidityBD}}</strong>
                                        Bond Validity Notes: <strong>{{$sec->ValidityBDNotes}}</strong>
                                    @elseif($sec->Type == 'BG')
                                        Bank Name: <strong>{{$sec->BankNameBG}}</strong><br>
                                        Bank Guarantee: <strong>{{$sec->DraftBG}}</strong><br>
                                        Validity: <strong>{{$sec->ValidityBG}}</strong>
                                        Bond Validity Notes: <strong>{{$sec->ValidityBGNotes}}</strong>
                                    @elseif($sec->Type == 'ILC')
                                        Bank Name: <strong>{{$sec->BankNameILC}}</strong><br>
                                        Bank Guarantee: <strong>{{$sec->ILC}}</strong><br>
                                        Validity: <strong>{{$sec->ValidityILC}}</strong>
                                        Bond Validity Notes: <strong>{{$sec->ValidityILCNotes}}</strong>
                                    @elseif($sec->Type == 'SB')
                                        Bank Name: <strong>{{$sec->CNameSurety}}</strong><br>
                                        Bank Guarantee: <strong>{{$sec->SuretyNum}}</strong><br>
                                        Validity: <strong>{{$sec->ValiditySurety}}</strong>
                                        Bond Validity Notes: <strong>{{$sec->ValiditySuretyNotes}}</strong>
                                    @endif
                                @endif
                            @endforeach
                        @else
                            No Performance Security Available
                        @endif
                    </td>
                @endif
                @if(in_array('IssuingCompany', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                        @if(!is_null($perfSec))
                            @foreach($perfSec as $sec)
                                @if($contract[$index]->Contract_ID == $sec->Contract_ID)
                                    <strong>{{$sec->Issuing_Company}}</strong><br>
                                @endif
                            @endforeach
                        @else
                            No Issuing Company Available
                        @endif
                    </td>
                @endif
                @if(in_array('SuppDocs', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                        <strong>
                            @foreach($documents as $docs)
                                @if($contract[$index]->Contract_ID == $docs->Contract_ID)
                                    {{basename($docs->Document_Link)}} <br>
                                @endif
                            @endforeach
                        </strong>
                    </td>
                @endif
                @if(in_array('Deliverables', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                        @foreach($deliverables as $deli)
                            Deliverable: <strong>{{$deli->Deliverable}}</strong> <br>
                            Target Date: <strong>{{$deli->Target_Completion_Date}}</strong> <br>
                            @if(!is_null($deli->Amended_Date))
                                Amended Date: <strong>{{$deli->Amended_Date}}</strong> <br>
                            @endif
                            @if(!is_null($deli->Actual_Completion_Date))
                                Amended Date: <strong>{{$deli->Actual_Completion_Date}}</strong> <br>
                            @endif
                                Payment Status: <strong>{{$deli->Payment_Status}} </strong> <br>
                        @endforeach
                        </strong>
                    </td>
                @endif
                @if(in_array('PmtStat', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;">
                    <?php 
                        $deliverables = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->get();
                        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->where('Payment_Status','Paid')->count();
                        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->where('Payment_Status','Ongoing')->count();
                        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$value -> Contract_ID)->whereNull('Payment_Status')->count();
                    ?>

                    <?php $stopforongoing = 0; ?>
                    @foreach($deliverables as $deliverable)
                        @if($deliverable->Payment_Status == 'Ongoing')
                            {{ $deliverable->Payment_Status }}
                            <?php $stopforongoing = 1; ?>                                        
                        @endif
                        <?php if($stopforongoing == 1) { ?>
                            @break
                        <?php } ?>
                    @endforeach
                                        
                    @if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                        Paid
                    @endif

                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID))
                        -
                    @endif

                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                        Ongoing
                    @endif
                    <br><br>
                    </td>
                @endif
                @if(in_array('PrjStat', $view))
                    <td style="border-left: 2px solid black;border-bottom: 2px solid black;border-right: 2px solid black;">{{$value->Contract_Status}}<br><br></td>
                @endif
            </tr>
        @endforeach
    </table>
    <br><br>
    <small><p style="color:rgb(105,105,105)">National Economic and Development Authority &copy; 2020<span style="float:right">CMS</span></p></small>
</body>
</html>
