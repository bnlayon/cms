@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">

    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">VIEW CONTRACT</h2><br>
            </div>
            <!-- <form action="" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }} -->
            <div class="col-lg-15 col-md-12">
            
                <div class="breadcrumb-holder">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a></li>
                            <?php $Contract_ID = encrypt($Contract_ID)?>
                            <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts/view-contract') }}/{{$Contract_ID}}"> View Contract</a></li>
                            <li class="breadcrumb-item active"> Project/Contract Status</li>
                        </ul>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="ProcMode"><strong>CONTRACT ID</strong></label><br>
                            <?php $Contract_ID = decrypt($Contract_ID)?>
                            {{$Contract_ID}}
                        </div>
                        <div class="form-group">
                            <label for="ProcMode"><strong>PROJECT NAME</strong></label><br>
                            {{$name}}
                        </div>
                        <div class="form-group">
                            <label for="ProcMode"><strong>PROJECT STATUS </strong></label><br>
                            {{$status}}
                        </div>
                        <div class="form-group">
                            <label for="ProcMode"><strong>DELIVERABLES PAYMENT STATUS</strong></label><br>
                            {{$status}} 
                        </div>
                        <div class="form-group">
                            <label for="ProcMode"><strong>CERTIFICATE OF FINAL ACCEPTANCE</strong> </label><br>
                                @foreach($getcfa as $docs)
                                    <a href="{{url('/storage/storage/CFA')}}/{{basename($docs)}}" target="_blank">{{basename($docs)}}</a><br>
                                @endforeach
                        </div>
                        <div class="form-group">
                            <label for="ProcMode"><strong>REVIEW NOTES ON PROJECT/CONTRACT</strong> </label><br>
                            <div class="table-responsive">
                                    <table class="table table-striped">
                                        <tbody >
                                            <tr>
                                                <th class="thead-light" scope="row" ><strong>INPUT DATE:</strong></th>
                                                @foreach($remarks as $remark)
                                                    <td>{{$remark->date_reviewed}}</td>
                                                @endforeach
                                            </tr>
                                            <tr>
                                                <th class="thead-light" scope="row"><strong>PROJECT UPDATE:</strong></th>
                                                @foreach($remarks as $remark)
                                                    <td>{{$remark->notes}}</td>
                                                @endforeach
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
