<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CMS Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fontastic.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body>
    <div class="page login-page">
      <div class="container">
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner" style="width:100%;">
            <div class="logo text-uppercase"><strong class="text-primary">Contract Management System</strong></div><br/>
            <p><img src="img/neda.jpg" alt="person" class="img-fluid rounded-circle"></p>
            <form class="text-left form-validate" action="{{ asset('/login') }}" method="POST" id="form" >{{ csrf_field() }}
              <div class="form-group-material">
                <input id="login-username" type="text" name="loginUsername" id="loginUsername" required data-msg="Please enter your username" class="input-material">
                <label for="login-username" class="label-material">Username</label>
              </div>
              <div class="form-group-material">
                <input id="login-password" type="password" name="loginPassword" id="loginPassword" required data-msg="Please enter your password" class="input-material">
                <label for="login-password" class="label-material">Password</label>
              </div>

              {{-- <div class="g-recaptcha" data-sitekey="6LfJA3AUAAAAAGht99VY1X_mWZiuzf4mSCKjRAZj"></div> --}}

              @include('error')
              <div class="form-group text-center">
                {{-- <input id="{{ asset('/login') }}" type="submit" value="Login" class="btn btn-primary"> --}}
                <input id="{{ asset('/login') }}" type="submit" value="Login" class="btn btn-primary g-recaptcha" 
                  data-sitekey="6LccS6oUAAAAABEf1kjN1rlnF952H12y7DsBCx6F" 
                  data-callback='onSubmit'>
              </div>
            {{-- </form><a href="#" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small><a href="/register" class="signup">Signup</a> --}}
            </form><a href="{{url('/password/reset')}}" class="forgot-pass">Forgot Password?</a><small>Do not have an account? </small><a href="{{url('/register')}}" class="signup">Signup</a>
          </div>
          <div class="copyrights text-center">
            <p>National Economic and Development Authority <a href="#" class="external">Information and Communications Technology Staff</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
    <script>
      function onSubmit(token) {
             document.getElementById("form").submit();
           }
    </script>
  </body>
</html>

@include('sweet::alert')