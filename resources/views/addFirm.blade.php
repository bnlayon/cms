@extends ('layouts')

@section ('content')
    
    @if(Session::has('consSuccess'))
        <div class="alert alert-success">
            {{ session()->get('consSuccess') }}
        </div>
    @endif
    
    <section class="firms-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <div class="col-lg-12 col-md-12">
                    <h2 style="display:inline;float:left;" class="display h4">CONSULTANCY FIRMS</h2><br>
                </div>
            </div>
            <div class="breadcrumb-holder">
                <div class="container-fluid">
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ asset('/consultancyfirms') }}"> Consultant/Firm Name</a></li>
                        <li class="breadcrumb-item active"> ADD NEW FIRM </li>
                    </ul>
                </div>
            </div>
            <br>
            @include('error')
            <form method="post" id="firmForm" action="{{ asset('/consultancyfirms')}}" novalidate=""  enctype="multipart/form-data">
                {{csrf_field()}}
                <table class="table">
                    <tbody class="table table-borderless tbody">
                            <tr>
                                <td>Firm Name:</td>
                                <td><input type=text name="newFirmName" id="newFirmName" size=40 value="{{old('newFirmName')}}">@include('alerts.feedback', ['field' => 'Firm_Name'])</td>
                            </tr>
                            <tr>
                                <td>Address:</td>
                                <td><input type=text name="newFirmAddress" id="newFirmAddress" size=40 value="{{old('newFirmAddress')}}"></td>
                            </tr>
                            <tr>
                                <td>Email Address:</td>
                                <td><input type=email name="newFirmEmail" id="newFirmEmail" size=40 value="{{old('newFirmEmail')}}"></td>
                            </tr>
                            <tr>
                                <td>Alternate Email Address1:</td>
                                <td><input type=email name="newFirmAltEmail1" id="newFirmAltEmail1" size=40 value="{{old('newFirmAltEmail1')}}"></td>
                            </tr>
                            <tr>
                                <td>Alternate Email Address2:</td>
                                <td><input type=email name="newFirmAltEmail2" id="newFirmAltEmail2" size=40 value="{{old('newFirmAltEmail2')}}"></td>
                            </tr>
                            <tr>
                                <td>Alternate Email Address3:</td>
                                <td><input type=email name="newFirmAltEmail3" id="newFirmAltEmail3" size=40 value="{{old('newFirmAltEmail3')}}"></td>
                            </tr>
                            <tr>
                                <td>Contact Person:</td>
                                <td><input type=text name="newContactPerson" id="newContactPerson" size=40 value="{{old('newContactPerson')}}"></td>
                            </tr>
                            <tr>
                                <td>Designation:</td>
                                <td><input type=text name="newDesignation" id="newDesignation" size=40 value="{{old('newDesignation')}}"></td>
                            </tr>
                            <tr>
                                <td>Telephone:</td>
                                <td><input type=text name="newFirmTelephone" id="newFirmTelephone" size=40 value="{{old('newFirmTelephone')}}"></td>
                            </tr>
                            <tr>
                                <td>Experts and Roles:</td>
                                <td>
                                    <input type="text" name="newexperts[]" id="newexperts[]" size=50
                                            required><input type="text" placeholder="Expert Role" name="newexpertsrole[]" id="newexpertsrole[]" size=30
                                            required> &nbsp<div id="new_experts" style="display:inline;"></div><br>
                                    <a href="javascript:void(0);" id="newexpertsButton">add experts</a><br>
                                </td>
                            </tr>
                            <tr>
                                <td>Fax Number:</td>
                                <td><input type=text name="newFirmFax" id="newFirmFax" size=40 value="{{old('newFirmFax')}}"></td>
                            </tr>
                            <tr>
                                <td>PhilGEPS Certification:</td>
                                <td><input type="file" name="newphilgeps" id="newphilgeps" size="40" value="{{old('newphilgeps')}}"></td>
                            </tr>
                            <tr>
                                <td>SEC/DTI Registration:</td>
                                <td><input type="file" name="newsecdti" id="newsecdti" size="40" value="{{old('newsecdti')}}"></td>
                            </tr>
                            <tr>
                                <td>Business Permit:</td>
                                <td><input type="file" name="newbuspermit" id="newbuspermit" size="40" value="{{old('newbuspermit')}}"></td>
                            </tr>
                            <tr>
                                <td>Tax Clearance:</td>
                                <td><input type="file" name="newtaxclrnc" id="newtaxclrnc" size="40" value="{{old('newtaxclrnc')}}"></td>
                            </tr>
                            <tr>
                                <td>Financial Documents:</td>
                                <td><input type="file" name="newfindocs[]" id="newfindocs[]" size="40" multiple value="{{old('newfindocs')}}"></td>
                            </tr>
                            <tr>
                                <td>Procurement/s Joined:</td>
                                <td>
                                    <input type="text" name="procjoined[]" id="procjoined[]" size=30 required>&nbsp<a
                                            href="javascript:void(0);" id="procjoinedButton">add</a><br><div id="new_procjoined" style="display:inline;"></div></td>
                            </tr>
                            <tr>
                                <td colspan=2 class="text-center"><strong>NOTE:</strong><i>Please follow this format on document's names for upload ></i><strong>FirmName_{file}</strong><br>
                                    Ex: TestConsultant_PHILGEPS 
                                </td>
                            </tr>
                            <tr>
                                <td colspan=2 class="text-center">
                                    <input type="submit" class="btn-primary text-center" id="addfirm"
                                                value="Add Firm">
                                </td>
                            </tr>
                    </tbody>
                </table>
            </form>
        </div>
    </section>
@endsection