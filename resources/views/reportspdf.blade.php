<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contract Details</title>
    <script>
        var logout = 'Session Expired. Login again to continue';
        var timeout;
        var url =  "{{ asset('/login') }}"; // route path;
        document.onmousemove = function(){
            clearTimeout(timeout);
                timeout = setTimeout(function () {
                    var confirmstatus = confirm( logout );
                    
                    if(confirmstatus === true) {
                       window.location.href = url;
                    }
                }, 60000*120);
            };
    </script>
</head>

<body>
    <table id="pdf" style="padding-left:20;width:device-width">
            
        @foreach($data as $report)
            <tr>
                <td colspan=2 style="text-align:center;"><img src="{{ public_path('img/header.png') }}" alt="NEDA HEADER"></td>
            </tr>
            <tr>
                <td colspan=2 style="text-align:center;font-family:Arial Narrow; font-size:15"><strong>CONTRACT MONITORING <br> Status Report for </strong>{{$date}} <br><strong> <br>Project: </strong>
                        {{$report->Contract_Name}} <br><br><br> </td>
            </tr>
            <tr>
                <td colspan=2 style="background-color:rgb(220,220,220)"><br></td>
            </tr>
            <tr>
                <td style="padding:10"></td>
            </tr>
            @if(in_array('PrjName', $view))
                <tr>
                    <td style="padding-left:30">PROJECT NAME</td>
                    <td style="padding-left:60"><strong>{{$report->Contract_Name}}</strong></td>
                </tr>
            @endif
            @if(in_array('ConsultancyFirm', $view))
            <tr>
                <td style="padding-left:30">CONSULTANT</td>
                <td style="padding-left:60">
                    <strong>
                        @foreach($consultant as $firm)
                        {{$firm->Firm_Name}}
                        @endforeach
                    </strong>
                </td>
            </tr>
            @endif
            @if(in_array('FirmType', $view))
            <tr>
                <td style="padding-left:30">FIRM TYPE</td>
                <td style="padding-left:60">
                    <strong>
                        @foreach($consultant as $firm)
                        {{$firm->Firm_Type_Desc}}
                        @endforeach
                    </strong>
                </td>
            </tr>
            @endif
            @if(in_array('EU', $view))
            <tr>
                <td style="padding-left:30">END-USER</td>
                <td style="padding-left:60">
                    <strong>{{$report->End_User}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('IAgency', $view))
            <tr>
                <td style="padding-left:30">IMPLEMENTING AGENCY</td>
                <td style="padding-left:60">
                    <strong>{{$report->Implementing_Agency}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('CoIAgency', $view))
            <tr>
                <td style="padding-left:30">CO-IMPLEMENTING AGENCY</td>
                <td style="padding-left:60">
                    <strong>{{$report->Co_Implementing_Agency}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('NOADt', $view))
            <tr>
                <td style="padding-left:30">Notice of Acceptance Date</td>
                <td style="padding-left:60">
                    <strong>{{$report->NOA_Release_Date}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('NTPDt', $view))
            <tr>
                <td style="padding-left:30">Notice To Proceed Date</td>
                <td style="padding-left:60">
                    <strong>{{$report->NTP_Release_Date}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('NTPConfDt', $view))
            <tr>
                <td style="padding-left:30">NTP Conforme Date</td>
                <td style="padding-left:60">
                    <strong>{{$report->Conforme_Date}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('CStartDt', $view))
            <tr>
                <td style="padding-left:30">Contract Start Date</td>
                <td style="padding-left:60">
                    <strong>{{$report->Contract_Start_Date}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('CEndDt', $view))
            <tr>
                <td style="padding-left:30">Contract End Date</td>
                <td style="padding-left:60">
                    <strong>{{$report->Contract_End_Date}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('CCost', $view))
            <tr>
                <td style="padding-left:30">Contract Cost</td>
                <td style="padding-left:60">
                    <strong><?php echo number_format($report->Contract_Cost,2)?></strong>
                </td>
            </tr>
            @endif
            @if(in_array('CABC', $view))
            <tr>
                <td style="padding-left:30">Approved Budget for Costing</td>
                <td style="padding-left:60">
                    <strong><?php echo number_format($report->ABC,2)?></strong>
                </td>
            </tr>
            @endif
            @if(in_array('FundSource', $view))
            <tr>
                <td style="padding-left:30">Fund_Source</td>
                <td style="padding-left:60">
                    <strong>{{$report->Fund_Source}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('FundYr', $view))
            <tr>
                <td style="padding-left:30">Fund Year</td>
                <td style="padding-left:60">
                    <strong>{{$report->Fund_Year}}</strong>
                </td>
            </tr>
            @endif
            @if(in_array('Performance', $view))
            <tr>
                <td style="padding-left:30">Performance Security</td>
                <td style="padding-left:60">
                    @if(!is_null($perfSec))
                        @foreach($perfSec as $sec)
                            Type:<strong>{{$sec->Type_Description}}</strong><br>
                            @if($sec->Type == 'C')
                                Official Receipt: <strong>{{$sec->CashOR}}</strong>
                                Bond Validity Notes: <strong>{{$sec->CashBVNotes}}</strong>
                            @elseif($sec->Type == 'CMC')
                                Check Number: <strong>{{$sec->CheckNum}}</strong><br>
                                Official Receipt: <strong>{{$sec->CheckOR}}</strong><br>
                                Bank Name: <strong>{{$sec->CheckBank}}</strong>
                                Bond Validity: <strong>{{$sec->CheckBV}}</strong>
                                Bond Validity Notes: <strong>{{$sec->CheckBVNotes}}</strong>
                            @elseif($sec->Type == 'BD')
                                Bank Name: <strong>{{$sec->BankNameBD}}</strong><br>
                                Bank Draft: <strong>{{$sec->DraftBD}}</strong><br>
                                Validity: <strong>{{$sec->ValidityBD}}</strong>
                                Bond Validity Notes: <strong>{{$sec->ValidityBDNotes}}</strong>
                            @elseif($sec->Type == 'BG')
                                Bank Name: <strong>{{$sec->BankNameBG}}</strong><br>
                                Bank Guarantee: <strong>{{$sec->DraftBG}}</strong><br>
                                Validity: <strong>{{$sec->ValidityBG}}</strong>
                                Bond Validity Notes: <strong>{{$sec->ValidityBGNotes}}</strong>
                            @elseif($sec->Type == 'ILC')
                                Bank Name: <strong>{{$sec->BankNameILC}}</strong><br>
                                Bank Guarantee: <strong>{{$sec->ILC}}</strong><br>
                                Validity: <strong>{{$sec->ValidityILC}}</strong>
                                Bond Validity Notes: <strong>{{$sec->ValidityILCNotes}}</strong>
                            @elseif($sec->Type == 'SB')
                                Bank Name: <strong>{{$sec->CNameSurety}}</strong><br>
                                Bank Guarantee: <strong>{{$sec->SuretyNum}}</strong><br>
                                Validity: <strong>{{$sec->ValiditySurety}}</strong>
                                Bond Validity Notes: <strong>{{$sec->ValiditySuretyNotes}}</strong>
                            @endif
                        @endforeach
                    @else
                        No Performance Security Available
                    @endif
                </td>
            </tr>
            @endif
            @if(in_array('IssuingCompany', $view))
            <tr>
                <td style="padding-left:30">Issuing Company</td>
                <td style="padding-left:60">
                    @if(!is_null($perfSec))
                        @foreach($perfSec as $sec)
                            <strong>{{$sec->Issuing_Company}}</strong><br>
                        @endforeach
                    @else
                        No Issuing Company Available
                    @endif
                </td>
            </tr>
            @endif
            @if(in_array('SuppDocs', $view))
            <tr>
                <td style="padding-left:30">Supporting Documents</td>
                <td style="padding-left:60">
                    <strong>
                        @foreach($documents as $docs)
                            {{basename($docs->Document_Link)}} <br>
                        @endforeach
                    </strong>
                </td>
            </tr>
            @endif
            @if(in_array('Deliverables', $view))
            <tr>
                <td style="padding-left:30">Deliverables</td>
                <td style="padding-left:60">
                    @foreach($deliverables as $deli)
                        Deliverable: <strong>{{$deli->Deliverable}}</strong> <br>
                        Target Date: <strong>{{$deli->Target_Completion_Date}}</strong> <br>
                        @if(!is_null($deli->Amended_Date))
                            Amended Date: <strong>{{$deli->Amended_Date}}</strong> <br>
                        @endif
                        @if(!is_null($deli->Actual_Completion_Date))
                            Amended Date: <strong>{{$deli->Actual_Completion_Date}}</strong> <br>
                        @endif
                            Payment Status: <strong>{{$deli->Payment_Status}} </strong> <br>
                    @endforeach
                    </strong>
                </td>
            </tr>
            @endif
            @if(in_array('PmtStat', $view))
            <tr>
                <td style="padding-left:30">Payment Status</td>
                <td style="padding-left:60">
                    <strong>
                    <?php 
                                        $deliverables = \App\Deliverables::where('Contract_ID',$report -> Contract_ID)->get();
                                        $deliverablescountPAID = \App\Deliverables::where('Contract_ID',$report -> Contract_ID)->where('Payment_Status','Paid')->count();
                                        $deliverablescountONGOING = \App\Deliverables::where('Contract_ID',$report -> Contract_ID)->where('Payment_Status','Ongoing')->count();
                                        $deliverablescountNULL = \App\Deliverables::where('Contract_ID',$report -> Contract_ID)->whereNull('Payment_Status')->count();
                                    ?>

                                    <?php $stopforongoing = 0; ?>
                                    @foreach($deliverables as $deliverable)
                                        @if($deliverable->Payment_Status == 'Ongoing')
                                            {{ $deliverable->Payment_Status }}
                                            <?php $stopforongoing = 1; ?>                                        
                                        @endif
                                        <?php if($stopforongoing == 1) { ?>
                                            @break
                                        <?php } ?>
                                    @endforeach
                                        
                                    @if(empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Paid
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND empty($deliverablescountPAID))
                                        -
                                    @endif

                                    @if(!empty($deliverablescountNULL) AND empty($deliverablescountONGOING) AND !empty($deliverablescountPAID))
                                        Ongoing
                                    @endif
                    </strong>
                </td>
            </tr>
            @endif
            @if(in_array('PrjStat', $view))
            <tr>
                <td style="padding-left:30">Project Status</td>
                <td style="padding-left:60">
                    <strong>{{$report->Contract_Status}}</strong>
                </td>
            </tr>
            @endif
        @endforeach
    </table>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <small><p style="color:rgb(105,105,105)">National Economic and Development Authority &copy; 2020<span style="float:right">CMS</span></p></small>
</body>
</html>
