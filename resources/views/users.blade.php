@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
  <div class="container-fluid">
  @if(Session::has('approveSuccess'))
          <div class="alert alert-success">
              {{ session()->get('approveSuccess') }}
          </div>
      @elseif(Session::has('approveFailed'))
          <div class="alert alert-warning">
              {{ session()->get('approveFailed') }}
          </div>
      @endif
      @if(Session::has('access_type'))
        @if(Session::get('access_type')!='A')
            <script>
                window.location = "/dashboard";
            </script>
        @endif  
      @endif   
    <div class="row d-flex align-items-md-stretch">
        <h2 class="display h4">USER MANAGEMENT</h2><br><br>
    </div>

    <div class="breadcrumb-holder">
      <div class="container-fluid">
        <ul class="breadcrumb">
          <li class="breadcrumb-item"><a href="/dashboard">Home</a></li>
          <li class="breadcrumb-item active">Users </li>
        </ul>
      </div>
    </div>
    <br>
  
      <ul class="nav nav-pills nav-justified" role="tablist">
            <li class="nav-item">
              <a class="nav-link" data-toggle="tab" href="#all" role="tab">Approved Users</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" data-toggle="tab" href="#req" role="tab">User Requests</a>
            </li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">

            <div class="tab-pane" id="all" role="tabpanel" style="background-color: white">
            <table id="userstable" class="display ui striped table" style="width:100%">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office/Staff - Division/Unit</th>
                        <th>Access Type</th>
                        <th>Email Address</th>
                        <th>Date Approved</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($active as $users)
                    <tr>
                        <td>{{$users->username}}</td>
                        <td>{{ $users->fname }} {{ $users->lname }}</td>
                        <td>{{ $users->position }}</td>
                        <td>{{ $users->office_staff }} - {{ $users->division_unit }}<?php $idtoapprove = $users->user_id ?></td>  
                        <td>{{ $users->access_type }}</td>
                        <td>{{ $users->email_address }}</td>
                        <td>{{ $users->account_approval_date }}</td>
                    </tr>    
                  @endforeach
                </tbody>
              
              </table>
            </div>
            
            <div class="tab-pane active" id="req" role="tabpanel" style="background-color: white">
              <table id="usersreqtable" class="table table-bordered gridview data-table" style="table-layout:fixed"> 
                <thead>
                    <tr>
                        <th>Email Address</th>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Office/Staff - Division/Unit</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody> 
                  @foreach ($requests as $userrequest)
                      <tr>
                        <td>{{ $userrequest->email_address }}</td>
                        <td>{{ $userrequest->fname }} {{ $userrequest->lname }}</td>
                        <td>{{ $userrequest->position }}</td>
                        <td>{{ $userrequest->office_staff }} - {{ $userrequest->division_unit }}<?php $idtoapprove = $userrequest->user_id ?></td>  
                        <td>
                          <form action="{{ asset('/activate') }}/{{$idtoapprove}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                            <button type="button" data-toggle="modal" data-target="#approveaccount<?php  $idtoapprove ?>" class="btn btn-primary"><i class="fa fa-check"></i></button> 
                            <div id="approveaccount<?php $idtoapprove ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                              <div role="document" class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 id="exampleModalLabel" class="modal-title">Approve Account Request</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                  </div>
                                  <div class="modal-body">
                                    <h5>Are you sure you want to approve this account?</h5>
                                    Select Access Type to grant user:
                                    <select name="access_type" id="access_type">
                                      <option value="A">Admin</option>
                                      <option value="V">Viewer</option>
                                      <option value="E">Editor</option>
                                      <option value="A1">Admin-PMD</option>
                                    </select>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>  
                                    <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>

                          <form action="{{ asset('/decline') }}/{{$idtoapprove}}" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }}
                            <button type="button" data-toggle="modal" data-target="#declineremarks<?php $idtoapprove ?>" class="btn btn-danger"><i class="fa fa-close"></i></button> 
                            <div id="declineremarks<?php  $idtoapprove ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel<?php  ?>" aria-hidden="true" class="modal fade text-left">
                              <div role="document" class="modal-dialog modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <h5 id="exampleModalLabel<?php  ?>" class="modal-title">Decline Account Request</h5>
                                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                                  </div>
                                  <div class="modal-body">
                                    <div class="row">
                                      <div class="col-lg-12">
                                        <div class="card">
                                          <div class="card-body">
                                            <div class="i-checks">
                                              <input id="reason1<?php  ?>" type="radio" value="The user is not authorized to access the Contract Management System" name="reason" class="form-control-custom radio-custom">
                                              <label for="reason1<?php  ?>">The user is not authorized to access the Contract Management System</label>
                                            </div>
                                            <div class="i-checks">
                                              <input id="reason2<?php  ?>" type="radio" value="The email used to register is already linked to another account " name="reason" class="form-control-custom radio-custom">
                                              <label for="reason2<?php  ?>">The email used to register is already linked to another account </label>
                                            </div><br/>
                                            <div class="i-checks">
                                              <input id="reason3<?php  ?>" type="radio" value="Others" name="reason" class="form-control-custom radio-custom">
                                              <label for="reason3<?php  ?>">Others </label>
                                            </div>
                                            <div class="col-sm-12">
                                              <textarea class="form-control" rows="5" id="comment" name="reason_others"></textarea>
                                          </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="submit" class="btn btn-danger">Submit</button>  
                                    <button type="button" data-dismiss="modal" class="btn btn-secondary">Close</button>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </form>
                        </td>
                      </tr>
                  @endforeach    
                </tbody>
              </table>
            </div>      
      </div>

  </div>
</section>

@endsection