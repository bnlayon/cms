<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CMS v1.0</title>

    <style>
        #menu {
            position: fixed;
            right: 0;
            top: 50%;
            width: 15em;
            margin-top: -2.5em;
        }

        #menuButton {
            position: fixed;
            padding-left: 180px;
            right: 0;
            top: 50%;
            width: 15em;
            margin-top: -2.5em;
        }

        .select2-search--inline input {
            width: 700px !important;
        }

        .select2-selection__rendered {
            width: 700px !important
        }

        input[type=date]::-webkit-inner-spin-button{
            display: none;
            -webkit-appearance: none;
        }

        input[type="number"]::-webkit-outer-spin-button,
        input[type="number"]::-webkit-inner-spin-button {
            -webkit-appearance: none;
            margin: 0;
        }

        input[type=number]{
            -moz-appearance: textfield;
        }

    </style>
    
    <link href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js" defer></script>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <script src="https://unpkg.com/sweetalert2@7.18.0/dist/sweetalert2.all.js"></script>
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/fontastic.css') }}">
    <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('/css/own.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="{{ asset('/css/grasp_mobile_progress_circle-1.0.0.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css') }}">
    <link rel="stylesheet" href="{{ asset('/css/style.default.css') }}" id="theme-stylesheet">
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/js/bootstrap-select.min.js" referrerpolicy="no-referrer"></script> -->

    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/select2.min.js') }}"></script>
    <script src="{{ asset('vendor/popper.js/umd/popper.min.js') }}"> </script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/grasp_mobile_progress_circle-1.0.0.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery.cookie/jquery.cookie.js') }}"> </script>
    <script src="{{ asset('vendor/jquery-validation/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js') }}"></script>
    <script src="{{ asset('js/front.js') }}"></script>
    <script src="{{ asset('js/kc.js') }}"></script>
    <script src="{{ asset('js/bootstrap-multiselect.js') }}"></script>
 
    <script src="{{ asset('/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('/vendor/scrollreveal/scrollreveal.min.js') }}"></script>
    <script src="{{ asset('/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    <script src="{{ asset('/vendor/chart.js/Chart.js')}}"></script>
    <script src="{{ asset('/vendor/chart.js/Chart.min.js')}}"></script> 
    
    <script src="{{ asset('js/dataTable.js')}}"></script>
    <script src="{{ asset('js/createcontract.js?v=1.1') }}"></script>
    <script src="{{ asset('js/deliverables.js') }}"></script>
    <script src="{{ asset('js/consultancyfirm.js?v=1.2') }}"></script>
    <script src="{{ asset('js/managecontract.js?v=1.1') }}"></script>
    <script type="text/javascript"> 
        var activityTimeout = setTimeout(inActive, 3600000);

        function resetActive(){
            clearTimeout(activityTimeout);
            activityTimeout = setTimeout(inActive, 3600000);
        }

        function inActive(){
            Session.dump();
            window.location.replace("{{ asset('/') }}");
        }

        // Check for mousemove, could add other events here such as checking for key presses ect.
        $(document).bind('mousemove', function(){resetActive()});
    </script>
    
</head>

<body>
    
    <div class="se-pre-con"></div>
        @include ('sidebar')
        <div class="page">
            @include ('header')
            @yield ('content')
            @yield('scripts')
            @include ('footer')
            @include('sweet::alert')
        </div>
        
</body>
</html>
