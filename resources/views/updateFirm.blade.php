@extends ('layouts')

@section ('content')
    
    @if(Session::has('consSuccess'))
        <div class="alert alert-success">
            {{ session()->get('consSuccess') }}
        </div>
    @endif
    @if(is_null(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
    @endif
    @if(Auth::user()->access_type == 'V')
            <script>
                window.location = "/dashboard";
            </script>
    @endif
    <section class="firms-header section-padding">
        <div class="container-fluid">
            <div class="row d-flex align-items-md-stretch">
                <div class="col-lg-12 col-md-12">
                    <h2 style="display:inline;float:left;" class="display h4">CONSULTANCY FIRMS</h2><br><br>
                    <div class="breadcrumb-holder">
                        <div class="container-fluid">
                            <ul class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ asset('/consultancyfirms') }}"> Consultant/Firm Name</a></li>
                                <li class="breadcrumb-item active"> Update Firm Details of {{$firmname}} </li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-pills nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#consdtl" data-toggle="tab" role="tab">CONSULTANT DETAILS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link " href="#consExp" data-toggle="tab" role="tab">CONSULTANT EXPERTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#consproc" data-toggle="tab" role="tab">CONSULTANT PROCUREMENTS</a>
                    </li>
                    </ul>
                    <div class="tab-content">

                        <div id="consdtl" class="tab-pane active" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    CONSULTANT DETAILS
                                </div>
                                <div class="card-body">
                                    @include('error')
                                    <form method="post" id="firmForm" action="{{ asset('/consultancyfirms/update')}}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        @foreach($firm as $data)
                                        <?php $encid = encrypt($data -> Firm_ID)?>
                                        <input type="hidden" name="firmid" id="firmid" value="{{$encid}}">
                                        
                                        <div class="form-group">
                                            <label for="editcontractName">Firm Name</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmName" id="updFirmName" value="{{$data -> Firm_Name}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Address</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmAddress" id="updFirmAddress" value="{{$data -> Address}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Email Address</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmEmail" id="updFirmEmail" value="{{$data -> email_address}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Alternate Email Address1</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmAltEmail1" id="updFirmAltEmail1" value="{{$data -> alt_email1}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Alternate Email Address2</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmAltEmail2" id="updFirmAltEmail2" value="{{$data -> alt_email2}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Alternate Email Address3</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmAltEmail3" id="updFirmAltEmail3" value="{{$data -> alt_email3}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Contact Person</label>
                                            <input class="form-control form-control-sm" type=text name="updContactPerson" id="updContactPerson" value="{{$data -> contact_person}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Designation</label>
                                            <input class="form-control form-control-sm" type=text name="updDesignation" id="updDesignation" value="{{$data -> designation}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Telephone</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmTelephone" id="updFirmTelephone" value="{{$data -> telephone}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Fax Number</label>
                                            <input class="form-control form-control-sm" type=text name="updFirmFax" id="updFirmFax" value="{{$data -> fax_number}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">PhilGEPS Certification</label>
                                            @foreach($legal1 as $doc1)
                                                @if($data->Firm_ID == $doc1->Firm_ID)
                                                    <a class="form-control form-control-sm" href="{{url('/storage/storage/PHILGEPS')}}/{{basename($doc1->Document_Link)}}" target="_blank" disabled>{{basename($doc1->Document_Link)}}</a>
                                                @endif
                                            @endforeach
                                            <input class="form-control form-control-sm" type="file" name="updphilgeps" id="updphilgeps" size="40">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">SEC/DTI Registration</label>
                                            @foreach($legal2 as $doc2)
                                                @if($data->Firm_ID == $doc2->Firm_ID)
                                                    <a class="form-control form-control-sm" href="{{url('/storage/storage/SEC_DTI')}}/{{basename($doc2->Document_Link)}}" target="_blank" disabled>{{basename($doc2->Document_Link)}}</a>
                                                @endif
                                            @endforeach
                                            <input class="form-control form-control-sm" type="file" name="updsecdti" id="updsecdti" size="40">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Business Permit</label>
                                            @foreach($legal3 as $doc3)
                                                @if($data->Firm_ID == $doc3->Firm_ID)
                                                    <a class="form-control form-control-sm" href="{{url('/storage/storage/BusinessPermit')}}/{{basename($doc3->Document_Link)}}" target="_blank" disabled>{{basename($doc3->Document_Link)}}</a>
                                                @endif
                                            @endforeach
                                            <input class="form-control form-control-sm" type="file" name="updbuspermit" id="updbuspermit" size="40">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Tax Clearance</label>
                                            @foreach($legal4 as $doc4)
                                                @if($data->Firm_ID == $doc4->Firm_ID)
                                                    <a class="form-control form-control-sm" href="{{url('/storage/storage/TaxClearance')}}/{{basename($doc4->Document_Link)}}" target="_blank" disabled>{{basename($doc4->Document_Link)}}</a>
                                                @endif
                                            @endforeach
                                            <input class="form-control form-control-sm" type="file" name="updtaxclrnc" id="updtaxclrnc" size="40">
                                        </div>
                                        <div class="form-group">
                                            <label for="editcontractName">Financial Documents</label>
                                            @foreach($findocs as $doc5)
                                                @if($data->Firm_ID == $doc5->Firm_ID)
                                                    <a class="form-control form-control-sm" href="{{url('/storage/storage/FinancialDocs')}}/{{basename($doc5->Document_Link)}}" target="_blank" disabled>{{basename($doc5->Document_Link)}}</a>
                                                @endif
                                            @endforeach
                                            <input class="form-control form-control-sm" type="file" name="updfindocs[]" id="updfindocs[]" size="40" multiple></td>
                                        </div>
                                        <button type="submit" class="btn btn-primary">UPDATE CONSULTANT DETAILS</button>
                                        @endforeach
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="consExp" class="tab-pane" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    CONSULTANT EXPERTS
                                </div>
                                <div class="card-body">
                                    <input type="hidden" name="expcount" id="expcount" value="{{$expertscount}}">
                                    @forelse($experts as $exp)
                                    <form method="post" id="firmForm" action="{{ route('updConsExp') }}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="firmid[]" id="firmid[]" value="{{$exp->Firm_ID}}">
                                        <?php $encexpid = encrypt($exp->expert_id) ?>
                                        <?php $encid = encrypt($exp->Firm_ID)?>
                                        <input type="hidden" name="expid" id="expid" value="{{$encexpid}}">
                                    <div class="row">
                                        <div class="col">
                                            <label for="editcontractName">Expert</label>
                                            <input class="form-control form-control-sm" type="text" name="updexperts[]" id="updexperts[]" size=50
                                            value = "{{$exp->expert_name}}">
                                        </div>
                                        <div class="col">
                                            <label for="editcontractName">Expert Role</label>
                                            <input class="form-control form-control-sm" type="text" placeholder="Expert Role" name="updexpertsrole[]" id="updexpertsrole[]" size=30
                                            value = "{{$exp->expert_role}}">
                                        </div>
                                    </div><br>
                                    <button type="submit" class="btn btn-primary">UPDATE CONSULTANT EXPERTS DATA</button>
                                    </form>
                                    @empty
                                    <form method="post" id="firmForm" action="{{ route('addConsExp') }}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="firmid" value="{{$encid}}">
                                    <div class="row">
                                        <div class="col">
                                            <label for="editcontractName">Expert</label>
                                            <input class="form-control form-control-sm" type="text" name="updaddexperts[]" id="updaddexperts[]" size=50
                                            >
                                        </div>
                                        <div class="col">
                                            <label for="editcontractName">Expert Role</label>
                                            <input class="form-control form-control-sm" type="text" placeholder="Expert Role" name="updaddexpertsrole[]" id="updaddexpertsrole[]" size=30
                                            >
                                        </div>
                                    </div><br>
                                    <div id="upd_experts" style="display:inline;"></div>
                                    <a href="javascript:void(0);" id="updexpertsButton">ADD NEW EXPERTS ROW</a>
                                    <button type="submit" class="btn btn-primary">ADD TO CONSULTANT DATA</button>
                                    </form>
                                    @endforelse
                                    @if(!empty($experts))
                                    <form method="post" id="firmForm" action="{{ route('addConsExp') }}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="firmid" value="{{$encid}}">
                                    <div class="row">
                                        <div class="col">
                                            <label for="editcontractName">Expert</label>
                                            <input class="form-control form-control-sm" type="text" name="updaddexperts[]" id="updaddexperts[]" size=50
                                            >
                                        </div>
                                        <div class="col">
                                            <label for="editcontractName">Expert Role</label>
                                            <input class="form-control form-control-sm" type="text" placeholder="Expert Role" name="updaddexpertsrole[]" id="updaddexpertsrole[]" size=30
                                            >
                                        </div>
                                    </div><br>
                                    <div id="upd_experts1" style="display:inline;"></div>
                                    <a href="javascript:void(0);" id="updexpertsButton1">ADD NEW EXPERTS ROW</a>
                                    <button type="submit" class="btn btn-primary">ADD TO CONSULTANT DATA</button>
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div id="consproc" class="tab-pane" role="tabpanel">
                            <div class="card">
                                <div class="card-header">
                                    CONSULTANT PROCUREMENTS
                                </div>
                                <div class="card-body">
                                    <input type="hidden" name="proccount" id="proccount" value="{{$procurementscount}}">
                                    @forelse($procurements as $proc)
                                    <form method="post" id="firmForm" action="{{ route('updConsProcs') }}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="firmid[]" id="firmid[]" value="{{$proc->Firm_ID}}">
                                        <?php $encid = encrypt($proc->id)?>
                                        <input type="hidden" name="procid" id="procid" value="{{$encid}}">
                                    <br><div class="row">
                                        <input class="form-control form-control-sm" type="text" name="updprocjoined[]" id="updprocjoined[]" size=70 value="{{$proc->procurement}}">
                                    </div><br>
                                    <button type="submit" class="btn btn-primary">UPDATE CONSULTANT DATA</button>
                                    </form>
                                    @empty
                                    <form method="post" id="firmForm" action="{{ route('addConsProcs') }}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        
                                        <input type="hidden" name="firmid" id="firmid" value="{{$encid}}">
                                    <div class="row">
                                        <input class="form-control form-control-sm" type="text" name="updaddprocjoined[]" id="updaddprocjoined[]" size=70>
                                    </div><br>
                                    <div id="upd_procjoined" style="display:inline;"></div><a href="javascript:void(0);" id="updprocjoinedbtn">ADD NEW PROCUREMENTS</a>
                                    <button type="submit" class="btn btn-primary">ADD TO CONSULTANT DATA</button>
                                    </form>
                                    @endforelse
                                    <br>
                                    @if(!empty($procurements))
                                    <form method="post" id="firmForm" action="{{ route('addConsProcs') }}" novalidate=""  enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="firmid" id="firmid" value="{{$encid}}">
                                    <div class="row">
                                        <input class="form-control form-control-sm" type="text" name="updaddprocjoined[]" id="updaddprocjoined[]" size=70>
                                    </div>
                                    <div id="upd_procjoined1" style="display:inline;"></div><a href="javascript:void(0);" id="updprocjoinedbtn1">ADD NEW PROCUREMENTS</a>
                                    <button type="submit" class="btn btn-primary">ADD TO CONSULTANT DATA</button>
                                    </form>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </section>
@endsection