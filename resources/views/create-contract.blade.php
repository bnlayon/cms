@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    @if(is_null(Auth::user()->username))
            <script>
                window.location = "/";
            </script>
    @endif
    @if(Auth::user()->access_type=='V')
            <script>
                window.location = "/dashboard";
            </script>
    @endif
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">CREATE CONTRACT</h2><br>
            </div>
            @include('error')
            <div class="col-lg-15 col-md-12">
                <div class="modal" tabindex="-1" id="modalctype" role="document">
                    <div class="modal-dialog modal-dialog-centered" >
                        <div class="modal-content">
                            <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Select Contract Type:</h5>
                            </div>
                            <div class="modal-body">
                                @foreach($contractTypeDB as $TypeData)
                                    <input id="CType" name="CType" type="radio" value="{{$TypeData->Contract_Type}}">
                                    {{$TypeData->Contract_Desc}} <br>
                                @endforeach
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="conType" class="btn btn-primary">Enter Contract Data</button>
                            </div>
                        </div>
                    </div>
                </div>
                @include('error')
                <div id="ContractConsulting">
                    <form method="POST" name="contractData" id="contractData" action="{{ asset('/create-contract')}}"
                        novalidate="" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        
                        <table id=contractInput class="table">
                            <tbody class="table table-borderless tbody">
                                <tr>
                                    <td>Contract ID:</td>
                                    <td>
                                        <strong><?php echo strtoupper($yearmonth)?><div id="selectedCType"
                                                name="selectedCType" style="display:inline;"></div><?php echo $counter; ?></strong>
                                        <input type=hidden name="contractID" id="contractID"
                                            value="{{strtoupper($yearmonth)}}"><input type=hidden
                                            name="contractIDCounter" id="contractIDCounter" value="{{$counter}}">
                                        <input type=hidden name="hiddenCType" id="hiddenCType" value="">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Procurement Mode:</td>
                                    <td>
                                        <select id="ProcMode" name="ProcMode" required>
                                            <option disabled="" selected="" value="">Select Procurement Mode</option>
                                            @foreach($procmode as $mode)
                                            <option value="{{$mode -> mode_code}}"><?php echo $mode -> mode_desc ?>
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Project Title:</td>
                                    <td><textarea name="ProjectTitle" id="ProjectTitle" cols="100" rows="5" required></textarea></td>
                                </tr>
                                <tr>
                                    <td>Firm Type:</td>
                                    <td colspan=2>

                                        <select id="firmType" name="firmType" onchange="java_script_:setFirmType(
                                                this.options[this.selectedIndex].value)">
                                            <option disabled="" selected="" value="">Select Firm Type</option>
                                            @foreach($firmtype as $type)
                                            <option value="{{$type->Firm_Type}}"><?php echo $type -> Firm_Type_Desc ?>
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>  
                                <tr><td>Consultant/Firm Name:</td>
                                    <td>
                                        <select class="selectpicker" id="ConsultancyFirm" name="ConsultancyFirm" data-live-search="true" onchange="java_script_:setExperts(
                                                this.options[this.selectedIndex].value)" required >
                                            <option disabled="" selected="" value="">Select Consultant/Firm Name</option>
                                            @foreach($firmname as $data1)
                                            <option value="{{$data1->Firm_ID}}"><?php echo $data1 -> Firm_Name ?>
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr class="role">
                                    <td>Firm Role:</td>
                                    <td colspan=2>
                                        <select id="firmrole" name="firmrole">
                                            <option disabled="" selected="" value="">Select Firm Role</option>
                                                @foreach($firmRole as $role)
                                                <option value="{{$role->firm_role}}">
                                                    <?php echo $role -> firm_role_desc ?></option>
                                                @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr class="others">
                                    <td> Partner Firm/s and Role/s:</td>
                                    <td>
                                        How many partner firms?
                                        <select id="PartnerCount" name="PartnerCount" onchange="java_script_:addpartners(
                                                this.options[this.selectedIndex].value)">
                                            <option disabled="" selected="" value="">0</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <br>
                                        <div id="partners" name="partners" class="partnerfirms">
                                        <select id="PartnerFirmName[]" name="PartnerFirmName[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Name
                                            </option>
                                            @foreach($firmname as $data1)
                                            <option value="{{$data1->Firm_ID}}">
                                                <?php echo $data1 -> Firm_Name ?></option>
                                            @endforeach
                                        </select>
                                        <select id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Role
                                            </option>
                                            @foreach($firmRole as $role)
                                            <option value="{{$role->firm_role}}">
                                                <?php echo $role -> firm_role_desc ?></option>
                                            @endforeach
                                        </select>
                                        <br>
                                        </div>
                                        <div id="partners" name="partners" class="partnerfirms">
                                        <select id="PartnerFirmName[]" name="PartnerFirmName[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Name
                                            </option>
                                            @foreach($firmname as $data1)
                                            <option value="{{$data1->Firm_ID}}">
                                                <?php echo $data1 -> Firm_Name ?></option>
                                            @endforeach
                                        </select>
                                        <select id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Role
                                            </option>
                                            @foreach($firmRole as $role)
                                            <option value="{{$role->firm_role}}">
                                                <?php echo $role -> firm_role_desc ?></option>
                                            @endforeach
                                        </select>
                                        <br>
                                        </div>
                                        <div id="partners" name="partners" class="partnerfirms">
                                        <select id="PartnerFirmName[]" name="PartnerFirmName[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Name
                                            </option>
                                            @foreach($firmname as $data1)
                                            <option value="{{$data1->Firm_ID}}">
                                                <?php echo $data1 -> Firm_Name ?></option>
                                            @endforeach
                                        </select>
                                        <select id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Role
                                            </option>
                                            @foreach($firmRole as $role)
                                            <option value="{{$role->firm_role}}">
                                                <?php echo $role -> firm_role_desc ?></option>
                                            @endforeach
                                        </select>
                                        <br>
                                        </div>
                                        <div id="partners" name="partners" class="partnerfirms">
                                        <select id="PartnerFirmName[]" name="PartnerFirmName[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Name
                                            </option>
                                            @foreach($firmname as $data1)
                                            <option value="{{$data1->Firm_ID}}">
                                                <?php echo $data1 -> Firm_Name ?></option>
                                            @endforeach
                                        </select>
                                        <select id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Role
                                            </option>
                                            @foreach($firmRole as $role)
                                            <option value="{{$role->firm_role}}">
                                                <?php echo $role -> firm_role_desc ?></option>
                                            @endforeach
                                        </select>
                                        <br>
                                        </div>
                                        <div id="partners" name="partners" class="partnerfirms">
                                        <select id="PartnerFirmName[]" name="PartnerFirmName[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Name
                                            </option>
                                            @foreach($firmname as $data1)
                                            <option value="{{$data1->Firm_ID}}">
                                                <?php echo $data1 -> Firm_Name ?></option>
                                            @endforeach
                                        </select>
                                        <select id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                            <option disabled="" selected="" value="">Select Partner Firm Role
                                            </option>
                                            @foreach($firmRole as $role)
                                            <option value="{{$role->firm_role}}">
                                                <?php echo $role -> firm_role_desc ?></option>
                                            @endforeach
                                        </select>
                                        <br>
                                        </div>
                                    </td>
                                </tr>      
                                <tr>
                                    <td>Experts:</td>
                                    <td>
                                    <input type="hidden" name="<?php $expid?>" id="expid">
                                        <input type="text" name="experts[]" id="experts[]" size=50
                                            required><input type="text" placeholder="Expert Role" name="expertsrole[]" id="expertsrole[]" size=30
                                            required> &nbsp<div id="new_experts" style="display:inline;"></div><br><a
                                            href="javascript:void(0);" id="expertsButton">add
                                            experts</a><br>
                                    </td>
                                </tr>
                                <tr>    
                                    <td>End-User (Staff):</td>
                                    <td>
                                        <select id="endUser" name="endUser" required>
                                            <option disabled="" selected="" value="">Select End User</option>
                                            @foreach($endusers as $eu)
                                            <option value="{{$eu->id}}"><?php echo $eu -> endUsers ?>
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Implementing Agency:</td>
                                    <td><input name="implementingAgency" type="text" size=50 required></td>
                                </tr>
                                <tr>
                                    <td>Co-Implementing Agency:</td>
                                    <td><input name="coimplementingAgency" type="text" size=50></td>
                                </tr>
                                <tr>
                                    <td>Notice of Award Date:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input name="NOA" type="date" required></td>
                                    <td>NOA Conforme Date:&nbsp&nbsp&nbsp&nbsp&nbsp<input name="NOAConforme" type="date" required></td>
                                </tr>
                                <tr>
                                    <td>Notice to Proceed Date:&nbsp&nbsp&nbsp&nbsp&nbsp<input name="NTP" id="NTP" type="date" data-date-format="yyyy/mm/dd"  onchange="java_script_:minConforme(this.value)" required></td>
                                    <td>NTP Conforme Date:&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<input name="conformeDate" id="conformeDate" type="date" data-date-format="yyyy/mm/dd" onchange="java_script_:minStart(this.value)" required></td>
                                </tr>
                                <tr>
                                    <td><strong>CONTRACT DURATION</strong> </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Contract Start Date:</td>
                                    <td><input id="contractStartDate" name="contractStartDate" type="date" data-date-format="yyyy/mm/dd" onchange="java_script_:minEnd(this.value)"
                                            required></td>
                                </tr>
                                <tr>
                                    <td>Contract End Date:</td>
                                    <td><input id="contractEndDate" name="contractEndDate" type="date" data-date-format="yyyy/mm/dd" 
                                            required></td>
                                </tr>
                                <tr>
                                    <td>Contract Price:</td>
                                    <td><input id="contractCost" name="contractCost" type="number" min="1" step="any"
                                            size=50 required></td>
                                </tr>
                                <tr>
                                    <td>Approved Budget for the Contract (ABC):</td>
                                    <td><input id="ABC" name="ABC" type="number" min="1" step="any" size=50 required>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fund Source:</td>
                                    <td>
                                        <select id="fundSource" name="fundSource" required>
                                            <option disabled="" selected="" value="">Select Fund Source</option>
                                            @foreach($fundsources as $fs)
                                            <option value="{{$fs->id}}"><?php echo $fs -> fundSource ?>
                                            </option>
                                            @endforeach
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Fund Year:</td>
                                    <td><input name="fundYear" type="text" size=50 required></td>
                                </tr>
                                <tr>
                                    <td>Performance Security:</td>
                                    <td><a href="javascript:void(0);" id="perfSec" data-toggle="modal"
                                            data-target="#performanceSecModal" title="Add Performance Security">Add</a> <div id="selectedperfsec" name="selectedperfsec"></div>
                                            <div class="modal" tabindex="-1" id="performanceSecModal" role="document">
                                                <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable" >
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <strong>Performance Security:</strong>
                                                            <button type="button" data-dismiss="modal"  id="closemodal1" aria-label="Close"
                                                                class="close"><span aria-hidden="true">×</span></button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                    <label>Select Performance Security Type/s:</label><br>
                                                                    <select id="perfSecVal" name="perfSecVal[]" multiple="multiple" data-placeholder="Select one or more..." hidden style="max-width:90%;">
                                                                        @foreach($secType as $data)
                                                                        <option class="option" value="{{$data->Type}}">
                                                                            <?php echo $data->Type_Description ?></option>
                                                                        @endforeach
                                                                    </select>       
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <!-- Cash -->
                                                                        <div id="CashData">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><strong>CASH</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Official Receipt Number:</td>
                                                                                    <td><input id="CashOR" name="CashOR" type="text"
                                                                                            required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period:</td>
                                                                                    <td><input id="ValidityCash" name="ValidityCash"
                                                                                            type="date" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period Notes:</td>
                                                                                    <td><textarea name="ValiditycashNotes" id="ValiditycashNotes" cols="40" rows="5" required></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <!-- Check -->
                                                                        <div id="CheckPSData">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><strong>CHECK</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Check Number:</td>
                                                                                    <td><input id="CheckNum" name="CheckNum" type="text"
                                                                                            required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Official Receipt Number:</td>
                                                                                    <td><input id="CheckOR" name="CheckOR" type="text"
                                                                                            required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Bank Name/Branch:</td>
                                                                                    <td><input id="CheckBank" name="CheckBank"
                                                                                            type="text" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period:</td>
                                                                                    <td><input id="ValidityCheck" name="ValidityCheck"
                                                                                            type="date" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period Notes:</td>
                                                                                    <td><textarea name="ValidityCheckNotes" id="ValidityCheckNotes" cols="40" rows="5" required></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <!-- bank draft  -->
                                                                        <div id="DraftPSData">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><strong>BANK DRAFT</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Name of Issuing Bank:</td>
                                                                                    <td><input id="BankNameBD" name="BankNameBD"
                                                                                            type="text" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Bank Draft:</td>
                                                                                    <td><input id="DraftBD" name="DraftBD" type="text"
                                                                                            required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period:</td>
                                                                                    <td><input id="ValidityBD" name="ValidityBD"
                                                                                            type="date" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period Notes:</td>
                                                                                    <td><textarea name="ValidityBDNotes" id="ValidityBDNotes" cols="40" rows="5" required></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <!-- bank guarantee -->
                                                                        <div id="GuaranteePSData">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><strong>BANK GUARANTEE</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Name of Issuing Bank:</td>
                                                                                    <td><input id="BankNameBG" name="BankNameBG"
                                                                                            type="text" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Bank Guarantee:</td>
                                                                                    <td><input id="DraftBG" name="DraftBG" type="text"
                                                                                            required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period:</td>
                                                                                    <td><input id="ValidityBG" name="ValidityBG"
                                                                                            type="date" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period Notes:</td>
                                                                                    <td><textarea name="ValidityBGNotes" id="ValidityBGNotes" cols="40" rows="5" required></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <!-- ILC -->
                                                                        <div id="ILCPSData">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><strong>IRREVOCABLE LETTER OF CREDIT</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Name of Issuing Bank:</td>
                                                                                    <td><input id="BankNameILC" name="BankNameILC"
                                                                                            type="text" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Letter of Credit(LOC) Number:</td>
                                                                                    <td><input id="ILC" name="ILC" type="text" required>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period:</td>
                                                                                    <td><input id="ValidityILC" name="ValidityILC"
                                                                                            type="date" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period Notes:</td>
                                                                                    <td><textarea name="ValidityILCNotes" id="ValidityILCNotes" cols="40" rows="5" required></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <!-- surety bond -->
                                                                        <div id="SuretyPSData">
                                                                            <table>
                                                                                <tr>
                                                                                    <td><strong>SURETY BOND</strong></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Name of Issuing Company:</td>
                                                                                    <td><input id="CNameSurety" name="CNameSurety"
                                                                                            type="text" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Surety Bond Number:</td>
                                                                                    <td><input id="SuretyNum" name="SuretyNum"
                                                                                            type="text" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period:</td>
                                                                                    <td><input id="ValiditySurety" name="ValiditySurety"
                                                                                            type="date" required></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Validity Period Notes:</td>
                                                                                    <td><textarea name="ValiditySuretyNotes" id="ValiditySuretyNotes" cols="40" rows="5" required></textarea></td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                        <small><i>Note: Click <strong>ONLY</strong> the types included in the contract</i></small>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" id="perfSecSelect" class="btn btn-primary">Select</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                      
                                    </td>
                                </tr>
                                <tr>
                                    <td>Issuing Company:</td>
                                    <td><input name="issuingCompany" type="text" size=50 required></td>
                                </tr>
                                <tr>
                                    <td>Supporting Documents:</td>
                                    <td>
                                        <input type="file" name="docs[]" id="docs[]" multiple required><br>
                                        <small id="emailHelp" class="form-text text-muted"><strong>NOTE:</strong> Upload files without spaces on its filename. Use "_" instead.</small>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align:top;">Deliverable/s:</td>
                                    <td>
                                        <input type="text" name="deliverables[]" id="deliverables[]" size=30
                                            required> <input placeholder="Select Target Date" type="text" name="dates[]" onfocus="(this.type='date')" id="dates[]"> <input placeholder="Notes" type="text" name="dnotes[]" id="dnotes[]" size=40><br>
                                        <input type="text" name="deliverables[]" id="deliverables[]" size=30
                                            required> <input placeholder="Select Target Date" type="text" name="dates[]" onfocus="(this.type='date')" id="dates[]"> <input placeholder="Notes" type="text" name="dnotes[]" id="dnotes[]" size=40><br>
                                        <input type="text" name="deliverables[]" id="deliverables[]" size=30
                                            required> <input placeholder="Select Target Date" type="text" name="dates[]" onfocus="(this.type='date')" id="dates[]"> <input placeholder="Notes" type="text" name="dnotes[]" id="dnotes[]" size=40>&nbsp<div id="new_deliverable" style="display:inline;"></div><br><a
                                            href="javascript:void(0);" id="deliverableButton">add
                                            deliverable</a><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan=2 class="text-center">
                                        <input type="submit" class="btn-primary text-center" id="contractDetails"
                                            value="Create Contract">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
