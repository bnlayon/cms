@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    @if(is_null(Auth::user()->username))
    <script>
        window.location = "/";
    </script>
    @endif
    @if(Auth::user()->access_type=='V')
    <script>
        window.location = "/dashboard";
    </script>
    @endif
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">EDIT CONTRACT</h2><br>
            </div>
            <div class="col-lg-15 col-md-12">
                <div class="breadcrumb-holder">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a>
                            </li>
                            <li class="breadcrumb-item active"> Edit Contract </li>
                        </ul>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body">
                        <?php $encryptedID = encrypt($Contract_ID); ?>
                        <h5 style="display:inline;float:left;" class="card-title">{{$Contract_ID}} - {{$name}}</h5> <a
                            href="{{ asset('/manage-contracts/view-contract')}}/{{$encryptedID}}" name="editCancel"
                            id="editCancel" style="display:inline;float:right">Cancel Update</a>
                    </div>
                </div>
            @include('error')
                <ul class="nav nav-pills nav-justified" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#contrDtls" data-toggle="tab" role="tab">CONTRACT DETAILS</a>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link " href="#contrExp" data-toggle="tab" role="tab">CONTRACT EXPERTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contrPS" data-toggle="tab" role="tab">PERFORMANCE SECURITY</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contrDlvrbls" data-toggle="tab" role="tab">DELIVERABLES</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="contrDtls" class="tab-pane active" role="tabpanel">
                        <div class="card-header">
                            CONTRACT DETAILS
                        </div>
                        <div class="card-body">
                            <form method="POST" name="contractData" id="contractData"
                                action="{{ route('updateCntrctDtls',['Contract_ID' => $encryptedID]) }}" novalidate=""
                                enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                @foreach($contract as $data)
                                <div class="form-group">
                                    <label for="ProcMode">Procurement Type</label>
                                    <br>
                                    @foreach($proc as $pr)
                                    <input class="form-control form-control-sm" type="text" name="editprocVal" id="editprocVal"
                                        value="{{$pr -> mode_code}}" hidden>
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$pr -> mode_desc}}" disabled>
                                    @endforeach
                                    <select class="form-control form-control-sm" id="udProcMode" name="udProcMode" class="form-select"
                                        aria-label="Select Procurement Mode">
                                        <option disabled="" selected="" value="">Select New Procurement Mode</option>
                                        @foreach($procmode as $mode)
                                        <option value="{{$mode -> mode_code}}" > <?php echo $mode -> mode_desc ?>
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="contractType">Contract Type</label>
                                    <input class="form-control form-control-sm" type="text" name="contractType" id="contractType"
                                        value="<?php echo $contractType ?>" disabled>
                                    <small id="emailHelp" class="form-text text-muted">We only process this type of
                                        contract
                                        at the moment</small>
                                </div>
                                <div class="form-group">
                                    <label for="editcontractName">Project Title</label>
                                    <textarea class="form-control form-control-sm" name="editcontractName" id="editcontractName"
                                        cols="52" rows="4">{{$data -> Contract_Name}}</textarea>
                                </div>
                                <div class="form-group">
                                    <label for="editcontractName">Firm Type</label>
                                    <input type="hidden" name='typecode' id='typecode' value="{{$typecode}}">
                                    <input class="form-control form-control-sm" type="text" name='initialType' id='initialType'
                                        value="{{$type}}" size=50 disabled>
                                        <small id="emailHelp" class="form-text text-muted">Updating this field is not allowed</small>
                                </div>
                                <div class="form-group">
                                    <label for="editcontractName">Consultant/Firm Name and Roles:</label>
                                    @if(!is_null($firm))    
                                        @forelse($firm as $name)
                                            <div class="row">
                                                <div class="col">
                                                    <input type="hidden" name="existID" if="existID" value="{{$name->Firm_ID}}">
                                                    <input class="form-control form-control-sm" type="text" name="firmName" id="firmName" value="{{$name->Firm_Name}}" disabled>
                                                </div>
                                                <div class="col">
                                                    <span class="role">
                                                        <input class="form-control form-control-sm" type="text"
                                                            value="{{$name->firm_role_desc}}" size=70 disabled>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <small id="emailHelp" class="form-text text-muted">Updating Firms currently associated with the contract not allowed</small>
                                                </div>
                                            </div>
                                        @empty
                                            <div class="row">
                                                <div class="col">
                                                    <select class="form-control form-control-sm" id="firmNameNew" name="firmNameNew">
                                                        <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                        </option>
                                                        @foreach($firmname as $data1)
                                                        <option value="{{$data1->Firm_ID}}">
                                                            <?php echo $data1 -> Firm_Name ?></option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <span class="role">
                                                        <select class="form-control form-control-sm"
                                                            id="firmrole" name="firmrole">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select></span>
                                                </div>
                                            </div>
                                        @endforelse
                                    @else
                                        <div class="row">
                                                <div class="col">
                                                    <select class="form-control form-control-sm" id="firmNameNew"
                                                        name="firmNameNew">
                                                        <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                        </option>
                                                        @foreach($firmname as $data1)
                                                        <option value="{{$data1->Firm_ID}}">
                                                            <?php echo $data1 -> Firm_Name ?></option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="col">
                                                    <span class="role">
                                                        <select class="form-control form-control-sm"
                                                            id="firmrole" name="firmrole">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select></span>
                                                </div>
                                            </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="role">
                                        <h6>How many additional partner firms?</h6>
                                            <select class="form-control form-control-sm" id="PartnerCount" name="PartnerCount" onchange="java_script_:addpartners(
                                                    this.options[this.selectedIndex].value)">
                                                <option disabled="" selected="" value="">0</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            <br>
                                            <div id="partners" name="partners" class="partnerfirms">
                                                <div class="row">
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmName[]" name="PartnerFirmName[]">
                                                            <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                            </option>
                                                            @foreach($firmname as $data1)
                                                            <option value="{{$data1->Firm_ID}}">
                                                                <?php echo $data1 -> Firm_Name ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="partners" name="partners" class="partnerfirms">
                                            <div class="row">
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmName[]" name="PartnerFirmName[]">
                                                            <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                            </option>
                                                            @foreach($firmname as $data1)
                                                            <option value="{{$data1->Firm_ID}}">
                                                                <?php echo $data1 -> Firm_Name ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="partners" name="partners" class="partnerfirms">
                                            <div class="row">
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmName[]" name="PartnerFirmName[]">
                                                            <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                            </option>
                                                            @foreach($firmname as $data1)
                                                            <option value="{{$data1->Firm_ID}}">
                                                                <?php echo $data1 -> Firm_Name ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="partners" name="partners" class="partnerfirms">
                                            <div class="row">
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmName[]" name="PartnerFirmName[]">
                                                            <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                            </option>
                                                            @foreach($firmname as $data1)
                                                            <option value="{{$data1->Firm_ID}}">
                                                                <?php echo $data1 -> Firm_Name ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="partners" name="partners" class="partnerfirms">
                                            <div class="row">
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmName[]" name="PartnerFirmName[]">
                                                            <option disabled="" selected="" value="">Select Consultant/Firm Name
                                                            </option>
                                                            @foreach($firmname as $data1)
                                                            <option value="{{$data1->Firm_ID}}">
                                                                <?php echo $data1 -> Firm_Name ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col">
                                                        <select class="form-control form-control-sm" id="PartnerFirmRole[]" name="PartnerFirmRole[]">
                                                            <option disabled="" selected="" value="">Select Firm Role
                                                            </option>
                                                            @foreach($firmRole as $role)
                                                            <option value="{{$role->firm_role}}">
                                                                <?php echo $role -> firm_role_desc ?></option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="endUser">End-User (Staff)</label>
                                    <input class="form-control form-control-sm" name="endUser" type="text" size=50 value="{{$data -> End_User}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="implementingAgency">Implementing Agency</label>
                                    <input class="form-control form-control-sm" name="implementingAgency" type="text" size=50 value="{{$data -> Implementing_Agency}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="coimplementingAgency">Co-Implementing Agency</label>
                                    <input class="form-control form-control-sm" name="coimplementingAgency" type="text" size=50 value="{{$data -> Co_Implementing_Agency}}">
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="coimplementingAgency">Notice of Award Date</label>
                                            <input class="form-control form-control-sm" type="text" value="{{$data -> NOA_Release_Date}}" size=50 disabled><input class="form-control form-control-sm" name="NOA" type="date" required>
                                        </div>
                                        <div class="col">
                                    <label for="NOAConforme">NOA Conforme Date</label>
                                    <input class="form-control form-control-sm" type="text" value="{{$data -> NOA_Conforme_Date}}" size=50 disabled><input class="form-control form-control-sm" name="NOAConforme" type="date" required>
                                        </div>
                                    </div>
                                </div>
                               <div class="form-group">
                                   <div class="row">
                                       <div class="col">
                                            <label for="coimplementingAgency">Notice to Proceed Date</label>
                                            <input class="form-control form-control-sm" type="text" value="{{$data -> NTP_Release_Date}}" size=50 disabled><input class="form-control form-control-sm" name="NTP" id="NTP" type="date" data-date-format="yyyy/mm/dd"  onchange="java_script_:minConforme(this.value)" required>
                                       </div>
                                       <div class="col">
                                            <label for="coimplementingAgency">NTP Conforme Date</label>
                                            <input class="form-control form-control-sm" type="text" value="{{$data -> NTP_Conforme_Date}}" size=50 disabled><input class="form-control form-control-sm" name="conformeDate" id="conformeDate" type="date" data-date-format="yyyy/mm/dd" onchange="java_script_:minStart(this.value)" required>
                                       </div>
                                   </div>
                                    
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="coimplementingAgency">Contract Start Date</label>
                                            <input class="form-control form-control-sm" type="text" value="{{$data -> Contract_Start_Date}}" size=50 disabled><input class="form-control form-control-sm" id="contractStartDate" name="contractStartDate" type="date" data-date-format="yyyy/mm/dd" onchange="java_script_:minEnd(this.value)"
                                                    required>
                                        </div>
                                        <div class="col">
                                            <label for="coimplementingAgency">Contract End Date</label>
                                            <input class="form-control form-control-sm" type="text" value="{{$data -> Contract_End_Date}}" size=50 disabled><input class="form-control form-control-sm" id="contractEndDate" name="contractEndDate" type="date" data-date-format="yyyy/mm/dd" 
                                                    required>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="editcontractCost">Contract Price</label>
                                    <input class="form-control form-control-sm" id="editcontractCost" name="editcontractCost" type="decimal" min="1" step="any"
                                            size=50  value="<?php echo number_format($data->Contract_Cost,2)?>" required>
                                </div>
                                <div class="form-group">
                                    <label for="editABC">Approved Budget for the Contract</label>
                                    <input class="form-control form-control-sm" id="editABC" name="editABC" type="decimal" min="1" value="<?php echo number_format($data->ABC,2)?>" step="any" size=50 required>
                                </div>
                                <div class="form-group">
                                    <label for="fundSource">Fund Source</label>
                                    <input class="form-control form-control-sm" name="fundSource" id="fundSource" type="text" size=50 value="{{$data -> Fund_Source}}" disabled>
                                </div>
                                <div class="form-group">
                                    <label for="fundYear">Fund Year</label>
                                    <input class="form-control form-control-sm" name="fundYear" id="fundYear" type="text" size=50 value="{{$data -> Fund_Year}}" required>
                                </div>
                                <div class="form-group">
                                    <label for="docs">Supporting Documents</label><br>
                                        @foreach($document as $docs)
                                        <a href="{{url('/storage/storage/supportingDocs')}}/{{basename($docs)}}" target="_blank">{{basename($docs)}}</a><br>
                                        @endforeach
                                        <input class="form-control form-control-sm" type="file" name="docs[]" id="docs[]" multiple >
                                        <small id="emailHelp" class="form-text text-muted"><strong>NOTE:</strong> Upload files without spaces on its filename. Use "_" instead. Deletion of documents are done manually. Contact System Administrator</small>
                                </div>
                                <div class="form-group">
                                    <label for="completionDate">Contract Completion Date</label>
                                    @if(!is_null($data -> Contract_Completion_Date))
                                    <input class="form-control form-control-sm" type="text" value="{{$data -> Contract_Completion_Date}}" size=50 disabled>
                                    @endif
                                    <input class="form-control form-control-sm" name="completionDate" id="completionDate" type="date" size=50 >
                                </div>
                                <div class="form-group">
                                    <label for="terminationDate">Contract Termination Date</label>
                                    @if(!is_null($data -> Contract_Termination_Date))
                                    <input class="form-control form-control-sm" type="text" value="{{$data -> Contract_Termination_Date}}" size=50 disabled>
                                    @endif
                                    <input class="form-control form-control-sm" name="terminationDate" id="terminationDate" type="date" size=50  >
                                </div>
                                <button type="submit" class="btn btn-primary">UPDATE CONTRACT DETAILS</button>
                                @endforeach
                            </form>
                        </div>
                    </div>
                    <div id="contrExp" class="tab-pane " role="tabpanel">
                        <div class="card-header">
                            CONTRACT EXPERTS DETAILS
                        </div>
                        <div class="card-body">
                           <input type="hidden" name="expintcnt" id="expintcnt" value="{{count($experts)}}">
                            @if(!is_null($experts) || !empty($experts) || isset($experts))
                                <form  method="POST" name="contractData" id="contractData"
                                    action="{{ route('updContrExp',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        @foreach($experts as $exp1)
                                        <?php  
                                        $encryptedfid = encrypt($exp1->Firm_ID); ?>
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        <input type="hidden" name="firmnameedit" id="firmnameedit" value="{{$encryptedfid}}">
                                        <input type="hidden" name="expid[]" id="expid[]" value="{{$exp1->id}}">
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="contractType">Expert Name</label>
                                                    <input class="form-control form-control-sm"  type="text" name="expname[]" id="expname[]" value="{{$exp1->expert_name}}" size=50 name="expname" id="expname" required>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="contractType">Expert Role</label>
                                                    <input class="form-control form-control-sm"  type="text" name="exprole[]" id="exprole[]" value="{{$exp1->expert_role}}" name="exprole" id="exprole" size=50 required>
                                                    <small id="emailHelp" class="form-text text-muted">Input <strong>N/A</strong> when no roles indicated. Do not leave blank</small>
                                                </div>
                                            </div>
                                        </div> 
                                        @endforeach  
                                        <button type="submit" class="btn btn-primary">UPDATE EXPERT/S TO CONTRACT</button>
                                </form>
                                <form  method="POST" name="contractData" id="contractData"
                                    action="{{ route('addContrExp',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                    @foreach($firm as $name)
                                        <?php  
                                        $encryptedfid = encrypt($name->Firm_ID); ?>
                                            <input type="hidden" name="firmnameedit" id="firmnameedit" value="{{$encryptedfid}}">
                                    @endforeach
                                    
                                         <div id="new_expertsEdit" style="display:inline;"></div><br><a
                                            href="javascript:void(0);" id="expertsButtonedit">ADD ROW </a>  
                                        <button type="submit" class="btn btn-primary">ADD NEW EXPERT/S TO CONTRACT</button>
                                </form>
                            @else
                                <form  method="POST" name="contractData" id="contractData"
                                    action="{{ route('addContrExp',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        @foreach($firm as $name)
                                        <?php  
                                        $encryptedfid = encrypt($name->Firm_ID); ?>
                                            <input type="hidden" name="firmnameedit" id="firmnameedit" value="{{$encryptedfid}}">
                                        @endforeach
                                        
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="contractType">Expert Name</label>
                                                    <input class="form-control form-control-sm"  type="text" size=50 name="expname[]" id="expname[]" required>
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="contractType">Expert Role</label>
                                                    <input class="form-control form-control-sm"  type="text" name="exprole[]" id="exprole[]" size=50 required>
                                                </div>
                                            </div>
                                        </div>
                                         <div id="new_expertsEdit" style="display:inline;"></div><br><a
                                            href="javascript:void(0);" id="expertsButtonedit">ADD ROW </a>  
                                        <button type="submit" class="btn btn-primary">ADD EXPERT/S TO CONTRACT</button>
                                </form>
                            @endif
                        </div>
                    </div>
                    <div id="contrPS" class="tab-pane" role="tabpanel">
                        <div class="card-header">
                            PERFORMANCE SECURITY DETAILS
                        </div>
                        <div class="card-body">
                                @forelse($perf as $details)
                                <?php $encryptedSID = encrypt($details->Security_ID) ?>
                                    <form method="POST" name="contractData" id="contractData"
                                    action="{{ route('updatePerfSecDtls',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        <input type="hidden" name="pid" id="pid" value="{{$encryptedSID}}">
                                        @if($details->Type=='C')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype1" id="pftype1" value="{{$details->Type}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="cornumber">Official Receipt Number</label>
                                            <input type="text" class="form-control form-control-sm" name="cornumber" id="cornumber"
                                                value="{{$details->CashOR}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="cvalidityperiod">Validity Period</label><br>
                                            @if(!is_null($details->ValidityBD))
                                            <input type="text" class="form-control form-control-sm" value="{{$details->CashBV}}"><input name="cvalidityperiod"
                                                id="cvalidityperiod" type="date" size=30>
                                            @else
                                            <input class="form-group" type="date" size=30 class="form-control form-control-sm"
                                                name="cvalidityperiod" id="cvalidityperiod">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="cvalinotes">Validity Notes</label>
                                            <textarea class="form-control form-control-sm" name="cvalinotes"
                                                id="cvalinotes">{{$details->CashBVNotes}}</textarea>
                                        </div>
                                        @elseif($details->Type=='CMC')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype2" id="pftype2" value="{{$details->Type}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="checknumber">Check Number</label>
                                            <input type="text" class="form-control form-control-sm" name="checknumber" id="checknumber"
                                                value="{{$details->CheckNum}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="cmcornumber">Official Receipt Number</label>
                                            <input type="text" class="form-control form-control-sm" name="cmcornumber" id="cmcornumber"
                                                value="{{$details->CheckOR}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="cmcbankname">Bank Name/Branch</label>
                                            <input type="text" class="form-control form-control-sm" name="cmcbankname" id="cmcbankname"
                                                value="{{$details->CheckBank}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="cmcvalperiod">Validity Period</label><br>
                                            @if(!is_null($details->CheckBV))
                                            <input class="form-control form-control-sm" value="{{$details->CheckBV}}" size=30
                                                disabled><input class="form-control form-control-sm" type="date" size=30 name="cmcvalperiod" id="cmcvalperiod">
                                            @else
                                            <input class="form-control form-control-sm" type="date" size=30 name="cmcvalperiod" id="cmcvalperiod" >
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="cmcvalinotes">Validity Notes</label>
                                            <textarea class="form-control form-control-sm" name="cmcvalinotes"
                                                id="cmcvalinotes">{{$details->CheckBVNotes}}</textarea>
                                        </div>
                                        @elseif($details->Type=='BD')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype3" id="pftype3" value="{{$details->Type}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="bdbankname">Name of Issuing Bank</label>
                                            <input type="text" class="form-control form-control-sm" name="bdbankname" id="bdbankname"
                                                value="{{$details->BankNameBD}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="bdbankdraft">Bank Draft</label>
                                            <input type="text" class="form-control form-control-sm" name="bdbankdraft" id="bdbankdraft"
                                                value="{{$details->DraftBD}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="bdvalperiod">Validity Period</label><br>
                                            @if(!is_null($details->ValidityBD))
                                            <input class="form-control form-control-sm" value="{{$details->ValidityBD}}" size=30
                                                disabled><input class="form-control form-control-sm" type="date" size=30 name="bdvalperiod" id="bdvalperiod">
                                            @else
                                            <input class="form-control form-control-sm" type="date" size=30 name="bdvalperiod" id="bdvalperiod" >
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="bdvalnotes">Validity Notes</label>
                                            <textarea class="form-control form-control-sm" name="bdvalnotes"
                                                id="bdvalnotes">{{$details->ValidityBDNotes}}</textarea>
                                        </div>
                                        @elseif($details->Type=='BG')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype4" id="pftype4" value="{{$details->Type}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="bgbankname">Name of Issuing Bank</label>
                                            <input type="text" class="form-control form-control-sm" name="bgbankname" id="bgbankname"
                                                value="{{$details->BankNameBG}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="bgbankguarantee">Bank Guarantee</label>
                                            <input type="text" class="form-control form-control-sm" name="bgbankguarantee" id="bgbankguarantee"
                                                value="{{$details->DraftBG}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="valperiod">Validity Period</label><br>
                                            @if(!is_null($details->ValidityBG))
                                            <input class="form-control form-control-sm" value="{{$details->ValidityBG}}" size=30
                                                disabled><input class="form-control form-control-sm" type="date" size=30 name="bgvalperiod" id="bgvalperiod">
                                            @else
                                            <input class="form-control form-control-sm" type="date" size=30 name="bgvalperiod" id="bgvalperiod">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="bgvalnotes">Validity Notes</label>
                                            <textarea class="form-control form-control-sm" name="bgvalnotes"
                                                id="bgvalnotes">{{$details->ValidityBGNotes}}</textarea>
                                        </div>
                                        @elseif($details->Type=='ILC')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype5" id="pftype5" value="{{$details->Type}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="ilcbankname">Name of Issuing Bank</label>
                                            <input type="text" class="form-control form-control-sm" name="ilcbankname" id="ilcbankname"
                                                value="{{$details->BankNameILC}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="loc">Letter of Credit(LOC) Number</label>
                                            <input type="text" class="form-control form-control-sm" name="loc" id="loc"
                                                value="{{$details->ILC}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="ilcvalperiod">Validity Period</label><br>
                                            @if(!is_null($details->ValidityILC))
                                            <input class="form-control form-control-sm" value="{{$details->ValidityILC}}" size=30
                                                disabled><input class="form-control form-control-sm" type="date" size=30  name="ilcvalperiod" id="ilcvalperiod">
                                            @else
                                            <input class="form-control form-control-sm" type="date" size=30 name="ilcvalperiod" id="ilcvalperiod">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="ilcvalnotes">Validity Notes</label>
                                            <textarea class="form-control form-control-sm" name="ilcvalnotes"
                                                id="ilcvalnotes">{{$details->ValidityILCNotes}}</textarea>
                                        </div>
                                        @elseif($details->Type=='SB')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype6" id="pftype6" value="{{$details->Type}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="sbcompanyname">Name of Issuing Company</label>
                                            <input type="text" class="form-control form-control-sm" name="sbcompanyname" id="sbcompanyname"
                                                value="{{$details->CNameSurety}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="sbnumber">Surety Bond Number</label>
                                            <input type="text" class="form-control form-control-sm" name="sbnumber" id="sbnumber"
                                                value="{{$details->SuretyNum}}">
                                        </div>
                                        <div class="form-group">
                                            <label for="sbvalperiod">Validity Period</label><br>
                                            @if(!is_null($details->ValiditySurety))
                                            <input class="form-control form-control-sm" value="{{$details->ValiditySurety}}" size=30
                                                disabled><input class="form-control form-control-sm" type="date" size=30 name="sbvalperiod" id="sbvalperiod">
                                            @else
                                            <input class="form-control form-control-sm" type="date" size=30 name="sbvalperiod" id="sbvalperiod">
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label for="sbvalnotes">Validity Notes</label>
                                            <textarea class="form-control form-control-sm" name="sbvalnotes"
                                                id="sbvalnotes">{{$details->ValiditySuretyNotes}}</textarea>
                                        </div>
                                        @elseif($details->Type=='NA')
                                        <div class="form-group">
                                            <label for="pftype">Type</label>
                                            <input type="text" class="form-control form-control-sm" disabled
                                                value="{{$details->Type_Description}}">
                                                <input type="hidden" name="pftype7" id="pftype7" value="{{$details->Type}}">
                                        </div>
                                        @endif
                                        <div class="form-group">
                                            <label for="issuingCompany">Issuing Company</label>
                                        <input class="form-control form-control-sm" type="text" name="issuingCompany" id="issuingCompany" value="{{$details->Issuing_Company}}">
                                        </div>
                                            <button type="submit"  class="btn btn-primary">UPDATE PERFORMANCE SECURITY</button>
                                    </form>
                                @empty
                                    <form method="POST" name="contractData" id="contractData"
                                        action="{{ route('addPerfSecDtls',['Contract_ID' => $encryptedID]) }}" novalidate=""
                                        enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        <div class="form-group">
                                            <label for="editperfSecVal">Select Performance Security Type/s:</label><br>
                                            <select class="form-control form-control-sm" id="editperfSecVal[]" name="editperfSecVal[]" required
                                                multiple="multiple" data-placeholder="Select one or more..." hidden
                                                style="max-width:90%;">
                                                @foreach($secType as $data)
                                                <option class="option" value="{{$data->Type}}">
                                                    <?php echo $data->Type_Description ?></option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <!-- Cash -->
                                        <div class="form-group" id="CashData">
                                            <h6>CASH</h6>
                                            <div class="form-group">
                                                <label for="cornumber">Official Receipt Number</label>
                                                <input type="text" class="form-control form-control-sm" name="cornumber" id="cornumber"
                                                    >
                                            </div>
                                            <div class="form-group">
                                                <label for="cvalidityperiod">Validity Period</label>
                                                <input type="date" class="form-control form-control-sm" name="cvalidityperiod" placeholder="YYYY-MM-DD"
                                                    id="cvalidityperiod">
                                            </div>
                                            <div class="form-group">
                                                <label for="cvalinotes">Validity Notes</label>
                                                <textarea class="form-control form-control-sm" name="cvalinotes"
                                                    id="cvalinotes"></textarea>
                                            </div>
                                        </div>
                                        <!-- Check -->
                                        <div class="form-group" id="CheckPSData">
                                            <h6>CHECK</h6>
                                            <div class="form-group">
                                                <label for="checknumber">Check Number</label>
                                                <input type="text" class="form-control form-control-sm" name="checknumber" id="checknumber"
                                                    >
                                            </div>
                                            <div class="form-group">
                                                <label for="cmcornumber">Official Receipt Number</label>
                                                <input type="text" class="form-control form-control-sm" name="cmcornumber" id="cmcornumber"
                                                    >
                                            </div>
                                            <div class="form-group">
                                                <label for="cmcbankname">Bank Name/Branch</label>
                                                <input type="text" class="form-control form-control-sm" name="cmcbankname" id="cmcbankname"
                                                    >
                                            </div>
                                            <div class="form-group">
                                                <label for="cmcvalperiod">Validity Period</label><br>
                                                <input class="form-control form-control-sm" type="date" size=30 name="cmcvalperiod" id="cmcvalperiod">
                                            </div>
                                            <div class="form-group">
                                                <label for="cmcvalinotes">Validity Notes</label>
                                                <textarea class="form-control form-control-sm" name="cmcvalinotes"
                                                    id="cmcvalinotes"></textarea>
                                            </div>
                                        </div>
                                        <!-- bank draft  -->
                                        <div id="DraftPSData">
                                            <h6>BANK DRAFT</h6>
                                            <div class="form-group">
                                                <label for="bdbankname">Name of Issuing Bank</label>
                                                <input type="text" class="form-control form-control-sm" name="bdbankname" id="bdbankname"
                                                    value="">
                                            </div>
                                            <div class="form-group">
                                                <label for="bdbankdraft">Bank Draft</label>
                                                <input type="text" class="form-control form-control-sm" name="bdbankdraft" id="bdbankdraft">
                                            </div>
                                            <div class="form-group">
                                                <label for="bdvalperiod">Validity Period</label><br>
                                                <input class="form-control form-control-sm" type="date" size=30 name="bdvalperiod" id="bdvalperiod">
                                            </div>
                                            <div class="form-group">
                                                <label for="bdvalnotes">Validity Notes</label>
                                                <textarea class="form-control form-control-sm" name="bdvalnotes" id="bdvalnotes"></textarea>
                                            </div>
                                        </div>
                                        <!-- bank guarantee -->
                                        <div id="GuaranteePSData">
                                            <h6>BANK GUARANTEE</h6>
                                            <div class="form-group">
                                                <label for="bgbankname">Name of Issuing Bank</label>
                                                <input type="text" class="form-control form-control-sm" name="bgbankname" id="bgbankname">
                                            </div>
                                            <div class="form-group">
                                                <label for="bgbankguarantee">Bank Guarantee</label>
                                                <input type="text" class="form-control form-control-sm" name="bgbankguarantee"
                                                    id="bgbankguarantee">
                                            </div>
                                            <div class="form-group">
                                                <label for="bgvalperiod">Validity Period</label><br>
                                                <input class="form-control form-control-sm" type="date" size=30 name="bgvalperiod"
                                                    id="bgvalperiod" placeholder="YYYY-MM-DD">
                                            </div>
                                            <div class="form-group">
                                                <label for="bgvalnotes">Validity Notes</label>
                                                <textarea class="form-control form-control-sm" name="bgvalnotes" id="bgvalnotes"></textarea>
                                            </div>
                                        </div>
                                        <!-- ILC -->
                                        <div id="ILCPSData">
                                            <h6>IRREVOCABLE LETTER OF CREDIT</h6>
                                            <div class="form-group">
                                                <label for="ilcbankname">Name of Issuing Bank</label>
                                                <input type="text" class="form-control form-control-sm" name="ilcbankname" id="ilcbankname">
                                            </div>
                                            <div class="form-group">
                                                <label for="loc">Letter of Credit(LOC) Number</label>
                                                <input type="text" class="form-control form-control-sm" name="loc" id="loc">
                                            </div>
                                            <div class="form-group">
                                                <label for="ilcvalperiod">Validity Period</label><br>
                                                <input class="form-control form-control-sm" type="date" size=30 name="ilcvalperiod"
                                                    id="ilcvalperiod">
                                            </div>
                                            <div class="form-group">
                                                <label for="ilcvalnotes">Validity Notes</label>
                                                <textarea class="form-control form-control-sm" name="ilcvalnotes" id="ilcvalnotes"></textarea>
                                            </div>
                                        </div>
                                        <!-- surety bond -->
                                        <div id="SuretyPSData">
                                            <h6>SURETY BOND</h6>
                                            <div class="form-group">
                                                <label for="sbcompanyname">Name of Issuing Company</label>
                                                <input type="text" class="form-control form-control-sm" name="sbcompanyname" id="sbcompanyname">
                                            </div>
                                            <div class="form-group">
                                                <label for="sbnumber">Surety Bond Number</label>
                                                <input type="text" class="form-control form-control-sm" name="sbnumber" id="sbnumber">
                                            </div>
                                            <div class="form-group">
                                                <label for="sbvalperiod">Validity Period</label><br>
                                                <input class="form-control form-control-sm" type="date" size=30 name="sbvalperiod"
                                                    id="sbvalperiod">
                                            </div>
                                            <div class="form-group">
                                                <label for="sbvalnotes">Validity Notes</label>
                                                <textarea class="form-control form-control-sm" name="sbvalnotes" id="sbvalnotes"></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="issuingCompany">Issuing Company</label>
                                        <input class="form-control form-control-sm" type="text" name="issuingCompany" id="issuingCompany">
                                        </div>
                                        <small class="form-text text-muted"><i>Note: Click <strong>ONLY</strong> the types
                                                included in the
                                                contract</i></small>
                                        <button type="submit" class="btn btn-primary">ADD PERFORMANCE SECURITY</button>
                                    </form>
                                @endforelse
                        </div>
                    </div>
                    <div id="contrDlvrbls" class="tab-pane" role="tabpanel">
                        <div class="card-header">
                            DELIVERABLES DETAILS
                        </div>
                        <div class="card-body">
                            <input type="hidden" name="delintcnt" id="delintcnt" value="{{count($deliverables)}}">
                                @if(!is_null($deliverables))
                                    <form method="POST" name="upddeliverablesdata" id="upddeliverablesdata"
                                    action="{{ route('updateContrDeliverables',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        @foreach($deliverables as $del)
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="editdeliverables">Deliverable:</label>
                                                    <input type="hidden" name="dlvrbleid[]" id="dlvrbleid[]"
                                                        value="{{$del->Deliverable_ID}}">
                                                    <input class="form-control form-control-sm" type="text" aria-label="Default"
                                                        name="editdeliverables[]" id="editdeliverables[]"
                                                        value="{{$del->Deliverable}}">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <?php $formatted = date('Y-m-d',strtotime($del->Target_Completion_Date)); ?>
                                                    <label for="editdeliverablesTD">Target Date:</label>
                                                    <input class="form-control form-control-sm" type="text" aria-label="Default"
                                                         name="editdeliverablesTD[]"
                                                        id="editdeliverablesTD[]" value="{{$formatted}}" disabled>
                                                        <input class="form-control form-control-sm" type="date" aria-label="Default"
                                                         name="editdeliverablesTD[]"
                                                        id="editdeliverablesTD[]">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="editdeliverablesTDNotes">Notes:</label>
                                                    <input class="form-control form-control-sm" type="text" aria-label="Default"
                                                            name="editdeliverablesTDNotes[]"
                                                            id="editdeliverablesTDNotes[]" value="{{$del->TCD_Notes}}">
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        <button type="submit" class="btn btn-primary">UPDATE DELIVERABLE</button>
                                    </form>
                                    <form method="POST" name="adddeliverablesdata" id="adddeliverablesdata"
                                    action="{{ route('addContrDeliverables',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        <div id="new_deliverable" style="display:inline;"></div><br>
                                            <a href="javascript:void(0);" id="deliverableButtonedit"
                                            name="deliverableButtonedit">ADD NEW ROW </a>
                                            
                                        <button type="submit" class="btn btn-primary">ADD NEW DELIVERABLE/S</button>
                                    </form>
                                @else
                                    <form method="POST" name="adddeliverablesdata" id="adddeliverablesdata"
                                    action="{{ route('addContrDeliverables',['Contract_ID' => $encryptedID]) }}"
                                    novalidate="" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <input type="hidden" name="cid" id="cid" value="{{$encryptedID}}">
                                        <div id="new_deliverable" style="display:inline;"></div><br>
                                            <a href="javascript:void(0);" id="deliverableButtonedit"
                                            name="deliverableButtonedit">ADD NEW ROW </a>
                                            
                                        <button type="submit" class="btn btn-primary">ADD NEW DELIVERABLE/S</button>
                                    </form>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>

@endsection