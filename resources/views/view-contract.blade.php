@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    @if(is_null(Auth::user()->username))
    <script>
        window.location = "/";
    </script>
    @endif
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">VIEW CONTRACT</h2><br>
            </div>
            <!-- <form action="" method="POST" enctype="multipart/form-data" style="display: inline-block;">{{ csrf_field() }} -->
            <div class="col-lg-15 col-md-12">

                <div class="breadcrumb-holder">
                    <div class="container-fluid">
                        <ul class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a>
                            </li>
                            <li class="breadcrumb-item active"> View Contract </li>
                        </ul>
                    </div>
                </div>
                @foreach($contract as $data)
                <?php $encryptedid= encrypt($Contract_ID); ?>
                <div class="card">
                    <div class="card-body">
                        @if(Auth::user()->access_type == 'A' || Auth::user()->access_type =='A1')
                        <div class="form-group">
                            <a href="{{ asset('/manage-contracts/edit-contract')}}/{{$encryptedid}}" name="editCntrct"
                                id="editCntrct" class="editContract" style="display:inline;float:right">Edit</a>
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="ProcMode">Procurement Type</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$proc}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Contract ID</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$Contract_ID}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Project Title</label>
                            <br>
                            <textarea class="form-control form-control-sm" name="contractName" id="contractName"
                                cols="52" rows="4" disabled>{{$data -> Contract_Name}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Firm Type</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$type}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Consultancy Firm/s and Role/s</label>
                            @foreach($firm as $name)
                            <div class="row">
                                <div class="col">
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$name->Firm_Name}}" disabled>
                                </div>
                                @if($type != "Single")
                                <div class="col">
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$name->firm_role_desc}}" disabled>
                                </div>
                                @endif
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Expert/s and Role/s</label>
                            @foreach($experts as $exp)
                            <div class="row">
                                <div class="col">
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$exp->expert_name}}" disabled>
                                </div>
                                <div class="col">
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$exp->expert_role}}" disabled>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">End-User (Staff):</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> End_User}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Implementing Agency:</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> Implementing_Agency}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Co-Implementing Agency:</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> Co_Implementing_Agency}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Notice to Award Date:</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> NOA_Release_Date}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">NOA Conforme Date:</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> NOA_Conforme_Date}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Notice to Proceed Date:</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> NTP_Release_Date}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">NTP Conforme Date:</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> NTP_Conforme_Date}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Contract Duration</label>
                            <div class="row">
                                <div class="col">
                                    <label for="ProcMode">Contract Start Date</label>
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$data -> Contract_Start_Date}}" disabled>
                                </div>
                                <div class="col">
                                    <label for="ProcMode">Contract End Date</label>
                                    <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                        value="{{$data -> Contract_End_Date}}" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Contract Price</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="<?php echo number_format($data->Contract_Cost,2)?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Approved Budget for the Contract (ABC)</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="<?php echo number_format($data->ABC,2)?>" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Fund Source</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> Fund_Source}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Fund Year</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$data -> Fund_Year}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Performance Security</label>
                            <br>
                            <a href="javascript:void(0);" id="viewperf" name="viewperf" data-toggle="modal"
                                onclick="java_script_:viewPS('#viewpsec')" data-target="#viewpsec">View</a>

                            <div id="viewpsec" name="viewpsec" role="dialog" class="modal modal-open">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <strong>Performance Security Details: {{$Contract_ID}}</strong>
                                            <button type="button" data-dismiss="modal" id="closeperf" aria-label="Close"
                                                onclick="java_script_:closePS('#viewpsec')" class="close"><span
                                                    aria-hidden="true">×</span></button>
                                        </div>
                                        <div class="modal-body">
                                            @forelse($perf as $details)
                                            @if($details->Type=='C')
                                            <div class="form-group">
                                                <label for="ProcMode">Performance Security Type</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->Type_Description}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Official Receipt Number</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CashOR}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CashBV}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period Notes</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CashBVNotes}}" disabled>
                                            </div>
                                            @elseif($details->Type=='CMC')
                                            <div class="form-group">
                                                <label for="ProcMode">Performance Security Type</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->Type_Description}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Check Number</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CheckNum}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Official Receipt Number</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CheckOR}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Bank Name/Branch</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CheckBank}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CheckBV}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period Notes</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CheckBVNotes}}" disabled>
                                            </div>
                                            @elseif($details->Type=='BD')
                                            <div class="form-group">
                                                <label for="ProcMode">Performance Security Type</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->Type_Description}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Name of Issuing Bank</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->BankNameBD}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Bank Draft</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->DraftBD}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValidityBD}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period Notes</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValidityBDNotes}}" disabled>
                                            </div>
                                            @elseif($details->Type=='BG')
                                            <div class="form-group">
                                                <label for="ProcMode">Performance Security Type</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->Type_Description}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Name of Issuing Bank</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->BankNameBG}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Bank Guarantee</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->DraftBG}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValidityBG}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period Notes</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValidityBGNotes}}" disabled>
                                            </div>
                                            @elseif($details->Type=='ILC')
                                            <div class="form-group">
                                                <label for="ProcMode">Performance Security Type</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->Type_Description}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Name of Issuing Company</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->BankNameILC}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Letter of Credit(LOC) Number</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ILC}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValidityILC}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period Notes</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValidityILCNotes}}" disabled>
                                            </div>
                                            @elseif($details->Type=='SB')
                                            <div class="form-group">
                                                <label for="ProcMode">Performance Security Type</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->Type_Description}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Name of Issuing Company</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->CNameSurety}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Surety Bond Number</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->SuretyNum}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValiditySurety}}" disabled>
                                            </div>
                                            <div class="form-group">
                                                <label for="ProcMode">Validity Period Notes</label>
                                                <br>
                                                <input class="form-control form-control-sm" type="text" name="procval"
                                                    id="procval" value="{{$details->ValiditySuretyNotes}}" disabled>
                                            </div>
                                            @endif
                                            @empty
                                            <?php echo "No Performance Security Available" ?>
                                            @endforelse
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Issuing Company</label>
                            <br>
                            <input class="form-control form-control-sm" type="text" name="procval" id="procval"
                                value="{{$perfIssuing}}" disabled>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Supporting Documents</label>
                            <br>
                            @foreach($document as $docs)
                            <a href="{{url('/storage/storage/supportingDocs')}}/{{basename($docs)}}"
                                target="_blank">{{basename($docs)}}</a><br>
                            @endforeach
                        </div>
                        <?php $Contract_ID = encrypt($Contract_ID) ?>
                        <div class="form-group">
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead class="thead-light">
                                        <tr>
                                            <th>Deliverable</th>
                                            <th>Completion Date</th>
                                            <th>Gross Amount Paid</th>
                                            <th>Remarks (Payment Status)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($deliverables as $del)
                                            <tr>
                                                <td>{{$del->Deliverable}}</td>
                                                <td>{{$del->Actual_Completion_Date}}</td>
                                                <td>
                                                @foreach($payments as $pay)
                                                @if($pay->Deliverable_ID == $del->Deliverable_ID)
                                                {{$pay->Gross_Payment}}
                                                @endif
                                                @endforeach
                                                </td>
                                                <td>{{$del->Payment_Status}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <a href="{{ asset('/manage-contracts/deliverables')}}/{{$Contract_ID }}"
                                        target="_blank" id="deliverableButton">VIEW DETAILED DELIVERABLES DATA</a>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Payment Status</label>
                            <br>
                            <?php
                                            if(count($deliverables) == 0){
                                                $percent = 0;
                                            }else{
                                                $percent = (count($pmtStatus)/count($deliverables))*100;
                                            }                                        
                                        ?>
                            <strong><?php echo number_format((float)$percent, 2, '.', '')?>% COMPLETED</strong>
                        </div>
                        <div class="form-group">
                            <label for="ProcMode">Project Status</label>
                            <br>
                            <a href="{{ asset('/manage-contracts/view-contract/status')}}/{{$Contract_ID}}">Details</a>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- </form> -->
            </div>
        </div>
</section>

@endsection
