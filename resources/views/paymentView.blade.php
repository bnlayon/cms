@extends ('layouts')

@section ('content')

<section class="firms-header section-padding">
    <div class="container-fluid">
        <div class="row d-flex align-items-md-stretch">
            <div class="col-lg-3 col-md-3">
                <h2 class="display h4">MANAGE CONTRACTS</h2>
            </div>
        </div>
        <div class="breadcrumb-holder">
            <div class="container-fluid">
                <ul class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ asset('/manage-contracts') }}"> Manage Contracts</a></li>
                    <li class="breadcrumb-item active"> Deliverable Payment Details for {{$contractName}}</li>
                </ul>
            </div>
        </div>
        <table id="paymentView" class="table table-bordered gridview" style="table-layout:fixed">
            <thead class="table thead">
                <tr>
                    <th>PAYMENT DATE</th>
                    <th>GROSS PAYMENT</th>
                    <th>RETENTION</th>
                    <th>VAT</th>
                    <th>REIMBURSIBLE AMOUNT</th>
                    <th>EWT</th>
                    <th>LIQUIDATED DAMAGES</th>
                    <th>NET PAYMENT</th>
                    <th>DISBURSEMENT VOUCHER</th>
                </tr>
            </thead>
            <tbody class="table tbody">
                @if(count($payment)==0)
                <tr>
                    <td colspan=9 style="text-align:center">No data available.</td>
                </tr>
                @else
                <?php $count=1; ?>
                @foreach($payment as $data)
                <tr>
                    <td>{{$data->Payment_Date}}</td>
                    <td><?php echo number_format($data->Gross_Payment,2)?></td>
                    <td><?php echo number_format($data->Retention,2)?></td>
                    <td><?php echo number_format($data->VAT,2)?></td>
                    <td><?php echo number_format($data->Reimbursible_Amt,2)?></td>
                    <td><?php echo number_format($data->EWT,2)?></td>
                    <td><?php echo number_format($data->Liquidated_Damages,2)?></td>
                    <td><?php echo number_format($data->Net_Payment,2)?></td>
                    <td>{{$data->Disbursement_Voucher}}</td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</section>

@endsection