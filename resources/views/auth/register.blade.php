<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>CMS Sign Up</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fontastic.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" href="css/grasp_mobile_progress_circle-1.0.0.min.css">
    <link rel="stylesheet" href="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
    <link rel="stylesheet" href="css/custom.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <link rel="shortcut icon" href="{{ asset('/img/neda.jpg') }}">
    <script src='https://www.google.com/recaptcha/api.js'></script>
  </head>
  <body>
    <div class="page register2-page">
      <div class="container">
       <div class="text-right"><a href="{{ asset('/') }}">Home</a>&nbsp|&nbsp<a href="">About</a>&nbsp|&nbsp<a href="">Contact Us</a></div>
       <div class="logo text-uppercase"><strong class="text-primary">Contract Management System Register</strong></div>
        <div class="form-outer text-center d-flex align-items-center">
          <div class="form-inner">
            
            <p style="text-align:left"><strong class="text-danger"><i>All fields with * are required. </i></strong></p>

            @include('error')

            <form class="text-left form-validate" action="{{ asset('/register') }}" method="POST" enctype="multipart/form-data" onsubmit="check_if_capcha_is_filled">{{ csrf_field() }}

              <div class="form-group row">
                
                <div class="col-sm-8"><label class="form-control-label">1. Basic Information*</label></div>
                
                <div class="col-sm-8">
                  <input id="login-1_loginLastname" type="text" name="1_loginLastname" required data-msg="Please enter your Last name" class="input-material">
                <label for="login-1_loginLastname" class="label-material">Last name:*</label>
                </div>
                <div class="col-sm-8">
                  <input id="login-1_loginFirstname" type="text" name="1_loginFirstname" required data-msg="Please enter your First name" class="input-material">
                <label for="1_loginFirstname" class="label-material">First name:*</label>
                </div>
                <div class="col-sm-8">
                  <input id="1_loginMiddlename" type="text" name="1_loginMiddlename" data-msg="Please enter your Middle name" class="input-material">
                <label for="1_loginMiddlename" class="label-material">Middle name:</label>
                </div>
                <div class="col-sm-8">
                  <input id="1_loginOfficeStaff" type="text" name="1_loginOfficeStaff" required data-msg="Please enter your Designation" class="input-material">
                <label for="1_loginOfficeStaff" class="label-material">Office/Staff:*</label>
                </div>
                <div class="col-sm-8">
                  <input id="1_loginDivUnit" type="text" name="1_loginDivUnit" required data-msg="Please enter your Designation" class="input-material">
                <label for="1_loginDivUnit" class="label-material">Division/Unit:*</label>
                </div>
                <div class="col-sm-5">
                  <input id="1_email" type="email" name="1_email" required class="input-material">
                <label for="1_email" class="label-material">Email Address (please double check):*</label>
                </div>
                <div class="col-sm-5">
                  <input id="1_loginPosition" type="text" name="1_loginPosition" required class="input-material">
                <label for="1_loginPosition" class="label-material">Position:* </label>
                </div>
                <div class="col-sm-5">
                <label for="1_loginGender" class="label-material">Gender:* </label>
                  <input id="1_loginGender" type="radio" name="1_loginGender" value="M"> Male &nbsp
                  <input id="1_loginGender" type="radio" name="1_loginGender" value="F"> Female
                </div>
                <div class="col-sm-5">
                  <input id="1_loginTel" type="text" name="1_loginTel" required data-msg="Please enter your Telephone Number" class="input-material">
                <label for="1_loginTel" class="label-material">Telephone Number/Local Number:* </label>
                </div>
                <div class="col-sm-5">
                <label for="1_loginContType" class="label-material">Select Contract Types to access:</label>
                  <input id="1_loginContType[]" type="checkbox" name="1_loginContType[]" value="CONS"> Consulting &nbsp
                  <input id="1_loginContType[]" type="checkbox" name="1_loginContType[]" value="GIP"> GIP
                </div>                
              </div>

              <strong class="text-danger"><label class="form-control-label">Please check reCAPTCHA below:</label></strong>
              <div class="g-recaptcha"
                   data-callback="capcha_filled"
                   data-expired-callback="capcha_expired"
                   data-sitekey="6LfJA3AUAAAAAGht99VY1X_mWZiuzf4mSCKjRAZj" style="align-content: center;"></div>

              <input id="1_loginAgree" type="checkbox" name="1_loginAgree" value="agree"> I agree to the NEDA Privacy Notice 

              <p>All the personal information contained in any document received or transmitted herein shall be used solely for documentation and processing purposes within NEDA and shall not be shared with any outside parties, unless with your written consent. Personal information shall be retained and stored by NEDA within a time period in accordance with the National Archives of the Philippines' General Disposition Schedule.</p>

              <p><small><i>NOTE: The registration is still to be evaluated by ICTS if the user is to be granted access to the CMS. An email will be sent for confirmation of the access. Thank you!</i></small></p>  

              <div class="form-group text-center">
                <input id="{{ asset('/register') }}" type="submit" value="Register" class="btn btn-primary">
              </div>
            </form><small>Already have an account? </small><a href="{{asset('/login')}}" class="signup">Login</a>
          </div>

          <div class="copyrights text-center">
            <p>National Economic and Development Authority <a href="#" class="external">Information and Communications Technology Staff</a></p>
            <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
          </div>
        </div>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/popper.js/umd/popper.min.js"> </script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
    <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
    <script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- Main File-->
    <script src="js/front.js"></script>
    <script type="text/javascript">
     
      $(document).ready(function() {
        $('select[name="agency"]').on('change', function() {
            var maValue = $(this).val();        

            $.ajax({
              url: './getmother',
              type: 'GET',
              dataType: 'json',
              data: {
                '_token': $('input[name=_token]').val(),
                'id'    : maValue
              },
              success: function(data) {

                if(data.Category == "ATT" || data.Category == "ATT_GOCC" || data.Category == "SUC") {
                  Display("show");
                  $('#mother_agency_desc').html(data.UACS_DPT_DSC);
                  $('#mother_agency').val(data.motheragency_id);
                } else {
                  Display("hidden");
                  $('#mother_agency_desc').html("");
                  $('#mother_agency').val(0);
                }                
                
              }
            });	 
            
        });
    
        Display("hidden");

      });

      function Display(visibility) {
        if(visibility == "hidden")
          {
            $('#withMotherAgency1').hide();
            $('#withMotherAgency2').hide();
            $('#login-5_loginLastname').removeAttr("required");
            $('#login-5_loginFirstname').removeAttr("required");
            $('#login-5_loginMiddlename').removeAttr("required");
            $('#login-5_loginDes').removeAttr("required");
            $('#login-5_loginTel').removeAttr("required");
            $('#login-4_email').removeAttr("required");
          }
          else {
            $('#withMotherAgency1').show();
            $('#withMotherAgency2').show();
            $('#login-5_loginLastname').prop('required', true);
            $('#login-5_loginFirstname').prop('required', true);
            $('#login-5_loginMiddlename').prop('required', true);
            $('#login-5_loginDes').prop('required', true);
            $('#login-5_loginTel').prop('required', true);
            $('#login-4_email').prop('required', true);
          }
      }

      function FileValidation(){
          var fileInput = document.getElementById('registerAttachment');
          var filePath = fileInput.value;
          var allowedExtensions = /(\.jpg|\.jpeg|\.pdf)$/i;
          if(!allowedExtensions.exec(filePath)){
              alert('File should be in PDF or JPEG Format only.');
              fileInput.value = '';
              return false;
          }
      }

    $('input').on('keypress', function (event) {
        var regex = new RegExp("^[a-zA-Z0-9@.- ]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
           event.preventDefault();
           return false;
        }
    });

    var allowSubmit = false;

    function capcha_filled () {
      allowSubmit = true;
    }

    function capcha_expired () {
      allowSubmit = false;
    }

    function check_if_capcha_is_filled (e) {
    if(allowSubmit) return true;
    e.preventDefault();
    alert('Fill in the capcha!');
}

   </script>
  </body>
</html>

@include('sweet::alert')