import Vuex from "vuex";
import Vue from "vue"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        //geninfo
        projtitle: "",
        progproj: "",
        selectedBasis: [],
        selectedRegProg: "",
        regprogtitle: "",
        project_components: "",
        geninfo: false,
        //Implementing Agency
        selectedCoAgency: [],
        selectedAttachAgency: [],
        selectedMotherAgency: [],
        implementing: false,
        //Spatial Coverage
        selectedCoverage: "",
        selectedRegions: [],
        selectedProvinces: [],
        selectedCities: [],
        selectedRegionsRegional: [],
        selectedProvincesRegional: [],
        selectedCitiesRegionspecific: [],
        spatial: false,
        //Project Inclusion
        selectedProgDocument1: [],
        selectedPIP: "",
        selectedRND: "",
        selectedRDIP: "",
        selectedEndorsement: "",
        cipRadio: "",
        dateEndorse: "",
        chkrdcyesno:"",
        projinclusion: false,
        //CIP
        selectedCIP: "",
        cipCard: false,
        //Level of Approval
        selectedLevelofApproval: "",
        levelofapprovaldate: "",
        levelofapproval: false,
        //levelof GAD
        selectedLevelofGad: "",
        levelofgad: false,
        //TRIP
        selectedStatus: "",
        ImplementationRisks: "",
        trip: false,
        //PDP Chapter
        selectedPdpChapter: "",
        selectedOtherPdpChapter: "",
        selectedRM1: [],
        selectedRM2: [],
        selectedRM3: [],
        selectedRM4: [],
        pdp: false,
        // Not PDP
        expectedoutput: "",
        NA_rm: "",
        notpdp: false,
        //Physical Accomplishments
        selectedImplementation: "",
        iccable: "",
        selectedLevelofReadiness: "",
        completionDate: "",
        dropreason: "",
        updates: "",
        asof: "",
        physicalaccomplishments: false,
        //TenPoint / SDG
        selectedTenPoint: "",
        selectedSdg: "",
        tenpointsdg: false,
        //Implementation
        startDate: "",
        endDate: "",
        startYear: "",
        endYear: "",
        //Project Preparation
        selectedProjectPrep: "",
        selectedFSAssistance: "",
        selectedFSStatus: "",
        fs_2017: 0,
        fs_2018: 0,
        fs_2019: 0,
        fs_2020: 0,
        fs_2021: 0,
        fs_2022: 0,
        fs_total: 0,
        othersProjectPrep: "",
        fsstatus_ongoing: "",
        fsstatus_prep: "",
        projectpreparation: false,
        //Infra Sector
        selectedSector: [],
        selectedSubSector: [],
        //Pre construction
        rowa1: "",
        rowa_affected: "",
        rowa2: "",
        rc_affected: "",
        rowa3: "",
        rowa1_2017: "",
        rowa1_2018: "",
        rowa1_2019: "",
        rowa1_2020: "",
        rowa1_2021: "",
        rowa1_2022: "",
        rowa1_total: "",
        rowa2_2017: "",
        rowa2_2018: "",
        rowa2_2019: "",
        rowa2_2020: "",
        rowa2_2021: "",
        rowa2_2022: "",
        rowa2_total: "",
        preconstruction: false,
        //Employment Generation
        employmentgeneration: "",
        //Project Cost
        selectedFundingSource: [],
        selectedModeofImplementation: "",
        selectedODAActFunding: "",
        selectedODAFunding: "",
        otherFundingSource: "",
        otherMode: "",
        other_fs: "",
        //Cost
        nglocal2016: 0,
        nglocal2017: 0,
        nglocal2018: 0,
        nglocal2019: 0,
        nglocal2020: 0,
        nglocal2021: 0,
        nglocal2022: 0,
        nglocaltotal: 0,
        nglocalCont: 0,
        nglocalOverall: 0,
        ngloan2016: 0,
        ngloan2017: 0,
        ngloan2018: 0,
        ngloan2019: 0,
        ngloan2020: 0,
        ngloan2021: 0,
        ngloan2022: 0,
        ngloantotal: 0,
        ngloanCont: 0,
        ngloanOverall: 0,
        nggrant2016: 0,
        nggrant2017: 0,
        nggrant2018: 0,
        nggrant2019: 0,
        nggrant2020: 0,
        nggrant2021: 0,
        nggrant2022: 0,
        nggranttotal: 0,
        nggrantCont: 0,
        nggrantOverall: 0,
        gocc2016: 0,
        gocc2017: 0,
        gocc2018: 0,
        gocc2019: 0,
        gocc2020: 0,
        gocc2021: 0,
        gocc2022: 0,
        gocctotal: 0,
        goccCont: 0,
        goccOverall: 0,
        lgu2016: 0,
        lgu2017: 0,
        lgu2018: 0,
        lgu2019: 0,
        lgu2020: 0,
        lgu2021: 0,
        lgu2022: 0,
        lgutotal: 0,
        lguCont: 0,
        lguOverall: 0,
        private2016: 0,
        private2017: 0,
        private2018: 0,
        private2019: 0,
        private2020: 0,
        private2021: 0,
        private2022: 0,
        privatetotal: 0,
        privateCont: 0,
        privateOverall: 0,
        others2016: 0,
        others2017: 0,
        others2018: 0,
        others2019: 0,
        others2020: 0,
        others2021: 0,
        others2022: 0,
        otherstotal: 0,
        othersCont: 0,
        othersOverall: 0,
        total2016: 0,
        total2017: 0,
        total2018: 0,
        total2019: 0,
        total2020: 0,
        total2021: 0,
        total2022: 0,
        total16to22: 0,
        totalCont: 0,
        totalOverall: 0,
        // Regional Breakdown
        selectedInterregionals: [],
        // Financial Accomplishments
        papCode: "",
        selectedCategory: "",
        tier1Uacs: "",
        selectedTier2Option: "",
        tier2Uacs: "",
        nep_2017: 0,
        nep_2018: 0,
        nep_2019: 0,
        nep_2020: 0,
        nep_2021: 0,
        nep_2022: 0,
        all_2017: 0,
        all_2018: 0,
        all_2019: 0,
        all_2020: 0,
        all_2021: 0,
        all_2022: 0,
        ad_2017: 0,
        ad_2018: 0,
        ad_2019: 0,
        ad_2020: 0,
        ad_2021: 0,
        ad_2022: 0,
        totalNep: 0,
        totalAllocated: 0,
        totalAmountDisbursed: 0,
        //Infra Cost
        icnglocal2016:0,
        icnglocal2017:0,
        icnglocal2018:0,
        icnglocal2019:0,
        icnglocal2020:0,
        icnglocal2021:0,
        icnglocal2022: 0,
        icnglocaltotal: 0,
        icnglocalCont: 0,
        icnglocalOverall: 0,
        //status of submission
        statusofsubmission: '',
        //project id
        proj_id: ''
    },
    getters: {

    },
    actions: {
        //Update State Data
        UpdateProj(context, data) {
            context.commit('UpdateGeninfo', data)
            // context.commit('savetodb')
        },
        UpdateImplementingData(context, data) {
            context.commit('UpdateImplementing', data)
            // context.commit('savetodb')
        },
        UpdateSpatialData(context, data) {
            context.commit('UpdateSpatial', data)
            // context.commit('savetodb')
        },
        UpdateProjInclusionData(context, data) {
            context.commit('UpdateProjInclusion', data)
        },
        UpdateCipData(context, data) {
            context.commit('UpdateCip', data)
        },
        UpdateCipCardData(context, data) {
            context.commit('UpdateCipCard', data)
        },
        UpdateLevelOfApprovalData(context, data) {
            context.commit('UpdateLevelOfApproval', data)
        },
        UpdateLevelOfGadData(context, data) {
            context.commit('UpdateLevelOfGad', data)
        },
        UpdateTripData(context, data) {
            context.commit('UpdateTrip', data)
        },
        UpdateInfraCostData(context, data) {
            context.commit('UpdateInfraCost', data)
        },
        UpdatePhysicalAccomplishData(context, data) {
            context.commit('UpdatePhysicalAccomplish', data)
        },
        UpdatePdpData(context, data) {
            context.commit('UpdatePdp', data)
        },
        UpdateNotPdpData(context, data) {
            context.commit('UpdateNotPdp', data)
        },
        UpdateTenPointSdgData(context, data) {
            context.commit('UpdateTenPointSdg', data)
        },
        UpdateStartDate(context, data) {
            context.commit('UpdateStartDate', data)
        },
        UpdateEndDate(context, data) {
            context.commit('UpdateEndDate', data)
        },
        UpdateProjectPrepData(context, data) {
            context.commit("UpdateProjectPrep", data)
        },
        UpdateInfraSectors(context, data) {
            context.commit('UpdateInfraSectors', data)
        },
        UpdatePreConstructionData(context, data) {
            context.commit('UpdatePreConstruction', data)
        },
        UpdateFundingSourceData(context, data) {
            context.commit('UpdateFundingSource', data)
            // context.commit('savetodb')
        },
        UpdateProjectCostData(context, data) {
            context.commit('UpdateProjectCost', data)
            // context.commit('savetodb')
        },
        UpdateRegionalBreakdownData(context, data) {
            context.commit('UpdateRegionalBreakdown', data)
        },
        UpdateFinancialAccomplishData(context, data) {
            context.commit('UpdateFinancialAccomplish', data)
        },
        UpdateStatusofSubmissionData(context, data) {
            context.commit('UpdateStatusofSubmission', data)
        },
        updateselectedCoverageData(context, data) {
            context.commit('updateselectedCoverage', data)
        },
        savedraftAdd(context, data) {
            context.commit('savedraftAdd', data)
        },
        EditSave(context, data) {
            context.commit('editsave', data)
        },
        UpdateRDCData(context, data) {
            context.commit('UpdateRDC', data)
        }
    },
    mutations: {
        UpdateGeninfo(state, data) {
            state.projtitle = data.projtitle
            state.progproj = data.progproj
            state.selectedBasis = data.selectedBasis
            state.selectedRegProg = data.selectedRegProg
            state.regprogtitle = data.regprogtitle
            state.project_components = data.project_components
            state.geninfo = data.geninfo
            // console.log(`projtitle : ${state.projtitle}`)
        },
        UpdateImplementing(state, data) {
            state.selectedAttachAgency = data.selectedAttachAgency
            state.selectedCoAgency = data.selectedCoAgency
            state.selectedMotherAgency = data.selectedMotherAgency
            state.implementing = data.implementing
        },
        UpdateSpatial(state, data) {
            state.selectedCoverage = data.selectedCoverage
            state.selectedRegions = data.selectedRegions
            state.selectedProvinces = data.selectedProvinces
            state.selectedCities = data.selectedCities
            state.selectedRegionsRegional = data.selectedRegionsRegional
            state.selectedProvincesRegional = data.selectedProvincesRegional
            state.selectedCitiesRegionspecific = data.selectedCitiesRegionspecific
            state.spatial = data.spatial
        },
        UpdateProjInclusion(state, data) {
            state.selectedProgDocument1 = data.selectedProgDocument1
            state.selectedPIP = data.selectedPIP
            state.selectedRND = data.selectedRND
            state.selectedRDIP = data.selectedRDIP
            state.selectedEndorsement = data.selectedEndorsement
            state.cipRadio = data.cipRadio
            state.dateEndorse = data.dateEndorse
            state.chkrdcyesno = data.chkrdcyesno
            state.projinclusion = data.projinclusion
        },
        UpdateCip(state, data) {
            state.selectedCIP = data.selectedCIP
            state.cip = data.cip
        },
        UpdateCipCard(state, data) {
            state.cipCard = data.cipCard
        },
        UpdateLevelOfGad(state, data) {
            state.selectedLevelofGad = data.selectedLevelofGad
        },
        UpdateLevelOfApproval(state, data) {
            state.selectedLevelofApproval = data.levelofApproval
            state.levelofapprovaldate = data.levelofapprovaldate
        },
        UpdateTrip(state, data) {
            state.selectedStatus = data.selectedStatus
            state.ImplementationRisks = data.ImplementationRisks
        },
        UpdateInfraCost(state, data) {
            state.icnglocal2016 = data.icnglocal2016
            state.icnglocal2017 = data.icnglocal2017
            state.icnglocal2018 = data.icnglocal2018
            state.icnglocal2019 = data.icnglocal2019
            state.icnglocal2020 = data.icnglocal2020
            state.icnglocal2021 = data.icnglocal2021
            state.icnglocal2022 = data.icnglocal2022
            state.icnglocaltotal = data.icnglocaltotal
            state.icnglocalCont = data.icnglocalCont
            state.icnglocalOverall = data.icnglocalOverall
        },
        UpdatePhysicalAccomplish(state, data) {
            state.selectedImplementation = data.selectedImplementation
            state.iccable = data.iccable
            state.selectedLevelofReadiness = data.selectedLevelofReadiness
            state.completionDate = data.completionDate
            state.dropreason = data.dropreason
            state.updates = data.updates
            state.asof = data.asof
            state.physicalaccomplishments = data.physicalaccomplishments
        },
        UpdatePdp(state, data) {
            state.selectedPdpChapter = data.selectedPdpChapter
            state.selectedOtherPdpChapter = data.selectedOtherPdpChapter
            state.selectedRM1 = data.selectedRM1
            state.selectedRM2 = data.selectedRM2
            state.selectedRM3 = data.selectedRM3
            state.selectedRM4 = data.selectedRM4
            state.pdp = data.pdp
        },
        UpdateNotPdp(state, data) {
            state.expectedoutput = data.expectedoutput
            state.NA_rm = data.NA_rm
            state.notpdp = data.notpdp
        },
        UpdateTenPointSdg(state, data) {
            state.selectedTenPoint = data.selectedTenPoint
            state.selectedSdg = data.selectedSdg
            state.tenpointsdg = data.tenpointsdg
        },
        UpdateStartDate(state, data) {
            state.startDate = data.date
            state.startYear = data.date.substr(0, 4)
            // console.log(`startDate : ${state.startDate}`)
        },
        UpdateEndDate(state, data) {
            state.endDate = data.date
            state.endYear = data.date.substr(0, 4)
            // console.log(`endDate : ${state.endDate}`)
        },
        UpdateProjectPrep(state, data) {
            state.selectedProjectPrep = data.selectedProjectPrep
            state.selectedFSAssistance = data.selectedFSAssistance
            state.selectedFSStatus = data.selectedFSStatus
            state.fs_2017 = data.fs_2017
            state.fs_2018 = data.fs_2018
            state.fs_2019 = data.fs_2019
            state.fs_2020 = data.fs_2020
            state.fs_2021 = data.fs_2021
            state.fs_2022 = data.fs_2022
            state.fs_total = data.fs_total
            state.othersProjectPrep = data.othersProjectPrep
            state.projectpreparation = data.projectpreparation
            state.fsstatus_ongoing = data.fsstatus_ongoing
            state.fsstatus_prep = data.fsstatus_prep
        },
        UpdateInfraSectors(state, data) {
            state.selectedSector = data.selectedSector
            state.selectedSubSector = data.selectedSubSector
        },
        UpdatePreConstruction(state, data) {
            state.rowa1 = data.rowa1,
            state.rowa_affected = data.rowa_affected,
            state.rowa2 = data.rowa2,
            state.rc_affected = data.rc_affected,
            state.rowa3 = data.rowa3,
            state.rowa1_2017 = data.rowa1_2017
            state.rowa1_2018 = data.rowa1_2018
            state.rowa1_2019 = data.rowa1_2019
            state.rowa1_2020 = data.rowa1_2020
            state.rowa1_2021 = data.rowa1_2021
            state.rowa1_2022 = data.rowa1_2022
            state.rowa1_total = data.rowa1_total
            state.rowa2_2017 = data.rowa2_2017
            state.rowa2_2018 = data.rowa2_2018
            state.rowa2_2019 = data.rowa2_2019
            state.rowa2_2020 = data.rowa2_2020
            state.rowa2_2021 = data.rowa2_2021
            state.rowa2_2022 = data.rowa2_2022
            state.rowa2_total = data.rowa2_total
            state.employmentgeneration = data.employmentgeneration
            state.preconstruction = data.preconstruction
        },
        UpdateFundingSource(state, data) {
            state.selectedFundingSource = data.selectedFundingSource
            state.selectedModeofImplementation = data.selectedModeofImplementation
            state.selectedODAActFunding = data.selectedODAActFunding
            state.selectedODAFunding = data.selectedODAFunding
            state.otherFundingSource = data.otherFundingSource
            state.otherMode = data.otherMode
            state.other_fs =  data.other_fs
        },
        UpdateProjectCost(state, data) {
            state.nglocal2016 = data.nglocal2016
            state.nglocal2017 = data.nglocal2017
            state.nglocal2018 = data.nglocal2018
            state.nglocal2019 = data.nglocal2019
            state.nglocal2020 = data.nglocal2020
            state.nglocal2021 = data.nglocal2021
            state.nglocal2022 = data.nglocal2022
            state.nglocaltotal = data.nglocaltotal
            state.nglocalCont = data.nglocalCont
            state.nglocalOverall = data.nglocalOverall
            state.ngloan2016 = data.ngloan2016
            state.ngloan2017 = data.ngloan2017
            state.ngloan2018 = data.ngloan2018
            state.ngloan2019 = data.ngloan2019
            state.ngloan2020 = data.ngloan2020
            state.ngloan2021 = data.ngloan2021
            state.ngloan2022 = data.ngloan2022
            state.ngloantotal = data.ngloantotal
            state.ngloanCont = data.ngloanCont
            state.ngloanOverall = data.ngloanOverall
            state.nggrant2016 = data.nggrant2016
            state.nggrant2017 = data.nggrant2017
            state.nggrant2018 = data.nggrant2018
            state.nggrant2019 = data.nggrant2019
            state.nggrant2020 = data.nggrant2020
            state.nggrant2021 = data.nggrant2021
            state.nggrant2022 = data.nggrant2022
            state.nggranttotal = data.nggranttotal
            state.nggrantCont = data.nggrantCont
            state.nggrantOverall = data.nggrantOverall
            state.gocc2016 = data.gocc2016
            state.gocc2017 = data.gocc2017
            state.gocc2018 = data.gocc2018
            state.gocc2019 = data.gocc2019
            state.gocc2020 = data.gocc2020
            state.gocc2021 = data.gocc2021
            state.gocc2022 = data.gocc2022
            state.gocctotal = data.gocctotal
            state.goccCont = data.goccCont
            state.goccOverall = data.goccOverall
            state.lgu2016 = data.lgu2016
            state.lgu2017 = data.lgu2017
            state.lgu2018 = data.lgu2018
            state.lgu2019 = data.lgu2019
            state.lgu2020 = data.lgu2020
            state.lgu2021 = data.lgu2021
            state.lgu2022 = data.lgu2022
            state.lgutotal = data.lgutotal
            state.lguCont = data.lguCont
            state.lguOverall = data.lguOverall
            state.private2016 = data.private2016
            state.private2017 = data.private2017
            state.private2018 = data.private2018
            state.private2019 = data.private2019
            state.private2020 = data.private2020
            state.private2021 = data.private2021
            state.private2022 = data.private2022
            state.privatetotal = data.privatetotal
            state.privateCont = data.privateCont
            state.privateOverall = data.privateOverall
            state.others2016 = data.others2016
            state.others2017 = data.others2017
            state.others2018 = data.others2018
            state.others2019 = data.others2019
            state.others2020 = data.others2020
            state.others2021 = data.others2021
            state.others2022 = data.others2022
            state.otherstotal = data.otherstotal
            state.othersCont = data.othersCont
            state.othersOverall = data.othersOverall
            state.total2016 = data.total2016,
                state.total2017 = data.total2017,
                state.total2018 = data.total2018,
                state.total2019 = data.total2019,
                state.total2020 = data.total2020,
                state.total2021 = data.total2021,
                state.total2022 = data.total2022,
                state.total16to22 = data.total16to22,
                state.totalCont = data.totalCont,
                state.totalOverall = data.totalOverall
        },
        UpdateRegionalBreakdown(state, data) {
            state.selectedInterregionals = data.selectedInterregionals
        },
        UpdateFinancialAccomplish(state, data) {
            state.papCode = data.papCode
            state.selectedCategory = data.selectedCategory
            state.tier1Uacs = data.tier1Uacs
            state.selectedTier2Option = data.selectedTier2Option
            state.tier2Uacs = data.tier2Uacs
            state.nep_2017 = data.nep_2017
            state.nep_2018 = data.nep_2018
            state.nep_2019 = data.nep_2019
            state.nep_2020 = data.nep_2020
            state.nep_2021 = data.nep_2021
            state.nep_2022 = data.nep_2022
            state.all_2017 = data.all_2017
            state.all_2018 = data.all_2018
            state.all_2019 = data.all_2019
            state.all_2020 = data.all_2020
            state.all_2021 = data.all_2021
            state.all_2022 = data.all_2022
            state.ad_2017 = data.ad_2017
            state.ad_2018 = data.ad_2018
            state.ad_2019 = data.ad_2019
            state.ad_2020 = data.ad_2020
            state.ad_2021 = data.ad_2021
            state.ad_2022 = data.ad_2022
            state.totalNep = data.totalNep
            state.totalAllocated = data.totalAllocated
            state.totalAmountDisbursed = data.totalAmountDisbursed
        },
        savetodb(state, data) {
            state.statusofsubmission = 'Endorsed'
            if (state.geninfo &&
                state.implementing &&
                state.spatial &&
                state.projinclusion &&
                state.pdp &&
                state.physicalaccomplishments &&
                state.notpdp &&
                state.tenpointsdg &&
                state.projectpreparation &&
                state.preconstruction
            ) {
                setTimeout(() => {
                    axios.post('savetodb', state)
                        .then(res => {
                            console.log(res.data)
                            state.geninfo = false
                            state.implementing = false
                            state.spatial = false
                            state.projinclusion = false
                            state.physicalaccomplishments = false
                            state.tenpointsdg = false
                            state.projectpreparation = false
                            state.preconstruction = false
                            console.log(`geninfo: ${state.geninfo}`)
                            console.log(`implementing: ${state.implementing}`)
                            console.log(`spatial: ${state.spatial}`)
                        })
                        .catch(err => {
                            console.error(err);
                        })
                }, 500);
            }
        },
        UpdateStatusofSubmission(state, data) {
            state.statusofsubmission = data
        },
        updateselectedCoverage(state, data) {
            state.selectedCoverage = data
        },
        editsave(state, data) {
            state.proj_id = data
            setTimeout(() => {
                axios.post('editsave', state)
                    .then(res => {
                        // console.log(res.data)
                    })
                    .catch(err => {
                        console.error(err);
                    })
            }, 500);
        },
        savedraftAdd(state, data) {
            setTimeout(() => {
                axios.post('savetodb', state)
                    .then(res => {
                        console.log(res.data)
                        state.geninfo = false
                        state.implementing = false
                        state.spatial = false
                        state.projinclusion = false
                        state.physicalaccomplishments = false
                        state.tenpointsdg = false
                        state.projectpreparation = false
                        state.preconstruction = false
                    })
                    .catch(err => {
                        console.error(err);
                    })
            }, 500);
        },
        UpdateRDC(state, data) {
            if (data.chkrdcyesno == false) {
                state.selectedEndorsement = 0
                state.dateEndorse = ""
            } else {
                state.selectedEndorsement = data.selectedEndorsement
                if (data.selectedEndorsement == 1) {
                    state.dateEndorse = data.dateEndorse
                } else {
                    state.dateEndorse = ""
                }
            }
        }
    }
})