import Vuex from "vuex";
import Vue from "vue";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        dataselectedLevel: "",
        datalevelofApproval: '',
        dataYettobesubmitted: '',
        dataUnderthenedasec: '',
        dataIcctbEndorsed: '',
        dataIccccApproved: '',
        datanedaBoardConfirmed: ''
    },
    getters: {

    },
    actions: {
        UpdateLevelOfApprovalDate(context, data) {
            context.commit('ResetDates')
            context.commit('UpdateLevelOfApprovalDate', data)
        },
        UploadCurrentDate(context, data) {
            context.commit('ResetDates')
            context.commit('UpdateLevelofApprovalDates', data)
        },
    },
    mutations: {
        UpdateLevelOfApprovalDate(state, data) {
            if (data.type == 'ICCNBradio1') {
                state.dataYettobesubmitted = data.date
                state.datalevelofApproval = "Yet to be submitted to the NEDA Secretariat"
                state.dataselectedLevel = 1
            } else if (data.type == 'ICCNBradio2') {
                state.datalevelofApproval = "Under the NEDA Secretariat Review"
                state.dataUnderthenedasec = data.date
                state.dataselectedLevel = 1
            } else if (data.type == 'ICCNBradio3') {
                state.datalevelofApproval = "ICC-TB Endorsed"
                state.dataIcctbEndorsed = data.date
                state.dataselectedLevel = 1
            } else if (data.type == 'ICCNBradio4') {
                state.datalevelofApproval = "ICC-CC Approved"
                state.dataIccccApproved = data.date
                state.dataselectedLevel = 1
            } else if (data.type == 'ICCNBradio5') {
                state.datalevelofApproval = "NEDA Board Confirmed"
                state.datanedaBoardConfirmed = data.date
                state.dataselectedLevel = 1
            }

        },
        UpdateLevelofApprovalDates(state, data) {
            // console.log(data)
            if (data.level == '1') {
                state.datalevelofApproval = "Yet to be submitted to the NEDA Secretariat"
                state.dataYettobesubmitted = data.date
                state.dataselectedLevel = 1
            } else if (data.level == '2') {
                state.datalevelofApproval = "Under the NEDA Secretariat Review"
                state.dataUnderthenedasec = data.date
                state.dataselectedLevel = 1
            } else if (data.level == '3') {
                state.datalevelofApproval = "ICC-TB Endorsed"
                state.dataIcctbEndorsed = data.date
                state.dataselectedLevel = 1
            } else if (data.level == '4') {
                state.datalevelofApproval = "ICC-CC Approved"
                state.dataIccccApproved = data.date
                state.dataselectedLevel = 1
            } else if (data.level == '5') {
                state.datalevelofApproval = "NEDA Board Confirmed"
                state.datanedaBoardConfirmed = data.date
                state.dataselectedLevel = 1
            }
        },
        ResetDates(state) {
            state.dataselectedLevel= "",
            state.datalevelofApproval= '',
            state.dataYettobesubmitted= '',
            state.dataUnderthenedasec= '',
            state.dataIcctbEndorsed= '',
            state.dataIccccApproved= '',
            state.datanedaBoardConfirmed= ''
        }
    }
})